<?php

error_reporting(E_ALL & ~E_NOTICE);

require_once __DIR__.'/lib/db.php';
require_once __DIR__.'/lib/ar.php';
require_once __DIR__.'/lib/sys.php';
require_once __DIR__.'/hook.php';

// автозагрузка моделей
spl_autoload_register(function ($class) {
    $file = __DIR__.'/lib/model/' . strtolower($class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});