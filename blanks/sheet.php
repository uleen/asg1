<!doctype html>
<html>
<head>
    <title>Бланк "Счет на оплату"</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        body {
            width: 210mm;
            margin-left: auto;
            margin-right: auto;
            font: 10pt Arial;
        }

        table.invoice_bank_rekv {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice_bank_rekv td {
            border: 1px solid;
            padding: 5px;
            vertical-align: top;
        }

        table.invoice_items {
            width: 100%;
            border: 2px solid;
            border-collapse: collapse;
        }

        table.invoice_items td, table.invoice_items th {
            border: 1px solid;
            padding: 2px 5px;
            font-size: 9pt;
        }

        table.invoice_total {
            width: 100%;
        }

        table.invoice_total td {
            padding: 1px 5px;
            font-weight: bold;
            text-align: right;
        }
    </style>
</head>
<body>
<div style="margin: 15px 0">
    <img src="./files/<?= $sheet->company->file_name ?>">
</div>
<div style="background-color:#000000; font-size:1px; height:2px;">&nbsp;</div>
<div style="margin: 15px 0"><?= $sheet->company->advertising_text ?></div>
<table class="invoice_bank_rekv">
    <tr>
        <td colspan="2" rowspan="2" style="width: 105mm">
            <div><?= $sheet->company_requisite->bank_title ?></div>
            <div>&nbsp;</div>
            <div>Банк получателя</div>
        </td>
        <td style="width: 25mm">БИK
        </td>
        <td rowspan="2" style="width: 60mm">
            <div><?= $sheet->company_requisite->bank_bik ?></div>
            <div>&nbsp;</div>
            <div><?= $sheet->company_requisite->bank_ks ?></div>
        </td>
    </tr>
    <tr>
        <td style="width: 25mm">Сч. №</td>
    </tr>
    <tr>
        <td style="width: 50mm">ИНН <?= $sheet->company->inn ?></td>
        <td style="width: 55mm">КПП <?= $sheet->company->kpp ?></td>
        <td rowspan="2" style="width: 25mm">Сч. №</td>
        <td rowspan="2" style="width: 60mm"><?= $sheet->company_requisite->number ?></td>
    </tr>
    <tr>
        <td colspan="2" style="">
            <div>Название организации</div>
            <div>&nbsp;</div>
            <div><?= $sheet->company->title ?></div>
        </td>
    </tr>
</table>
<div style="font-weight: bold; font-size: 16pt; margin: 15px 0 15px 5px">Счет № <?= $sheet->id ?> от <?= sys::date($sheet->create_date) ?></div>
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<table width="100%" style="margin: 5px 0">
    <tr>
        <td style="width: 30mm; padding-left:2px;">Поставщик:</td>
        <td style="font-weight:bold;  padding:2px;"><?= $sheet->company->title ?>, <?= $sheet->company->ur_address_text ?>, тел. <?= $sheet->company->phone ?></td>
    </tr>
    <tr>
        <td style="width: 30mm; padding:2px;">Покупатель:</td>
        <td style="font-weight:bold;  padding:2px;"><?= $sheet->contractor->title ?>, ИНН <?= $sheet->contractor->inn ?>, КПП <?= $sheet->contractor->kpp ?>, <?= $sheet->contractor->ur_address_text ?>, тел. <?= $sheet->contractor->phone ?></td>
    </tr>
</table>
<table class="invoice_items">
    <thead>
    <tr>
        <th style="width:10mm;">№</th>
        <th>Товары (работы, услуги)</th>
        <th style="width:20mm;">Кол-во</th>
        <th style="width:15mm;">Ед.</th>
        <th style="width:20mm;">Цена</th>
        <th style="width:20mm;">Сумма</th>
    </tr>
    </thead>
    <tbody>
    <? $n = 0; ?>
    <? foreach ($sheet->client_order->getPosition() as $position) { ?>
        <tr>
            <td align="center"><?= ++$n ?></td>
            <td align="left"><?= $position['nomenclature_title'] ?> [арт. <?= $position['nomenclature_article'] ?>] <?= $position['nomenclature_characteristics'] ?></td>
            <td align="right"><?= $position['volume'] ?></td>
            <td align="left"><?= $position['unit_title'] ?></td>
            <td align="right"><?= $position['price'] ?></td>
            <td align="right"><?= $position['cost'] ?></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<table class="invoice_total">
    <tr>
        <td></td>
        <td style="width:45mm">Итого:</td>
        <td style="width:20mm"><?= $sheet->sheet_sum ?></td>
    </tr>
    <tr>
        <td></td>
        <td style="width:45mm">В том числе НДС:</td>
        <td style="width:20mm"><?= $sheet->sheet_sum_nds ?></td>
    </tr>
</table>
<div style="margin: 5px 0 10px">
    Всего наименований <?= $n ?> на сумму <?= $sheet->sheet_sum ?> рублей.<br/>
    <?= $sheet->sheet_sum_in_words ?>
</div>
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<div style="margin: 15px 0">Руководитель ______________________ (<?= $sheet->company->chief->fio_short_ip ?>)</div>
<div style="margin: 15px 0">Главный бухгалтер ______________________ (<?= $sheet->company->booker->fio_short_ip ?>)</div>
<div style="margin: 15px 0; width: 85mm; text-align: center;">М.П.</div>
<div style="margin: 15px 0; font-size:9pt">Счет действителен к оплате в течении трех дней.</div>
</body>
</html>
