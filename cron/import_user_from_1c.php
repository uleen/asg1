<?php

require_once '../init.php';

$fileName = 'ASG.csv'; // путь к файлу из 1С


// перебираем все строки файла
foreach (file($fileName) as $strUser) {
    $dataUser = explode(';', trim($strUser));
    print_r($dataUser);

    // поиск среди существующих пользователей
    $conditions = array(
        'name = :name',
        'date_birth = :date_birth',
    );
    $params = array(
        'name' => $dataUser[0],
        'date_birth' => convert_date_from_1c_to_iso($dataUser[1]),
    );
    $users = User::find($conditions, $params);
    $user = $users ? $users[0] : new User;

    // компания
    $company = Company::find_by_pk(1); // всегда берем ООО "АвтоСтройГрад"

    // поиск должности
    $positions = Position::find(array('title = :title'), array('title' => $dataUser[5]));
    if ($positions) {
        $position = $positions[0];
    } else {
        $position = new Position;
        $position->title = $dataUser[5];
        $position->status = 1;
        $position->save();
    }

    // данные сотрудника
    $user->name = $dataUser[0]; // ФИО сотрудника
    $user->company_id = $company->id; // ID компании
    $user->company_unit_id = 1; // ID подразделения компании (всегда берем "Основное подразделение")',
    $user->position_id = $position->id; // ID должности
    $user->date_birth = convert_date_from_1c_to_iso($dataUser[1]); // Дата рождения
    $user->date_hire = convert_date_from_1c_to_iso($dataUser[3]); // Дата приема на работу
    $user->date_dismissal = convert_date_from_1c_to_iso($dataUser[4]); // Дата увольнения
    $user->phone_mobile = $dataUser[8] ? $dataUser[8] : null; // Телефон мобильный
    $user->import_1c = 1; // Импортирован из 1С
    $user->status = $user->date_dismissal ? 2 : 1; // Статус сотрудника (уволен / не уволен)
    $user->save();
}


/**
 * Конвертация даты из формата 1С в ISO
 */
function convert_date_from_1c_to_iso($date_1c)
{
    return $date_1c ? preg_replace('/(\d{2})\.(\d{2})\.(\d{4}) 0:00:00/', '\\3-\\2-\\1', $date_1c) : null;
}