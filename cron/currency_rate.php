<?php
require_once __DIR__.'/../lib/db.php';

/**
 * Загрузка курсов валют ЦБ РФ на текущую дату
 */

$date = date('Y-m-d');

$client = new SoapClient('http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL');
$any = $client->GetCursOnDate(array('On_date' => $date))->GetCursOnDateResult->any;
$valuteData = simplexml_load_string($any)->ValuteData;

$sql = "insert ignore into d_currency_rate (num, cdate, code, rate, nom)
              value (:num, :cdate, :code, :rate, :nom)";
$sth = db::get()->prepare($sql);

foreach ($valuteData->ValuteCursOnDate as $valuteCursOnDate) {
    $params = array(
        'num' => (int)$valuteCursOnDate->Vcode,
        'cdate' => $date,
        'code' => (string)$valuteCursOnDate->VchCode,
        'rate' => (double)$valuteCursOnDate->Vcurs,
        'nom' => (int)$valuteCursOnDate->Vnom,
    );
    $sth->execute($params);
}

// рублевый курс
$params = array(
    'num' => 643,
    'cdate' => $date,
    'code' => 'RUB',
    'rate' => 1,
    'nom' => 1,
);
$sth->execute($params);