<?php

require_once 'init.php';

require_once 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->config(array(
    'mode' => 'development', // режим работы приложения
    'templates.path' => './templates', // папка шаблонов
));

User::auth();
db::setLoggedUser(User::getLoggedID());

// авторизация пользователя
$app->post('/auth', function () use ($app) {
    if (User::auth($app->request->post('login'), $app->request->post('password'))) {
        // сессионные куки, не на сутки
        $app->setCookie('login', $app->request->post('login'), '1 day');
        $app->setCookie('password', $app->request->post('password'), '1 day');
        echo json_encode(array(
            'success' => true
        ));
    } else {
        echo json_encode(array(
            'success' => false,
            'msg' => 'Логин и пароль указаны не верно',
        ));
    }
});

// выход из системы
$app->get('/logout', function () use ($app) {
    $app->deleteCookie('login');
    $app->deleteCookie('password');
    $app->redirect('/');
});

// отображение главного шаблона
$app->get('/', function () use ($app) {
    if (User::auth()) {
        $app->render('main.php');
    } else {
        $app->render('auth.php');
    }
});


// данные пользователя
$app->get('/get_user_data', function () use ($app) {
    $userData = User::get();
    $userData->rights = User::getRights();
    echo $userData ? json_encode($userData) : '';
});


// Сотрудники / Пользователи
$app->group('/user', function () use ($app) {
    include_once('lib/inc/user.php');
});


// работа со справочниками
$app->group('/spec', function () use ($app) {

    // реклама
    $app->group('/d_reclama', function () use ($app) {
        include_once('lib/inc/d_reclama.php');
    });

    // направление продаж
    $app->group('/d_direction_sales', function () use ($app) {
        include_once('lib/inc/d_direction_sales.php');
    });

    // номенклатура
    $app->group('/d_nomenclature', function () use ($app) {
        include_once('lib/inc/d_nomenclature.php');
    });

    // номенклатурные группы
    $app->group('/d_nomenclature_group', function () use ($app) {
        include_once('lib/inc/d_nomenclature_group.php');
    });

    // категории клиентов
    $app->group('/d_customer_category', function () use ($app) {
        include_once('lib/inc/d_customer_category.php');
    });

    // сфера деятельности
    $app->group('/d_reference_scope', function () use ($app) {
        include_once('lib/inc/d_reference_scope.php');
    });

    // контактные данные "Контактного лица"
    // @todo: перенсти в новую таблицу
    $app->group('/d_personal_contacs_data', function () use ($app) {
        include_once('lib/inc/d_personal_contacts_data.php');
    });

    // контактные лица
    $app->group('/d_contact_personal', function () use ($app) {
        include_once('lib/inc/d_contact_personal.php');
    });

    // типы выставляемых счетов
    $app->group('/d_sheet_type', function () use ($app) {
        include_once('lib/inc/d_sheet_type.php');
    });

    // статусы выставляемых счетов
    $app->group('/d_sheet_status', function () use ($app) {
        include_once('lib/inc/d_sheet_status.php');
    });

    // адреса
    $app->group('/d_address', function () use ($app) {
        include_once('lib/inc/d_address.php');
    });

    // контактные лица
    $app->group('/d_client_order_status', function () use ($app) {
        include_once('lib/inc/d_client_order_status.php');
    });

});


//подразделения компании
$app->group('/spec/d_company_unit', function () use ($app) {
    include_once('lib/inc/d_company_unit.php');
});

//транспортные средства
$app->group('/vehicle', function () use ($app) {
	include_once('lib/inc/vehicle.php');
});

// входящий запрос
$app->group('/incoming_call_register', function () use ($app) {
    include_once('lib/inc/ic.php');
});

// справочник "Форма собственности"
$app->group('/spec/d_okpfo_type', function () use ($app) {
    include_once('lib/inc/d_okpfo_type.php');
});

// справочник "Компании"
$app->group('/spec/d_company', function () use ($app) {
    include_once('lib/inc/d_company.php');
});


// контрагент
$app->group('/d_contractor', function () use ($app) {
    include_once('lib/inc/contractor.php');
});

// счета контрагента
$app->group('/d_contractor_requisites', function () use ($app) {
    include_once('lib/inc/contractor_requisites.php');
});

// договоры контрагента
$app->group('/d_contractor_contracts', function () use ($app) {
    include_once('lib/inc/contractor_contracts.php');
});

// точки доставки контрагента
$app->group('/d_contractor_delivery', function () use ($app) {
    include_once('lib/inc/contractor_delivery.php');
});

// справочник единицы измерения
$app->group('/spec/d_unit', function () use ($app) {
    include_once('lib/inc/d_unit.php');
});

// справочник виды расчетов
$app->group('/spec/d_type_payment', function () use ($app) {
    include_once('lib/inc/d_type_payment.php');
});

// справочник виды цен
$app->group('/spec/d_type_price', function () use ($app) {
    include_once('lib/inc/d_type_price.php');
});

// справочник характеристики по номенклатурным группам
$app->group('/spec/d_characteristic', function () use ($app) {
    include_once('lib/inc/d_characteristic.php');
});

// заказы клиента
$app->group('/client_order', function () use ($app) {
    include_once('lib/inc/client_order.php');
});

// выставленные счета
$app->group('/sheet', function () use ($app) {
    include_once('lib/inc/sheet.php');
});

// поиск вх. запросов
$app->group('/ic_search', function () use ($app) {
    include_once('lib/inc/ic_search.php');
});


// загрузка файла
$app->post('/upload', function () use ($app) {
    include_once('lib/inc/upload.php');
});


// работа со справочниками
$app->group('/dictionary', function () use ($app) {
    $path = explode('/', $app->request->getPath());
    $dName = $path[2];
    $dFile = 'lib/inc/dictionary/' . $dName . '.php';
    if (file_exists($dFile)) {
        $app->group('/' . $dName, function () use ($app, $dFile) {
                include_once($dFile);
            });
    }
});
$app->run();
