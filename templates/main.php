<html>
<head>
    <title>CRM</title>
    <meta charset='utf-8'>
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="/css/icons16.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script type="text/javascript" src="/extjs/ext-all-debug.js"></script>
    <script type="text/javascript" src="/extjs/locale/ext-lang-ru.js"></script>
    <script src="//api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript" src="/app.js"></script>
    <script type="text/javascript" src="/js/name.js"></script>

    <style>
        .field-required {
            background: #c8fafa none;
        }

        .field-required-invalid-field {
            background: #c8fafa url('/extjs/resources/ext-theme-classic/images/grid/invalid_line.gif') repeat-x bottom;
            border-color: #c30;
        }

        .read-only-default input, .read-only-default input {
            color: #999999;
           
        }
                
          .sel-row td {
             color: darkgreen;
             font-weight: bold !important;
        
          }

        /*
        .x-grid-row-selected .x-grid-cell-inner {
            font-weight: bold;
            background-color:whitesmoke;
        }
        */

        .bg-panel {
            background-color: #dfe8f6;
        }

        .incoming-call-is-new {
            background: url('/icons/16x16/information.png') no-repeat left center;
            padding-left: 20px;
        }
        
        .incoming-call-is-executed {
            background: url('/icons/16x16/accept.png') no-repeat left center;
            padding-left: 20px;
        }

        .x-boundlist-item img.chkCombo {
            background: transparent url('/extjs/resources/themes/images/default/menu/unchecked.gif');

        }
        .x-boundlist-selected img.chkCombo {
            background: transparent url('/extjs/resources/themes/images/default/menu/checked.gif');
        }

        .x-form-point-trigger {
            background-image: url('/extjs/resources/ext-theme-classic/images/form/trigger-point.gif');
        }

    </style>
</head>
<body></body>
</html>

