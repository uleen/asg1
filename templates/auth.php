<html>
<head>
    <title>CRM</title>
    <meta charset='utf-8'>
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="/css/icons16.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script type="text/javascript" src="/extjs/ext-all-debug.js"></script>
    <script type="text/javascript" src="/extjs/locale/ext-lang-ru.js"></script>
    <script type="text/javascript">
        Ext.onReady(function () {

        	function submitOnEnter(field, event) {
        		if (event.getKey() == event.ENTER) {
                    Ext.getCmp('login-form')
                    .getForm()
                    .submit({
                        success: function (form, action) {
                            window.location = '/';
                        },
                        failure: function (form, action) {
                            Ext.Msg.alert(action.result.msg);
                        }
                    });
                    return true;
        		}
        	}
        	
            var login = new Ext.form.FormPanel({
                id: 'login-form',
                bodyStyle: 'padding: 7 7 5',
                method: 'POST',
                url: '/auth?_dc=' + Math.random(),
                items: [
                    {   xtype: 'textfield',
                        labelAlign: 'right',
                        fieldLabel: 'Логин',
                        name: 'login',
                        width: 200,
                        labelWidth: 50
                    },
                    {
                        xtype: 'textfield',
                        labelAlign: 'right',
                        name: 'password',
                        fieldLabel: 'Пароль',
                        inputType: 'password',
                        width: 200,
                        labelWidth: 50,
                        listeners: {
                    		specialkey: submitOnEnter
                    	}
                    }
                ],
                buttons: [
                    {
                        text: 'Войти',
                        id: 'btn-login',
                        handler: function () {
                            Ext.getCmp('login-form')
                                .getForm()
                                .submit({
                                    success: function (form, action) {
                                        window.location = '/';
                                    },
                                    failure: function (form, action) {
                                        Ext.Msg.alert(action.result.msg);
                                    }
                                });
                        }

                    }
                   
                ]
            });

            var win = new Ext.Window({
                layout: 'fit',
                closable: false,
                resizable: false,
                plain: false,
                border: 0,
                autoShow: true,
                items: [login]
            });
        });
    </script>
</head>
<body></body>
</html>