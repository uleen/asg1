<?php

class FiasApi {
    private $token;
    private $key;
    
    private $error;
    
    public function __construct($token, $key) {
        $this->token = $token;
        $this->key = $key;
        $this->error = null;
    }
    
    private function GetURL(FiasQuery $query){
        if(empty($this->token)){
            $this->error = 'Токен не может быть пустым';
            return false;
        }
        
        if(empty($this->key)){
            $this->error = 'Ключ не может быть пустым';
            return false;
        }
        
        if(empty($query)){
            $this->error = 'Объект запроса не может быть пустым';
            return false;
        }        
        
        return 'http://kladr-api.ru/api.php?' . $query . '&token=' . $this->token . '&key=' . $this->key;
    }
    
    /**
     * Возвращает результат запроса к сервису в виде объекта Json
     * @param \Fias\Query $query Объект запроса
     * @return mixed|boolean
     */
    public function QueryToJson(FiasQuery $query){
        $url = $this->GetURL($query);
        if(!$url) return false;        
        
        $result = file_get_contents($url);
        
        if(preg_match('/Error: (.*)/', $result, $matches)){
            $this->error = $matches[1];
            return false;
        }
        
        return json_decode($result);
    }
    
    /**
     * Возвращает результат запроса к сервису в виде массива
     * @param \Fias\Query $query Объект запроса
     * @return array
     */
    public function QueryToArray(FiasQuery $query)
    {       
        $obResult = $this->QueryToJson($query);        
        if(!$obResult) return array();
        
        $arResult = array();        
        foreach($obResult->result as $obObject)
        {
            $arObject = array(
                'id' => $obObject->id,
                'name' => $obObject->name,
                'zip' => $obObject->zip,
                'type' => $obObject->type,
                'typeShort' => $obObject->typeShort,
                'fullname' => $obObject->typeShort.'. '.$obObject->name,
            );
            
            if(isset($obObject->parents) && !empty($obObject->parents))
            {
                $tobj = array();
                foreach($obObject->parents as $arParent)
                {
                  $tobj[]= $arParent->name.' '.$arParent->type;
          
                }
                $arObject['parent'] = $arObject['fullname'].', '.implode(', ', $tobj);
            }
            else {
                $arObject['parent'] = $arObject['fullname'];
            }
            $arResult[] = $arObject;
        }
        return $arResult;
    }
    
    /**
     * Возвращает результат запроса к сервису в виде массива объектов
     * @param \Fias\Query $query Объект запроса
     * @return Object[]
     */
    public function QueryToObjects(Query $query){
        $obResult = $this->QueryToJson($query);        
        if(!$obResult) return array();
        
        $arObjects = array();        
        foreach($obResult->result as $obObject){
            $arObjects[] = new FiasObject($obObject);
        }
        
        return $arObjects;
    }
    
    public function __get($name) {
        switch($name){
            case 'Error': return $this->error;
        }
    }
}

/**
 * @property-read string $Id Идентификатор объекта
 * @property-read string $Name Название объекта
 * @property-read string $Zip Почтовый индекс объекта
 * @property-read string $Type Тип объекта полностью (область, район)
 * @property-read string $TypeShort Тип объекта коротко (обл, р-н)
 * @property-read string $Okato ОКАТО объекта
 * @property-read Object[] $Parents Массив родительских объектов
 */
class FiasObject {
    private $id;
    private $name;
    private $zip;
    private $type;
    private $typeShort;
    private $okato;
    private $arParents;
    
    public function __construct($obObject) {
        $this->id = $obObject->id;
        $this->name = $obObject->name;
        $this->zip = $obObject->zip;
        $this->type = $obObject->type;
        $this->typeShort = $obObject->typeShort;
        //$this->okato = $obObject->okato;
        
        $this->arParents = array();
        
        if(isset($obObject->parents)){
            foreach($obObject->parents as $obParent){
                $this->arParents[] = new Object($obParent);
            }
        }
    }
    
    public function __get($name)
     {
        switch($name)
        {
            case 'Id': return $this->id;
            case 'Name': return $this->name;
            case 'Zip': return $this->zip;
            case 'Type': return $this->type;
            case 'TypeShort': return $this->typeShort;
            //case 'Okato': return $this->okato;
            case 'Parents': return $this->arParents;
        }
    }
}

/**
 * Перечисление типов объектов
 */
class FiasObjectType {
    /**
     * Регион
     */
    const Region = 'region';
    
    /**
     * Район
     */
    const District = 'district';
    
    /**
     * Населённый пункт
     */
    const City = 'city';
    
    /**
     * Улица
     */
    const Street = 'street';
    
    /**
     * Строение
     */
    const Building = 'building';
}

/**
 * Класс запроса
 * @property string $ParentType Тип родительского объекта для ограничения области поиска (регион, район, город)
 * @property string $ParentId Идентификатор родительского объекта
 * @property string $ContentType Тип искомых объектов (регион, район, город)
 * @property string $ContentName Название искомого объекта (частично либо полностью)
 * @property boolean $WithParent Получить объекты вместе с родителями
 * @property integer $Limit Ограничение количества возвращаемых объектов
 */
class FiasQuery {
    private $parentType;
    private $parentId;
    
    private $contentType;
    private $contentName;
    
    private $withParent;
    private $limit;
    
    public function __construct($parentType=null, $parentId=null, $contentType=null, $contentName=null, $withParent=null, $limit=null) 
    {
        $this->parentType = $parentType;
        $this->parentId = $parentId;
        $this->contentType = $contentType;
        $this->contentName = $contentName;
        $this->withParent = $withParent;
        $this->limit = $limit;
    }
    
    public function __get($name) 
    {
        switch($name)
        {
            case 'ParentType': return $this->parentType;
            case 'ParentId': return $this->parentId;
            case 'ContentType': return $this->contentType;
            case 'ContentName': return $this->contentName;
            case 'WithParent': return $this->withParent;
            case 'Limit': return $this->limit;
            default: null;
        }
    }
    
    public function __set($name, $value)
    {
        switch($name)
        {
            case 'ParentType': $this->parentType = $value; break;
            case 'ParentId': $this->parentId = $value; break;
            case 'ContentType': $this->contentType = $value; break;
            case 'ContentName': $this->contentName = $value; break;
            case 'WithParent': $this->withParent = $value; break;
            case 'Limit': $this->limit = $value; break;
        }
    }
    
    public function __toString()
    {
        $string = '';
        
        if($this->parentType && $this->parentId){
            $string .= $this->parentType . 'Id=' . $this->parentId;
        }
        
        if($this->contentName){
            if(!empty($string)) $string .= '&';
            $string .= 'query=' . urlencode($this->contentName); 
        }
        
        if($this->contentType){
            if(!empty($string)) $string .= '&';
            $string .= 'contentType=' . $this->contentType;
        }
        
        if($this->withParent){
            if(!empty($string)) $string .= '&';
            $string .= 'withParent=1';
        }
        
        if($this->limit){
            if(!empty($string)) $string .= '&';
            $string .= 'limit=' . $this->limit;
        }
        
        return $string;
    }
}

class FiasRequest
{
  static $key = '52d3a86431608fd706000002';
  static $token = '86a2c2a06f1b2451a87d05512cc2c3edfdf41969';

  public static function getCity($req)
  {
    $api = new FiasApi(self::$key, self::$token);

    $query = new FiasQuery();
    $query->ContentName = "{$req}";
    $query->ContentType = FiasObjectType::City;
    $query->WithParent = true;
    $query->Limit = 30;
    $arResult = $api->QueryToArray($query);
    return $arResult;
  }

  public static function getStreet($req, $city_id)
  {
    $api = new FiasApi(self::$key, self::$token);


    $query = new FiasQuery();
    $query->ContentName = "{$req}";
    $query->ParentType = FiasObjectType::City;
    $query->ParentId = "{$city_id}";
    $query->ContentType = FiasObjectType::Street;
    $query->WithParent = false;
    $query->Limit = 30;

    // Получение данных в виде ассоциативного массива
    $arResult = $api->QueryToArray($query);

    return $arResult;
  }

  public static function getBuilding($req, $street_id)
  {
    $api = new FiasApi(self::$key, self::$token);

    $query = new FiasQuery();
    $query->ContentName = "{$req}";
    $query->ParentType = FiasObjectType::Street;
    $query->ParentId = "{$street_id}";
    $query->ContentType = FiasObjectType::Building;
    $query->WithParent = false;

    $arResult = $api->QueryToArray($query);

    return $arResult;
  }
}



