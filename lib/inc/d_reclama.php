<?php

// получение списка записей
$app->get('/', function () use ($app) {
    $sql = "select dr.*
                    from d_reclama dr
                    where 1 = 1 and dr.id>0 ";
    $params = array();
    if ($app->request->get('status')) {
        $sql .= " and dr.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and dr.status in (1,2)";
    }
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});

// добавление записи
$app->post('/0', function () use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;

    // валидация
    if ($data['title']) {
        $sql = "insert into d_reclama (title, status) values (:title, :status)";
        $params = array(
            'title' => $data['title'],
            'status' => $data['status']
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();
        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});

// редактирование записи
$app->put('/:id', function ($id) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;

    // валидация
    if ($data['title']) {
        $sql = "update d_reclama set title = :title, status = :status where id = :id";
        $params = array(
            'id' => $id,
            'title' => $data['title'],
            'status' => $data['status'],
        );
        db::get()->prepare($sql)->execute($params);

        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});