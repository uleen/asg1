<?php


// список
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "cc.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "cc.status in (1,2)";
    }
    if ($app->request->get('contractor_id')) {
        $conditions[] = "cc.contractor_id = :contractor_id";
        $params['contractor_id'] = $app->request->get('contractor_id');
    }
    if ($app->request->get('is_actual')) {
        $conditions[] = "(cc.prolongation = 1 or cc.action_to_date is null or cc.action_to_date > :current_date)";
        $params['current_date'] = time();
    }
    if ($app->request->get('query')) {
        $conditions[] = "(cc.doc_num like :query or c.title like :query)";
        $params['query'] = '%' . $app->request->get('query') . '%';
    }

    // поиск
    $items = array();
    foreach (Contractor_Contract::find($conditions, $params, $_GET['start'], $_GET['limit']) as $contractorContract) {
        $items[] = $contractorContract->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => Contractor_Contract::calc_found_rows(),
        'items' => $items,
    );
    echo json_encode($response);
});

// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $sql = "select d.*, f.title as file_title, f.name as file_name,
              (SELECT dc.title FROM d_contractor dc WHERE dc.id = d.contractor_id) AS contractor,
              DATE_FORMAT(FROM_UNIXTIME(d.doc_date), '%d.%m.%Y') as doc_date,
              DATE_FORMAT(FROM_UNIXTIME(d.action_from_date), '%d.%m.%Y') as action_from_date,
              DATE_FORMAT(FROM_UNIXTIME(d.action_to_date), '%d.%m.%Y') as action_to_date
            from d_contractor_contracts d
            left join uploaded_files f on d.file_id = f.id
            where d.id = :id";
    $params = array('id' => $id);
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'item' => $data,
    );
    echo json_encode($response);
});


// добавление, изменение записей
$app->map('(/:id)', function ($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $contractorContract = new Contractor_Contract($data);
    $contractorContract->id = $id;

    // преобразование дат
    $contractorContract->doc_date = $data['doc_date'] ? strtotime($data['doc_date']) : null;
    $contractorContract->action_from_date = $data['action_from_date'] ? strtotime($data['action_from_date']) : null;
    $contractorContract->action_to_date = $data['action_to_date'] ? strtotime($data['action_to_date']) : null;

    // валидация
    if ($contractorContract->validate()) {
        $contractorContract->save();
        $response = array(
            'success' => true,
            'item' => $contractorContract->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $contractorContract->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->via('POST', 'PUT');