<?php


// сохранение прав для дерева
$app->post('/up_tree', function () use ($app) {
  // входные данные
  $data = $app->request()->post();

  if (isset($data['uid']))
  {  
    $perms = array();
    $perms = explode(',', substr($data['tree'],1,strlen($data['tree'])));
    print_r($perms);

    db::get()->beginTransaction();
    try{
      db::get()->exec("delete from u_perms where uid={$data['uid']};");
      $cnt = count($perms);
      for($i=0; $i<$cnt; $i++)
      {
        $sql = "insert into u_perms(uid, perms) values (:uid, :perms)";
        $params = array('uid'=>$data['uid'], 'perms'=>$perms[$i]);
        $sth = db::get()->prepare($sql)->execute($params);
      }
      db::get()->commit();
     
      $response = array(
          'success' => true,
          'items' => array('update'=>'ok'),
      );
    }
    catch(PDOException $e)
    {
      $response = array(
          'success' => false,
          'items' => "pdo: {$e->getMessage()}",
      );
      db::get()->rollBack();
    }
    echo json_encode($response);

    // 
  }
});

// получение дерева для редактирования
$app->get('/up_tree/edit/:uid', function($uid) use ($app){

  $sql="select perms from u_perms where uid=:uid";
  $params = array('uid'=>$uid);
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetchAll(PDO::FETCH_COLUMN);

  $tree = "{\"text\": \".\",\"expanded\": true, \"children\":[
  {
  obj_text: 'Системные настройки',
  iconCls: 'icon_cog',
  expanded: true,
  children:[
  {
  obj_text: 'Управление Пользователями',
  iconCls: 'icon_user_add',
  checked: ";
  if (in_array(1,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 1,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Документы',
  expanded: true,
  children: [
  {
  obj_text: 'Входящий запрос',
  iconCls: 'icon_bell',
  expanded: true,
  children:[
  {
  obj_text: 'Создание',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(20,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 20,
  leaf: true,
   
  },
  {
  obj_text: 'Создать Клиента',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(30,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 30,
  leaf: true,
   
  },
  ],
  
  },
  {
  obj_text: 'Заказ Клиента',
  iconCls: 'icon_document_prepare',
  expanded: true,
  children: [
  {
  obj_text: 'Изменение',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(40,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 40,
  leaf: true,
  },
  ]
  }
  ]
  },
  {
  obj_text: 'Справочники',
  expanded: true,
  children: [
  {
  obj_text: 'Валюты',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(100,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 100,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(101,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 101,
  leaf: true,
  },
  ]
   
  },
  {
  obj_text: 'Виды расчетов',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(102,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 102,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(103,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 103,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Договоры',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(104,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 104,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(105,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 105,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Должности',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(106,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 106,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
  checked: ";
  if (in_array(107,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 107,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Единицы измерения',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(108,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 108,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(109,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 109,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Категория клиента',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(110,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 110,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(111,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 111,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Контактные лица',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(112,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 112,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(113,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 113,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Направления продаж',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(114,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 114,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(115,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 115,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Номенклатурные группы',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(116,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 116,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(117,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 117,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'ОПФ',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(118,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 118,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
 checked: ";
  if (in_array(119,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 119,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Организации',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(120,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 120,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(121,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 121,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Реквизиты',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(122,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 122,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(123,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 123,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Реклама',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(124,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 124,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(125,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 125,
  leaf: true,
  },
  ]
   
  },
  {
  obj_text: 'Сотрудники',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(126,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 126,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(127,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 127,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Пользователи',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(128,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 128,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(129,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 129,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Сфера деятельности',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(130,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 130,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(131,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 131,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Точки доставки/отгрузки',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(132,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 132,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(133,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 133,
  leaf: true,
  },
  ]
  },
  {
  obj_text: 'Цены',
  iconCls: 'icon_bullet_blue',
  children: [
  {
  obj_text: 'Просмотр',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(134,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 134,
  leaf: true,
  },
  {
  obj_text: 'Редактирование',
  iconCls: 'icon_bullet_blue',
checked: ";
  if (in_array(135,$data)){$tree .="true,"; }else {$tree .="false,";}
  $tree .="
  access_id: 135,
  leaf: true,
  },
  ]
  },
  ]
  }
   
  ]
  
  }";
  
  echo $tree;

});


// получение дерева для просмотра
$app->get('/up_tree/view/:uid', function($uid) use ($app){

  $sql="select perms from u_perms where uid=:uid";
  $params = array('uid'=>$uid);
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetchAll(PDO::FETCH_COLUMN);

  $tree = "{\"text\": \".\",\"expanded\": true, \"children\":[";
  if (in_array(1, $data))
  {
    $tree .= "
    {
      obj_text: 'Системные настройки',
      iconCls: 'icon_cog',
      expanded: true,
      children:[
        {
        obj_text: 'Управление Пользователями',
        iconCls: 'icon_user_add',
        leaf: true,
       },
     ]
    },";
  }

  $f1 = false;
  $ic_create = '';
  $ic_client = '';
  if (in_array(20, $data)){
    $ic_create = 
    "  {
        	obj_text: 'Создание',
        	iconCls: 'icon_bullet_blue',
        	leaf: true,
       },
    ";
    $f1 = true;
  }
  
  if (in_array(30, $data)){
    $ic_client =
    "{
          	obj_text: 'Создать Клиента',
          	iconCls: 'icon_bullet_blue',
          	leaf: true,
          	
          },    
    ";
    $f1 = true;
  }
  if ($f1 == true) {
    $ic =
    "{
    	obj_text: 'Входящий запрос',
        iconCls: 'icon_bell',
        expanded: true,
        children:[
          {$ic_create}
          {$ic_client}       				                    
        ]
    },
    ";
  }
  
  $zc = '';
  if (in_array(40, $data)){
    $f1 = true;
    $zc = 
    "{
          	obj_text: 'Заказ Клиента',
          	iconCls: 'icon_document_prepare',
          	expanded: true,
          	children: [
	            	      {
	            	           obj_text: 'Изменение',
	            	           iconCls: 'icon_bullet_blue',
	            	           leaf: true,  
	            	      },  
          	]
       }    
    ";
  }
  if ($f1 == true)
  {
    $tree .=
    "{
	        obj_text: 'Документы',
	        expanded: true,
	        children: [
	          {$ic}      
              {$zc}
	        ]
    },
  ";
  }
 
  // Справочники
  $dict = '';
  $dict .= createData(100,101,$data,'Валюты');
  $dict .= createData(102,103,$data,'Виды расчетов');
  $dict .= createData(104,105,$data,'Договоры');
  $dict .= createData(105,106,$data,'Должности');
  $dict .= createData(108,109,$data,'Единицы измерения');  

  $dict .= createData(110,111,$data,'Категория клиента');
  $dict .= createData(112,113,$data,'Контактные лица');
  $dict .= createData(114,115,$data,'Направления продаж');
  $dict .= createData(116,117,$data,'Номенклатурные группы');
  $dict .= createData(118,119,$data,'ОПФ');
  $dict .= createData(120,121,$data,'Организации'); 
  $dict .= createData(122,123,$data,'Реквизиты');
  $dict .= createData(124,125,$data,'Реклама');
  $dict .= createData(126,127,$data,'Сотрудники');
  $dict .= createData(128,129,$data,'Пользователи');
  $dict .= createData(130,131,$data,'Сфера деятельности');
  $dict .= createData(132,133,$data,'Точки доставки/отгрузки');
  $dict .= createData(134,135,$data,'Цены');
//  $dict .= createData(1,1,$data,'');

  if (strlen($dict)>0){
    $tree .=
    "
      {
        obj_text: 'Справочники',
        expanded: true,
        children: [
          {$dict}
        ]
        },
    ";
  }
  
  $tree .="]}";
  echo $tree;
});

function createData($a, $b, $data, $title){
  $f1 = false;
  if (in_array($a, $data)){
    $f1 = true;
    $val =
    "{
    obj_text: 'Просмотр',
    iconCls: 'icon_bullet_blue',
    leaf: true,
  },
  ";
  }
  if (in_array($b, $data)){
    $f1 = true;
    $val2 =
    "{
    obj_text: 'Редактирование', iconCls: 'icon_bullet_blue', leaf: true,
  },
  ";
  }
  if ($f1==true){
    $val3 =
    "{
    obj_text: '{$title}',
    iconCls: 'icon_bullet_green',
    children: [
    {$val}
    {$val2}
    ]
  },
  ";
    }
  return $val3;
}