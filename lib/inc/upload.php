<?php

$pathInfo = pathinfo($_FILES['file']['name']);
$data = array(
    'title' => $pathInfo['filename'],
    'name' => uniqid() . '.' . $pathInfo['extension'],
    'type' => $_FILES['file']['type'],
    'size' => $_FILES['file']['size'],
);

if (copy($_FILES['file']['tmp_name'], './files/' . $data['name'])) {
    $sql = "insert into uploaded_files (title, name, type, size) values (:title, :name, :type, :size)";
    db::get()->prepare($sql)->execute($data);
    $data['id'] = db::get()->lastInsertId();
    $response = array(
        'success' => true,
        'item' => $data,
    );
} else {
    $response = array(
        'success' => false,
        'errors' => 'Ошибка загрузки файла',
    );
}

// ответ
echo json_encode($response);