<?php

// получение списка записей
$app->get('(/:id)', function ($id = null) use ($app) {

    $sql = "select ds.*,
                case when exists (select 1 from d_direction_sales dsc where dsc.pid = ds.id) then 0 else 1 end as leaf,
                case when exists (select 1 from d_direction_sales dsc where dsc.pid = ds.id) then null else 'x-tree-icon-parent' end as iconCls
            from d_direction_sales ds
            where 1 = 1";
    $params = array();
    if ($id) {
        $sql .= " and ds.pid = :pid";
        $params['pid'] = $id;
    } else {
        $sql .= " and ds.pid is null";
    }
    if ($app->request->get('status')) {
        $sql .= " and ds.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and ds.status in (1,2)";
    }
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $items = $sth->fetchAll(PDO::FETCH_ASSOC);

    // данные самого элемента
    if ($id) {
        $sql = "select ds.* from d_direction_sales ds where ds.id = :id";
        $params = array('id' => $id);
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        $item = $sth->fetch(PDO::FETCH_ASSOC);

        // получение списка характеристик
        $sql = "select * from d_characteristic where nomenclature_group_id = :id order by sort";
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        $item['characteristics'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    // ответ
    $response = array(
        'success' => true,
        'item' => $item,
        'total' => count($items),
        'items' => $items,

    );
    echo json_encode($response);

});


// добавление записи
$app->post('/0', function () use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;

    // валидация
    if ($data['title']) {
        $sql = "insert into d_direction_sales (pid, title, status) values (:pid, :title, :status)";
        $params = array(
            'pid' => $data['pid'],
            'title' => $data['title'],
            'status' => $data['status']
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();
        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});


// редактирование записи
$app->put('/:id', function ($id) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;

    // валидация
    if ($data['title']) {
        $sql = "update d_direction_sales set title = :title, status = :status where id = :id";
        $params = array(
            'id' => $id,
            'title' => $data['title'],
            'status' => $data['status'],
        );
        db::get()->prepare($sql)->execute($params);

        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});


// получение пути от корня до категории
$app->get('(/path/:id)', function ($id) use ($app) {
    $ng = Nomenclature_Group::find_by_pk($id);
    if ($ng) {
        echo json_encode($ng->getPath());
    }
});