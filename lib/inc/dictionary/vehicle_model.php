<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Vehicle_Model::find($conditions, $params) as $vehicleModel) {
        $items[] = $vehicleModel->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});
// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
	$vehicleModel = Vehicle_Model::find_by_pk($id);
	$response = array(
		'success' => true,
		'item' => $vehicleModel ? $vehicleModel->getData() : null,
	);
	echo json_encode($response);
})->conditions(array('id' => '\d+'));

// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $vehicleModel = new Vehicle_Model($data);
    $vehicleModel->id = $id;

    // валидация
    if ($vehicleModel->validate()) {
        $vehicleModel->save();
        $response = array(
            'success' => true,
            'item' => $vehicleModel->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $vehicleModel->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');