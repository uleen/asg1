<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Currency::find($conditions, $params) as $currency) {
        $items[] = $currency->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// добавление, изменение записей
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];
    $currency = new Currency($item);
    $currency->id = $id;

    // валидация
    if ($currency->validate()) {
        $currency->save();
        $response = array(
            'success' => true,
            'item' => $currency->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $currency->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->via('POST', 'PUT');


// получение списка всех доступных валют
$app->get('/all', function () use ($app) {
    $data = Currency::getAll();
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});


// получение истории изменения курса
$app->get('/rate', function () use ($app) {
    if ($_GET['num']) {
        $data = Currency::getHistoryRate($_GET['num']);
        $response = array(
            'success' => true,
            'total' => count($data),
            'items' => $data,
        );
        echo json_encode($response);
    }
});
