<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $sql = "select c.id, c.additional_info, c.code, c.name,
                      concat(c.code, '. ', c.name) as title,
                      if(c.node_count = 0, true, false) as leaf
                    from c_okved c where c.parent_id " . ($app->request->get('node') ? "= :parent_id" : "is null");
    $params = $app->request->get('node') ? array('parent_id' => $app->request->get('node')) : array();
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($data);
});

// сохранение связей ОКВЭДа и Сфер деятельности
$app->put('/:id', function ($id) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    if ($data['reference_scope_id']) {
        $sql = "insert into d_reference_scope_okved (reference_scope_id, okved_id) values (:reference_scope_id, :okved_id)";
        $params = array(
            'reference_scope_id' => $data['reference_scope_id'],
            'okved_id' => $id,
        );
        db::get()->prepare($sql)->execute($params);
    }
});

// удаление связи ОКВЭД - Сфера деятельности
$app->delete('/:id', function ($id) use ($app) {
});