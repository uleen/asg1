<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Shift::find($conditions, $params) as $shift) {
        $items[] = $shift->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $shift = new Shift($data);
    $shift->id = $id;

    // валидация
    if ($shift->validate()) {
        $shift->save();
        $response = array(
            'success' => true,
            'item' => $shift->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $shift->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');