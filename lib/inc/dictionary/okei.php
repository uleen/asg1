<?php

$app->get('/', function () use ($app) {
    $sql = "select * from c_okei";
    $sth = db::get()->prepare($sql);
    $sth->execute();
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($data);
});