<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Vehicle_Type::find($conditions, $params) as $vehicleType) {
        $items[] = $vehicleType->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $vehicleType = new Vehicle_Type($data);
    $vehicleType->id = $id;

    // валидация
    if ($vehicleType->validate()) {
        $vehicleType->save();
        $response = array(
            'success' => true,
            'item' => $vehicleType->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $vehicleType->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');