<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Fuel::find($conditions, $params) as $fuel) {
        $items[] = $fuel->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $fuel = new Fuel($data);
    $fuel->id = $id;

    // валидация
    if ($fuel->validate()) {
        $fuel->save();
        $response = array(
            'success' => true,
            'item' => $fuel->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $fuel->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');