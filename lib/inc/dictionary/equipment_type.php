<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Equipment_Type::find($conditions, $params) as $equipmentType) {
        $items[] = $equipmentType->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $equipmentType = new Equipment_Type($data);
    $equipmentType->id = $id;

    // валидация
    if ($equipmentType->validate()) {
        $equipmentType->save();
        $response = array(
            'success' => true,
            'item' => $equipmentType->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $equipmentType->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');