<?php

$app->get('/', function () use ($app) {

    // список регионов
    if ($app->request->get('node') === '0') {
        $sql = "select dfa.aoid as id, concat(dfa.offname, ' ', dfa.shortname) as title, dfa.aolevel, 0 as leaf
                    from d_fias_addrobj dfa
                    where dfa.aolevel = 1 and dfa.actstatus = 1
                    order by dfa.regioncode";
        $params = $app->request->get('node') ? array('parent_id' => $app->request->get('node')) : array();
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    } // список городов в регионе
    elseif ($app->request->get('node')) {
        $sql = "select dfa.aoid as id, concat(dfa.offname, ' ', dfa.shortname) as title, dfa.aolevel, 1 as leaf
                    from d_fias_addrobj dfa
                    where dfa.aolevel = 4 and dfa.actstatus = 1
                      and dfa.regioncode = (select a.regioncode from d_fias_addrobj a where a.aoid = :aoid)
                    order by dfa.offname";
        $params = array('aoid' => $app->request->get('node'));
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

});