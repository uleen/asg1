<?php
// получение списка записей
$app->get('/', function () use ($app) {
  $params = array();
  // is_main!

  $org_id = (int)$app->request()->get('org_id');
  $contractor_id = (int)$app->request()->get('contractor_id');
  $status = (int)$app->request()->get('status');


  // HHHH!
  $sql = "
  SELECT sql_calc_found_rows
  cp.id AS contact_personal_id,
  cp.id,
  cp.id AS cagent_id,
  cp.fio,
  cp.position,
  cp.dateofbirth,
  cp.address,
  cp.is_send,
  cp.status as cp_status,
  ca.status,
  cp.fio AS cagent_name,
  cp.fio AS cagent_old_name,
  org.company_title AS company_title,
  org.company_title AS cagent_title,
  org.id as org_id,
  org.company_type,
  org.is_contractor as contractor_id,
  dc.main_personal_id,

  (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1) AS cagent_phone,
  (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1) AS cagent_add_phone,
  (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email
  FROM d_contact_personal cp

  LEFT JOIN org2personal AS o2p ON (cp.id=o2p.personal_id)
  LEFT JOIN organization AS org ON (org.`id` = o2p.`org_id`)
  LEFT JOIN ic2org ON (ic2org.`ic_id` = o2p.`org_id`)
  LEFT JOIN incoming_call_register AS ca ON (ca.id = ic2org.`ic_id`)
  LEFT JOIN d_contractor AS dc ON (dc.id = org.is_contractor) where 1=1 ";

  if ($org_id) {
    $sql .=" and o2p.`org_id` = {$org_id}";
  }

  if ($contractor_id){
    $sql .= " and org.is_contractor = {$contractor_id}";
  }

  if ($status) {
    $sql .= " and cp.status = {$status}";

  }

  $start = (int)$app->request->get('start');
  $limit = (int)$app->request->get('limit');
  $sql .= " limit $start, $limit";

  $params = array();

  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetchAll(PDO::FETCH_ASSOC);

  $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);

  $response = array(
      'success' => true,
      'total' => $foundRows[0],
      'items' => $data,
  );
  echo json_encode($response);
});

$app->get('/setmain', function () use ($app)
{
  $contact_personal_id = $app->request()->get('contact_personal_id');
  $contractor_id = $app->request()->get('contractor_id');

  if ($contact_personal_id!=0 && $contractor_id!=0)
  {
    $sql = "update d_contractor set main_personal_id = {$contact_personal_id} where id = {$contractor_id}";
     
    $params = array();

    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $response = array(
        'success' => true,
        'items' => "",
    );
    echo json_encode($response);

  }

});

// получение одной записи
$app->get('/:id', function ($id) use ($app) {

  //$id = (string)$app->request()->get('id');


  $sql = "
  SELECT
  cp.id AS contact_personal_id,
  cp.id,
  cp.id AS cagent_id,
  cp.fio,
  cp.position,
  cp.dateofbirth,
  cp.address,
  cp.is_send,
  cp.status as cp_status,
  ca.status,
  cp.fio AS cagent_name,
  cp.fio AS cagent_old_name,
  org.company_title AS company_title,
  org.company_title AS cagent_title,
  org.is_contractor as contractor_id,
  org.id as org_id,
  org.company_type,
  dc.main_personal_id

   
   
  --  (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id) AS cagent_phone,
  --  (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id) AS cagent_email

  FROM d_contact_personal cp

  LEFT JOIN org2personal AS o2p ON (cp.id=o2p.personal_id)
  LEFT JOIN organization AS org ON (org.`id` = o2p.`org_id`)
  LEFT JOIN ic2org ON (ic2org.`ic_id` = o2p.`org_id`)
  LEFT JOIN incoming_call_register AS ca ON (ca.id = ic2org.`ic_id`)
  LEFT JOIN d_contractor AS dc ON (dc.id = org.is_contractor)

  WHERE cp.id = :id";


  $params = array('id' => $id);
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetch(PDO::FETCH_ASSOC);
  $ar = array();
  // получение списка телефонов
  // джоинить признаки активности и главности
  $sql = "SELECT
        id,
        CASE
        contact_type
        WHEN 1 THEN 'Телефон'
        WHEN 2 THEN 'E-mail'
        END AS contact_type,
        contact AS number,
        add_contact,
        is_active,
        is_main
        FROM d_contacts_ref WHERE personal_id = :id";
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data['personal_contacts_data'] = $sth->fetchAll(PDO::FETCH_ASSOC);

  $response = array(
      'success' => true,
      'item' => $data,
  );
  echo json_encode($response);
});


// поиск контактного лица
$app->get('/search', function () use ($app) {
  $sql = "select cp.*
  from d_contact_personal cp
  where cp.status in (1, 2)";
  $params = array();
  if ($app->request->get('phone')) {
    $sql .= " and cp.phone = :phone";
    $params['phone'] = $app->request->get('phone');
  }
  $sql .= " limit 1";
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetchAll(PDO::FETCH_ASSOC);
  echo $data ? json_encode($data[0]) : '';
});

//-----------
// Сохранение Контактного Лица
// @todo: перенести все в объектную модель

$app->post('/0', function () use ($app) {

  // входные данные
  $data = json_decode($app->environment['slim.input'], true);
  $data['status'] = $data['status'] == 1 ? 1 : 2;
  $errors = array();
  if ($data['fio']=='')
  {
    $errors['fio'] = 'Поле "ФИО" обязательно для заполнения';
  }
  if (empty($errors))
  {
    $sql = "insert into d_contact_personal (fias_id, fio, status, position,  dateofbirth, address, is_send, is_fired)
    values (:fias_id, :fio, :status, :position, :dateofbirth, :address, :is_send, :is_fired )";
    $params = array(

        'fias_id' => 0,
        'fio' => $data['fio'],
        'status' => 1,
        // 'status' => $data['status'],
        'position' => $data['position'],
        'dateofbirth' => $data['dateofbirth'],
        'is_send' => $data['is_send'],
        'is_fired' => $data['is_fired'],
        'address' => $data['address']
    );
    db::get()->prepare($sql)->execute($params);
    $contact_personal_id = db::get()->lastInsertId();


    // cуществует контрагент
    if ($data['org_id']!=0)
    {
      $sql = "insert into org2personal (org_id, personal_id) values (:org_id, :personal_id)";
      $params = array(
          'org_id' => $data['org_id'],
          'personal_id' => $contact_personal_id,
      );
      db::get()->prepare($sql)->execute($params);
    }

    $response = array(
        'success' => true,
        'item' => array('id'=>$contact_personal_id),
    );
  } else {
    $response = array(
        'success' => false,
        'errors' => $errors
    );
  }
  // ответ
  echo json_encode($response);
});

// редактирование записи
$app->put('/:id', function ($id) use ($app) {
  // входные данные
  $data = json_decode($app->environment['slim.input'], true);
  $data['status'] = $data['status'] == "1" ? "1" : "2";

  // валидация
  if (1) {
    $sql = "update d_contact_personal set
    fio = :fio,
    status = :status,
    is_fired = :is_fired,
    is_send = :is_send,
    dateofbirth = :dateofbirth,
    position = :position,
    address = :address
    where id = :id";

    $params = array(
        'status' => $data['cp_status'],
        'fio' => $data['fio'],
        'position' => $data['position'] ? $data['position'] : '',
        'dateofbirth' => $data['dateofbirth'] ? $data['dateofbirth'] : '',
        'is_send' => $data['is_send'] ? $data['is_send'] : 0,
        'is_fired' => $data['is_fired'] ? $data['is_send'] : 0,
        'address' => $data['address'] ? $data['address'] : '',
        'id' => $data['contact_personal_id']
    );

    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    // ответ
    $response = array(
        'success' => true,
        'items' => $data,
    );
  } else {
    $response = array(
        'success' => false,
        'errors' => array(
            'title' => 'не указан заголовок',
        ),
    );
  }
  // ответ
  echo json_encode($response);
});