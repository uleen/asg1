<?php

// получение списка записей
$app->get('/', function () use ($app) {
    $data = Client_Order::getOrderStatuses();
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});