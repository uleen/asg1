<?php


// список записей
$app->get('/', function () use ($app) {
    $sql = "select SQL_CALC_FOUND_ROWS cd.*, c.title as contractor
            from d_contractor_delivery cd, d_contractor c
            where c.id = cd.contractor_id";
    $params = array();
    if ($app->request->get('status')) {
        $sql .= " and cd.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and cd.status in (1,2)";
    }
    if ($app->request->get('contractor_id')) {
        $sql .= " and cd.contractor_id = :contractor_id";
        $params['contractor_id'] = $app->request->get('contractor_id');
    }
    if ($app->request->get('org_type')) {
        $sql .= " and exists (select 1 from d_contractor c where cd.contractor_id = c.id and c.org_type = :org_type)";
        $params['org_type'] = $app->request->get('org_type');
    }
    if ($app->request->get('query')) {
        $sql .= " and ( cd.address like :query or c.title like :query)";
        $params['query'] = '%' . $app->request->get('query') . '%';
    }
    if ($app->request->get('exclude')) {
        $sql .= " and cd.id not in (" . trim($app->request->get('exclude'), ',') . ")";
    }
    $start = (int)$app->request->get('start');
    $limit = (int)$app->request->get('limit');
    $sql .= " limit $start, $limit";
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);
    $response = array(
        'success' => true,
        'total' => $foundRows[0],
        'items' => $data,
    );
    echo json_encode($response);
});

// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $sql = "select cd.*,
              (select c.title from d_contractor c where c.id = cd.contractor_id) as contractor
            from d_contractor_delivery cd
            where cd.id = :id";
    $params = array('id' => $id);
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'item' => $data,
    );
    echo json_encode($response);
});

// изменение
$app->put('/:id', function ($id) use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $errors = array();

    // правила валидации
    // ...


    // валидация
    if (empty($errors)) {
        $sql = "update d_contractor_delivery set
                  contractor_id = :contractor_id,
                  address = :address,
                  commentary = :commentary,
                  lat = :lat,
                  lng = :lng,
                  delay = :delay,
                  time_from = :time_from,
                  time_to = :time_to,
                  status = :status
                where id = :id";
        $params = array(
            'contractor_id' => $data['contractor_id'],
            'address' => $data['address'],
            'commentary' => $data['commentary'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'delay' => $data['delay'],
            'time_from' => $data['time_from'],
            'time_to' => $data['time_to'],
            'status' => $data['status'],
            'id' => $id,
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();

        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $errors,
        );
    }
    // ответ
    echo json_encode($response);
});

// сохранение
$app->post('/0', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $errors = array();

    // правила валидации
    // ...


    // валидация
    if (empty($errors)) {
        $sql = "insert into d_contractor_delivery set
                  contractor_id = :contractor_id,
                  address = :address,
                  commentary = :commentary,
                  lat = :lat,
                  lng = :lng,
                  delay = :delay,
                  time_from = :time_from,
                  time_to = :time_to,
                  status = :status";
        $params = array(
            'contractor_id' => $data['contractor_id'],
            'address' => $data['address'],
            'commentary' => $data['commentary'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'delay' => $data['delay'],
            'time_from' => $data['time_from'],
            'time_to' => $data['time_to'],
            'status' => $data['status'],
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();

        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $errors,
        );
    }
    // ответ
    echo json_encode($response);
});