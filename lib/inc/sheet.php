<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('contractor_id')) {
        $conditions[] = "t.contractor_id = :contractor_id";
        $params['contractor_id'] = $app->request->get('contractor_id');
    }

    // поиск
    $items = array();
    foreach (Sheet::find($conditions, $params, $_GET['start'], $_GET['limit']) as $sheet) {
        $items[] = $sheet->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => Sheet::calc_found_rows(),
        'items' => $items,
    );
    echo json_encode($response);
});


// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $sheet = Sheet::find_by_pk($id);
    $response = array(
        'success' => true,
        'item' => $sheet ? $sheet->getData() : null,
    );
    echo json_encode($response);
})->conditions(array('id' => '\d+'));


// получение истории изменения статусов
$app->get('/status_history', function () use ($app) {
    $sheet = Sheet::find_by_pk($_GET['sheet_id']);
    if ($sheet) {
        $response = array(
            'success' => true,
            'items' => $sheet->getStatusHistory(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Счет не найден',
        );
    }
    echo json_encode($response);
});


// добавление, изменение записей
$app->map('(/:id)', function ($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    unset($data['create_date']);
    unset($data['create_user_id']);
    $sheet = new Sheet($data);
    $sheet->id = $id;

    if (!$sheet->id) {
        $sheet->create_date = date('Y-m-d H:i:s');
        $sheet->create_user_id = User::get()->id;
    }

    // валидация
    if ($sheet->validate()) {
        $sheet->save();
        $sheet->changeStatus($data['sheet_status_id'], null, User::get()->id);
        $sheet->reload();
        $response = array(
            'success' => true,
            'item' => $sheet->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $sheet->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');


// генерация печатного документа
$app->get('/print/:id', function ($id) use ($app) {
    $sheet = Sheet::find_by_pk($id);
    if ($sheet) {
        $app->response->headers->set('Content-Type', 'application/pdf');
        require_once('lib/mpdf/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($sheet->document->content);
        $mpdf->Output($sheet->title . '.pdf', 'I');
    }
})->conditions(array('id' => '\d+'));