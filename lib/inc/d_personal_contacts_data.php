<?php

$app->post('/0', function () use ($app) {
  // входные данные
  $data = json_decode($app->environment['slim.input'], true);
   

  $p_sql = "insert into d_contacts_ref (personal_id, contact_type, contact, add_contact, is_main, is_active)
  values (:personal_id, :contact_type, :contact, :add_contact, :is_main, :is_active)";
  $params = array(
      'personal_id' => $data['contact_personal_id'],
      'contact_type' => $data['contact_type'],
      'contact' => $data['number'],
      'add_contact' => $data['add_contact'],
      'is_main' => $data['is_main'],
      'is_active' => $data['is_active']
  );
  db::get()->prepare($p_sql)->execute($params);
  $contact_id = db::get()->lastInsertId();

  $e_sql = "insert into contacts2personal (contact_id, personal_id)
  values (:contact_id, :personal_id)";
  $params = array(
      'contact_id' => $contact_id,
      'personal_id' => $data['contact_personal_id'],
  );
  db::get()->prepare($e_sql)->execute($params);
  $response = array(
      'success' => true,
      'items' => $data,
  );
  echo json_encode($response);
});

$app->put('/:id', function ($id) use ($app) {
  $data = json_decode($app->environment['slim.input'], true);
  $p_sql = 'update d_contacts_ref set
  add_contact = :add_contact,
  contact = :contact,
  is_main = :is_main,
  is_active = :is_active
  where id = :id and personal_id = :personal_id and contact_type = :contact_type';
  $params = array(
      'id' => $data[id],
      'personal_id' => $data['contact_personal_id'],
      'contact_type' => $data['contact_type'],
      'add_contact' => $data['add_contact'],
      'contact' => $data['number'],
      'is_active' => $data['is_active'],
      'is_main' => $data['is_main']
  );
  db::get()->prepare($p_sql)->execute($params);
  $response = array(
      'success' => true,
      'items' => $data,
  );
  echo json_encode($response);
});