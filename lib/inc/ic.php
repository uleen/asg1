<?php
// получение списка входящих запросов
$app->get('/', function () use ($app) {

  $start = (int)$app->request->get('start');
  $limit = (int)$app->request->get('limit');
  $org_id = (int)$app->request->get('org_id');

  $sql = "SELECT SQL_CALC_FOUND_ROWS * from (
  SELECT
  ca.id,
  ca.root_id AS root_id,
  IF (ca.root_id = 0, 1, 2) AS is_new,
  org.id AS org_id,
  (SELECT fio FROM d_contact_personal WHERE id = ic2org.`personal_id`) AS cagent_name,
  (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1) AS cagent_phone,
  (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1) AS cagent_add_phone,
  (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email,
  org.company_title,
  ca.client_interest_srv,
  ca.status,
  c2.title AS reclama_title,
  IF (dc.id IS NULL, 0, dc.id) AS contractor_id,
  IF (ca.root_id = 0, 'да','нет') AS is_new_title,

  CASE ca.incoming_source_id
  WHEN 1 THEN 'Телефон'
  WHEN 2 THEN 'E-mail'
  WHEN 3 THEN 'Сайт'
  END AS source_title,

  DATE_FORMAT(FROM_UNIXTIME(ca.call_date), '%d.%m.%Y %H:%i') AS call_date,
  ca.client_interest,

  ca.is_executed as executed,
  u.name as manager_name,
  ca.manager_id,
  (SELECT u.id FROM `user` u WHERE u.id = ca.user_id) AS author_id

  FROM incoming_call_register AS ca
  LEFT JOIN ic2org ON (ic2org.ic_id = ca.id )
  LEFT JOIN organization AS org ON (org.id = ic2org.org_id)
  LEFT JOIN org2personal AS o2p ON (o2p.personal_id=org.id)
  LEFT JOIN d_contact_personal cp ON (ic2org.personal_id = cp.id)
  LEFT JOIN d_reclama AS c2 ON c2.id = org.reclama_id
  LEFT JOIN d_contractor AS dc ON (dc.id = org.id)
  LEFT JOIN `user` as u ON (u.id = ca.manager_id)
   
  ) as x where 1=1 ";
  $params = array();

  if ($app->request->get('query') != '') {
    $sql .= " and (x.cagent_name like :query
    or x.cagent_phone like :query
    or x.cagent_email like :query
    or x.company_title like :query
    or x.client_interest_srv like :query)";
    $params['query'] = '%' . $app->request->get('query') . '%';
  } else {
    if ($app->request->get('cagent_name')) {
      $sql .= " and  x.cagent_name like :cagent_name";
      $params['cagent_name'] = '%'.$app->request->get('cagent_name').'%';
    }
    if ($app->request->get('cagent_phone')) {
      $sql .= " and x.cagent_phone like :cagent_phone";
      $params['cagent_phone'] = '%'.$app->request->get('cagent_phone').'%';
    }
    if ($app->request->get('cagent_email')) {
      $sql .= " and x.cagent_email like :cagent_email";
      $params['cagent_email'] = '%'.$app->request->get('cagent_email').'%';
    }
    if ($app->request->get('company_title')) {
      $sql .= " and x.company_title like :company_title";
      $params['company_title'] = '%'.$app->request->get('company_title').'%';
    }
    if ($app->request->get('reference_scope_id')) {
      $sql .= " and x.reference_scope_id = :reference_scope_id";
      $params['reference_scope_id'] = $app->request->get('reference_scope_id');
    }
    if ($app->request->get('status')) {
      $sql .= " and x.status = :status";
      $params['status'] = $app->request->get('status');
    }
    if ($app->request->get('contractor_id')) {
      $sql .= " and x.org_id = (select id from organization where is_contractor = :contractor_id)";
      $params['contractor_id'] = $app->request->get('contractor_id');
    }
    if ($app->request->get('is_new_title')) {
      $sql .= " and x.is_new_title = :is_new_title";
      $params['is_new_title'] = $app->request->get('is_new_title');
    }

  }
  //$sql .= " order by x.call_date desc limit $start, $limit";
  $sql .= " order by x.id desc limit $start, $limit";
   
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetchAll(PDO::FETCH_ASSOC);

  $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);
  $response = array(
      'success' => true,
      'total' => $foundRows[0],
      'items' => $data,
  );
  // ответ
  echo json_encode($response);
});

// поиск контактных данных по входящим запросам
$app->get('/search', function () use ($app) {

  // поиск по телефону
  // изменнение логики сохранения и логики поиска
  if ($app->request->get('cagent_phone'))
  {
    // 1. Ищем среди контрагентов
    $sql = "  SELECT
    cp.id AS contact_personal_id,
    ca.id AS root_id,
    org.id AS org_id,
    if ( (select count(ic_id) from ic2org where org_id = org.id) = 0, 1, 2) as is_new,
    ca.id AS icr_id,
   -- ca.id AS id,
  
    ca.status,
    cp.fio AS cagent_name,
    cp.fio AS cagent_old_name,

    org.company_title AS company_title,
     
    IF (org.is_contractor IS NULL, 0, org.is_contractor) AS cagent_id,
    org.is_contractor AS contractor_id,
    dc.manager_id as manager_id,
    u.name as manager_name,
    ca.is_executed,

    (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1) AS cagent_add_phone,
    (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email
     
    FROM d_contact_personal cp

    LEFT JOIN org2personal AS o2p ON (cp.id=o2p.personal_id)
    LEFT JOIN organization AS org ON (org.`id` = o2p.`org_id`)
    LEFT JOIN ic2org ON (ic2org.`ic_id` = o2p.`org_id`)
    LEFT JOIN incoming_call_register AS ca ON (ca.id = ic2org.`ic_id`)
    LEFT JOIN d_contractor AS dc ON (dc.id = org.is_contractor)
    LEFT JOIN `user` as u ON (u.id = dc.manager_id)


    WHERE cp.id = (SELECT personal_id FROM
    d_contacts_ref AS cont_ref
    WHERE
    cont_ref.contact = :phone
    AND contact_type=1 LIMIT 1) GROUP BY org_id ;"; // hack!


     

    $params = array(
        'phone' => $app->request->get('cagent_phone'),
    );

    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);

    echo $data ? json_encode($data) : '';


  }

  //поиск по названию организации
  if ($app->request->get('company_title'))
  {
    $sql =" SELECT
    cp.id AS contact_personal_id,
    ca.id AS root_id,
    org.id AS org_id,
    IF (ca.root_id = 0, 1, 2) AS is_new,
    --if ( (select count(ic_id) from ic2org where org_id = org.id) = 0, 1, 2) as is_new,
    ca.id AS icr_id,
    -- ca.id AS id,
    ca.status,
    cp.fio AS cagent_name,
    cp.fio AS cagent_old_name,

    org.company_title AS company_title,
     
    IF (org.is_contractor IS NULL, 0, org.is_contractor) AS cagent_id,
    org.is_contractor AS contractor_id,
    dc.manager_id,
    u.name as manager_name,
    ca.is_executed,

    (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1 ) AS cagent_phone,
    (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1 ) AS cagent_add_phone,
    (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email
    FROM incoming_call_register AS ca

    LEFT JOIN ic2org ON (ic2org.ic_id = ca.id )
    LEFT JOIN organization AS org ON (org.id = ic2org.org_id)
    LEFT JOIN org2personal AS o2p ON (o2p.personal_id=org.id)
    LEFT JOIN d_contact_personal cp ON (ic2org.personal_id = cp.id)
    LEFT JOIN d_reclama AS c2 ON c2.id = org.reclama_id
    LEFT JOIN d_contractor AS dc ON (dc.id = org.is_contractor)
    LEFT JOIN `user` as u ON (u.id = dc.manager_id)


    WHERE org.company_title = :company_title AND ca.root_id=0";

    $params = array(

        'company_title' => $app->request->get('company_title'),
    );
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    echo $data ? json_encode($data) : '';
  }

  // поиск по имени и названию организации
  if ($app->request->get('cagent_name') && $app->request->get('company_title')) {
    $sql = "select ic.cagent_id, ic.id AS root_id, ic.cagent_name, ic.cagent_phone, ic.cagent_email, ic.company_title, ic.company_type
    from incoming_call_register ic
    where ic.cagent_name = :cagent_name
    and ic.company_title = :company_title
    order by ic.call_date
    limit 1";
    $params = array(
        'cagent_name' => $app->request->get('cagent_name'),
        'company_title' => $app->request->get('company_title'),
    );
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    echo $data ? json_encode($data) : '';
  }

});

// получение данных одного запроса
// с привязкой на контакт.лица
$app->get('/:id', function ($id) use ($app) {

  $sql = "
      SELECT
      ca.id,
      ca.root_id,
      -- if ( (select count(ic_id) from ic2org where org_id = org.id) >=0, 1, 2) as is_new,
      IF (ca.root_id = 0, 1, 2) AS is_new,
      org.id AS icr_id,
      org.id AS org_id,
      cp.fio AS cagent_name,
      cp.fio AS cagent_old_name,
      cp.id AS contact_personal_id,
      ca.is_executed as executed,
      u.name as manager_name,
      ca.manager_id,
      (SELECT u.id FROM `user` u WHERE u.id = ca.user_id) AS author_id,
    
      (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1 ) AS cagent_phone,
      (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1 ) AS cagent_add_phone,
      (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email,
       
    
      org.company_title,
      (select inn from d_contractor where id = org.is_contractor) as company_inn,
      org.company_title AS cagent_title,
      ca.client_interest_srv,
      ca.status,
      c2.title AS reclama_title,
      org.company_fias_title AS  company_fias_title,
      ca.client_interest,
      ca.client_interest_nom,
      org.reclama_interest,
      org.company_type,
      ca.incoming_source_id,
      (SELECT u.name FROM `user` u WHERE u.id = ca.user_id) AS author_name,
      (SELECT drs.title FROM d_reference_scope drs WHERE drs.id = org.reference_scope_id) AS reference_scope,
    
      org.ref_user_id,
      org.ref_cagent_id,
      (select title from d_contractor where id=org.ref_cagent_id) as title,
      (select u.name from user u where u.id = org.ref_user_id) as ref_user,

      org.`reclama_id`,
      org.`is_contractor` AS contractor_id,
      IF (ca.root_id = 0, 'да','нет') AS is_new_title,
      DATE_FORMAT(FROM_UNIXTIME(ca.call_date), '%d.%m.%Y %H:%i') AS call_date,
      DATE_FORMAT(FROM_UNIXTIME(ca.save_date), '%d.%m.%Y %H:%i') AS save_date
       
    
      FROM incoming_call_register AS ca
    
      LEFT JOIN ic2org ON (ic2org.ic_id = ca.id )
      LEFT JOIN organization AS org ON (org.id = ic2org.org_id)
      LEFT JOIN org2personal AS o2p ON (o2p.personal_id=org.id)
      LEFT JOIN d_contact_personal cp ON (ic2org.personal_id = cp.id)
      LEFT JOIN d_reclama AS c2 ON c2.id = org.reclama_id
      LEFT JOIN d_contractor AS dc ON (dc.id = org.id )
      LEFT JOIN `user` as u ON (u.id = ca.manager_id)
    
      WHERE ca.id=:id;";

  $params = array('id' => $id);
  $sth = db::get()->prepare($sql);
  $sth->execute($params);
  $data = $sth->fetch(PDO::FETCH_ASSOC);
  $response = array(
      'success' => true,
      'item' => $data,
  );
  echo json_encode($response);
});

// сохранение нового запроса
$app->post('/0', function () use ($app) {
  $data = json_decode($app->environment['slim.input'], true);

  $errors = array();
   
  // правила валидации
  if (!$data['company_title']) {
    $errors['company_title'] = 'Поле "Наименование" обязательно для заполнения';
  }

  if (!$data['client_interest_srv'] && $data['is_new']==1) {
    $errors['client_interest_srv'] = 'Поле "Услуга" обязательно для заполнения';
  }

  if (!$data['reclama_id'] && $data['is_new'] == 1) {
    $errors['reclama_id'] = 'Поле "Реклама" обязательно для заполнения';
  }


  if ($data['responsible_user_id']== 0) {
    $errors['responsible_user'] = 'Поле "Ответственный" обязательно для заполнения';
  }


  // валидация
  if (empty($errors)) {

    // сохранение входящего запроса
    $ic_sql = "insert into incoming_call_register
    (
    root_id, call_date, save_date, user_id,incoming_source_id,
    client_interest, client_interest_srv, client_interest_nom,
    status, ref_cagent_id, ref_user_id, is_executed, manager_id
    )
    values (:root_id, :call_date, :save_date, :user_id, :incoming_source_id,
    :client_interest, :client_interest_srv, :client_interest_nom,
    :status, :ref_cagent_id, :ref_user_id, :is_executed, :manager_id)";

    $params = array(
        'incoming_source_id' => $data['incoming_source_id'],
        'call_date' => strtotime($data['call_date']),
        'root_id' => $data['root_id'],
        'save_date' => time(),
        'user_id' => User::get()->id,
        'client_interest' => $data['client_interest'],
        'client_interest_srv' => $data['client_interest_srv'],
        'client_interest_nom' => $data['client_interest_nom'],
        'status' => 1,
        'ref_cagent_id' => $data['ref_cagent_id'],
        'ref_user_id' => $data['ref_user_id'],
        'is_executed' => $data['executed'],
        'manager_id' => $data['responsible_user_id']
    );
    db::get()->prepare($ic_sql)->execute($params);
    $ic_id = db::get()->lastInsertId();
    $data['uniq_form_id'] = $ic_id;
    $data['id'] = $ic_id;


    if ($data['org_id'] == 0)
    {
      // $ic_id = db::get()->lastInsertId();
      $org_sql = "insert into organization
      (
        company_title,
        company_type,
        company_fias_id,
        company_fias_title,
        reference_scope_id,
        reference_scope_text,
        reclama_id,
        reclama_interest,
        ref_cagent_id,
        ref_user_id
      )
      values
      (
        :company_title,
        :company_type,
        :company_fias_id,
        :company_fias_title,
        :reference_scope_id,
        :reference_scope_text,
        :reclama_id,
        :reclama_interest,
        :ref_cagent_id,
        :ref_user_id)
      ";
      $params = array(

          'company_title' => $data['company_title'],
          'company_type' => $data['company_type'],
          'company_fias_id' => $data['company_fias_id'],
          'company_fias_title' => $data['company_fias_title'],
          'reference_scope_id' => $data['reference_scope_id'],
          'reference_scope_text' => $data['reference_scope_text'],
          'reclama_id' => $data['reclama_id'],
          'reclama_interest' => $data['reclama_interest'],
          'ref_cagent_id' => $data['ref_cagent_id'],
          'ref_user_id' => $data['ref_user_id'],
      );
      db::get()->prepare($org_sql)->execute($params);
      $data['org_id'] = db::get()->lastInsertId();
    }

    if ($data['contact_personal_id'] == 0)
    {
      $cp_sql = "insert into d_contact_personal
      (fias_id, fio, status)
      values (:fias_id, :fio, :status)";
      $params = array(
          'status' => 1,
          'fias_id' => 0,
          'fio' => $data['cagent_name'],
      );
      db::get()->prepare($cp_sql)->execute($params);
      $contact_personal_id = db::get()->lastInsertId();

      // связь контакное лицо к организации
      $sql = "insert into org2personal (org_id, personal_id) values (:org_id, :personal_id)";
      $params = array(
          'org_id' => $data['org_id'],
          'personal_id' => $contact_personal_id,
      );
      db::get()->prepare($sql)->execute($params);

      //
      // валидация, если есть телефон
      if ($data['cagent_phone'])
      {
        // добавление телефона
        $p_sql = "insert into d_contacts_ref (personal_id, contact_type, contact, add_contact,  is_main, is_active)
        values (:personal_id, 1, :contact, :add_contact, 1, 1)";
        $params = array(
            'personal_id' => $contact_personal_id,
            'contact' => $data['cagent_phone'],
            'add_contact' => $data['cagent_add_phone'],
        );
        db::get()->prepare($p_sql)->execute($params);
        $phone_contact_id = db::get()->lastInsertId();

        $e_sql = "insert into contacts2personal (contact_id, personal_id)
        values (:contact_id, :personal_id)";
        $params = array(
            'contact_id' => $phone_contact_id,
            'personal_id' => $contact_personal_id,
        );
        db::get()->prepare($e_sql)->execute($params);
      }

      if ($data['cagent_email'])
      {
        $sql = "insert into d_contacts_ref (personal_id, contact_type, contact, is_main, is_active)
        values (:personal_id, 2, :contact, 1, 1)";
        $params = array(
            'personal_id' => $contact_personal_id,
            'contact' => $data['cagent_email'],
        );
        db::get()->prepare($sql)->execute($params);
        $email_contact_id = db::get()->lastInsertId();

        // добавление связи, что это именоо телефон
        $sql = "insert into contacts2personal (contact_id, personal_id)
        values (:contact_id, :personal_id)";
        $params = array(
            'contact_id' => $email_contact_id,
            'personal_id' => $contact_personal_id,
        );
        db::get()->prepare($sql)->execute($params);
      }
    }
    else{
      $contact_personal_id = $data['contact_personal_id'];
    }


    $sql = "insert into ic2org (ic_id, org_id, personal_id, is_main) values (:ic_id, :org_id, :personal_id, :is_main)";
    $params = array(
        'ic_id' => $ic_id,
        'org_id' => $data['org_id'],
        'personal_id' => $contact_personal_id,
        'is_main' => 0,
    );
    db::get()->prepare($sql)->execute($params);

    // ответ
    $response = array(
        'success' => true,
        'items' => $data,
    );
  }
  else {
    $response = array(
        'success' => false,
        'errors' => $errors,
    );
  }

  // ответ
  echo json_encode($response);

});

// редактирование запроса
$app->put('/:id', function ($id) use ($app) {
  $data = json_decode($app->environment['slim.input'], true);

  $errors = array();

  // правила валидации
  if (!$data['company_title']) {
    $errors['company_title'] = 'Поле "Наименование" обязательно для заполнения';
  }
  if (!$data['client_interest_srv'] && $data['is_new']==1) {
    $errors['client_interest_srv'] = 'Поле "Услуга" обязательно для заполнения';
  }
  if (!$data['reclama_id'] && $data['is_new']==1) {
    $errors['reclama_id'] = 'Поле "Реклама" обязательно для заполнения';
  }

  // валидация
  if (empty($errors))
  {
    if ($data['executed'] == 1)
    {
      $sql = "update incoming_call_register set
      is_executed = :executed
      where id = :id";
      $params = array(
          'id' => $data['id'],
          'executed' => $data['executed']
      );
      db::get()->prepare($sql)->execute($params);
    }
    else
    {
      // обновление входящего запроса
      $ic_sql = "update incoming_call_register set
      save_date = :save_date,
      incoming_source_id = :incoming_source_id,
      client_interest = :client_interest,
      client_interest_srv = :client_interest_srv,
      client_interest_nom = :client_interest_nom,
      ref_cagent_id = :ref_cagent_id,
      ref_user_id = :ref_user_id,
      manager_id = :manager_id
      where id = :id
      ";

      $params = array(
          'id'=>$data['id'],
          'incoming_source_id' => $data['incoming_source_id'],
          'save_date' => time(),
          'client_interest' => $data['client_interest'],
          'client_interest_srv' => $data['client_interest_srv'],
          'client_interest_nom' => $data['client_interest_nom'],
          'ref_cagent_id' => $data['ref_cagent_id'],
          'ref_user_id' => $data['ref_user_id'],
          'manager_id' => $data['responsible_user_id']
      );
      db::get()->prepare($ic_sql)->execute($params);
       
      // изменить имя конт.лица + заголовки
      if ($data['root_id'] == 0)
      {
         
        $org_sql = "update  organization set
        company_type = :company_type,
        company_fias_id = :company_fias_id,
        company_fias_title = :company_fias_title,
        reference_scope_id = :reference_scope_id,
        reference_scope_text = :reference_scope_text,
        reclama_id = :reclama_id,
        reclama_interest = :reclama_interest,
        ref_cagent_id = :ref_cagent_id,
        ref_user_id = :ref_user_id
        where id = :org_id";

        $params = array(
            'company_type' => $data['company_type'],
            'company_fias_id' => $data['company_fias_id'],
            'company_fias_title' => $data['company_fias_title'],
            'reference_scope_id' => $data['reference_scope_id'],
            'reference_scope_text' => $data['reference_scope'],
            'reclama_id' => $data['reclama_id'],
            'reclama_interest' => $data['reclama_interest'],
            'ref_cagent_id' => $data['ref_cagent_id'],
            'ref_user_id' => $data['ref_user_id'],
            'org_id' => $data['org_id']
        );
        db::get()->prepare($org_sql)->execute($params);
      }

    }

    // ответ
    $response = array(
        'success' => true,
        'items' => $data,
    );
  } else {
    $response = array(
        'success' => false,
        'errors' => $errors,
    );
  }
  // ответ
  echo json_encode($response);

});