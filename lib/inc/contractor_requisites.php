<?php

// получение данных банка по бику
$app->get('/bank/:bic', function ($bic) use ($app) {

    // Обновлять файл отсюда - http://cbrates.rbc.ru/bnk/bnk.zip

    if (file_exists('./cbr/bnkseek.txt')) {
        $bnkseek = file_get_contents('./cbr/bnkseek.txt');
        foreach(explode("\n", $bnkseek) as $bnk) {
            $row = explode("\t", trim($bnk));
            if ($row[5] == $bic) {
                $data = array(
                    'bic' => $row[5],
                    'ks' => $row[6],
                    'full_name' => iconv('cp1251', 'utf-8', trim($row[3])). (trim($row[1]) ? ' Г. '.iconv('cp1251', 'utf-8', trim($row[1])) : null),
                );
                break;
            }
        }
    }
    elseif ($db = dbase_open('./cbr/BNKSEEK.DBF', 0)) {
        $record_numbers = dbase_numrecords($db);
        for ($i = 1; $i <= $record_numbers; $i++) {
            $row = dbase_get_record_with_names($db, $i);
            if ($row['NEWNUM'] == $bic) {
                $data = array(
                    'bic' => $row['NEWNUM'],
                    'ks' => $row['KSNP'],
                    'full_name' => iconv('cp866', 'utf-8', trim($row['NAMEP'])). (trim($row['NNP']) ? ' Г. '.iconv('cp866', 'utf-8', trim($row['NNP'])) : null),
                );
                break;
            }
        }
        dbase_close($db);
    }

    echo !empty($data) ? json_encode($data) : null;

});


// список реквизитов
$app->get('/', function () use ($app) {
    $sql = "select SQL_CALC_FOUND_ROWS cr.*, c.title as contractor
            from d_contractor_requisites cr, d_contractor c
            where c.id = cr.contractor_id";
    $params = array();
    if ($app->request->get('status')) {
        $sql .= " and cr.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and cr.status in (1,2)";
    }
    if ($app->request->get('contractor_id')) {
        $sql .= " and cr.contractor_id = :contractor_id";
        $params['contractor_id'] = $app->request->get('contractor_id');
    }
    if ($app->request->get('query')) {
        $sql .= " and ( cr.title like :query or c.title like :query)";
        $params['query'] = '%'.$app->request->get('query').'%';
    }
    $start = (int)$app->request->get('start');
    $limit = (int)$app->request->get('limit');
    $sql .= " limit $start, $limit";
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);
    $response = array(
        'success' => true,
        'total' => $foundRows[0],
        'items' => $data,
    );
    echo json_encode($response);
});


// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $sql = "select cr.*,
              (select c.title from d_contractor c where c.id = cr.contractor_id) as contractor
            from d_contractor_requisites cr
            where cr.id = :id";
    $params = array('id' => $id);
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'item' => $data,
    );
    echo json_encode($response);
});

// изменение
$app->put('/:id', function ($id) use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $errors = array();

    // правила валидации
    if (!$data['title']) {
        $errors['title'] = 'Поле "Наименование" обязательно для заполнения';
    }

    // валидация
    if (empty($errors)) {
        $sql = "update d_contractor_requisites set
                  contractor_id = :contractor_id,
                  title = :title,
                  number = :number,
                  bank_title = :bank_title,
                  bank_bik = :bank_bik,
                  bank_ks = :bank_ks,
                  currency_id = :currency_id,
                  bank2_title = :bank2_title,
                  bank2_bik = :bank2_bik,
                  bank2_ks = :bank2_ks,
                  payer = :payer,
                  type = :type,
                  status = :status
                where id = :id";
        $params = array(
            'contractor_id' => $data['contractor_id'],
            'title' => $data['title'],
            'number' => $data['number'],
            'bank_title' => $data['bank_title'],
            'bank_bik' => $data['bank_bik'],
            'bank_ks' => $data['bank_ks'],
            'currency_id' => $data['currency_id'],
            'bank2_title' => $data['bank2_title'],
            'bank2_bik' => $data['bank2_bik'],
            'bank2_ks' => $data['bank2_ks'],
            'payer' => $data['payer'],
            'type' => $data['type'],
            'status' => $data['status'],
            'id' => $id,
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();

        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $errors,
        );
    }
    // ответ
    echo json_encode($response);
});

// сохранение
$app->post('/0', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $errors = array();

    // правила валидации
    if (!$data['title']) {
        $errors['title'] = 'Поле "Наименование" обязательно для заполнения';
    }

    // валидация
    if (empty($errors)) {
        $sql = "insert into d_contractor_requisites set
                  contractor_id = :contractor_id,
                  title = :title,
                  number = :number,
                  bank_title = :bank_title,
                  bank_bik = :bank_bik,
                  bank_ks = :bank_ks,
                  currency_id = :currency_id,
                  bank2_title = :bank2_title,
                  bank2_bik = :bank2_bik,
                  bank2_ks = :bank2_ks,
                  payer = :payer,
                  type = :type,
                  status = :status";
        $params = array(
            'contractor_id' => $data['contractor_id'],
            'title' => $data['title'],
            'number' => $data['number'],
            'bank_title' => $data['bank_title'],
            'bank_bik' => $data['bank_bik'],
            'bank_ks' => $data['bank_ks'],
            'currency_id' => $data['currency_id'],
            'bank2_title' => $data['bank2_title'],
            'bank2_bik' => $data['bank2_bik'],
            'bank2_ks' => $data['bank2_ks'],
            'payer' => $data['payer'],
            'type' => $data['type'],
            'status' => $data['status'],
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();

        // ответ
        $response = array(
            'success' => true,
            'items' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $errors,
        );
    }
    // ответ
    echo json_encode($response);
});