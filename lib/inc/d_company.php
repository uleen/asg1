<?php

// получение списка записей
$app->get('/', function () use ($app) {
    $sql = "select d.*
                    from d_company d
                    where 1 = 1";
    $params = array();
    if ($app->request->get('status')) {
        $sql .= " and d.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and d.status in (1,2)";
    }
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});


// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $company = Company::find_by_pk($id);
    $response = array(
        'success' => true,
        'item' => $company ? $company->getData() : null,
    );
    echo json_encode($response);
})->conditions(array('id' => '\d+'));


// получение данных ответственного лица
$app->get('/person', function () use ($app) {
    $company = Company::find_by_pk($_GET['company_id']);
    if ($company) {
        $response = array(
            'success' => true,
            'item' => $company->getPerson($_GET['type'])->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Компания не найдена',
        );
    }
    echo json_encode($response);
});


// сохранение данных ответственного лица
$app->put('/person(/:id)', function ($id) use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $company = Company::find_by_pk($data['company_id']);
    // преобразование дат
    $dataPerson = $data;
    $dataPerson['date_from'] = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_from']);
    $dataPerson['date_to'] = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_to']);
    if ($company) {
        $company->setPerson($data['type'], $dataPerson);
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Компания не найдена',
        );
    }
    echo json_encode($response);
});



// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $company = new Company($data);
    $company->id = $id;

    // валидация
    if ($company->validate()) {
        $company->save();
        $response = array(
            'success' => true,
            'item' => $company->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $company->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');



// получение данных счета
$app->get('/requisite/:id', function ($id) use ($app) {
    $companyRequisite = Company_Requisite::find_by_pk($id);
    if ($companyRequisite) {
        $response = array(
            'success' => true,
            'item' => $companyRequisite->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Данные не найдены',
        );
    }
    echo json_encode($response);
})->conditions(array('id' => '\d+'));


// сохранение реквизтов компании
$app->map('/requisite(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $companyRequisite = new Company_Requisite($data);
    $companyRequisite->id = $id;

    // валидация
    if ($companyRequisite->validate()) {
        $companyRequisite->save();
        $response = array(
            'success' => true,
            'item' => $companyRequisite->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $companyRequisite->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');


// получение реквизитов
$app->get('/requisites', function () use ($app) {
    $company = Company::find_by_pk($_GET['company_id']);
    if ($company) {
        $response = array(
            'success' => true,
            'items' => $company->getRequisites(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Компания не найдена',
        );
    }
    echo json_encode($response);
});