<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('nomenclature_group_id')) {
        $ng = Nomenclature_Group::find_by_pk($app->request->get('nomenclature_group_id'));
        if ($ng) {
            $conditions[] = "dn.nomenclature_group_id in (" . implode(', ', $ng->getChildrensId()) . ")";
        }
    }
    if ($app->request->get('query')) {
        $conditions[] = "(dn.title like :query or dn.article like :query)";
        $params['query'] = '%' . $app->request->get('query') . '%';
    }
    if ($app->request->get('title')) {
        $conditions[] = "dn.title like :title";
        $params['title'] = '%' . $app->request->get('title') . '%';
    }
    if ($app->request->get('article')) {
        $conditions[] = "dn.article like :article";
        $params['article'] = '%' . $app->request->get('article') . '%';
    }
    if ($app->request->get('type')) {
        $conditions[] = "dn.type = type";
        $params['type'] = $app->request->get('type');
    }
    if ($app->request->get('manager_id')) {
        $conditions[] = "dn.manager_id = :manager_id";
        $params['manager_id'] = $app->request->get('manager_id');
    }
    if ($app->request->get('supplier_id')) {
        $conditions[] = "exists (select 1
                            from d_nomenclature_supplier ns, d_contractor_delivery cd
                            where ns.contractor_delivery_id = cd.id
                                and cd.contractor_id = :supplier_id)";
        $params['supplier_id'] = $app->request->get('supplier_id');
    }

    if ($app->request->get('status')) {
        $conditions[] = "dn.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "dn.status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Nomenclature::find($conditions, $params) as $nomenclature) {
        $items[] = $nomenclature->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// получение списка характеристик
$app->get('/characteristic', function () use ($app) {
    $n = Nomenclature::find_by_pk($_GET['nomenclature_id']);
    if ($n) {
        $response = array(
            'success' => true,
            'items' => $n->getCharacteristic(),
        );
        echo json_encode($response);
    }
});
// обновление значения характеристики
$app->put('/characteristic', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];
    $nomenclature = Nomenclature::find_by_pk($item['nomenclature_id']);
    if ($nomenclature) {
        $nomenclature->setValueCharacteristic($item['characteristic_id'], $item['value']);
        $response = array(
            'success' => true,
            'items' => $data['items'],
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Номенклатура не найдена',
        );
    }
    echo json_encode($response);
});


// добавление (обновление) поставщиков
$app->map('/supplier', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];
    $nomenclature = Nomenclature::find_by_pk($item['nomenclature_id']);
    if ($nomenclature) {
        $nomenclature->setSupplier($item['contractor_delivery_id'], $item['article'], $item['price'], $item['currency_id'], $item['unit_id'], $item['in_stock'], $item['note']);
        $response = array(
            'success' => true,
            'items' => $data['items'],
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Номенклатура не найдена',
        );
    }
    echo json_encode($response);
})->via('POST', 'PUT');

// получение списка поставщиков и их последних цен
$app->get('/supplier', function () use ($app) {

    // в разрезе номенклатуры
    if ($_GET['nomenclature_id']) {
        $nomenclature = Nomenclature::find_by_pk($_GET['nomenclature_id']);
        if ($nomenclature) {
            $response = array(
                'success' => true,
                'items' => $nomenclature->getSuppliers(),
            );
            echo json_encode($response);
        }
    }

    // в разрезе поставщика
    if ($_GET['contractor_id']) {
        $contractor = Contractor::find_by_pk($_GET['contractor_id']);
        if ($contractor) {
            $response = array(
                'success' => true,
                'items' => $contractor->getNomenclature(),
            );
            echo json_encode($response);
        }
    }

    // каталог менеджера
    if (!$_GET['nomenclature_id'] && !$_GET['contractor_id']) {
        $response = array(
            'success' => true,
            'items' => Nomenclature::catalog($_GET['nomenclature_group_id'], $_GET['query'], $_GET['start'], $_GET['limit']),
            'total' => AR::calc_found_rows(),
        );
        echo json_encode($response);
    }

});


// получение истории цен поставщика
$app->get('/supplier/history', function () use ($app) {
    $nomenclature = Nomenclature::find_by_pk($_GET['nomenclature_id']);
    if ($nomenclature) {
        $response = array(
            'success' => true,
            'items' => $nomenclature->getHistory($_GET['contractor_delivery_id']),
        );
        echo json_encode($response);
    }
});


// получение истории отпускных цен
$app->get('/price/history', function () use ($app) {
    $nomenclature = Nomenclature::find_by_pk($_GET['nomenclature_id']);
    if ($nomenclature) {
        $response = array(
            'success' => true,
            'items' => $nomenclature->getHistoryPrice($_GET['type_price_id']),
        );
        echo json_encode($response);
    }
});


// получение списка цен
$app->get('/price', function () use ($app) {
    $nomenclature = Nomenclature::find_by_pk($_GET['nomenclature_id']);
    if ($nomenclature) {
        $response = array(
            'success' => true,
            'items' => $nomenclature->getPrice(),
        );
        echo json_encode($response);
    }
});

// обновление цен
$app->put('/price', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];
    $nomenclature = Nomenclature::find_by_pk($item['nomenclature_id']);
    if ($nomenclature) {
        $nomenclature->setPrice($item['type_price_id'], $item['price']);
        $response = array(
            'success' => true,
            'items' => $data['items'],
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Номенклатура не найдена',
        );
    }
    echo json_encode($response);
});

// получение записи
$app->get('/:id', function ($id) use ($app) {
    $nomenclature = Nomenclature::find_by_pk($id);
    $response = array(
        'success' => true,
        'item' => $nomenclature ? $nomenclature->getData() : null,
    );
    echo json_encode($response);
});


// добавление записи
$app->post('/', function () use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $data['create_date'] = time();
    $nomenclature = new Nomenclature($data);

    // валидация
    if ($nomenclature->validate()) {
        $nomenclature->save();
        $response = array(
            'success' => true,
            'item' => $nomenclature->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $nomenclature->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
});


// редактирование записи
$app->put('/:id', function ($id) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $data['id'] = $id;
    $nomenclature = new Nomenclature($data);

    // валидация
    if ($nomenclature->validate()) {
        $nomenclature->save();
        $response = array(
            'success' => true,
            'item' => $nomenclature->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $nomenclature->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
});