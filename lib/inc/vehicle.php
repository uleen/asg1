<?php
$app->get('/', function () use ($app) {

	$conditions = $params = array();

	if ($app->request->get('vehicle_status_id')) {
		$conditions[] = "vehicle_status_id = :vehicle_status_id";
		$params['vehicle_status_id'] = $app->request->get('vehicle_status_id');
	} else {
		$conditions[] = "vehicle_status_id in (1,2)";
	}

	// поиск
	$items = array();
	foreach (Vehicle::find($conditions, $params) as $vehicle) {
		$items[] = $vehicle->getData();
	}

	// ответ
	$response = array(
		'success' => true,
		'total' => count($items),
		'items' => $items,
	);
	echo json_encode($response);
});
$app->get('/:id', function ($id) use ($app) {
	$vehicle = Vehicle::find_by_pk($id);
	$response = array(
		'success' => true,
		'item' => $vehicle ? $vehicle->getData() : null,
	);
	echo json_encode($response);
})->conditions(array('id' => '\d+'));

// добавление, изменение записи
$app->map('(/:id)', function($id = null) use ($app) {
	// входные данные
	$data = json_decode($app->environment['slim.input'], true);
	$vehicle = new Vehicle($data);
	$vehicle->id = $id;

	// валидация
	if ($vehicle->validate()) {
		$vehicle->save();
		$response = array(
			'success' => true,
			'item' => $vehicle->getData(),
		);
	} else {
		$response = array(
			'success' => false,
			'errors' => $vehicle->getErrors(),
		);
	}
	// ответ
	echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');

