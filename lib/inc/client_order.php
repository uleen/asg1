<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('client_order_status_id')) {
        $conditions[] = "client_order_status_id = :client_order_status_id";
        $params['client_order_status_id'] = $app->request->get('client_order_status_id');
    }
    if ($app->request->get('contractor_id')) {
        $conditions[] = "c.id = :contractor_id";
        $params['contractor_id'] = $app->request->get('contractor_id');
    }

    // поиск
    $items = array();
    foreach (Client_Order::find($conditions, $params, $_GET['start'], $_GET['limit']) as $clientOrder) {
        $items[] = $clientOrder->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});


// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $clientOrder = Client_Order::find_by_pk($id);
    $response = array(
        'success' => true,
        'item' => $clientOrder ? $clientOrder->getData() : null,
    );
    echo json_encode($response);
})->conditions(array('id' => '\d+'));


// получение позиций заказа
$app->get('/nomenclature', function () use ($app) {
    $clientOrder = Client_Order::find_by_pk($_GET['client_order_id']);
    if ($clientOrder) {
        $response = array(
            'success' => true,
            'items' => $clientOrder->getPosition(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Клиентский заказ не найден',
        );
    }
    echo json_encode($response);
});


// получение истории изменения статусов
$app->get('/status_history', function () use ($app) {
    $clientOrder = Client_Order::find_by_pk($_GET['client_order_id']);
    if ($clientOrder) {
        $response = array(
            'success' => true,
            'items' => $clientOrder->getStatusHistory(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Клиентский заказ не найден',
        );
    }
    echo json_encode($response);
});


// добавление, изменение записей
$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    unset($data['create_date']);
    unset($data['create_user_id']);
    $clientOrder = new Client_Order($data);
    $clientOrder->id = $id;

    // преобразование дат
    $clientOrder->date_from = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_from']);
    $clientOrder->date_to = preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_to']);
    if (!$clientOrder->id) {
        $clientOrder->create_date = date('Y-m-d H:i:s');
        $clientOrder->create_user_id = User::get()->id;
    }

    // валидация
    if ($clientOrder->validate()) {
        $clientOrder->save();
        $clientOrder->changeStatus($data['client_order_status_id'], $data['client_order_status_note'], User::get()->id);
        $clientOrder->reload();
        $response = array(
            'success' => true,
            'item' => $clientOrder->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $clientOrder->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');


// сохранение позиций заказа
$app->map('/nomenclature(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];

    $clientOrder = Client_Order::find_by_pk($item['client_order_id']);
    if ($clientOrder) {
        $clientOrder->setPosition($item['nomenclature_id'], $item['contractor_delivery_id'], $item['unit_id'], $item['volume'], $item['price']);
        $response = array(
            'success' => true,
            'items' => $data['items'],
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Клиентский заказ не найден',
        );
    }
    echo json_encode($response);
})->via('POST', 'PUT');


// удаление позиций заказа
$app->delete('/nomenclature(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $item = $data['items'];
    $clientOrder = Client_Order::find_by_pk($item['client_order_id']);
    if ($clientOrder) {
        $clientOrder->delPosition($item['nomenclature_id']);
        $response = array(
            'success' => true,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => 'Клиентский заказ не найден',
        );
    }
    echo json_encode($response);
});