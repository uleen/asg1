<?php

// добавление записи
$app->post('/', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);

    $sql = "insert into d_characteristic set
                nomenclature_group_id = :nomenclature_group_id,
                title = :title,
                title_short = :title_short,
                unit = :unit,
                sort = :sort";
    $params = array(
        'nomenclature_group_id' => $data['nomenclature_group_id'],
        'title' => $data['title'],
        'title_short' => $data['title_short'],
        'unit' => $data['unit'],
        'sort' => $data['sort'],
    );
    db::get()->prepare($sql)->execute($params);
});


// редактирование записи
$app->put('/:id', function ($id) use ($app) {
    $data = json_decode($app->environment['slim.input'], true);

    $sql = "update d_characteristic set
                nomenclature_group_id = :nomenclature_group_id,
                title = :title,
                title_short = :title_short,
                unit = :unit,
                sort = :sort
            where id = :id";
    $params = array(
        'nomenclature_group_id' => $data['nomenclature_group_id'],
        'title' => $data['title'],
        'title_short' => $data['title_short'],
        'unit' => $data['unit'],
        'sort' => $data['sort'],
        'id' => $id,
    );
    db::get()->prepare($sql)->execute($params);
});


// удаление записи
$app->delete('/:id', function ($id) use ($app) {
    $sql = "delete
            from d_characteristic
            where id = :id";
    $params = array('id' => $id);
    db::get()->prepare($sql)->execute($params);
});