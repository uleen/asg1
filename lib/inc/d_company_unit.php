<?php

// получение списка записей
$app->get('/', function () use ($app) {
    $sql = "select * from company_unit";
                   
    $params = array();
    if ($app->request->get('company_id')){
      $sql .= ' where cu.company_id = :company_id';
      $params['company_id'] = $app->request->get('company_id');
    }
    //
    
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});
