<?php

require_once('./core/fias.helper.php');

// город
$app->get('/city', function () use ($app) {
    $query = $app->request->get('query');
    $data = FiasRequest::getCity($query);
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});

// улицы
$app->get('/street', function () use ($app) {
    $query = $app->request->get('query');
    $city_id = $app->request->get('city_id');
    $data = FiasRequest::getStreet($query, $city_id);
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});

// дома
$app->get('/house', function () use ($app) {
    $query = $app->request->get('query');
    $street_id = $app->request->get('street_id');
    $data = array();
    foreach (FiasRequest::getBuilding($query, $street_id) as $b) {
        $b['fias_id'] = $b['id'];
        unset($b['id']);
        $data[] = $b;
    }
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});