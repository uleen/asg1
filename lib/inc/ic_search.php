<?php

$app->get('/', function () use ($app) {

    $query = $app->request->get('query');
    $start = $app->request->get('start');
    $limit = $app->request->get('limit');

    $sql = " SELECT SQL_CALC_FOUND_ROWS
              ca.id,
              org.id AS cagent_id,
              ca.id AS root_id,
              ca.root_id AS xroot_id,
              org.id AS org_id,
              if ( (select count(ic_id) from ic2org where org_id = org.id) = 0, 1, 2) as is_new,
              ca.id AS icr_id,
              cp.fio AS cagent_name,
              cp.fio AS cagent_old_name,
              cp.fio AS cagent_personal,
              cp.id AS contact_personal_id,

              (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1  ) AS cagent_phone,
              (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1 limit 1  ) AS cagent_add_phone,
              (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 limit 1) AS cagent_email,

              org.company_title,
              org.company_title AS cagent_title,
              ca.client_interest_srv,
              ca.status,
              c2.title AS reclama_title,
              dc.inn AS cagent_inn,


              IF (org.is_contractor IS NULL, 0, org.is_contractor) AS contractor_id,
              IF (org.is_contractor IS NULL, 0, org.is_contractor) AS is_cagent,

              IF (ca.root_id = 0, 'да','нет') AS is_new_title,
              dc.main_personal_id,
              dc.manager_id,
              u.name as manager_name,


              DATE_FORMAT(FROM_UNIXTIME(ca.call_date), '%d.%m.%Y %H:%i') AS call_date

              FROM incoming_call_register AS ca

              LEFT JOIN ic2org ON (ic2org.ic_id = ca.id )
              LEFT JOIN organization AS org ON (org.id = ic2org.org_id)
              LEFT JOIN org2personal AS o2p ON (o2p.personal_id=org.id)
              LEFT JOIN d_contact_personal cp ON (ic2org.personal_id = cp.id)
              LEFT JOIN d_reclama AS c2 ON c2.id = org.reclama_id
              LEFT JOIN d_contractor AS dc ON (dc.id = org.is_contractor)
              LEFT JOIN `user` as u ON (u.id = dc.manager_id)

              WHERE

        org.company_title LIKE '%{$query}%'
        or cp.fio like '%{$query}%'
        or dc.inn like '%{$query}%'

         GROUP BY org.id order by org.company_title limit {$start}, {$limit}";

    $params = array();
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);

    foreach ($data as &$item) {

        if ($item['main_personal_id'] != 0 && ($item['main_personal_id'] != $item['contact_personal_id']) && $item['is_cagent'] != 0) {
            $id = $item['main_personal_id'];
            $pers_sql = "
          SELECT
	        cp.fio,
              (SELECT contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1  AND cp.id = {$id} LIMIT 1) AS cagent_phone,
              (SELECT add_contact FROM d_contacts_ref AS cont_ref WHERE cont_ref.contact_type = 1 AND cont_ref.personal_id=cp.id AND cont_ref.is_main = 1  AND cp.id = {$id} LIMIT 1) AS cagent_add_phone,
              (SELECT contact FROM d_contacts_ref AS cont_ref2 WHERE cont_ref2.contact_type = 2 AND cont_ref2.personal_id=cp.id AND cont_ref2.is_main = 1 AND cp.id = {$id} LIMIT 1) AS cagent_email
             FROM d_contact_personal cp
             WHERE cp.id={$id};
        ";
            $pers_params = array();
            $pers_sth = db::get()->prepare($pers_sql);
            $pers_sth->execute($pers_params);
            $pers = $pers_sth->fetchAll(PDO::FETCH_ASSOC);
            //print_r($pers);

            $item['cagent_name'] = $item['cagent_old_name'] = $item['cagent_personal'] = $pers[0]['fio'];
            $item['cagent_phone'] = $pers[0]['cagent_phone'];
            $item['cagent_add_phone'] = $pers[0]['cagent_add_phone'];
            $item['cagent_email'] = $pers[0]['cagent_email'];

        }

    }
    $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);

    $response = array(
        'success' => true,
        'total' => $foundRows[0],
        'items' => $data,
    );
    // ответ
    echo json_encode($response);
});