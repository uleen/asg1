<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $items = array();
    foreach (Sheet_Status::find() as $sheetType) {
    $items[] = $sheetType->getData();
    }

// ответ
$response = array(
'success' => true,
'total' => count($items),
'items' => $items,
);
echo json_encode($response);
});