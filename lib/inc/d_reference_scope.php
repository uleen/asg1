<?php

// получение списка записей
$app->get('/', function () use ($app) {
    $sql = "select drs.*
                    from d_reference_scope drs
                    where 1 = 1";
    $params = array();
    if ($app->request->get('status')) {
        $sql .= " and drs.status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $sql .= " and drs.status in (1,2)";
    }
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    $response = array(
        'success' => true,
        'total' => count($data),
        'items' => $data,
    );
    echo json_encode($response);
});

// получение одной записи
$app->get('/:id', function ($id) use ($app) {
    $sql = "select rs.* from d_reference_scope rs where id = :id";
    $params = array('id' => $id);
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);

    // получение списка ОКВЭДов
    $sql = "select c.* from c_okved c where exists (select 1 from d_reference_scope_okved d where d.okved_id = c.id and d.reference_scope_id = :id)";
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data['okved'] = $sth->fetchAll(PDO::FETCH_ASSOC);

    $response = array(
        'success' => true,
        'item' => $data,
    );
    echo json_encode($response);
});

// добавление записи
$app->post('/0', function () use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);

    // валидация
    if ($data['title']) {
        $sql = "insert into d_reference_scope (status, title) values (:status, :title)";
        $params = array(
            'title' => $data['title'],
            'status' => 1,
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();
        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );

    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});

// редактирование записи
$app->put('/:id', function ($id) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);

    // валидация
    if ($data['title']) {
        $sql = "update d_reference_scope set title = :title, status = :status where id = :id";
        $params = array(
            'id' => $id,
            'title' => $data['title'],
            'status' => $data['status'],
        );
        db::get()->prepare($sql)->execute($params);

        // удаление всех связей
        $sql = "delete from d_reference_scope_okved where reference_scope_id = :id";
        db::get()->prepare($sql)->execute(array('id' => $id));

        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => array(
                'title' => 'не указан заголовок',
            ),
        );
    }
    // ответ
    echo json_encode($response);
});