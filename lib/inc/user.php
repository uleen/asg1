<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('company_id')) {
        $conditions[] = "u.company_id = :company_id";
        $params['company_id'] = $app->request->get('company_id');
    }
    if ($app->request->get('can_auth')) {
        $conditions[] = "u.can_auth = :can_auth";
        $params['can_auth'] = $app->request->get('can_auth');
    }
    if ($app->request->get('query')) {
        $conditions[] = "u.name like :query";
        $params['query'] = '%' . $app->request->get('query') . '%';
    }
    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (User::find($conditions, $params, $app->request->get('start'), $app->request->get('limit')) as $user) {
        $items[] = $user->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => User::calc_found_rows(),
        'items' => $items,
    );
    echo json_encode($response);
});


// получение данных одной записи
$app->get('/:id', function ($id) use ($app) {
    $user = User::find_by_pk($id);
    $response = array(
        'success' => true,
        'item' => $user ? $user->getData() : null,
    );
    echo json_encode($response);
})->conditions(array('id' => '\d+'));


// добавление, изменение записи
$app->map('(/:id)', function ($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $user = new User($data);
    $user->id = $id;

    // преобразование дат
    $user->date_birth = array_key_exists('date_birth', $data) ? preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_birth']) : null;
    $user->date_hire = array_key_exists('date_hire', $data) ? preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_hire']) : null;
    $user->date_dismissal = array_key_exists('date_dismissal', $data) ? preg_replace('/^(\d{2})\.(\d{2})\.(\d{4})$/', '\\3-\\2-\\1', $data['date_dismissal']) : null;

    // пароль
    $user->password = array_key_exists('password', $data) ? md5($data['password']) : null;

    // валидация
    if ($user->validate()) {
        $user->save();
        $response = array(
            'success' => true,
            'item' => $user->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $user->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->conditions(array('id' => '\d+'))->via('POST', 'PUT');