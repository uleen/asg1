<?php

// получение списка контрагентов
$app->get('/', function () use ($app) {

    $start = (int)$app->request->get('start');
    $limit = (int)$app->request->get('limit');

    $sql = "select sql_calc_found_rows 
             org.id as org_id,
             c.id as contractor_id,
             c.main_personal_id, 
             c.id, c.title, c.inn, c.phone, c.email, c.is_blocked,
              (select do.title from d_okpfo_type do where do.id = c.okpfo_type) as okpfo_type_title,
              case c.type when 1 then 'Юр. лицо' when 2 then 'Физ. лицо' when 3 then 'ИП' end as type_title,
              (select cc.title from d_customer_category cc where cc.id = c.client_category) as client_category_title,
              (select u.name from user u where u.id = c.manager_id) as manager_title
            from d_contractor c
            left join organization as org on (org.is_contractor = c.id)
            where 1 = 1";
    $params = array();
    $query = $app->request->get('query');
    if ($query) {
        $sql .= " and (c.title like :query or c.inn like :query or c.phone like :query or c.email like :query)";
        $params['query'] = '%' . $app->request->get('query') . '%';
    }
    if ($app->request->get('type')) {
        $sql .= " and c.type = :type";
        $params['type'] = $app->request->get('type');
    }
    if ($app->request->get('title')) {
        $sql .= " and c.title = :title";
        $params['title'] = $app->request->get('title');
    }
    if ($app->request->get('client_category')) {
        $sql .= " and c.client_category = :client_category";
        $params['client_category'] = $app->request->get('client_category');
    }
    if ($app->request->get('manager_id')) {
        $sql .= " and c.manager_id = :manager_id";
        $params['manager_id'] = $app->request->get('manager_id');
    }
    if ($app->request->get('inn')) {
        $sql .= " and c.inn = :inn";
        $params['inn'] = $app->request->get('inn');
    }
    if ($app->request->get('phone')) {
        $sql .= " and c.phone = :phone";
        $params['phone'] = $app->request->get('phone');
    }
    if ($app->request->get('email')) {
        $sql .= " and c.email = :email";
        $params['email'] = $app->request->get('email');
    }
    if ($app->request->get('is_blocked')) {
        $sql .= " and c.is_blocked = :is_blocked";
        $params['is_blocked'] = $app->request->get('is_blocked');
    }
    if ($app->request->get('org_type')) {
        $sql .= " and c.org_type = :org_type";
        $params['org_type'] = $app->request->get('org_type');
    }
    $sql .= " order by c.title asc limit $start, $limit";

    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);

    $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);

    $response = array(
        'success' => true,
        'total' => $foundRows[0],
        'items' => $data,
    );
    // ответ
    echo json_encode($response);
});


// получение данных одного запроса
$app->get('/:id', function ($id) use ($app) {
    $sql = "select org.id as org_id,
     c.id as contractor_id, 
     c.main_personal_id,
     c.*,
              (select u.name from user u where u.id = c.manager_id) as manager_title,
              (select r.title from d_reclama r where r.id = c.reclama_id) as reclama,
              DATE_FORMAT(FROM_UNIXTIME(c.create_date), '%d.%m.%Y %H:%i') AS create_date,
              concat (pf.full_title, ' \"', c.title_full, '\"') as opf_title_full,
              concat (pf.title, ' \"', c.title, '\"') as opf_title
            from d_contractor c
            left join d_okpfo_type pf on c.okpfo_type = pf.id
            left join organization as org on (org.is_contractor = c.id)
            where c.id = :id";
    $params = array('id' => $id);
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    $data = $sth->fetch(PDO::FETCH_ASSOC);

    // сферы деятельности
    if ($data['ref_scope_id']) {
        $ref_scope = array();
        $sql = "select r.title
                from d_reference_scope r
                where r.id in (" . $data['ref_scope_id'] . ")";
        $sth = db::get()->prepare($sql);
        $sth->execute();
        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $_) {
            $ref_scope[] = $_['title'];
        }
        $data['ref_scope'] = implode(', ', $ref_scope);
    }
    $response = array(
        'success' => true,
        'item' => $data,
    );
    echo json_encode($response);
});

// сохранение нового запроса
$app->post('/', function () use ($app) {
    $data = json_decode($app->environment['slim.input'], true);

    $contractor = new Contractor($data);

    // валидация
    if ($contractor->validate()) {
        $sql = "insert into d_contractor set
                  type = :type,
                  okpfo_type = :okpfo_type,
                  org_type = :org_type,
                  ref_scope_id = :ref_scope_id,
                  client_category = :client_category,
                  manager_id = :manager_id,
                  reclama_id = :reclama_id,
                  create_date = :create_date,
                  ur_address_fias = :ur_address_fias,
                  post_address_fias = :post_address_fias,
                  fact_address_fias = :fact_address_fias,
                  ur_address_text = :ur_address_text,
                  post_address_text = :post_address_text,
                  fact_address_text = :fact_address_text,
                  ur_address_json = :ur_address_json,
                  post_address_json = :post_address_json,
                  fact_address_json = :fact_address_json,
                  phone = :phone,
                  fax = :fax,
                  email = :email,
                  url = :url,
                  title = :title,
                  title_full = :title_full,
                  comment = :comment,
                  is_blocked = :is_blocked,
                  inn = :inn,
                  kpp = :kpp,
                  ic_root_id =:ic_root_id";
        $params = array(
            'type' => $data['type'],
            'okpfo_type' => array_key_exists('okpfo_type', $data) ? $data['okpfo_type'] : null,
            'org_type' => array_key_exists('org_type', $data) ? $data['org_type'] : null,
            'ref_scope_id' => array_key_exists('ref_scope_id', $data) ? $data['ref_scope_id'] : null,
            'client_category' => array_key_exists('client_category', $data) ? $data['client_category'] : null,
            'manager_id' => array_key_exists('manager_id', $data) ? $data['manager_id'] : null,
            'reclama_id' => array_key_exists('reclama_id', $data) ? $data['reclama_id'] : null,
            'create_date' => time(),
            'ur_address_fias' => array_key_exists('ur_address_fias', $data) ? $data['ur_address_fias'] : null,
            'post_address_fias' => array_key_exists('post_address_fias', $data) ? $data['post_address_fias'] : null,
            'fact_address_fias' => array_key_exists('fact_address_fias', $data) ? $data['fact_address_fias'] : null,
            'ur_address_text' => array_key_exists('ur_address_text', $data) ? $data['ur_address_text'] : null,
            'post_address_text' => array_key_exists('post_address_text', $data) ? $data['post_address_text'] : null,
            'fact_address_text' => array_key_exists('fact_address_text', $data) ? $data['fact_address_text'] : null,
            'ur_address_json' => array_key_exists('ur_address_json', $data) ? $data['ur_address_json'] : null,
            'post_address_json' => array_key_exists('post_address_json', $data) ? $data['post_address_json'] : null,
            'fact_address_json' => array_key_exists('fact_address_json', $data) ? $data['fact_address_json'] : null,
            'phone' => array_key_exists('phone', $data) ? $data['phone'] : null,
            'fax' => array_key_exists('fax', $data) ? $data['fax'] : null,
            'email' => array_key_exists('email', $data) ? $data['email'] : null,
            'url' => array_key_exists('url', $data) ? $data['url'] : null,
            'title' => $data['title'],
            'title_full' => $data['title_full'],
            'comment' => array_key_exists('comment', $data) ? $data['comment'] : null,
            'is_blocked' => $data['is_blocked'],
            'inn' => array_key_exists('inn', $data) ? $data['inn'] : null,
            'kpp' => array_key_exists('kpp', $data) ? $data['kpp'] : null,
            'ic_root_id' => array_key_exists('ic_root_id', $data) ? $data['ic_root_id'] : 0,
        );
        db::get()->prepare($sql)->execute($params);
        $data['id'] = db::get()->lastInsertId();

        // просатвляем организацию
        $org_sql = "insert into organization
        (
        company_title,
        company_type,
        company_fias_id,
        company_fias_title,
        reference_scope_id
        )
        values
        (
        :company_title,
        :company_type,
        :company_fias_id,
        :company_fias_title,
        :reference_scope_id)
        ";
        $params = array(
            'company_title' => $data['title'],
            'company_type' => $data['okpfo_type'],
            'company_fias_id' => $data['fact_address_fias'],
            'company_fias_title' => $data['fact_address_text'],
            'reference_scope_id' => $data['ref_scope_id'],
        );
        db::get()->prepare($org_sql)->execute($params);
        $org_id = db::get()->lastInsertId();

        $sql = "update organization set is_contractor = :contractor_id  WHERE id = :org_id";
        $params = array(
            'contractor_id' => $data['id'],
            'org_id' => $org_id,
        );
        db::get()->prepare($sql)->execute($params);

        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $contractor->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
});


// редактирование запроса
$app->put('/:id', function ($id) use ($app) {
    $data = json_decode($app->environment['slim.input'], true);
    $contractor = new Contractor($data);

    // валидация
    if ($contractor->validate()) {
        $sql = "update d_contractor set
                  type = :type,
                  okpfo_type = :okpfo_type,
                  org_type = :org_type,
                  ref_scope_id = :ref_scope_id,
                  client_category = :client_category,
                  manager_id = :manager_id,
                  ur_address_fias = :ur_address_fias,
                  post_address_fias = :post_address_fias,
                  fact_address_fias = :fact_address_fias,
                  ur_address_text = :ur_address_text,
                  post_address_text = :post_address_text,
                  fact_address_text = :fact_address_text,
                  ur_address_json = :ur_address_json,
                  post_address_json = :post_address_json,
                  fact_address_json = :fact_address_json,
                  phone = :phone,
                  fax = :fax,
                  email = :email,
                  url = :url,
                  title = :title,
                  title_full = :title_full,
                  comment = :comment,
                  is_blocked = :is_blocked,
                  inn = :inn,
                  kpp = :kpp
                where id = :id";
        $params = array(
            'type' => $data['type'],
            'okpfo_type' => array_key_exists('okpfo_type', $data) ? $data['okpfo_type'] : null,
            'org_type' => array_key_exists('org_type', $data) ? $data['org_type'] : null,
            'ref_scope_id' => array_key_exists('ref_scope_id', $data) ? $data['ref_scope_id'] : null,
            'client_category' => array_key_exists('client_category', $data) ? $data['client_category'] : null,
            'manager_id' => array_key_exists('manager_id', $data) ? $data['manager_id'] : null,
            'ur_address_fias' => array_key_exists('ur_address_fias', $data) ? $data['ur_address_fias'] : null,
            'post_address_fias' => array_key_exists('post_address_fias', $data) ? $data['post_address_fias'] : null,
            'fact_address_fias' => array_key_exists('fact_address_fias', $data) ? $data['fact_address_fias'] : null,
            'ur_address_text' => array_key_exists('ur_address_text', $data) ? $data['ur_address_text'] : null,
            'post_address_text' => array_key_exists('post_address_text', $data) ? $data['post_address_text'] : null,
            'fact_address_text' => array_key_exists('fact_address_text', $data) ? $data['fact_address_text'] : null,
            'ur_address_json' => array_key_exists('ur_address_json', $data) ? $data['ur_address_json'] : null,
            'post_address_json' => array_key_exists('post_address_json', $data) ? $data['post_address_json'] : null,
            'fact_address_json' => array_key_exists('fact_address_json', $data) ? $data['fact_address_json'] : null,
            'phone' => array_key_exists('phone', $data) ? $data['phone'] : null,
            'fax' => array_key_exists('fax', $data) ? $data['fax'] : null,
            'email' => array_key_exists('email', $data) ? $data['email'] : null,
            'url' => array_key_exists('url', $data) ? $data['url'] : null,
            'title' => $data['title'],
            'title_full' => $data['title_full'],
            'comment' => array_key_exists('comment', $data) ? $data['comment'] : null,
            'is_blocked' => $data['is_blocked'],
            'inn' => array_key_exists('inn', $data) ? $data['inn'] : null,
            'kpp' => array_key_exists('kpp', $data) ? $data['kpp'] : null,
            'id' => $id
        );
        db::get()->prepare($sql)->execute($params);

        // ответ
        $response = array(
            'success' => true,
            'item' => $data,
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $contractor->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
});


// сохранение формы контрагента для привязки к нему данных
$app->post('/firstsave', function () use ($app) {
    $data = $app->request()->post();
    $cagent_id = 0;
    
    $errors = array();
    
    if ($data['org_id'] == 0) {

        // сохранение входящего запроса
        $ic_sql = "insert into incoming_call_register
      (
      root_id, call_date, save_date, user_id,incoming_source_id,
      client_interest, client_interest_srv, client_interest_nom,
      status, ref_cagent_id, ref_user_id
      )
      values (:root_id, :call_date, :save_date, :user_id, :incoming_source_id,
      :client_interest, :client_interest_srv, :client_interest_nom,
      :status, :ref_cagent_id, :ref_user_id)";

        $params = array(
            'incoming_source_id' => $data['incoming_source_id'],
            'call_date' => strtotime($data['call_date']),
            'root_id' => $data['root_id'],
            'save_date' => time(),
            'user_id' => User::get()->id,
            'client_interest' => $data['client_interest'],
            'client_interest_srv' => $data['client_interest_srv'],
            'client_interest_nom' => $data['client_interest_nom'],
            'status' => 1,
            'ref_cagent_id' => $data['ref_cagent_id'],
            'ref_user_id' => $data['ref_user_id'],
        );
        db::get()->prepare($ic_sql)->execute($params);
        $ic_id = db::get()->lastInsertId();


        // Новый вх.запрос
        // если лид - создаем  организацию и вх.запрос и новое контактное лицо

        // $ic_id = db::get()->lastInsertId();
        $org_sql = "insert into organization
        (
         
        company_title,
        company_type,
        company_fias_id,
        company_fias_title,
        reference_scope_id,
        reference_scope_text,
        reclama_id,
        reclama_interest,
        ref_cagent_id,
        ref_user_id
        )
        values
        (
      
        :company_title,
        :company_type,
        :company_fias_id,
        :company_fias_title,
        :reference_scope_id,
        :reference_scope_text,
        :reclama_id,
        :reclama_interest,
        :ref_cagent_id,
        :ref_user_id)
        ";
        $params = array(

            'company_title' => $data['company_title'] ? $data['company_title'] : $data['cagent_title'],
            'company_type' => $data['company_type'] ? $data['company_type'] : $data['cagent_type'],
            'company_fias_id' => $data['company_fias_id'],
            'company_fias_title' => $data['company_fias_title'],
            'reference_scope_id' => $data['reference_scope_id'],
            'reference_scope_text' => $data['reference_scope_text'],
            'reclama_id' => $data['reclama_id'],
            'reclama_interest' => $data['reclama_interest'],
            'ref_cagent_id' => $data['ref_cagent_id'],
            'ref_user_id' => $data['ref_user_id'],
        );
        db::get()->prepare($org_sql)->execute($params);
        $org_id = db::get()->lastInsertId();

        $cp_sql = "insert into d_contact_personal
        (fias_id, fio, status)
        values (:fias_id, :fio, :status)";
        $params = array(
            'status' => 1,
            'fias_id' => 0,
            'fio' => $data['cagent_name'],
        );
        db::get()->prepare($cp_sql)->execute($params);
        $contact_personal_id = db::get()->lastInsertId();

        // связь контакное лицо к организации
        $sql = "insert into org2personal (org_id, personal_id) values (:org_id, :personal_id)";
        $params = array(
            'org_id' => $org_id,
            'personal_id' => $contact_personal_id,
        );
        db::get()->prepare($sql)->execute($params);

        // валидация, есдли есть телефон
        if ($data['cagent_phone']) {
            // добавление телефона
            $p_sql = "insert into d_contacts_ref (personal_id, contact_type, contact, add_contact, is_main, is_active)
        values (:personal_id, 1, :contact, :add_contact, 1, 1)";
            $params = array(
                'personal_id' => $contact_personal_id,
                'contact' => $data['cagent_phone'],
                'add_contact' => $data['add_contact'],
            );
            db::get()->prepare($p_sql)->execute($params);
            $phone_contact_id = db::get()->lastInsertId();

            $e_sql = "insert into contacts2personal (contact_id, personal_id)
        values (:contact_id, :personal_id)";
            $params = array(
                'contact_id' => $phone_contact_id,
                'personal_id' => $contact_personal_id,
            );
            db::get()->prepare($e_sql)->execute($params);
        }

        if ($data['cagent_email']) {
            $sql = "insert into d_contacts_ref (personal_id, contact_type, contact, is_main, is_active)
        values (:personal_id, 2, :contact, 1, 1)";
            $params = array(
                'personal_id' => $contact_personal_id,
                'contact' => $data['cagent_email'],
            );
            db::get()->prepare($sql)->execute($params);
            $email_contact_id = db::get()->lastInsertId();

            // добавление связи, что это именоо телефон
            $sql = "insert into contacts2personal (contact_id, personal_id)
        values (:contact_id, :personal_id)";
            $params = array(
                'contact_id' => $email_contact_id,
                'personal_id' => $contact_personal_id,
            );
            db::get()->prepare($sql)->execute($params);
        }

        $sql = "insert into ic2org (ic_id, org_id, personal_id, is_main) values (:ic_id, :org_id, :personal_id, :is_main)";
        $params = array(
            'ic_id' => $ic_id,
            'org_id' => $org_id,
            'personal_id' => $contact_personal_id,
            'is_main' => 0,
        );
        db::get()->prepare($sql)->execute($params);
    }

    if ($org_id != 0) {
        $data['org_id'] = $org_id;
    }

    if (empty($errors)) {
        $sql = "insert into d_contractor set
    type = :type,
    org_type = :org_type,
    ref_scope_id = :ref_scope_id,
    reclama_id = :reclama_id,
    create_date = :create_date,
    fact_address_fias = :fact_address_fias,
    fact_address_text = :fact_address_text,
    title = :title,
    title_full = :title_full,
    ic_root_id = :ic_root_id";


        $xsql = "select reference_scope_id, reclama_id from organization where id=:id";
        $sth = db::get()->prepare($xsql);
        $sth->execute(array('id' => $data['org_id']));
        $dat = $sth->fetchAll(PDO::FETCH_ASSOC);
        $data['ref_scope_id'] = $dat[0]['reference_scope_id'];
        $data['reclama_id'] = $dat[0]['reclama_id'];

        $params = array(
            'org_type' => 1,
            'type' => array_key_exists('cagent_type', $data) ? $data['cagent_type'] : null,
            'ref_scope_id' => array_key_exists('ref_scope_id', $data) ? $data['ref_scope_id'] : null,
            'reclama_id' => array_key_exists('reclama_id', $data) ? $data['reclama_id'] : null,
            'create_date' => time(),
            'fact_address_fias' => array_key_exists('fact_address_fias', $data) ? $data['fact_address_fias'] : null,
            'fact_address_text' => array_key_exists('fact_address_text', $data) ? $data['fact_address_text'] : null,
            'title' => $data['cagent_title'],
            'title_full' => $data['cagent_title'],
            'ic_root_id' => $data['root_id'],

        );
        db::get()->prepare($sql)->execute($params);
        $cagent_id = db::get()->lastInsertId();

        // проставляем сразу контактное лицо
        // если первый вх.запрос - данные из запроса
        // если не первый - ищем первого, кто оставил звпрос и делаем его основным
        if ($cagent_id) {
            // проставляем, что есть уже контрагент
            $sql = "update organization set is_contractor = :cagent_id  WHERE id = :org_id";
            $params = array(
                'cagent_id' => $cagent_id,
                'org_id' => $data['org_id'],
            );
            db::get()->prepare($sql)->execute($params);


            // делаем основным лицом сотрудника, который уже есть
            $main_personal_sql = "update d_contractor set main_personal_id = (
                  SELECT personal_id FROM ic2org WHERE org_id={$data['org_id']} ORDER BY ic_id ASC LIMIT 1) where id={$cagent_id};";
            $p = array();
            db::get()->prepare($main_personal_sql)->execute($p);

        }

    }

    $response = array(
        'success' => true,
        'cagent_id' => $cagent_id,
    );
    echo json_encode($response);
});


// Функция проверяет правильность инн
function is_valid_inn($inn)
{
    if (preg_match('/\D/', $inn)) return false;

    $inn = (string)$inn;
    $len = strlen($inn);

    if ($len === 10) {
        return $inn[9] === (string)(((
                    2 * $inn[0] + 4 * $inn[1] + 10 * $inn[2] +
                    3 * $inn[3] + 5 * $inn[4] + 9 * $inn[5] +
                    4 * $inn[6] + 6 * $inn[7] + 8 * $inn[8]
                ) % 11) % 10);
    } elseif ($len === 12) {
        $num10 = (string)(((
                    7 * $inn[0] + 2 * $inn[1] + 4 * $inn[2] +
                    10 * $inn[3] + 3 * $inn[4] + 5 * $inn[5] +
                    9 * $inn[6] + 4 * $inn[7] + 6 * $inn[8] +
                    8 * $inn[9]
                ) % 11) % 10);
        $num11 = (string)(((
                    3 * $inn[0] + 7 * $inn[1] + 2 * $inn[2] +
                    4 * $inn[3] + 10 * $inn[4] + 3 * $inn[5] +
                    5 * $inn[6] + 9 * $inn[7] + 4 * $inn[8] +
                    6 * $inn[9] + 8 * $inn[10]
                ) % 11) % 10);
        return $inn[11] === $num11 && $inn[10] === $num10;
    }

    return false;
}

// Проверка уникальности ИНН
function checkUniqInn($inn, $org_type, $id = null)
{
    $sql = "select 1
            from d_contractor c
            where c.inn = :inn
              and c.org_type = :org_type
              and c.id != :id";
    $params = array(
        'inn' => $inn,
        'org_type' => $org_type == 1 ? 1 : 2,
        'id' => $id,
    );
    $sth = db::get()->prepare($sql);
    $sth->execute($params);
    return (bool)$sth->rowCount();
}
