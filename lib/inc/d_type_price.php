<?php

// получение списка записей
$app->get('/', function () use ($app) {

    $conditions = $params = array();

    if ($app->request->get('status')) {
        $conditions[] = "status = :status";
        $params['status'] = $app->request->get('status');
    } else {
        $conditions[] = "status in (1,2)";
    }

    // поиск
    $items = array();
    foreach (Type_Price::find($conditions, $params) as $typePrice) {
        $items[] = $typePrice->getData();
    }

    // ответ
    $response = array(
        'success' => true,
        'total' => count($items),
        'items' => $items,
    );
    echo json_encode($response);
});

$app->map('(/:id)', function($id = null) use ($app) {
    // входные данные
    $data = json_decode($app->environment['slim.input'], true);
    $data['status'] = $data['status'] == 1 ? 1 : 2;
    $typePrice = new Type_Price($data);
    if ($id) {
        $typePrice->id = $id;
    }

    // валидация
    if ($typePrice->validate()) {
        $typePrice->save();
        $response = array(
            'success' => true,
            'item' => $typePrice->getData(),
        );
    } else {
        $response = array(
            'success' => false,
            'errors' => $typePrice->getErrors(),
        );
    }
    // ответ
    echo json_encode($response);
})->via('POST', 'PUT');