<?php

abstract class AR
{
    protected $data = array();
    protected $errors = array();

    public function __construct($data = array())
    {
        $this->data = $data;
    }


    /**
     * Получения свойств объекта
     */
    public function __get($key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }
    }


    /**
     * Присвоение свойств объекту
     */
    public function __set($key, $value)
    {
        if (substr($key, 0, 1) == '_') {
            $this->$key = $value;
        } else {
            $this->data[$key] = $value;
        }
    }


    /**
     * Загрузка записи из БД
     */
    public function reload()
    {
        $obj = static::find_by_pk($this->id);
        $this->data = $obj->getData();
    }


    /**
     * Сохранение записи в БД
     */
    public function save()
    {
        $set = array();
        foreach ($this->getTableColumns() as $column) {
            if (array_key_exists($column, $this->data)) {
                $set[] = $column . ' = :' . $column;
                $params[$column] = $this->data[$column];
            }
        }

        if (!empty($set)) {
            if ($this->id) { // редактирование
                $sql = "update " . static::$table . " set " . implode(', ', $set) . " where id = :id";
            } else { // сохранение
                $sql = "insert into " . static::$table . " set " . implode(', ', $set);
            }

            $sth = db::get()->prepare($sql);
            $sth->execute($params);
            if (!$this->id) {
                $this->id = db::get()->lastInsertId();
            }
        }
    }


    /**
     * Отображение всех свойств объекта
     */
    public function debug()
    {
        echo '<pre>' . print_r($this->data, true) . '</pre>';
    }

    /**
     * Полученеи всех атрибутов объекта
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Полученеи всех атрибутов объекта в формате JSON
     * @return string
     */
    public function getJson()
    {
        return json_encode($this->data);
    }

    /**
     * Валидация объекта
     */
    public function validate()
    {
        $this->errors = array();
        return empty($this->errors);
    }


    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Поиск записи по первичному ключу
     * @param $val - значение ключа
     * @param string $field - первичный ключ
     * @return null
     */
    public static function find_by_pk($val, $field = 'id')
    {
        $conditions = array($field . " = :" . $field);
        $params = array($field => $val);
        $items = static::find($conditions, $params);
        return empty($items) ? null : $items[0];
    }


    /**
     * Поиск объектов, по указанному условию условию
     *
     * @param $conditions - условия
     * @param $params - параметры
     * @return array - список объектов
     */
    public static function find($conditions = array(), $params = array())
    {
        $sql = "select * from " . static::$table;
        if (!empty($conditions)) {
            $sql .= " where " . implode(' and ', $conditions);
        }
        return self::find_by_sql($sql, $params);
    }


    /**
     * Поиск объектов, по sql-запросу
     *
     * @param $sql - запрос
     * @param $params - параметры запроса
     * @return array - список объектов
     */
    public static function find_by_sql($sql, $params = array())
    {
        $modelName = get_called_class();
        $items = array();
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $itemData) {
            $items[] = new $modelName($itemData);
        }
        return $items;
    }


    /**
     * Получение списка колонок таблицы
     * @return array
     */
    public function getTableColumns()
    {
        $sql = "select column_name
             from information_schema.columns
             where table_schema = database()
               and table_name = :table_name";

        $sth = db::get()->prepare($sql);
        $sth->execute(array('table_name' => static::$table));
        $tableRows = array();
        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $r) {
            $tableRows[] = $r['column_name'];
        }
        return $tableRows;
    }


    /**
     * Кол-во строк в последнем поиске
     */
    public static function calc_found_rows()
    {
        $foundRows = db::get()->query("select FOUND_ROWS()")->fetch(PDO::FETCH_NUM);
        return $foundRows[0];
    }

}