<?php

class DB
{
    private static $_conn;

    private function __construct()
    {
    }

    static function get()
    {
        if (!isset(self::$_conn)) {
            self::$_conn = new PDO('mysql:host=37.143.10.180;dbname=acrm', 'web', '7pS8QyLX');
            self::$_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$_conn->exec("set names utf8 COLLATE utf8_general_ci");
        }
        return self::$_conn;
    }
    
    static function setLoggedUser($uid){
      if (!empty($uid)) {
       self::$_conn->exec("set @logged_uid={$uid}");
      }
      
    }

}