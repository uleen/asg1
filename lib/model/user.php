<?php

class User extends AR
{
    static $table = 'user';
    private static $_auth;

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Авторизация пользователя
     * @param null $login - логин
     * @param null $password - пароля
     * @return mixed
     */
    static function auth($login = null, $password = null)
    {
        if (!isset(self::$_auth)) {

            if (!$login) {
                $login = array_key_exists('login', $_COOKIE) ? $_COOKIE['login'] : null;
                $password = array_key_exists('password', $_COOKIE) ? $_COOKIE['password'] : null;
            }
            if ($login && $password) {
                $sql = "select u.*
                    from user u
                    where u.login = :login and u.password = :password
                    and status = 1
                    limit 1
                    ";
                $params = array(
                    'login' => $login,
                    'password' => md5($password),
                );

                $sth = db::get()->prepare($sql);
                $sth->execute($params);

                self::$_auth = $sth->fetch(PDO::FETCH_ASSOC);

            }
        }
        return self::$_auth;
    }


    /**
     * Поиск объектов, по указанному условию условию
     *
     * @param $conditions - условия
     * @param $params - параметры
     * @param $start - сдвиг выборки
     * @param $limit - количество возвращаемы записей
     * @return User[] - список объектов
     */
    public static function find($conditions = array(), $params = array(), $start = 0, $limit = 25)
    {
        $sql = "select SQL_CALC_FOUND_ROWS u.*,
                    date_format(u.date_birth, '%d.%m.%Y') as date_birth,
                    date_format(u.date_hire, '%d.%m.%Y') as date_hire,
                    date_format(u.date_dismissal, '%d.%m.%Y') as date_dismissal,
                    (select p.title from d_position p where p.id = u.position_id) as position_title
                from user u
                " . (!empty($conditions) ? " where " . implode(' and ', $conditions) : "") . "
                order by u.name
                limit $start, $limit";
        return self::find_by_sql($sql, $params);
    }


    /**
     * Поиск записи по первичному ключу
     * @param $val - значение ключа
     * @param string $field - первичный ключ
     * @return null
     */
    public static function find_by_pk($val, $field = 'id')
    {

        $sql = "select u.*, null as password,
                    date_format(u.date_birth, '%d.%m.%Y') as date_birth,
                    date_format(u.date_hire, '%d.%m.%Y') as date_hire,
                    date_format(u.date_dismissal, '%d.%m.%Y') as date_dismissal,
                    (select c.title from d_company c where c.id = u.company_id) as company_title,
                    (select cu.title from company_unit cu where cu.id = u.company_unit_id) as company_unit_title,
                    (select p.title from d_position p where p.id = u.position_id) as position_title
                from user u
                where u.id = :id";
        $items = self::find_by_sql($sql, array('id' => $val));
        return empty($items) ? null : $items[0];
    }


    static function getLoggedID()
    {
        return self::$_auth['id'];
    }

    /**
     * Данные о текущем пользователе
     * @return object
     */
    static function get()
    {
        $userData = self::$_auth;
        unset($userData['password']);
        return (object)$userData;
    }

    static function getRights()
    {
        //getLoggedID
        $sql = "select perms from u_perms where uid=:uid";
        $params = array('uid' => self::getLoggedID());
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        $data = $sth->fetchAll(PDO::FETCH_COLUMN);
        if (count($data) > 0) {
            $data = array_combine($data, array_fill(1, count($data), 1));
        } else {
            $data = array();
        }
        return array('rights' => $data);
    }

}

