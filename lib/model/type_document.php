<?php

class Type_Document extends AR
{
    static $table = 'd_type_document';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }

        return empty($this->errors);
    }

}

