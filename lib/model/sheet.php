<?php

class Sheet extends AR
{
    static $table = 'sheet';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    public function __get($key)
    {
        // компания
        if ($key == 'company') {
            if (empty($this->_company)) {
                $this->_company = Company::find_by_pk($this->company_id);
            }
            return $this->_company;
        }

        // реквизиты компании
        if ($key == 'company_requisite') {
            if (empty($this->_company_requisite)) {
                $this->_company_requisite = Company_Requisite::find_by_pk($this->company_requisite_id);
            }
            return $this->_company_requisite;
        }

        // контрагент
        if ($key == 'contractor') {
            if (empty($this->_contractor)) {
                $this->_contractor = Contractor::find_by_pk($this->contractor_id);
            }
            return $this->_contractor;
        }

        // договор контрагента
        if ($key == 'contractor_contract') {
            if (empty($this->_contractor_contract)) {
                $this->_contractor_contract = Contractor_Contract::find_by_pk($this->contractor_contract_id);
            }
            return $this->_contractor_contract;
        }

        // клиентский заказ
        if ($key == 'client_order') {
            if (empty($this->_client_order)) {
                $this->_client_order = Client_Order::find_by_pk($this->client_order_id);
            }
            return $this->_client_order;
        }

        // название счета
        if ($key == 'title') {
            return 'Счет № ' . $this->id . ' от ' . sys::date($this->create_date);
        }

        // сумма счета прописью
        if ($key == 'sheet_sum_in_words') {
            return sys::num2str($this->sheet_sum);
        }

        // документ
        if ($key == 'document') {
            if (empty($this->_document)) {
                $this->_document = Document::find_by_pk($this->document_id);
            }
            return $this->_document;
        }

        return parent::__get($key);
    }


    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->client_order_id) {
            $this->errors['client_order_id'] = 'Поле "Клиентский заказ" обязательно для заполнения';
        }
        if (!$this->company_id) {
            $this->errors['company_id'] = 'Поле "Организация" обязательно для заполнения';
        }
        if (!$this->company_requisite_id) {
            $this->errors['company_requisite_id'] = 'Поле "Счет получателя" обязательно для заполнения';
        }
        if (!$this->sheet_status_id) {
            $this->errors['sheet_status_id'] = 'Поле "Статус" обязательно для заполнения';
        }
        if (!$this->sheet_type_id) {
            $this->errors['sheet_type_id'] = 'Поле "Тип" обязательно для заполнения';
        }
        if ($this->contractor_contract && !$this->contractor_contract->isActual()) {
            $this->errors['contractor_contract_title'] = 'Договор не актуален на текущую дату';
        }


        return empty($this->errors);
    }


    /**
     * Переопределено сохранение счета
     */
    public function save()
    {
        $is_new = !(bool)$this->id;
        parent::save();

        if ($is_new) {
            // генерация документа
            $this->createDocument();
            // обновление клиентского заказа
            $this->client_order->sheet_id = $this->id;
            $this->client_order->save();
            $this->client_order->changeStatus(4, $this->title, User::get()->id);
        }
    }

    /**
     * Переопределен поиск
     *
     * @param array $conditions - условия поиска
     * @param array $params - параметры поиска
     * @param $start - сдвиг выборки
     * @param $limit - количество возвращаемы записей
     * @return Sheet[] - массив объектов
     */
    public static function find($conditions = array(), $params = array(), $start = 0, $limit = 25)
    {
        $sql = "select t.*, round(t.sheet_sum * 18 / 118, 2) as sheet_sum_nds
                from (select s.*,
                        c.id as contractor_id, c.title as contractor_title, c.inn as contractor_inn,
                        co.currency_id, co.create_date as client_order_date,
                        (select u.name from user u where u.id = s.create_user_id) as create_user_name,
                        (select c.title from d_company c where c.id = s.company_id) as company_title,
                        (select cr.title from d_company_requisites cr where cr.id = s.company_requisite_id) as company_requisite_title,
                        (select ss.title from d_sheet_status ss where ss.id = s.sheet_status_id) as sheet_status_title,
                        (select st.title from d_sheet_type st where st.id = s.sheet_type_id) as sheet_type_title,
                        (select c.code from d_currency c where c.id = co.currency_id) as currency_code,
                        (select sum(con.volume * con.price) from client_order_nomenclature con where con.status = 1 and con.client_order_id = co.id) as sheet_sum
                      from sheet s, client_order co, d_contractor c
                      where co.id = s.client_order_id and c.id = co.contractor_id
                ) t
                " . (!empty($conditions) ? " where " . implode(' and ', $conditions) : "") . "
                order by t.create_date desc
                limit $start, $limit";
        return parent::find_by_sql($sql, $params);
    }


    /**
     * Изменение статуса счета
     *
     * @param $sheet_status_id - ID статуса счета
     * @param null $sheet_status_note - Комментарий к изменению статуса
     * @param $user_id - ID пользователя, который меняет статус
     */
    public function changeStatus($sheet_status_id, $sheet_status_note = null, $user_id = null)
    {
        // если пользователь не указан, действие выполняется от системного пользователя
        if (!$user_id) {
            $user_id = 0;
        }

        // добавление записи в историю статусов
        $sql = "insert into sheet_status set
                  sheet_id = :sheet_id,
                  sheet_status_id = :sheet_status_id,
                  create_date = :create_date,
                  user_id = :user_id,
                  note = :note";
        $params = array(
            'sheet_id' => $this->id,
            'sheet_status_id' => $sheet_status_id,
            'note' => $sheet_status_note,
            'create_date' => date('Y-m-d H:i:s'),
            'user_id' => $user_id,
        );
        db::get()->prepare($sql)->execute($params);

        // сохранение текущего статуса
        $sql = "update sheet set
                  sheet_status_id = :sheet_status_id
                where id = :sheet_id";
        $params = array(
            'sheet_id' => $this->id,
            'sheet_status_id' => $sheet_status_id,
        );
        db::get()->prepare($sql)->execute($params);
    }


    /**
     * Получение истории изменения статусов
     *
     * @return array
     */
    public function getStatusHistory()
    {
        $sql = "select cs.*,
                  (select s.title from d_sheet_status s where s.id = ss.sheet_status_id) as sheet_status_title,
                  (select u.name from user u where u.id = ss.user_id) as user_name
                from sheet_status ss
                where ss.sheet_id = :sheet_id
                order by cs.create_date";
        $params = array(
            'sheet_id' => $this->id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Создание документа
     */
    public function createDocument()
    {
        // сохранение документа
        $document = new Document;
        $document->create_date = date('Y-m-d H:i:s');
        $document->create_user_id = User::get()->id;
        $document->title = $this->title;
        $document->content = sys::render('blanks/sheet.php', array('sheet' => $this));
        $document->save();

        // обновление счета
        $this->document_id = $document->id;
        $this->save();
    }

}