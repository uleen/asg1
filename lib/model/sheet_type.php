<?php

class Sheet_Type extends AR
{
    static $table = 'd_sheet_type';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'не указан заголовок';
        }

        return empty($this->errors);
    }

}