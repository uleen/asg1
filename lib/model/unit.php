<?php

class Unit extends AR
{
    static $table = 'd_unit';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'не указан заголовок';
        }

        return empty($this->errors);
    }

}

