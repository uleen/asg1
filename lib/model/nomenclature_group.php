<?php

class Nomenclature_Group extends AR
{
    static $table = 'd_direction_sales';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'не указан заголовок';
        }

        return empty($this->errors);
    }


    /**
     * Получение пути к корню
     * @return object
     */
    public function getPath()
    {
        $sql = "select concat_ws('.', d5.id, d4.id, d3.id, d2.id, d1.id, d0.id) as path,
              concat_ws(' » ', d5.title, d4.title, d3.title, d2.title, d1.title, d0.title) as title
              from d_direction_sales d0
              left join d_direction_sales d1 on d1.id = d0.pid
              left join d_direction_sales d2 on d2.id = d1.pid
              left join d_direction_sales d3 on d3.id = d2.pid
              left join d_direction_sales d4 on d4.id = d3.pid
              left join d_direction_sales d5 on d5.id = d4.pid
              where d0.id = :id";
        $params = array('id' => $this->id);
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return (object)$sth->fetch(PDO::FETCH_ASSOC);
    }


    /**
     * Получение всех подкатегорий в виде списка ID
     */
    public function getChildrensId()
    {
        $sql = "select id from " . static::$table . " where pid = :id";
        $sth = db::get()->prepare($sql);
        $get = function ($id) use ($sth, &$get) {
            $cid = array($id);
            $sth->execute(array('id' => $id));
            foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $c) {
                $cid = array_merge($cid, $get($c['id']));
            }
            return array_unique($cid);
        };
        return $get($this->id);
    }

}