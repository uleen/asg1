<?php

class Nomenclature extends AR
{
    static $table = 'd_nomenclature';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }
        if (!$this->nomenclature_group_id) {
            $this->errors['nomenclature_group_full_title'] = 'Поле "Номенклатурная группа" обязательно для заполнения';
        }
        if (!$this->unit_id) {
            $this->errors['unit_id'] = 'Поле "Ед. изм" обязательно для заполнения';
        }
        if (!$this->manager_id) {
            $this->errors['manager_title'] = 'Поле "Продукт-менеджер" обязательно для заполнения';
        }

        return empty($this->errors);
    }

    /**
     * Переопределен поиск
     * @param null $conditions - условия поиска
     * @param null $params - параметры поиска
     * @return array - массив объектов
     */
    public static function find($conditions = null, $params = null)
    {
        $sql = "select dn.*,
                  (select ds.title from d_direction_sales ds where ds.id = dn.nomenclature_group_id) as nomenclature_group_title,
                  (select u.name from user u where u.id = dn.manager_id) as manager_title,
                  (select u.title from d_unit u where u.id = dn.unit_id) as unit_title,
                  (select u.title from d_unit u where u.id = dn.shipping_unit_id) as shipping_unit_title,
                  (select max(np.price) from d_nomenclature_price np where np.nomenclature_id = dn.id) as price,
                  (select group_concat(concat(c.title_short, ':', nc.value) separator ', ')
                    from d_nomenclature_characteristic nc, d_characteristic c
                    where nc.characteristic_id = c.id
                        and nc.nomenclature_id = dn.id
                    order by c.sort) as characteristics
                from d_nomenclature dn
                where " . implode(' and ', $conditions);
        return parent::find_by_sql($sql, $params);
    }


    /**
     * Полчение списка характеристик со значениями
     */
    public function getCharacteristic()
    {
        $ng = Nomenclature_Group::find_by_pk($this->nomenclature_group_id);
        $ngId = explode('.', $ng->getPath()->path);
        $sql = "select :id as nomenclature_id, c.id as characteristic_id, nc.value, c.title, c.title_short, c.unit
                from d_characteristic c
                left join d_nomenclature_characteristic nc on nc.characteristic_id = c.id and nc.nomenclature_id = :id
                where c.nomenclature_group_id in (" . implode(',', $ngId) . ")";
        $sth = db::get()->prepare($sql);
        $sth->execute(array('id' => $this->id));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Сохранение списка характеристик
     */
    public function setValueCharacteristic($characteristic_id, $value)
    {
        $sql = "replace into d_nomenclature_characteristic set
                  nomenclature_id = :nomenclature_id,
                  characteristic_id = :characteristic_id,
                  value = :value";
        $params = array(
            'nomenclature_id' => $this->id,
            'characteristic_id' => $characteristic_id,
            'value' => $value,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
    }


    /**
     * Полчение списка цен
     */
    public function getPrice()
    {
        $sql = "select tp.id as type_price_id, tp.title as type_price_title, np.price, 'руб' as currency,
                    :id as nomenclature_id, (select n.title from d_nomenclature n where n.id = :id) as nomenclature_title
                from d_type_price tp
                left join d_nomenclature_price np on np.type_price_id = tp.id and np.nomenclature_id = :id
                where tp.status = 1";
        $sth = db::get()->prepare($sql);
        $sth->execute(array('id' => $this->id));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Сохранение цены
     *
     * @param $type_price_id - ID типа цены
     * @param $price - значение цены
     * @param $currency_id - ID валюты
     */
    public function setPrice($type_price_id, $price, $currency_id = 1)
    {
        $sql = "replace into d_nomenclature_price set
                  nomenclature_id = :nomenclature_id,
                  type_price_id = :type_price_id,
                  price = :price,
                  currency_id = :currency_id";
        $params = array(
            'nomenclature_id' => $this->id,
            'type_price_id' => $type_price_id,
            'price' => $price,
            'currency_id' => $currency_id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);

        // сохранение истории изменений
        $sql = "insert into h_nomenclature_price set
                  nomenclature_id = :nomenclature_id,
                  type_price_id = :type_price_id,
                  price = :price,
                  currency_id = :currency_id,
                  change_date = :change_date,
                  change_user_id = :change_user_id";
        $params = array(
            'nomenclature_id' => $this->id,
            'type_price_id' => $type_price_id,
            'price' => $price,
            'currency_id' => $currency_id,
            'change_date' => date('Y-m-d H:i:s'),
            'change_user_id' => User::get()->id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
    }


    /**
     * Установка номенклатуры
     *
     * @param $contractor_delivery_id - ID точки доставки
     * @param $article - артикул поставщика
     * @param $price - цена поставщика
     * @param $currency_id - валюта
     * @param $unit_id - ед. изм. поставщика
     * @param $in_stock - в наличии
     * @param $note - комментарий
     */
    public function setSupplier($contractor_delivery_id, $article, $price, $currency_id, $unit_id, $in_stock, $note)
    {
        $user = User::auth();

        $sql = "insert into d_nomenclature_supplier set
                  create_date = :create_date,
                  nomenclature_id = :nomenclature_id,
                  contractor_delivery_id = :contractor_delivery_id,
                  price = :price,
                  currency_id = :currency_id,
                  unit_id = :unit_id,
                  article = :article,
                  in_stock = :in_stock,
                  note = :note,
                  user_id = :user_id";
        $params = array(
            'create_date' => time(),
            'nomenclature_id' => $this->id,
            'contractor_delivery_id' => $contractor_delivery_id,
            'price' => $price,
            'currency_id' => $currency_id,
            'unit_id' => $unit_id,
            'article' => $article,
            'in_stock' => $in_stock,
            'note' => $note,
            'user_id' => $user['id'],
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
    }

    /**
     * Список поставщиков и их последние цены
     * @return array
     */
    public function getSuppliers()
    {
        $sql = "select ns.*, c.title as contractor_title, cd.address as contractor_delivery_address,
                  cd.lat as contractor_delivery_lat, cd.lng as contractor_delivery_lng,
                  n.article as nomenclature_article, n.title as nomenclature_title,
                  cr.code as currency_code,
                  (select crr.rate / crr.nom from d_currency_rate crr where crr.num = cr.num and crr.cdate = :currency_rate_date) as currency_rate,
                  (select u.title from d_unit u where u.id = ns.unit_id) as unit_title
                from d_nomenclature_supplier ns
                inner join (select max(create_date) as create_date, contractor_delivery_id
                            from d_nomenclature_supplier
                            where nomenclature_id = :nomenclature_id
                            group by contractor_delivery_id) ls
                  on ns.create_date = ls.create_date
                    and ns.contractor_delivery_id = ls.contractor_delivery_id
                inner join d_contractor_delivery cd
                  on cd.id = ns.contractor_delivery_id
                inner join d_contractor c
                  on c.id = cd.contractor_id
                inner join d_nomenclature n
                  on n.id = ns.nomenclature_id
                inner join d_currency cr
                  on cr.id = ns.currency_id
                where ns.nomenclature_id = :nomenclature_id";
        $sth = db::get()->prepare($sql);
        $params = array(
            'nomenclature_id' => $this->id,
            'currency_rate_date' => date('Y-m-d'),
        );
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Получение данных для "каталога менеджера"
     * @param int $nomenclature_group_id - ID номенклатурной группы
     * @param null $query - поисковая строка
     * @param int $start
     * @param int $limit
     * @return array
     */
    public static function catalog($nomenclature_group_id = 0, $query = null, $start = 0, $limit = 25)
    {
        $ng = Nomenclature_Group::find_by_pk($nomenclature_group_id);
        $sql = "select sql_calc_found_rows ns.*, cd.address as contractor_delivery_address,
                  c.title as contractor_title, n.article as nomenclature_article, n.title as nomenclature_title,
                  n.unit_id as nomenclature_unit_id, n.shipping_min as nomenclature_shipping_min,
                  n.shipping_unit_id as nomenclature_shipping_unit_id, 'руб' as our_currency_code,
                  (select u.title from d_unit u where u.id = n.unit_id) as nomenclature_unit_title,
                  (select u.title from d_unit u where u.id = n.shipping_unit_id) as nomenclature_shipping_unit_title,
                  n.type as nomenclature_type, ng.title as nomenclature_group_title,
                  (select c.code from d_currency c where c.id = ns.currency_id) as currency_code,
                  (select max(np.price) from d_nomenclature_price np where np.nomenclature_id = n.id) as our_price,
                  (select group_concat(concat(c.title_short, ':', nc.value) separator ', ')
                    from d_nomenclature_characteristic nc, d_characteristic c
                    where nc.characteristic_id = c.id
                        and nc.nomenclature_id = n.id
                    order by c.sort) as nomenclature_characteristics
                from d_nomenclature n
                left join d_nomenclature_supplier ns on ns.nomenclature_id = n.id
                left join d_contractor_delivery cd on ns.contractor_delivery_id = cd.id
                left join d_contractor c on cd.contractor_id = c.id
                left join (select max(ns.create_date) as create_date, ns.nomenclature_id, ns.contractor_delivery_id
                            from d_nomenclature_supplier ns
                            group by ns.nomenclature_id, ns.contractor_delivery_id) lp
                    on lp.create_date = ns.create_date and lp.nomenclature_id = ns.nomenclature_id and lp.contractor_delivery_id = ns.contractor_delivery_id
                inner join d_direction_sales ng on n.nomenclature_group_id = ng.id
                where n.status = 1
                    and (n.title like :query or n.article like :query)
                    and (n.type = 'service' or c.id is not null)
                    and (n.type = 'service' or lp.create_date = ns.create_date)
                    " . ($ng ? "and n.nomenclature_group_id in (" . implode(', ', $ng->getChildrensId()) . ")" : "") . "
                limit $start, $limit";
        $params = array(
            'query' => '%' . $query . '%',
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Получение истории цен поставщиков
     * @param $contractor_delivery_id - ID точки поставки
     * @return array
     */
    public function getHistory($contractor_delivery_id)
    {
        $sql = "select ns.*,
                  date_format(from_unixtime(ns.create_date), '%d.%m.%Y %H:%i') as create_date,
                  (select c.code from d_currency c where c.id = ns.currency_id) as currency_code,
                  (select u.name from user u where u.id = ns.user_id) as user_title,
                  (select u.title from d_unit u where u.id = ns.unit_id) as unit_title
                from d_nomenclature_supplier ns
                where ns.nomenclature_id = :nomenclature_id
                  and ns.contractor_delivery_id = :contractor_delivery_id
                order by ns.create_date desc";
        $params = array(
            'nomenclature_id' => $this->id,
            'contractor_delivery_id' => $contractor_delivery_id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * История отпускных цен
     * @param $type_price_id - ID типа цены
     * @return array
     */
    public function getHistoryPrice($type_price_id)
    {
        $sql = "select np.*,
                  date_format(np.change_date, '%d.%m.%Y %H:%i') as change_date,
                  (select c.code from d_currency c where c.id = np.currency_id) as currency_code,
                  (select u.name from user u where u.id = np.change_user_id) as change_user_name
                from h_nomenclature_price np
                where np.nomenclature_id = :nomenclature_id
                  and np.type_price_id = :type_price_id
                order by np.change_date desc";
        $params = array(
            'nomenclature_id' => $this->id,
            'type_price_id' => $type_price_id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

}

