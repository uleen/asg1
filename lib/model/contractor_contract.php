<?php

class Contractor_Contract extends AR
{
    static $table = 'd_contractor_contracts';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }


    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }
        if (!$this->doc_num) {
            $this->errors['doc_num'] = 'Поле "Номер договора" обязательно для заполнения';
        }
        if (!$this->doc_date) {
            $this->errors['doc_date'] = 'Поле "Дата договора" обязательно для заполнения';
        }
        if (!$this->action_from_date) {
            $this->errors['action_from_date'] = 'Поле "Период действия" обязательно для заполнения';
        }
        if (!$this->org_id) {
            $this->errors['org_id'] = 'Поле "Организация" обязательно для заполнения';
        }
        if (!$this->type) {
            $this->errors['type'] = 'Поле "Вид договора" обязательно для заполнения';
        }
        if (!$this->amount && !$this->is_frame) {
            $this->errors['amount'] = 'Поле "Сумма" обязательно для заполнения';
        }
        if (!$this->currency_id && !$this->is_frame) {
            $this->errors['currency_id'] = 'Поле "Валюта" обязательно для заполнения';
        }
        if (!$this->direction_sales_id) {
            $this->errors['direction_sales_id'] = 'Поле "Направление продаж" обязательно для заполнения';
        }
        if (!$this->type_payment_id) {
            $this->errors['type_payment_id'] = 'Поле "Вид расчетов" обязательно для заполнения';
        }

        return empty($this->errors);
    }


    /**
     * Переопределен поиск
     *
     * @param array $conditions - условия поиска
     * @param array $params - параметры поиска
     * @param $start - сдвиг выборки
     * @param $limit - количество возвращаемы записей
     * @return Sheet[] - массив объектов
     */
    public static function find($conditions = array(), $params = array(), $start = 0, $limit = 25)
    {
        $sql = "select sql_calc_found_rows cc.*, c.title as contractor, f.title as file_title, f.name as file_name,
                    (select dc.code from d_currency dc where dc.id = cc.currency_id) as currency,
                    (select dds.title from d_direction_sales dds where dds.id = cc.direction_sales_id) as direction_sales,
                    (select dc.title from d_company dc where dc.id = cc.org_id) as company,
                    case cc.type when 1 then 'с покупателем' when 2 then 'с поставщиком' end as type_title,
                    date_format(from_unixtime(cc.action_from_date), '%d.%m.%Y') as action_from_date,
                    date_format(from_unixtime(cc.action_to_date), '%d.%m.%Y') as action_to_date
                from d_contractor_contracts cc
                left join uploaded_files f on cc.file_id = f.id
                inner join d_contractor c on c.id = cc.contractor_id
                " . (!empty($conditions) ? " where " . implode(' and ', $conditions) : "") . "
                order by cc.title
                limit $start, $limit";
        return self::find_by_sql($sql, $params);
    }


    /**
     * Поиск записи по первичному ключу
     *
     * @param $val - значение ключа
     * @param string $field - первичный ключ
     * @return Company
     */
    public static function find_by_pk($val, $field = 'id')
    {
        $items = static::find(array('cc.' . $field . ' = :' . $field), array($field => $val));
        return empty($items) ? null : $items[0];
    }


    /**
     * Проверка актуальности договора
     *
     * @return bool
     */
    public function isActual()
    {
        if ($this->prolongation) {
            return true;
        }
        if (!$this->action_to_date) {
            return true;
        }
        if ($this->action_to_date > time()) {
            return true;
        }
        return false;
    }

}