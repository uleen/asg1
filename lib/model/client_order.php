<?php

class Client_Order extends AR
{
    static $table = 'client_order';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->contractor_id) {
            $this->errors['contractor_title'] = 'Поле "Наименование контрагента" обязательно для заполнения';
        }
        if (!$this->contact_personal_id) {
            $this->errors['contact_personal_id'] = 'Поле "Контактное лицо контрагента" обязательно для заполнения';
        }
        if (!$this->date_from) {
            $this->errors['date_from'] = 'Поле "Дата доставки" обязательно для заполнения';
        }
        if (!$this->date_to) {
            $this->errors['date_to'] = 'Поле "Дата доставки" обязательно для заполнения';
        }
        if (!$this->time_from) {
            $this->errors['time_from'] = 'Поле "Время доставки" обязательно для заполнения';
        }
        if (!$this->time_to) {
            $this->errors['time_to'] = 'Поле "Время доставки" обязательно для заполнения';
        }
        if (!$this->contractor_delivery_id) {
            $this->errors['contractor_delivery_id'] = 'Поле "Адрес доставки" обязательно для заполнения';
        }
        if (!$this->client_order_status_id) {
            $this->errors['client_order_status_id'] = 'Поле "Статус заказа" обязательно для заполнения';
        }
        if (!$this->incoming_call_id) {
            $this->errors['incoming_call'] = 'Поле "Вх. запрос" обязательно для заполнения';
        }

        return empty($this->errors);
    }


    /**
     * Переопределен поиск
     *
     * @param array $conditions - условия поиска
     * @param array $params - параметры поиска
     * @param $start - сдвиг выборки
     * @param $limit - количество возвращаемы записей
     * @return Client_Order[] - массив объектов
     */
    public static function find($conditions = array(), $params = array(), $start = 0, $limit = 25)
    {
        $sql = "select co.*, s.id as sheet_id, date_format(s.create_date, '%d.%m.%Y') as sheet_date,
                    co.contractor_contract_id, cc.title as contractor_contract_title,
                    date_format(co.date_from, '%d.%m.%Y') as date_from, date_format(co.date_to, '%d.%m.%Y') as date_to,
                    (select u.name from user u where u.id = co.create_user_id) as create_user_name,
                    c.title as contractor_title, c.inn as contractor_inn,
                    (select cp.fio from d_contact_personal cp where cp.id = co.contact_personal_id) as contact_personal_fio,
                    cd.address as contractor_delivery_address, cd.lat as contractor_delivery_lat, cd.lng as contractor_delivery_lng,
                    (select concat('№ ', ic.id ,' от ', date_format(from_unixtime(ic.call_date), '%d.%m.%Y')) from incoming_call_register ic where ic.id = co.incoming_call_id) as incoming_call,
                    (select os.title from d_client_order_status os where os.id = co.client_order_status_id) as client_order_status_title
                from client_order co
                inner join d_contractor c on c.id = co.contractor_id
                inner join d_contractor_delivery cd on cd.id = co.contractor_delivery_id
                left join d_contractor_contracts cc on cc.id = co.contractor_contract_id
                left join sheet s on s.id = co.sheet_id
                " . (!empty($conditions) ? " where " . implode(' and ', $conditions) : "") . "
                order by co.create_date desc
                limit $start, $limit";
        return parent::find_by_sql($sql, $params);
    }


    /**
     * Поиск записи по первичному ключу
     *
     * @param $val - значение ключа
     * @param string $field - первичный ключ
     * @return Client_Order
     */
    public static function find_by_pk($val, $field = 'id')
    {
        $items = static::find(array('co.' . $field . ' = :' . $field), array($field => $val));
        return empty($items) ? null : $items[0];
    }


    /**
     * Получение списка статусов
     */
    public static function getOrderStatuses()
    {
        $sql = "select * from d_client_order_status";
        $sth = db::get()->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Получение списка позиций
     */
    public function getPosition()
    {
        $sql = "select con.*, (con.volume * con.price) as cost, (con.volume * con.price * 18 / 118) as cost_nds,
                  n.title as nomenclature_title, n.article as nomenclature_article,
                  (select group_concat(concat(c.title_short, ':', nc.value) separator ', ')
                    from d_nomenclature_characteristic nc, d_characteristic c
                    where nc.characteristic_id = c.id
                        and nc.nomenclature_id = n.id
                    order by c.sort) as nomenclature_characteristics,
                  (select u.title from d_unit u where u.id = con.unit_id) as unit_title,
                  c.title as contractor_title, cd.address as contractor_delivery_address
                from client_order_nomenclature con
                inner join d_nomenclature n on n.id = con.nomenclature_id
                left join d_contractor_delivery cd on cd.id = con.contractor_delivery_id
                left join d_contractor c on c.id = cd.contractor_id
                where con.status = 1
                  and con.client_order_id = :client_order_id";
        $params = array(
            'client_order_id' => $this->id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Установка позиции заказа
     *
     * @param $nomenclature_id - ID номенклатуры
     * @param $contractor_delivery_id - ID точки поставки
     * @param $unit_id - ID ед. изм.
     * @param $volume - объем
     * @param $price - цена
     */
    public function setPosition($nomenclature_id, $contractor_delivery_id, $unit_id, $volume, $price)
    {
        $this->delPosition($nomenclature_id);

        $sql = "insert into client_order_nomenclature set
                  client_order_id = :client_order_id,
                  nomenclature_id = :nomenclature_id,
                  contractor_delivery_id = :contractor_delivery_id,
                  unit_id = :unit_id,
                  volume = :volume,
                  price = :price,
                  status = :status,
                  create_date = :create_date";
        $params = array(
            'client_order_id' => $this->id,
            'nomenclature_id' => $nomenclature_id,
            'contractor_delivery_id' => $contractor_delivery_id,
            'unit_id' => $unit_id,
            'volume' => $volume,
            'price' => $price,
            'status' => 1,
            'create_date' => date('Y-m-d H:i:s'),
        );
        db::get()->prepare($sql)->execute($params);
    }


    /**
     * Удаление позиции из заказа
     *
     * @param $nomenclature_id - ID номенклатуры
     */
    public function delPosition($nomenclature_id)
    {
        $sql = "update client_order_nomenclature set status = 2
                where client_order_id = :client_order_id
                    and nomenclature_id = :nomenclature_id";
        $params = array(
            'client_order_id' => $this->id,
            'nomenclature_id' => $nomenclature_id
        );
        db::get()->prepare($sql)->execute($params);
    }


    /**
     * Изменение статуса заказа
     *
     * @param $client_order_status_id - ID статуса клиенсткого заказа
     * @param null $client_order_status_note - Комментарий к изменению статуса
     * @param $user_id - ID пользователя, который меняет статус
     */
    public function changeStatus($client_order_status_id, $client_order_status_note = null, $user_id = null)
    {
        // если пользователь не указан, действие выполняется от системного пользователя
        if (!$user_id) {
            $user_id = 0;
        }

        // добавление записи в историю статусов
        $sql = "insert into client_order_status set
                  client_order_id = :client_order_id,
                  client_order_status_id = :client_order_status_id,
                  client_order_status_note = :client_order_status_note,
                  create_date = :create_date,
                  user_id = :user_id";
        $params = array(
            'client_order_id' => $this->id,
            'client_order_status_id' => $client_order_status_id,
            'client_order_status_note' => $client_order_status_note,
            'create_date' => date('Y-m-d H:i:s'),
            'user_id' => $user_id,
        );
        db::get()->prepare($sql)->execute($params);

        // сохранение текущего статуса
        $sql = "update client_order set
                  client_order_status_id = :client_order_status_id,
                  client_order_status_note = :client_order_status_note
                where id = :client_order_id";
        $params = array(
            'client_order_id' => $this->id,
            'client_order_status_id' => $client_order_status_id,
            'client_order_status_note' => $client_order_status_note,
        );
        db::get()->prepare($sql)->execute($params);
    }


    /**
     * Получение истории изменения статусов
     *
     * @return array
     */
    public function getStatusHistory()
    {
        $sql = "select cs.*,
                  (select s.title from d_client_order_status s where s.id = cs.client_order_status_id) as client_order_status_title,
                  (select u.name from user u where u.id = cs.user_id) as user_name
                from client_order_status cs
                where cs.client_order_id = :client_order_id
                order by cs.create_date";
        $params = array(
            'client_order_id' => $this->id,
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}

