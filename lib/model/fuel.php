<?php

class Fuel extends AR
{
    static $table = 'd_fuel';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }
        if (!$this->nomenclature_id) {
            $this->errors['nomenclature_title'] = 'Поле "Номенклатура" обязательно для заполнения';
        }

        return empty($this->errors);
    }


    /**
     * Переопределен поиск
     *
     * @param array $conditions - условия поиска
     * @param array $params - параметры поиска
     * @return Fuel[] - массив объектов
     */
    public static function find($conditions = array(), $params = array())
    {
        $sql = "select f.*,
                    (select n.title from d_nomenclature n where n.id = f.nomenclature_id) as nomenclature_title
                from d_fuel f
                " . (!empty($conditions) ? " where " . implode(' and ', $conditions) : "");
        return parent::find_by_sql($sql, $params);
    }

}