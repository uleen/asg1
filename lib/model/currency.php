<?php

class Currency extends AR
{
    static $table = 'd_currency';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }


    /**
     * Список последних цен поставщика
     * @return array
     */
    public function getNomenclature()
    {
        $sql = "select ns.*, cd.address as contractor_delivery_address,
                  n.article as nomenclature_article, n.title as nomenclature_title,
                  ng.title as nomenclature_group_title
                from d_nomenclature_supplier ns
                inner join d_contractor_delivery cd
                  on ns.contractor_delivery_id = cd.id
                    and cd.contractor_id = :contractor_id
                inner join (select max(ns.create_date) as create_date, ns.nomenclature_id, ns.contractor_delivery_id
                            from d_nomenclature_supplier ns
                            group by ns.nomenclature_id, ns.contractor_delivery_id) lp
                  using (create_date, nomenclature_id, contractor_delivery_id)
                inner join d_nomenclature n on ns.nomenclature_id = n.id
                inner join d_direction_sales ng on n.nomenclature_group_id = ng.id";
        $sth = db::get()->prepare($sql);
        $sth->execute(array('contractor_id' => $this->id));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Список всех валют достпных для выбора (по которым ЦБ дает курсы)
     * @return array
     */
    public static function getAll()
    {
        $sql = "select distinct cr.num, cr.code, cr.num as id
                from d_currency_rate cr
                order by cr.code";
        $sth = db::get()->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Получение истории изменения курса
     * @param $num - код валюты
     * @return array
     */
    public static function getHistoryRate($num)
    {
        $sql = "select date_format(cr.cdate, '%d.%m.%Y') as cdate, cr.rate, cr.nom
                from d_currency_rate cr
                where cr.num = :num
                order by cr.cdate desc
                limit 100";
        $params = array(
            'num' => $num
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

}