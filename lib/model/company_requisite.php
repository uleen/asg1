<?php

class Company_Requisite extends AR
{
    static $table = 'd_company_requisites';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }
        if (!$this->currency_id) {
            $this->errors['currency_id'] = 'Поле "Валюта" обязательно для заполнения';
        }
        if (!$this->bank_bik) {
            $this->errors['bank_bik'] = 'Поле "БИК банка" обязательно для заполнения';
        }
        if (!$this->bank_title) {
            $this->errors['bank_title'] = 'Поле "Название банка" обязательно для заполнения';
        }
        if (!$this->number) {
            $this->errors['number'] = 'Поле "Номер счета" обязательно для заполнения';
        }
        if (!$this->type) {
            $this->errors['type'] = 'Поле "Вид счета" обязательно для заполнения';
        }

        return empty($this->errors);
    }

}

