<?php

class Sheet_Status extends AR
{
    static $table = 'd_sheet_status';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'не указан заголовок';
        }

        return empty($this->errors);
    }

}