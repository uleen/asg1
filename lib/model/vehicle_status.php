<?php

class Vehicle_Status extends AR
{
    static $table = 'd_vehicle_status';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }

        return empty($this->errors);
    }

}

