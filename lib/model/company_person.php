<?php

class Company_Person extends AR
{
    static $table = 'd_company_person';

    public function __get($key)
    {
        // сокращенное ФИО (им.п.)
        if ($key == 'fio_short_ip') {
            return preg_replace('/(\S*) (\S{1})\S* (\S{1})\S*/iu', '\\1 \\2. \\3.', $this->fio_ip);
        }

        return parent::__get($key);
    }

}

