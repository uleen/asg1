<?php

class Company extends AR
{
    static $table = 'd_company';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    public function __get($key)
    {
        // руководитель
        if ($key == 'chief') {
            if (empty($this->_chief)) {
                $this->_chief = $this->getPerson('chief');
            }
            return $this->_chief;
        }

        // главный бухгалтер
        if ($key == 'booker') {
            if (empty($this->_booker)) {
                $this->_booker = $this->getPerson('booker');
            }
            return $this->_booker;
        }

        return parent::__get($key);
    }


    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'не указан заголовок';
        }

        return empty($this->errors);
    }


    /**
     * Переопределен поиск
     * @param array|null $conditions - условия поиска
     * @param array|null $params - параметры поиска
     * @return array - массив объектов
     */
    public static function find($conditions = array(), $params = array())
    {
        $sql = "select c.*, f.title as file_title, f.name as file_name
                from d_company c
                left join uploaded_files f on c.logo_file_id = f.id";
        if (!empty($conditions)) {
            $sql .= " where " . implode(' and ', $conditions);
        }
        return self::find_by_sql($sql, $params);
    }


    /**
     * Поиск записи по первичному ключу
     *
     * @param $val - значение ключа
     * @param string $field - первичный ключ
     * @return Company
     */
    public static function find_by_pk($val, $field = 'id')
    {
        $items = static::find(array('c.' . $field . ' = :' . $field), array($field => $val));
        return empty($items) ? null : $items[0];
    }


    /**
     * Получение данных ответственного лица
     * @param $type
     * @return array
     */
    public function getPerson($type)
    {
        $sql = "select cp.*, f.title as file_title, f.name as file_name,
                  date_format(cp.date_from, '%d.%m.%Y') as date_from,
                  date_format(cp.date_to, '%d.%m.%Y') as date_to
                from d_company_person cp
                left join uploaded_files f on cp.scan_file_id = f.id
                where cp.company_id = :company_id
                  and cp.type = :type";
        $params = array(
            'company_id' => $this->id,
            'type' => $type,
        );
        $items = Company_Person::find_by_sql($sql, $params);
        if (!empty($items)) {
            $person = $items[0];
        } else {
            $person = new Company_Person;
            $person->id = null;
            $person->company_id = $this->id;
            $person->type = $type;
        }
        return $person;
    }


    /**
     * Сохранение данных ответственного лица
     * @param $type
     * @param $data
     */
    public function setPerson($type, $data)
    {
        $sql = "replace into d_company_person set
                  company_id = :company_id,
                  type = :type,
                  fio_ip = :fio_ip,
                  fio_rp = :fio_rp,
                  post_ip = :post_ip,
                  post_rp = :post_rp,
                  reason = :reason,
                  date_from = :date_from,
                  date_to = :date_to,
                  title = :title,
                  scan_file_id = :scan_file_id";
        $params = array(
            'company_id' => $this->id,
            'type' => $type,
            'fio_ip' => $data['fio_ip'],
            'fio_rp' => $data['fio_rp'],
            'post_ip' => $data['post_ip'],
            'post_rp' => $data['post_rp'],
            'reason' => $data['reason'],
            'date_from' => $data['date_from'],
            'date_to' => $data['date_to'],
            'title' => $data['title'],
            'scan_file_id' => $data['scan_file_id'],
        );
        $sth = db::get()->prepare($sql);
        $sth->execute($params);
    }


    /**
     * Получение данных реквизитов
     *
     * @return array
     */
    public function getRequisites()
    {
        $items = array();
        $conditions = array(
            'company_id = :company_id',
        );
        $params = array(
            'company_id' => $this->id,
        );
        foreach (Company_Requisite::find($conditions, $params) as $companyRequisite) {
            $items[] = $companyRequisite->getData();
        }
        return $items;
    }

}

