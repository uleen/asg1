<?php

class Contractor extends AR
{
    static $table = 'd_contractor';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }


    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }
        if (!$this->inn && $this->type != 2) {
            $this->errors['inn'] = 'Поле "ИНН" обязательно для заполнения';
        } else {
            if (($this->type == 1 && strlen($this->inn) == 10) || strlen($this->inn) == 12) {
                if (!is_valid_inn($this->inn)) {
                    $this->errors['inn'] = 'ИНН указан не правильно';
                } else if (checkUniqInn($this->inn, $this->org_type)) {
                    $this->errors['inn'] = $errors['inn_alert'] = ($this->org_type == 1 ? 'Клиент' : 'Поставщик') . ' c таким ИНН уже существует';
                }
            } elseif ($this->inn && $this->type != 2) {
                $this->errors['inn'] = 'Поле "ИНН" - 12 знаков для физ.лиц и ИП или 10 знаков для юр.лиц.';
            }
        }
        if ($this->type == 1 && !$this->kpp) {
            $this->errors['kpp'] = 'Поле "КПП" обязательно для заполнения.';
        }
        if ($this->type == 1 && !$this->okpfo_type) {
            $this->errors['okpfo_type'] = 'Поле "ОКПО" обязательно для заполнения.';
        }
        if (!$this->manager_id) {
            $this->errors['manager_title'] = 'Поле "Ответственный менеджер" обязательно для заполнения';
        }

        return empty($this->errors);
    }


    /**
     * Список последних цен поставщика
     * @return array
     */
    public function getNomenclature()
    {
        $sql = "select ns.*, cd.address as contractor_delivery_address,
                  n.article as nomenclature_article, n.title as nomenclature_title,
                  ng.title as nomenclature_group_title,
                  (select c.code from d_currency c where c.id = ns.currency_id) as currency_code,
                  (select c.title from d_contractor c where c.id = cd.contractor_id) as contractor_title,
                  (select u.title from d_unit u where u.id = ns.unit_id) as unit_title
                from d_nomenclature_supplier ns
                inner join d_contractor_delivery cd
                  on ns.contractor_delivery_id = cd.id
                    and cd.contractor_id = :contractor_id
                inner join (select max(ns.create_date) as create_date, ns.nomenclature_id, ns.contractor_delivery_id
                            from d_nomenclature_supplier ns
                            group by ns.nomenclature_id, ns.contractor_delivery_id) lp
                  using (create_date, nomenclature_id, contractor_delivery_id)
                inner join d_nomenclature n on ns.nomenclature_id = n.id
                inner join d_direction_sales ng on n.nomenclature_group_id = ng.id";
        $sth = db::get()->prepare($sql);
        $sth->execute(array('contractor_id' => $this->id));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}