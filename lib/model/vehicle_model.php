<?php

class Vehicle_Model extends AR
{
    static $table = 'd_vehicle_model';

    public function __construct($data = array())
    {
        parent::__construct($data);
    }

    /**
     * Переопределена валидация объекта
     */
    public function validate()
    {
        $this->errors = array();

        if (!$this->title) {
            $this->errors['title'] = 'Поле "Наименование" обязательно для заполнения';
        }

        return empty($this->errors);
    }

	/**
	 * Переопределения метода поиска по первичному ключу
	 *
	 * @param $val				значение ключа
	 * @param string $field		первичный ключ
	 * @return null
	 */
	public static function find_by_pk($val, $field = 'id')
	{

		$sql = "select d.*, f.title as fuel_title
            from d_vehicle_model d
            left join d_fuel f on d.fuel_id = f.id
            where d.id = :id";
		$items = self::find_by_sql($sql, array('id' => $val));
		return empty($items) ? null : $items[0];
	}

}

