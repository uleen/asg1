Ext.Loader.setPath('Ext.ux', '/ux');
Ext.Loader.setConfig({
    enabled: true
});

Ext.define('App.Global', {
    statics: {
        user_id: 0,
        user_name: '',
        access: [],
        checkRights: function(right){
        	if (App.Global.access['rights'][right] != undefined && App.Global.access['rights'][right]==1){
        		return true;
        	}
        	return false;
        }
    }
});
var store = Ext.create('app.store.position').load();
Ext.define('App.Global.Collapse', {
	statics: {
		collapsedWidth: 150,
		position: 0,
		positionFree: [],
		collapseWidth: function (obj) {
			obj.toggleCollapse();
			if (obj.getCollapsed()) {
				if (store.find('name', obj.title) == -1) {
					if (this.positionFree.length == 0) {
						store.add({name: obj.title, winWidth: obj.getWidth(), position: this.position});
						this.position++;
					} else {
						this.positionFree.sort();
						store.add({name: obj.title, winWidth: obj.getWidth(), position: this.positionFree[0]});
						this.positionFree.shift();
					}

				}
				obj.setWidth(this.collapsedWidth);
				var currentPosition = store.findRecord('name', obj.title).get('position');
				obj.alignTo(Ext.getBody(), 'bl-bl', [currentPosition * this.collapsedWidth, 0]);
			} else {
				obj.setWidth(store.findRecord('name', obj.title).get('winWidth'));
				obj.center();
			}
		},
		collapseWidthClose: function (obj) {
			if (store.find('name', obj.title) !== -1) {
				this.positionFree.push(store.findRecord('name', obj.title).get('position'));
				store.remove(store.findRecord('name', obj.title));
			}

		}
	}
});

Ext.application({
    name: 'app',
    controllers: [
        'Main',
        'reclama',
        'company',
        'company_requisite',
        'currency',
        'okved',
        'okei',
        'fias',
        'reference_scope',
        'contact_personal',
        'incoming_call',
        'phone_call',
        'address',
        'direction_sales',
        'nomenclature_group',
        'customer_category',
        'cagent',
        'nomenclature',
        'contractor_requisites',
        'contractor_contracts',
        'contractor_delivery',
        'okpfo_type',
        'type_payment',
        'type_price',
        'unit',
        'client_order',
        'sheet'
    ],
    requires: [
        'Ext.ux.InputTextMask',
        'app.view.element.price_field',
        'app.view.element.user_field',
        'app.view.element.address_field'
    ],
    appFolder: 'app',
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'main',
                    width: '100%',
                    height: '100%'
                }
            ]
        });

        Ext.Ajax.request({
            url: '/get_user_data',
            async: false,
            success: function (result, request) {
                user = JSON.parse(result.responseText);
                App.Global.user_id = user.id;
                App.Global.user_name = user.name;
                App.Global.access = user.rights;
                Ext.getCmp('user-name-toolbar').setText('Пользователь: <b>'+App.Global.user_name+'</b>');
            }
        });
    },
});
