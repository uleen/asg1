Ext.define('app.view.vehicle.form.menu', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.vehicle.form.menu',
    rootVisible: false,
    initComponent: function () {
        this.store = Ext.create('Ext.data.TreeStore', {
            root: {
                text: 'Все разделы',
                expanded: true,
                children: [
                    {text: "Основное", leaf: true, iconCls: 'icon_tipper', formId: 'vehicle-form-main'},
                    {text: "Оборудование", leaf: true, iconCls: 'icon_gps_handheld', formId: 'vehicle-grid-equipment'},
                    {text: "Пропуска", leaf: true, iconCls: 'icon_ssl_certificates', formId: 'vehicle-grid-permit'},
                    {text: "Водители", leaf: true, iconCls: 'icon_user_pilot', formId: 'vehicle-grid-driver'}
                ]
            }
        });
        this.callParent(arguments);
    }
});