Ext.define('app.view.vehicle.form.main', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.vehicle.form.main',
	layout: 'hbox',
    html: 'основные данные автомобиля',
    items: [
		{
			xtype: 'panel',
			border: 0,
			flex: 1,
			items: [
				{
					xtype: 'fieldset',
					title: 'Основное',
					margin: 10,
					padding: '5 10',
					items: [
						{
							xtype: 'fieldcontainer',
							layout: 'vbox',
							items: [
								{
									xtype: 'hiddenfield',
									name: 'id',
									readOnly: true
								},
								{
									xtype: 'combobox',
									fieldLabel: 'Модель',
									name: 'vehicle_model_title',
									labelWidth: 130,
									editable: false,
									store: Ext.create('app.store.dictionary.vehicle_model'),
									displayField: 'title',
									listeners: {
										select: function (combo, records) {
											var rVehicleModel = records[0];
											this.up('form').getForm().setValues({
												vehicle_model_id: rVehicleModel.get('id'),
												vehicle_status_id: rVehicleModel.get('status')
											});
										}
									}
								},
								{
									xtype: 'hidden',
									name: 'vehicle_model_id'
								},
								{
									xtype: 'hidden',
									name: 'vehicle_status_id'
								},
								{
									xtype: 'textfield',
									name: 'reg_number',
									fieldLabel: 'Гос.номер',
									labelWidth: 130
								},
								{
									xtype: 'numberfield',
									name: 'year',
									value: 1990,
									minValue: 1990,
									maxValue: 2100,
									fieldLabel: 'Год выпуска',
									labelWidth: 130
								}
							]
						}
					]
				},
				{
					xtype: 'fieldset',
					title: 'Приобретение',
					margin: 10,
					padding: '5 10',
					items: [
						{
							xtype: 'panel',
							border: 0,
							margin: '0 0 10 0',
							layout: {
								type: 'vbox',
								align: 'stretch'
							},
							items: [
								{
									xtype: 'datefield',
									name: 'buy_date',
									fieldLabel: 'Дата приобретения',
									labelWidth: 130
								},
								{
									xtype: 'numberfield',
									name: 'buy_price',
									fieldLabel: 'Цена',
									labelWidth: 130
								},
								{
									xtype: 'numberfield',
									name: 'buy_km',
									fieldLabel: 'Пробег',
									labelWidth: 130
								}
							]
						}
					]
				}
			]
		},
		{
			xtype: 'panel',
			border: 0,
			flex: 1,
			items: [
				{
					xtype: 'fieldset',
					title: 'ПТС',
					margin: 10,
					padding: '5 10',
					items: [
						{
							xtype: 'fieldcontainer',
							layout: 'vbox',
							items: [
								{
									xtype: 'textfield',
									name: 'pts_series',
									fieldLabel: 'Серия',
									labelWidth: 130
								},
								{
									xtype: 'textfield',
									name: 'pts_number',
									fieldLabel: 'Номер',
									labelWidth: 130
								},
								{
									xtype: 'datefield',
									name: 'pts_date',
									fieldLabel: 'Дата выдачи',
									labelWidth: 130
								},
								{
									xtype: 'textfield',
									name: 'vin',
									fieldLabel: 'VIN',
									labelWidth: 130
								}
							]
						}
					]
				},
				{
					xtype: 'fieldset',
					title: 'Продажа',
					margin: 10,
					padding: '5 10',
					items: [
						{
							xtype: 'fieldcontainer',
							layout: 'vbox',
							items: [
								{
									xtype: 'datefield',
									name: 'sell_date',
									fieldLabel: 'Дата продажи',
									labelWidth: 130
								},
								{
									xtype: 'numberfield',
									name: 'sell_price',
									fieldLabel: 'Цена',
									labelWidth: 130
								},
								{
									xtype: 'numberfield',
									name: 'sell_km',
									fieldLabel: 'Пробег',
									labelWidth: 130
								}
							]
						}
					]
				}
			]
		}
	],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
			itemId: 'buttons',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});