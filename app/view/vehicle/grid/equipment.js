Ext.define('app.view.vehicle.grid.equipment', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.vehicle.grid.equipment',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.vehicle.equipment');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                itemId: 'top',
                dock: 'top',
                items: [
                    {
                        text: 'Добавить',
                        itemId: 'add',
                        iconCls: 'icon_add',
                        padding: 5
                    }
                ]
            }
        ];
        this.callParent(arguments);
    },
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});