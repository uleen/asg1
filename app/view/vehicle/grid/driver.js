Ext.define('app.view.vehicle.grid.driver', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.vehicle.grid.driver',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.vehicle.driver');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                itemId: 'top',
                dock: 'top',
                items: [
                    {
                        text: 'Добавить',
                        itemId: 'add',
                        iconCls: 'icon_add',
                        padding: 5
                    }
                ]
            }
        ];
        this.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});