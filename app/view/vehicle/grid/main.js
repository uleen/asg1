Ext.define('app.view.vehicle.grid.main', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.vehicle.grid.main',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Название',
            dataIndex: 'reg_number',
            flex: 1
        },
		{
			xtype: 'actioncolumn',
			width: 30,
			sortable: false,
			align: 'center',
			action: 'edit',
			tooltip: 'Редактировать',
			iconCls: 'icon_pencil',
			handler: function (grid, row) {
				var rVehicle = grid.getStore().getAt(row);
				var cVehicle = new app.controller.vehicle;
				cVehicle.gridReload = grid;
				cVehicle.open(rVehicle.get('id'));
			}
		},
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            handler: function (grid, row) {
                var record = grid.getStore().getAt(row);
                record.set('vehicle_status_id', record.get('vehicle_status_id') == 1 ? 2 : 1);
            },
            getClass: function (v, meta, rec) {
                if (rec.get('vehicle_status_id') == 1) {
                    this.tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('vehicle_status_id') == 2) {
                    this.tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.vehicle.main');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                itemId: 'top',
                dock: 'top',
                items: [
                    {
                        text: 'Добавить',
                        itemId: 'add',
                        iconCls: 'icon_add',
                        padding: 5
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ];
        this.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});