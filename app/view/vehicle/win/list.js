Ext.define('app.view.vehicle.win.list', {
    extend: 'Ext.window.Window',
    alias: 'widget.vehicle.win.list',
    title: 'Транспортные средства',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'vehicle.grid.main',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});