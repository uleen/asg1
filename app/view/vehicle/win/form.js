Ext.define('app.view.vehicle.win.form', {
    extend: 'Ext.window.Window',
    alias: 'widget.vehicle.win.form',
    title: 'Транспортное средство',
    layout: 'border',
    height: 500,
    width: 900,
    minimizable: true,
    items: [
        {
            xtype: 'vehicle.form.menu',
            itemId: 'menu',
            region: 'west',
            width: 170,
            padding: 1
        },
        {
            xtype: 'panel',
            itemId: 'form',
            region: 'center',
			width: 730,
            padding: 1,
            border: 0,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'vehicle.form.main',
                    itemId: 'vehicle-form-main',
                    flex: 1,
                    hidden: false
                },
                {
                    xtype: 'vehicle.grid.equipment',
                    itemId: 'vehicle-grid-equipment',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'vehicle.grid.permit',
                    itemId: 'vehicle-grid-permit',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'vehicle.grid.driver',
                    itemId: 'vehicle-grid-driver',
                    flex: 1,
                    hidden: true
                }
            ]
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});