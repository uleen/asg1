Ext.define('app.view.form.incoming_call_edit', {
    id: 'formIncomingCalls',
    extend: 'Ext.window.Window',
    alias: 'widget.formIncomingCallEdit',
    title: 'Входящий запрос',
    layout: 'fit',
    modal: true,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    flex: 1,
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Данные запроса',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            labelWidth: 130,
                                            name: 'call_date',
                                            width: 255,
                                            padding: '0 20 0 0',
                                            readOnly: true,
                                            fieldLabel: 'Дата и время звонка'
                                        },
                                        {
                                            xtype: 'combobox',
                                            labelWidth: 60,
                                            name: 'incoming_source_id',
                                            width: 200,
                                            fieldLabel: 'Источник',
                                            editable: false,
                                            displayField: 'title',
                                            valueField: 'id',
                                            store: Ext.create('Ext.data.Store', {
                                                fields: ['id', 'title'],
                                                data: [
                                                    {id: 1, title: 'Телефон'},
                                                    {id: 2, title: 'E-mail'},
                                                    {id: 3, title: 'Сайт'}
                                                ]
                                            })
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            border: 0,
                            padding: '0 0',
                            margin: '0 10',
                            //width:475,
                            layout: {
                                type: 'hbox',
                                align: 'left'
                            },
                            
                         
                            items: [
                              
                                {
                                    xtype: 'label',
                                    id: 'incoming-call-is-new',
                                    text: 'Новый запрос',
                                    baseCls: 'incoming-call-is-new',
                                    margin: '5 0 0 397'
                                }
                                
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                        {
                                            labelWidth: 110,
                                            fieldLabel: 'Наименование',                          
                                            xtype: 'textfield',
                                            name: 'company_title',
                                            width: 457,                             
                                            readOnly: true,
                                        },
                                        {
                                            xtype: 'label',
                                            id: 'label-company-title-is-find',
                                            html: '<img src="/icons/16x16/client_account_template.png">',
                                            padding: 3,
                                            hidden: true
                                        },
                                        {
                                            xtype: 'label',
                                            id: 'label-company-title-is-not-find',
                                            html: '<img src="/icons/16x16/error.png">',
                                            padding: 3,
                                            hidden: true
                                        }
                                    ]
                                },
                                {
                                    labelWidth: 110,
                                    xtype: 'textfield',
                                    name: 'cagent_name',
                                    id: 'conctact-personal-id',
                                    fieldLabel: 'Контактное лицо',
                                    readOnly: true,
                                    width: 475,
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'cagent_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contact_personal_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'is_new'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'root_id'
                                }, 
                                {
                                	xtype: 'hidden',
                                	name: 'contractor_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'from_search'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'icr_id',
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'org_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'cagent_old_name'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'change_cp'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'author_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'executed'
                                },
                                
                                {
                                	xtype: 'hidden',
                                	name: 'x_responsible_user_id'
                                },
                                
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                        {
                                            labelWidth: 110,
                                            name: 'cagent_phone',
                                            width: 255,
                                            padding: '0 20 0 0',
                                            fieldLabel: 'Телефон',
                                            readOnly: true,
                                            
                                        },
                                        {
                                            labelWidth: 40,
                                            name: 'cagent_email',
                                            width: 200,
                                            fieldLabel: 'E-mail',
                                            vtype: 'email',
                                            readOnly: true,
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                            {
                                            	 xtype: 'button',
                                                 text: 'История запросов',
                                                 action: 'showHistoryIncomingCall'
                                            },
                                           
                                            {
                                                labelWidth: 40,
                                                name: 'cagent_add_phone',
                                                width: 100,
                                                align: 'right',
                                                padding: '0 0 0 50',
                                                fieldLabel: 'Доп',
                                                xtype: 'textfield',
                                                readOnly: true,
                                            },
                                    ]
                                },
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            id: 'incoming-call-cagent-info',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'Тип',
                                    id: 'company-type-radios',
                                    items: [
                                        {boxLabel: 'Юр. лицо', name: 'company_type', inputValue: '1'},
                                        {boxLabel: 'Физ. лицо', name: 'company_type', inputValue: '2'},
                                        {boxLabel: 'ИП', name: 'company_type', inputValue: '3'}
                                    ]
                                },
                                {
                                    labelWidth: 110,
                                    xtype: 'triggerSelectReferenceScope',
                                    name: 'reference_scope',
                                    fieldLabel: 'Сфера деят.',
                                    width: 475
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'reference_scope_id'
                                },
                                {
                                    xtype: 'panel',
                                    border: 0,
                                    margin: '0 0 10 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            labelWidth: 110,
                                            xtype: 'textfield',
                                            name: 'company_fias_title',
                                            fieldLabel: 'Факт. адрес',
                                            readOnly: true,
                                            baseCls: 'read-only',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'hidden',
                                            name: 'company_fias_id'
                                        }
                                        ,
                                        {
                                            xtype: 'button',
                                            text: '...',
                                            margin: '0 0 0 2',
                                            handler: function () {
                                                var form = this.up('form');
                                                var winSA = Ext.create('widget.formSelectAddress');
                                                winSA.down('form').getForm().setValues({
                                                    address: form.getForm().getValues().company_fias_title,
                                                    fias_id: form.getForm().getValues().company_fias_id
                                                });
                                                winSA.down('button').on({click: function () {
                                                    form.getForm().setValues({
                                                        company_fias_title: winSA.down('form').getForm().getValues().address,
                                                        company_fias_id: winSA.down('form').getForm().getValues().fias_id
                                                    });
                                                    winSA.close();
                                                }});
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            border: 0,
                            padding: '0 10',
                            margin: '10',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Н/А',
                                    name: 'na',
                                    inputValue: 1
                                },
                                {
                                    xtype: 'textfield',
                                    labelWidth: 40,
                                    name: 'author_name',
                                    width: 200,
                                    padding: '0 0 0 50',
                                    readOnly: true,
                                    baseCls: 'read-only',
                                    fieldLabel: 'Автор',
                                    id: 'id-author-name'

                                },
                                {
                                    xtype: 'textfield',
                                    labelWidth: 60,
                                    name: 'save_date',
                                    width: 180,
                                    padding: '0 0 0 20',
                                    readOnly: true,
                                    baseCls: 'read-only',
                                    fieldLabel: 'Вр. сохр.'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: 0,
                    flex: 1,
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Что интересует клиента',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [
                                {
                                    xtype: 'panel',
                                    border: 0,
                                    margin: '0 0 10 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            labelWidth: 70,
                                            xtype: 'combobox',
                                            name: 'client_interest_srv',
                                            id: 'incoming-call-client-interest-srv',
                                            fieldLabel: 'Услуга',
                                            width: 375,
                                            editable: false,
                                            multiSelect: true,
                                            store: 'direction_sales',
                                            displayField: 'title',
                                            valueField: 'title',
                                            fieldCls: 'field-required',
                                            invalidCls: 'field-required-invalid',
                                            listConfig: {
                                                getInnerTpl: function () {
                                                    return '<div class="x-combo-list-item"><img src="' + Ext.BLANK_IMAGE_URL + '" class="chkCombo-default-icon chkCombo" width="16" /> {title}</div>';
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'textarea',
                                    name: 'client_interest',
                                    width: 375,
                                    height: 70,
                                    emptyText: 'Комментарий к запросу',
                                    allowBlank: false,
                                    fieldCls: 'field-required',
                                    invalidCls: 'field-required-invalid'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Источник обращения',
                            id: 'incoming-call-source',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [
                                {
                                    labelWidth: 70,
                                    xtype: 'combobox',
                                    name: 'reclama_id',
                                    fieldLabel: 'Реклама',
                                    width: 375,
                                    editable: false,
                                    store: 'reclama',
                                    displayField: 'title',
                                    valueField: 'id',
                                    fieldCls: 'field-required',
                                    invalidCls: 'field-required-invalid',
                                    listeners: {
                                        select: function (combo, rec) {
                                            if (rec[0].data.title == 'Рекомендация Клиента вашей Компании') {
                                                Ext.getCmp('field-ref-cagent').show();
                                            } else {
                                                Ext.getCmp('field-ref-cagent').hide();
                                            }
                                            if (rec[0].data.title == 'Сотрудник Компании') {
                                                Ext.getCmp('field-ref-user').show();
                                            } else {
                                                Ext.getCmp('field-ref-user').hide();
                                            }
                                        }
                                    },
                                    validator: function (val) {
                                        if (!val && this.up('form').getForm().getValues().is_new != 2) {
                                            return 'Поле "Реклама" обязательно для заполнения';
                                        } else {
                                            return true;
                                        }
                                    }
                                },
                                {
                                    labelWidth: 70,
                                    xtype: 'triggerfield',
                                    id: 'field-ref-cagent',
                                    name: 'title',
                                    hidden: true,
                                    fieldLabel: 'Контрагент',
                                    width: 375,
                                    editable: false,
                                    allowBlank: false,
                                    fieldCls: 'field-required',
                                    invalidCls: 'field-required-invalid',
                                    onTriggerClick: function () {
                                        var form = this.up('form').getForm();
                                        var win = Ext.create('widget.winContactractorList');                                   	 
                                        var store = Ext.StoreManager.get('cagent');
                                        store.clearFilter();                                        
                                        store.getProxy().extraParams = {status: 1};
                                        store.load();
                                        win.show();
                                        // обработка двойного клика в гриде
                                        win.down('grid').on(
                                            'itemdblclick', function(grid, record) {
                                            	console.log(record);
                                                form.setValues({
                                                    //ref_cagent_id: record.get('id')
                                                    ref_cagent_id: record.get('contractor_id'),

                                                });
                                                Ext.getCmp('field-ref-cagent').setValue(record.get('title'));
                                                grid.up('window').close();
                                            }
                                        );
                                    }
                                },
                                {
                                	xtype: 'combobox',
                                    id: 'field-ref-user',
                                    name: 'ref_user_id',
                                    hidden: true,
                                	typeAhead:true,
                                    fieldLabel: 'Сотрудник',
                                    width: 375,
                                    store: Ext.create('app.store.user.main'),
                                    labelWidth: 70,
                                    editable: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    minChars: 2,
                                    fieldCls: 'field-required',
                                    invalidCls: 'field-required-invalid'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ref_user_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ref_cagent_id'
                                },
                                {
                                    xtype: 'textarea',
                                    name: 'reclama_interest',
                                    width: 375,
                                    height: 40,
                                    emptyText: 'Комментарий к источнику информации'
                                }
                            ]
                        },
                        {
                        	xtype: 'fieldset',
                            id: 'incoming-call-responsible',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [
                            	
                                {
                                    xtype: 'panel',
                                    border: 0,
                                    padding: '0 0',
                                    margin: '2',
                                    layout: {
                                        type: 'vbox',
                                        align: 'left', 
                                        pack: 'left', 
                                       
                                    },
                                    items: [
                                            {     
                                               	xtype: 'combobox',
                                                id: 'field-responsible-user',
                                                name: 'responsible_user_id',
                                            	typeAhead:true,
                                                fieldLabel: 'Ответственный',
                                                width: 375,
                                                store: Ext.create('app.store.user.can_auth'),
                                                labelWidth: 100,
                                                editable: true,
                                                allowBlank: false,
                                                displayField: 'name',
                                                valueField: 'id',
                                                fieldCls: 'field-required',
                                                invalidCls: 'field-required-invalid',
                                                minChars: 2,
                                                listeners: {
                                                	select: function(fi, records, opts)
                                             	    {
                                                		 var form = this.up('form').getForm();
                                                		var author_id = this.up('form').getForm().getValues().author_id;
                                                		var responsible_user_id = records[0].get('id');
                                                		if (author_id != responsible_user_id) {
                                                	    	form.setValues({
                                                	    		x_responsible_user_id: 0,
                                                	    	});
                                                			Ext.getCmp('checkbox-executed_by_author').setValue(false);
                                                		}
                                                		else {
                                                			Ext.getCmp('checkbox-executed_by_author').setValue(true);
                                                			
                                                		}

                                             	    }
        	                                      },
        	                                      validator: function (val) {
        	                                            if (!val && this.up('form').getForm().getValues().responsible_user == '') {
        	                                                return 'Поле "Ответственный" обязательно для заполнения';
        	                                            } else {
        	                                                return true;
        	                                            }
        	                                        },

                                               },
                                       
                                       {
                                    	   xtype: 'panel',
                                           border: 0,
                                           padding: '0 0',
                                           margin: '0',
                                           layout: {
                                               type: 'hbox',
                                               align: 'stretch',
                                           },
                                           items:[
   		                                       {
		                                           xtype: 'checkbox',
		                                           boxLabel: 'Совпадает с автором',
		                                           name: 'executed_by_author',
		                                           id: 'checkbox-executed_by_author',
		                                           inputValue: 1,
		                                           align: 'left',
		                                           listeners: {
		                                        	   change: function(checkbox, value)
		                                        	   {		                                       
		                                        		    var form = this.up('form').getForm();
			                                        	    if (value) {
			                                        	    	var author = Ext.getCmp('id-author-name').getValue();
			                                        	    	var author_id = form.getValues().author_id;
			                                        	    	form.setValues({
			                                        	    		responsible_user_id: author_id,
			                                        	    		responsible_user: author,
			                                        	    		x_responsible_user_id: author_id,
			                                        	    	});
			                                        	    	
			                                        	    	
			                                        	    	var combo = Ext.getCmp('field-responsible-user');
			                                        	    	combo.setValue(author);
			                                        	    	
			                                        	    	Ext.getCmp('btn-execute-incomming-call').setDisabled(false);

			                                        	    }
			                                        	    else {
			                                        	    	Ext.getCmp('field-responsible-user').setValue('');
			                                        	    	Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
			                                        	    	form.setValues({
			                                        	    		x_responsible_user_id: 0,
			                                        	    	});
			                                        	    }
		                                        	   	 }
			                                        	}
		                                          
		                                       },
		                                       {
		                                          
		                                           margin: '5 0 0 295',
	                                               xtype: 'label',
	                                               id: 'label-executed-incomming-call',
	                                               text: 'Выполнен',
	                                               baseCls: 'incoming-call-is-executed',
	                                               hidden: true
	                                          
		                                       },
                                           ]
                                       }
                                       
                                     
                                     
                                    ]
                                }
                            ]
                        },

                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Создать Клиента',
            iconCls: 'icon_group_add',
            id: 'btn-create-client-incoming-call',
            action: 'save'
        },
        {
            text: 'Создать Заказ',
            iconCls: 'icon_document_prepare',
            id: 'btn-create-client-order',
            action: 'create-client-order',
            disabled: true
        },
        {
        	text: 'Выполнить',
        	iconCls: 'icon_accept',
        	id: 'btn-execute-incomming-call',
        	action: 'execute'
        },
        {
            text: 'Сохранить',
            iconCls: 'icon_disk',
            id: 'btn-save-incoming-call',
            action: 'save'
        },
        {
            text: 'Закрыть',
            id: 'btn-close-incoming-call',
            handler: function () {
                this.up('window').close();
            }
        }
    ]
});


// выбор данных из справочника "Контактные лица"
Ext.define('Ext.ux.CustomTriggerSelectContactPersonal', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.triggerSelectContactPersonal',
    onTriggerClick: function ()
    {
        var form = this.up('form');
        var winCp = Ext.create('widget.winContactPersonal');
        var gridCP = winCp.getComponent('grid-contact-personal');
        var storeCP = gridCP.getStore();
        var valRecord = form.getValues();

        if (valRecord.org_id != 0) {
	        storeCP.getProxy().extraParams = {
	            org_id: valRecord.org_id,
	            status: 1
	        }
        } else {
        	storeCP.getProxy().extraParams = {
	            status: 10
	        }
        }
      
        storeCP.clearFilter();
        storeCP.load();
        winCp.show();

        gridCP.getView().on({'cellmousedown':function(view, cell, cellIdx, record)	{
            form.getForm().cagent = record;
            form.getForm().setValues({
                cagent_id: record.get('id'),
                cagent_name: record.get('name'),
                cagent_old_name: record.get('name'),
                cagent_phone: record.get('cagent_phone'),
                cagent_email: record.get('cagent_email'),
                company_title: record.get('cagent_title'),
                org_id: record.get('org_id'),
                contact_personal_id: record.get('contact_personal_id')
            });
            winCp.close();
        }});
    }
});

// выбор данных из справочника "Сферы деятельности"
Ext.define('Ext.ux.CustomTriggerSelectReferenceScope', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.triggerSelectReferenceScope',
    onTriggerClick: function () {
        var form = this.up('form');
        var winRS = Ext.create('widget.winReferenceScope');

        var storeRS = Ext.StoreManager.get('reference_scope');
        storeRS.clearFilter();
        storeRS.load();
        winRS.show();

        winRS.down('grid').on('itemdblclick', function (store, record) {
            if (form.getForm().getValues().reference_scope_id.split(', ').indexOf(String(record.get('id'))) == -1) {
                form.getForm().setValues({
                    reference_scope: (form.getForm().getValues().reference_scope ? form.getForm().getValues().reference_scope + ', ' : '') + record.get('title'),
                    reference_scope_id: (form.getForm().getValues().reference_scope_id ? form.getForm().getValues().reference_scope_id + ', ' : '') + record.get('id')
                });
            }
            winRS.close();
        });

    }
});

// выбор данных из справочника "ФИАС"
Ext.define('Ext.ux.CustomTriggerSelectFias', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.triggerSelectFias',
    onTriggerClick: function () {

    }
});