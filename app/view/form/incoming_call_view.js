Ext.define('app.view.form.incoming_call_view', {
    id: 'formIncomingCalls',
    extend: 'Ext.window.Window',
    alias: 'widget.formIncomingCallViewOnly',
    title: 'Входящий запрос',
    layout: 'fit',
    modal: true,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    flex: 1,
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Данные запроса',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            labelWidth: 130,
                                            name: 'call_date',
                                            width: 255,
                                            padding: '0 20 0 0',
                                            readOnly: true,
                                            fieldLabel: 'Дата и время звонка'
                                        },
                                        {
                                            xtype: 'combobox',
                                            labelWidth: 60,
                                            name: 'incoming_source_id',
                                            width: 200,
                                            fieldLabel: 'Источник',
                                            editable: false,
                                            displayField: 'title',
                                            valueField: 'id',
                                            hideTrigger: true,
                                            readOnly: true,
                                            store: Ext.create('Ext.data.Store', {
                                                fields: ['id', 'title'],
                                                data: [
                                                    {id: 1, title: 'Телефон'},
                                                    {id: 2, title: 'E-mail'},
                                                    {id: 3, title: 'Сайт'}
                                                ]
                                            })
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            border: 0,
                            padding: '0 0',
                            margin: '0 10',
                            //width:475,
                            layout: {
                                type: 'hbox',
                                align: 'left'
                            },


                            items: [

                                {
                                    xtype: 'label',
                                    id: 'incoming-call-is-new',
                                    text: 'Новый запрос',
                                    baseCls: 'incoming-call-is-new',
                                    margin: '5 0 0 397'
                                }

                            ]
                        },
                        {
                            xtype: 'fieldset',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                        {
                                            labelWidth: 110,
                                            fieldLabel: 'Наименование',
                                            xtype: 'textfield',
                                            name: 'company_title',
                                            width: 455,
                                            readOnly: true,
                                        },
                                        {
                                            xtype: 'label',
                                            id: 'label-company-title-is-find',
                                            html: '<img src="/icons/16x16/client_account_template.png">',
                                            padding: 3,
                                            hidden: true
                                        },
                                        {
                                            xtype: 'label',
                                            id: 'label-company-title-is-not-find',
                                            html: '<img src="/icons/16x16/error.png">',
                                            padding: 3,
                                            hidden: true
                                        }
                                    ]
                                },

                                {
                                    labelWidth: 110,
                                    xtype: 'textfield',
                                    name: 'cagent_name',
                                    id: 'conctact-personal-id',
                                    fieldLabel: 'Контактное лицо',
                                    readOnly: true,
                                    width: 475,
                                },

                                {
                                    xtype: 'hidden',
                                    name: 'cagent_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contact_personal_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'is_new'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'root_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'contractor_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'from_search'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'icr_id',
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'org_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'cagent_old_name'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'change_cp'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'author_id'
                                },
                                {
                                	xtype: 'hidden',
                                	name: 'executed'
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                        {
                                            labelWidth: 110,
                                            name: 'cagent_phone',
                                            width: 255,
                                            padding: '0 20 0 0',
                                            fieldLabel: 'Телефон',
                                            readOnly: true,

                                        },
                                        {
                                            labelWidth: 40,
                                            name: 'cagent_email',
                                            width: 200,
                                            fieldLabel: 'E-mail',
                                            vtype: 'email',
                                            readOnly: true,
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    layout: 'hbox',
                                    defaultType: 'textfield',
                                    items: [
                                            {
                                            	 xtype: 'button',
                                                 text: 'История запросов',
                                                 action: 'showHistoryIncomingCall'
                                            },

                                            {
                                                labelWidth: 40,
                                                name: 'cagent_add_phone',
                                                width: 100,
                                                align: 'right',
                                                padding: '0 0 0 50',
                                                fieldLabel: 'Доп',
                                                xtype: 'textfield',
                                                readOnly: true,
                                            },
                                    ]
                                },
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            id: 'incoming-call-cagent-info',
                            margin: 10,
                            padding: '5 10',
                            width: 500,
                            items: [
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'Тип',
                                    id: 'company-type-radios',
                                    items: [
                                        {boxLabel: 'Юр. лицо', name: 'company_type', inputValue: '1'},
                                        {boxLabel: 'Физ. лицо', name: 'company_type', inputValue: '2'},
                                        {boxLabel: 'ИП', name: 'company_type', inputValue: '3'}
                                    ]
                                },
                                {
                                    labelWidth: 110,
                                    xtype: 'textfield',
                                    name: 'reference_scope',
                                    fieldLabel: 'Сфера деят.',
                                    width: 475,
                                    readOnly: true,
                                },
                                {
                                    xtype: 'panel',
                                    border: 0,
                                    margin: '0 0 10 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            labelWidth: 110,
                                            xtype: 'textfield',
                                            name: 'company_fias_title',
                                            fieldLabel: 'Факт. адрес',
                                            readOnly: true,
                                            flex: 1
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            border: 0,
                            padding: '0 10',
                            margin: '10',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Н/А',
                                    name: 'na',
                                    inputValue: 1
                                },
                                {
                                    xtype: 'textfield',
                                    labelWidth: 40,
                                    name: 'author_name',
                                    width: 200,
                                    padding: '0 0 0 50',
                                    readOnly: true,
                                    baseCls: 'read-only',
                                    fieldLabel: 'Автор',
                                    id: 'id-author-name'

                                },
                                {
                                    xtype: 'textfield',
                                    labelWidth: 60,
                                    name: 'save_date',
                                    width: 180,
                                    padding: '0 0 0 20',
                                    readOnly: true,
                                    baseCls: 'read-only',
                                    fieldLabel: 'Вр. сохр.'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: 0,
                    flex: 1,
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Что интересует клиента',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [
                                {
                                    xtype: 'panel',
                                    border: 0,
                                    margin: '0 0 10 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            labelWidth: 100,
                                            xtype: 'combobox',
                                            name: 'client_interest_srv',
                                            id: 'incoming-call-client-interest-srv',
                                            readOnly: true,
                                            fieldLabel: 'Услуга',
                                            width: 373,

                                        }
                                    ]
                                },
                                {
                                    xtype: 'textarea',
                                    name: 'client_interest',
                                    width: 375,
                                    height: 70,
                                    readOnly: true,
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Источник обращения',
                            id: 'incoming-call-source',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [
                                {
                                    labelWidth: 70,
                                    xtype: 'textfield',
                                    name: 'reclama_title',
                                    fieldLabel: 'Реклама',
                                    width: 375,

                                },
                                {
                                    labelWidth: 70,
                                    xtype: 'combobox',
                                    id: 'field-ref-cagent',
                                    name: 'ref_cagent_id',
                                    hidden: true,
                                    fieldLabel: 'Контрагент',
                                    width: 375,
                                    editable: false,
                                    store: 'cagent',
                                    displayField: 'title',
                                    valueField: 'id',
                                    allowBlank: false,
                                    readOnly:true,
                                    hideTrigger: true,
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'field-ref-user',
                                    name: 'ref_user',
                                    hidden: true,
                                    fieldLabel: 'Сотрудник',
                                    width: 375,
                                    labelWidth: 70,
                                    editable: false,
                                    allowBlank: false,
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ref_user_id'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'responsible_user_id',

                                },
                                {
                                    xtype: 'textarea',
                                    name: 'reclama_interest',
                                    width: 375,
                                    height: 40,
                                    emptyText: 'Комментарий к источнику информации'
                                }
                            ]
                        },
                        {
                        	xtype: 'fieldset',
                            id: 'incoming-call-responsible',
                            margin: 10,
                            padding: '5 10',
                            width: 400,
                            items: [

                                {
                                    xtype: 'panel',
                                    border: 0,
                                    padding: '0 0',
                                    margin: '2',
                                    layout: {
                                        type: 'vbox',
                                        align: 'left',
                                        pack: 'left',

                                    },
                                    items: [
                                       {
                                           xtype: 'textfield',
                                           triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                           id: 'field-responsible-user',
                                           name: 'responsible_user',
                                           fieldLabel: 'Ответственный',
                                           width: 373,
                                           labelWidth: 100,
                                           readOnly: true,
                                       },
                                       
                                       

                                       {
                                    	   xtype: 'panel',
                                           border: 0,
                                           padding: '0 0',
                                           margin: '0',
                                           layout: {
                                               type: 'hbox',
                                               align: 'stretch',
                                           },
                                           items:[
		                                       {

		                                           margin: '5 0 0 295',
	                                               xtype: 'label',
	                                               id: 'label-executed-incomming-call',
	                                               text: 'Выполнен',
	                                               baseCls: 'incoming-call-is-executed',
	                                               hidden: true

		                                       },
                                           ]
                                       }



                                    ]
                                }
                            ]
                        },

                    ]
                }
            ]
        }
    ],
    buttons: [
              {
                  text: 'Создать Клиента',
                  iconCls: 'icon_group_add',
                  id: 'btn-create-client-incoming-call',
                  action: 'save'
              },
        {
            text: 'Создать Заказ',
            iconCls: 'icon_document_prepare',
            id: 'btn-create-client-order',
            action: 'create-client-order',
            disabled: true
        },
              {
              	text: 'Выполнить',
              	iconCls: 'icon_accept',
              	id: 'btn-execute-incomming-call',
              	action: 'execute',

              },
              {
                  text: 'Закрыть',
                  id: 'btn-close-incoming-call',
                  handler: function () {
                      this.up('window').close();
                  }
              }
          ]
});

//выбор данных из справочника "Контактные лица"
Ext.define('Ext.ux.CustomTriggerSelectContactPersonal', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.triggerSelectContactPersonal',
    onTriggerClick: function ()
    {
        var form = this.up('form');
        var winCp = Ext.create('widget.winContactPersonal');
        var gridCP = winCp.getComponent('grid-contact-personal');
        var storeCP = gridCP.getStore();
        var valRecord = form.getValues();

        if (valRecord.org_id != 0) {
	        storeCP.getProxy().extraParams = {
	            org_id: valRecord.org_id,
	            status: 1
	        }
        } else {
        	storeCP.getProxy().extraParams = {
	            status: 10
	        }
        }

        storeCP.clearFilter();
        storeCP.load();
        winCp.show();

        gridCP.getView().on({'cellmousedown':function(view, cell, cellIdx, record)	{
            form.getForm().cagent = record;
            form.getForm().setValues({
                cagent_id: record.get('id'),
                cagent_name: record.get('name'),
                cagent_old_name: record.get('name'),
                cagent_phone: record.get('cagent_phone'),
                cagent_email: record.get('cagent_email'),
                company_title: record.get('cagent_title'),
                org_id: record.get('org_id'),
                contact_personal_id: record.get('contact_personal_id')
            });
            winCp.close();
        }});
    }
});