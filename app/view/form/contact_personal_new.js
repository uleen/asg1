Ext.define('app.view.form.contact_personal_new', {
    extend: 'Ext.window.Window',
    alias: 'widget.formContactPersonalNew',
    title: 'Контактное лицо',
    autoShow: true,
    layout: {
        type: 'vbox',
        align: 'stretch',

    }, 
    items: [
        {
            xtype: 'form',
            border: 0,
            layout: {
            	type: 'hbox',
            	align: 'strech',
            },
            items: [
                    {
                        xtype: 'fieldset',
                        margin: 10,
                        padding: '5 10',
                        width: 500,
                        items: [
                            {
                                labelWidth: 100,
                                xtype: 'textfield',
                                name: 'fio',
                                fieldLabel: 'ФИО',
                                width: 475,
                                fieldCls: 'field-required',
                                invalidCls: 'field-required-invalid',
                                enableKeyEvents: true,
                                validator: function (value) {
                                    if (!value) {
                                        return 'Поле "ФИО" обязательно для заполнения';
                                    }
                                    var reg = new RegExp(/^[A-Za-zА-Яа-я0-9]/);
                                    if (!reg.test(value)) {
                                        return 'Поле "ФИО" должно начинаться с буквы или цифры';
                                    }
                                    return true;
                                },
                               

                            },
                            {
                                labelWidth: 100,
                                xtype: 'triggerSelectOrgs',
                                name: 'cagent_title',
                                fieldLabel: 'Контрагент',
                                id: 'conctact-personal-id',
                                width: 475,
                                enableKeyEvents: true,
                              
                                
                            },
                            {
                            	xtype: 'hidden',
                                name: 'contractor_id'

                            },
                            {
                            	xtype: 'hidden',
                                name: 'org_id'

                            },
                            {
                                labelWidth: 100,
                                xtype: 'textfield',
                                name: 'position',
                                fieldLabel: 'Должность',
                                width: 475,  
                                enableKeyEvents: true,
                               
                            },
                            {
                                labelWidth: 100,
                                xtype: 'textfield',
                                name: 'address',
                                fieldLabel: 'Адрес',
                                width: 475, 
                                enableKeyEvents: true,
                               
                            },
                            {
                                labelWidth: 100,
                                xtype: 'datefield',
                                name: 'dateofbirth',
                                fieldLabel: 'Дата рождения', 
                                enableKeyEvents: true,
                                
                            },                            
                       
                            {
                                xtype: 'checkboxgroup',
                                layout: {
                                    type: 'vbox',
                                    align: 'left', // or 'right'
                                    pack: 'left', // controls vertical align
                                    width:200,
                                },
                                items: [
                                        {
                                            xtype: 'checkbox',
                                            align: 'left',
                                            inputValue: 1,
                                            boxLabel: 'Н/А',
                                            name: 'na',
                                            id: 'is-active-user',
                                            width:200,
                                            enableKeyEvents: true,
                                           
                                            
                                        },
                                        {
                                        	xtype: 'checkbox',
                                        	align: 'left',
                                            inputValue: 1,
                                            boxLabel: 'не включать в рассылку',
                                            name: 'is_send',
                                            enableKeyEvents: true,
                                            
                                        },
                                ]
                            },
                          
                        ],
                       
                    },
                    
                    
              ],
             
        },
        
        {
            id: 'cp-grid',
            xtype: 'grid',
            tbar: [

                {
                    //text: 'Добавить телефон',
                    action: 'add_pho',
                    iconCls: 'icon_phone_add'
                },
                {
                    //text: 'Добавить E-mail',
                    action: 'add_email',
                    iconCls: 'icon_email_add'
                }
            ],
            border: 1,
            flex: 1,
            name: 'contacts',
            height: 200,
            selType: 'cellmodel',
            plugins: 
            [{ 
                ptype: 'cellediting',
                clicksToEdit: 1
            }],
            viewConfig:{
                markDirty:false
            },
            columns: [
            
               {
	        	 text: 'Вид контакта',
	             dataIndex: 'contact_type',
	             width: 100, 
	             field: 
	             {   	 
	            	  xtype: 'textfield',
	                  selectOnTab: true,
	                  editable: false,
	                  displayField: 'title',
	                  valueField: 'title',
	                  readOnly: true,
	              },      
	             
	              
	          },  
	          	
	            {
	                text: 'Содержание',
	                dataIndex: 'number',
	                editor: 'textfield',
	                id: 'ct-type',
	                flex: 1,
	                enableKeyEvents: true,
	                field: {
	                   xtype: 'textfield',
	                }, 
             
	            },
  	            {
  	                text: 'Доп.',
  	                dataIndex: 'add_contact',
  	                editor: 'textfield',
  	               width:50,
  	                field: {
  	                   xtype: 'textfield',
  	                }, 
  	            },

                {
                    xtype: 'actioncolumn',
                    text: 'Основной',
                    width: 70,
                    sortable: true,
                    align: 'center',
                    dataIndex: 'is_main',
                    action: 'changeMain',
                    getClass: function (v, meta, rec) {
                        return rec.get('is_main') == 1 ? 'icon_bullet_black' : 'icon_bullet_white';
                    },

                    handler: function (grid, row, record)
                    {                 	
            	        var record = grid.getStore().getAt(row);
            	        var row_count =  grid.getStore().getCount(); 
            	        
            	        // перебор циклом по типу контакта и статусу, для выставления новых значений
            	        rec_type = record.get('contact_type');
            	        console.log('click on is main: '+record.get('is_main')+', type: '+rec_type);
            	        
            	        if (record.get('is_main') == 0 && rec_type == 'Телефон')
            	        {
                            this.items[0].tooltip = 'Главный';
                            var idx =0;
                            for(idx=0; idx<row_count; idx++)
                	        {
                            
                            	var xrecord = grid.getStore().getAt(idx);
                            	if ( (row != idx) && rec_type=='Телефон')
                            	{
                            		xrecord.set('is_main', 0);
                            		
                            		grid.getStore().data.items[idx].data['is_main'].style = 'icon_bullet_white';;
                            	}
                	        }
                            record.set('is_main', 1);
                            return 'icon_bullet_black';
                        }
            	        
            	        // email
            	        
            	        if (record.get('is_main') == 0 && rec_type == 'E-mail')
            	        {
                            this.items[0].tooltip = 'Главный';
                            var idx =0;
                            for(idx=0; idx<row_count; idx++)
                	        {
                            	var xrecord = grid.getStore().getAt(idx);
                            	if ( (row != idx) && rec_type=='E-mail')
                            	{
                            		xrecord.set('is_main', 0);
                            		grid.getStore().data.items[idx].data['is_main'].style = 'icon_bullet_white';;
                            	}
                	        }
                            record.set('is_main', 1);
                            return 'icon_bullet_black';
                        }
            	    }
                },
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'is_active',
                    text: 'Активен',
                    width: 70,
                    sortable: true,
                    align: 'center',
                    action: 'changeStatus',
                    getClass: function (v, meta, rec) {
                    	 if (rec.get('is_active') == 1) {
                             this.items[0].tooltip = 'Активно';
                             return 'icon_bullet_green';
                         }
                         if (rec.get('is_active') == 0) {
                             this.items[0].tooltip = 'Не активно';
                             return 'icon_bullet_red';
                         }

                    },
  	             
                    handler: function (grid, row){
                    	        var record = grid.getStore().getAt(row);
                    	        
                    	        if (record.get('is_active') == 1) {
                                    this.items[0].tooltip = 'Активно';
                                    record.set('is_active', 0);
                                    return 'icon_bullet_red';
                                }
                                if (record.get('is_active') == 0) {
                                    this.items[0].tooltip = 'Не активно';
                                    record.set('is_active', 1);
                                    return 'icon_bullet_green';
                                }
                    	    }
                }
                
            ],
            //enableKeyEvents: true,

            editor: {
                xtype: 'textfield',
                allowBlank: false
            },
            
            listeners : {
            	
            	cellclick: function(iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx, iEvent) {

            		if (iColIdx == 1)
            		{
            			var fieldName = iView.getGridColumns()[iColIdx].dataIndex;
                        
                        var type = iView.getGridColumns()[iColIdx].contact_type;
                        var cp = iView.getSelectionModel().getSelection()[0].get('contact_type');

                        
                        console.log('click in cell: '+fieldName);
                        console.log('click in cell[type]: '+cp);
                        
                        if (cp == "Телефон")
                        {
                        	 var gridview = Ext.getCmp('cp-grid').getView();
                             var nameCell = gridview.getHeaderCt().gridDataColumns[1];
                         
                             
                             nameCell.setEditor(
                               {
                                    xtype: 'textfield',
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)], 
                                    enableKeyEvents: true,
                      
                               }
                             );
                        }
                        
                        if (cp == "E-mail"){
                        	 var gridview = Ext.getCmp('cp-grid').getView();
                             var nameCell = gridview.getHeaderCt().gridDataColumns[1];
                         
                             
                             nameCell.setEditor({
                                 xtype: 'textfield',
                                 allowBlank: true,
                                 enableKeyEvents: true,
                                 
                             });
                        }
                      
            		}
                    
            	}
            },

            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1,
                    
                })
            ],
            selType: 'cellmodel',
            width: 500
        },

    ],

    buttons: [
        {
            text: 'Сохранить',
            action: 'save',
            id: 'btn-save-contact-personal'
        },
        {
        	 text: 'Закрыть',
        	 id: 'btn-close-contact-personal',
             handler: function ()
             {
            	 this.up('window').close();
             }
        }
    ],
    
       
});


//выбор данных из справочника "Контактные лица"
// Контрагенты
Ext.define('Ext.ux.CustomTriggerSelectOrgs', {
 extend: 'Ext.form.field.Trigger',
 alias: 'widget.triggerSelectOrgs',
 onTriggerClick: function ()
 {
	 var form = this.up('form');
	 
	 var win = Ext.create('widget.winContactractorList');
	 
     var store = Ext.StoreManager.get('cagent');
     store.clearFilter();
     store.getProxy().extraParams = {};
     store.load();

     win.show();

     win.down('grid').on('itemdblclick', function (store, record) {
         form.getForm().cagent = record;
         form.getForm().setValues({
        	 cagent_title: record.get('title'),
             org_id: record.get('org_id'),
             contractor_id: record.get('contractor_id'),
         });
        
         win.close();
     });
 	}
});
