Ext.define('app.view.form.user_add', {
    extend: 'Ext.window.Window',
    alias: 'widget.userAdd',
    title: '',
    layout: 'fit',
    width: 415,
    height:240,
    autoShow: true,
    items: [
	        {
	            xtype: 'form',
	            border: 0,
	            layout: {
	            	type: 'vbox',
	            	align: 'strech',
	            },
	            items: [
	                    
                {
                    xtype: 'fieldset',
                    title: 'Учетные данные',
                    margin: 5,
                    items: [
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            name: 'name',
                            fieldLabel: 'Пользователь',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Пользователь" обязательно для заполнения';
                                }
                                return true;
                            },


                        },
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            name: 'login',
                            fieldLabel: 'Login',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Login" обязательно для заполнения';
                                }
                                var reg = new RegExp(/^[a-z0-9_-]{1,16}$/);
                                if (!reg.test(value)) {
                                    return 'Поле "Login" должно содержать только латинские буквы и/или цифры';
                                }
                                return true;
                            },
                           
                        },
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            name: 'work_email',
                            fieldLabel: 'Рабочий E-mail',
                            width: 370,
                            vtype: 'email'
                        },
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            inputType: 'password',
                            id: 'pass-field-one',
                            name: 'password',
                            fieldLabel: 'Пароль',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Пароль" обязательно для заполнения';
                                }
                                var reg = new RegExp(/^[a-z0-9_-]{1,16}$/);
                                if (!reg.test(value)) {
                                    return 'Поле "Пароль" должно содержать только латинские буквы и/или цифры';
                                }
                                return true;
                            },
                            listeners:{
                            	blur: function(field){
                            		
                            		var p1 = Ext.getCmp('pass-field-one').getValue();
                            		var p2 = Ext.getCmp('pass-field-two').getValue();
                            		if (p2 != '')
                            		{
                            			Ext.getCmp('btn-create-user-save').setDisabled(true);
                                		if (p1 == p2){
                                			Ext.getCmp('btn-create-user-save').setDisabled(false);
                                		}

                            		}
                            	}
                            }
                        },
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            inputType: 'password',
                            id: 'pass-field-two',
                            name: 're-password',
                            fieldLabel: 'Повтор пароля',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Повтор пароля" обязательно для заполнения';
                                }
                                var reg = new RegExp(/^[a-z0-9_-]{1,16}$/);
                                if (!reg.test(value)) {
                                    return 'Поле "Повтор пароля" должно содержать только латинские буквы и/или цифры';
                                }
                                return true;
                            },
                            listeners:{
                            	blur: function(field){
                            		 Ext.getCmp('btn-create-user-save').setDisabled(true);
                            		 
                            		var p1 = Ext.getCmp('pass-field-one').getValue();
                            		var p2 = Ext.getCmp('pass-field-two').getValue();
                            		if (p1 == p2){
                            			Ext.getCmp('btn-create-user-save').setDisabled(false);
                            		}
                            	}
                            }
                        },
                        {
                        	xtype: 'hidden',
                        	name:'id'
                        },
                        {
                        	xtype: 'hidden',
                        	name: 'empl_id'
                        },

                    ]
                },
	        ],
		 }
	],      
	
	buttons: [
	      	{
	    		text: 'Создать пользователя',
	    		action: 'createUserTwo',
	    		iconCls: 'icon_user_add',
	    	    id: 'btn-create-user-save',
	    	    
	    	},
    	   {
        	text: 'Сменить пароль',
        	action: 'changeUserPassword',
        	iconCls: 'icon_change_user_password',
            id: 'btn-change-password-user',
            handler: function () {
            	Ext.getCmp('pass-field-one').hide();
    			Ext.getCmp('pass-field-two').hide();
            }

        },        
	        {
	            text: 'Сохранить',
	            action: 'save',
	            id: 'btn-save-user'
	        }
	]
});