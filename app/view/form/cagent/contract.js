Ext.define('app.view.form.cagent.contract', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.formCagentContract',
    border: 0,
    flex: 1,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'gridContractorRequisites',
            itemId: 'grid-contractor-requisites',
            title: 'Реквизиты',
            flex: 1
        },
        {
            xtype: 'gridContractorContracts',
            itemId: 'grid-contractor-contracts',
            padding: '2 0 0 0',
            title: 'Договоры',
            flex: 1
        }
    ]

});

