Ext.define('app.view.form.cagent.incoming_call', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.formCagentIncomingCall',
    border: 0,
    flex: 1,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    title: 'Входящие запросы',
    items: [
        {
            xtype: 'gridIncomingCall',
            itemId: 'grid-cagent-incomming-call',
            store: 'contractor_requisites',
            flex: 1
        }
    ]
});

