Ext.define('app.view.form.cagent.main', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formCagentMain',
    title: 'Основные сведения',
    layout: 'fit',
    items: [
        {
            xtype: 'form',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    itemId: 'lpanel',
                    padding: 0,
                    border: 0,
                    items: [
                        {
                            xtype: 'fieldset',
                            itemId: 'main',
                            title: 'Основное',
                            margin: 10,
                            anchor: '100%',
                            items: [
                                {
                                    xtype: 'panel',
                                    itemId: 'primary',
                                    layout: 'hbox',
                                    border: 0,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'title',
                                            fieldLabel: 'Краткое наименование',
                                            labelAlign: 'right',
                                            labelWidth: 140,
                                            flex: 1
                                        },
                                        {
                                            xtype: 'combobox',
                                            itemId: 'opf',
                                            name: 'okpfo_type',
                                            width: 100,
                                            store: 'okpfo_type',
                                            displayField: 'title',
                                            valueField: 'id',
                                            padding: '0 0 0 5',
                                            allowBlank: false
                                        }
                                    ]
                                },
                                {
                                    xtype: 'textfield',
                                    labelWidth: 140,
                                    name: 'title_full',
                                    fieldLabel: 'Полное наименование',
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    margin: '7 0 0 0'
                                },
                                {
                                    xtype: 'panel',
                                    itemId: 'secondary',
                                    layout: 'hbox',
                                    border: 0,
                                    items: [
                                        {
                                            xtype: 'radiogroup',
                                            padding: '7 2 0 5',
                                            items: [
                                                {name: 'type', inputValue: '1', width: 81, boxLabel: 'Юр. лицо', checked: true},
                                                {name: 'type', inputValue: '2', width: 85, boxLabel: 'Физ. лицо'},
                                                {name: 'type', inputValue: '3', width: 43, boxLabel: 'ИП'}
                                            ],
                                            listeners: {
                                                change: function (field, newValue) {
                                                    var kpp = field.up('form').getComponent('lpanel').getComponent('main').getComponent('secondary').getComponent('kpp');
                                                    var opf = field.up('form').getComponent('lpanel').getComponent('main').getComponent('primary').getComponent('opf');
                                                    if (newValue.type != 1) {
                                                        this.kpp = kpp.getValue();
                                                        this.opf = opf.getValue();
                                                        kpp.setValue();
                                                        opf.setValue();
                                                        kpp.disable();
                                                        opf.disable();
                                                    } else {
                                                        kpp.setValue(this.kpp);
                                                        opf.setValue(this.opf);
                                                        kpp.enable();
                                                        opf.enable();
                                                    }
                                                }
                                            }

                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'inn',
                                            width: 150,
                                            labelWidth: 30,
                                            padding: '7 0 10 46',
                                            fieldLabel: 'ИНН',
                                            labelAlign: 'right',
                                            maskRe: /\d/,
                                            validator: function (val) {
                                                if (val && !checkINN(val)) {
                                                    return 'ИНН указан не правильно';
                                                } else {
                                                    return true;
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            itemId: 'kpp',
                                            name: 'kpp',
                                            width: 150,
                                            labelWidth: 30,
                                            padding: '7 0 10 30',
                                            fieldLabel: 'КПП',
                                            labelAlign: 'right',
                                            allowBlank: false
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            itemId: 'contact',
                            title: 'Контакты',
                            margin: 10,
                            anchor: '100%',
                            items: [
                                {
                                    xtype: 'addressfield',
                                    name: 'ur_address_text',
                                    labelWidth: 150,
                                    fieldLabel: 'Юридический адрес',
                                    valueField: 'ur_address_fias',
                                    jsonField: 'ur_address_json',
                                    anchor: '100%',
                                    labelAlign: 'right',
                                    afterLabelTextTpl: '<img src="/icons/16x16/mail_yellow.png" align="center" style="margin: -3px 0 0 5px">',
                                    listeners: {
                                        render: function (e) {
                                            var form = e.up('form').getForm();
                                            e.getEl().on('contextmenu', function (e) {
                                                e.stopEvent();
                                                Ext.create('Ext.menu.Menu', {
                                                    items: [
                                                        {
                                                            text: 'Скопировать из почтового адреса',
                                                            iconCls: 'icon_mail_blue',
                                                            handler: function () {
                                                                form.setValues({
                                                                        ur_address_text: form.getValues().post_address_text,
                                                                        ur_address_fias: form.getValues().post_address_fias
                                                                    }
                                                                );
                                                            }
                                                        },
                                                        {
                                                            text: 'Скопировать из фактического адреса',
                                                            iconCls: 'icon_mail_green',
                                                            handler: function () {
                                                                form.setValues({
                                                                        ur_address_text: form.getValues().fact_address_text,
                                                                        ur_address_fias: form.getValues().fact_address_fias
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    ]
                                                }).showAt(e.getXY());
                                            });
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ur_address_fias'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ur_address_json'
                                },
                                {
                                    xtype: 'addressfield',
                                    name: 'post_address_text',
                                    labelWidth: 150,
                                    fieldLabel: 'Почтовый адрес',
                                    valueField: 'post_address_fias',
                                    jsonField: 'post_address_json',
                                    anchor: '100%',
                                    labelAlign: 'right',
                                    afterLabelTextTpl: '<img src="/icons/16x16/email.png" align="center" style="margin: -3px 0 0 5px">',
                                    listeners: {
                                        render: function (e) {
                                            var form = e.up('form').getForm();
                                            e.getEl().on('contextmenu', function (field) {
                                                Ext.create('Ext.menu.Menu', {
                                                    items: [
                                                        {
                                                            text: 'Скопировать из юридического адреса',
                                                            iconCls: 'icon_mail_yellow',
                                                            handler: function () {
                                                                form.setValues({
                                                                        post_address_text: form.getValues().ur_address_text,
                                                                        post_address_fias: form.getValues().ur_address_fias
                                                                    }
                                                                );
                                                            }
                                                        },
                                                        {
                                                            text: 'Скопировать из фактического адреса',
                                                            iconCls: 'icon_mail_green',
                                                            handler: function () {
                                                                form.setValues({
                                                                        post_address_text: form.getValues().fact_address_text,
                                                                        post_address_fias: form.getValues().fact_address_fias
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    ]
                                                }).showAt(field.getXY());
                                                field.stopEvent();
                                            });
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'post_address_fias'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'post_address_json'
                                },
                                {
                                    xtype: 'addressfield',
                                    name: 'fact_address_text',
                                    labelWidth: 150,
                                    fieldLabel: 'Фактический адрес',
                                    valueField: 'fact_address_fias',
                                    jsonField: 'fact_address_json',
                                    anchor: '100%',
                                    labelAlign: 'right',
                                    afterLabelTextTpl: '<img src="/icons/16x16/mail_green.png" align="center" style="margin: -3px 0 0 5px">',
                                    listeners: {
                                        render: function (e) {
                                            var form = e.up('form').getForm();
                                            e.getEl().on('contextmenu', function (field) {
                                                Ext.create('Ext.menu.Menu', {
                                                    items: [
                                                        {
                                                            text: 'Скопировать из юридического адреса',
                                                            iconCls: 'icon_mail_yellow',
                                                            handler: function () {
                                                                form.setValues({
                                                                        fact_address_text: form.getValues().ur_address_text,
                                                                        fact_address_fias: form.getValues().ur_address_fias
                                                                    }
                                                                );
                                                            }
                                                        },
                                                        {
                                                            text: 'Скопировать из почтового адреса',
                                                            iconCls: 'icon_mail_blue',
                                                            handler: function () {
                                                                form.setValues({
                                                                        fact_address_text: form.getValues().post_address_text,
                                                                        fact_address_fias: form.getValues().post_address_fias
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    ]
                                                }).showAt(field.getXY());
                                                field.stopEvent();
                                            });
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'fact_address_fias'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'fact_address_json'
                                },
                                {
                                    labelWidth: 130,
                                    xtype: 'textfield',
                                    name: 'phone',
                                    fieldLabel: 'Телефон',
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                                },
                                {
                                    labelWidth: 130,
                                    xtype: 'textfield',
                                    name: 'fax',
                                    fieldLabel: 'Факс',
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                                },
                                {
                                    labelWidth: 130,
                                    xtype: 'textfield',
                                    name: 'email',
                                    fieldLabel: 'E-mail',
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    vtype: 'email'
                                },
                                {
                                    labelWidth: 130,
                                    xtype: 'textfield',
                                    name: 'url',
                                    fieldLabel: 'Сайт',
                                    labelAlign: 'right',
                                    anchor: '100%'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            itemId: 'comment',
                            title: 'Комментарий',
                            margin: 10,
                            anchor: '100%',
                            items: [
                                {
                                    xtype: 'textarea',
                                    name: 'comment',
                                    anchor: '100%'
                                }
                            ]
                        }
                    ],
                    flex: 1
                },
                {
                    xtype: 'panel',
                    itemId: 'rpanel',
                    padding: 0,
                    border: 0,
                    width: 250,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'id',
                            fieldLabel: 'Код',
                            labelWidth: 25,
                            width: 100,
                            padding: '10 10 10 130',
                            readOnly: true
                        },
                        {
                            xtype: 'fieldset',
                            margin: '10 10 10 0',
                            anchor: '100%',
                            items: [
                                {
                                    xtype: 'radiogroup',
                                    items: [
                                        {
                                            boxLabel: 'Клиент',
                                            name: 'org_type',
                                            inputValue: '1',
                                            readOnly: true
                                        },
                                        {
                                            boxLabel: 'Поставщик',
                                            name: 'org_type',
                                            inputValue: '2',
                                            readOnly: true,
                                            checked: true
                                        }
                                    ]
                                },
                                {
                                    xtype: 'triggerfield',
                                    name: 'ref_scope',
                                    fieldLabel: 'Сфера деятельности',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    onTriggerClick: function () {
                                        var form = this.up('form');
                                        var winRS = Ext.create('widget.winReferenceScope');

                                        var storeRS = Ext.StoreManager.get('reference_scope');
                                        storeRS.clearFilter();
                                        storeRS.load();
                                        winRS.show();

                                        winRS.down('grid').on('itemdblclick', function (store, record) {
                                            if (form.getForm().getValues().ref_scope_id.split(', ').indexOf(String(record.get('id'))) == -1) {
                                                form.getForm().setValues({
                                                    ref_scope: (form.getForm().getValues().ref_scope ? form.getForm().getValues().ref_scope + ', ' : '') + record.get('title'),
                                                    ref_scope_id: (form.getForm().getValues().ref_scope_id ? form.getForm().getValues().ref_scope_id + ', ' : '') + record.get('id')
                                                });
                                            }
                                            winRS.close();
                                        });

                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'ref_scope_id'
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'client_category',
                                    fieldLabel: 'Категория Клиента',
                                    labelAlign: 'top',
                                    store: 'customer_category',
                                    displayField: 'title',
                                    valueField: 'id',
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'manager_title',
                                    typeAhead: true,
                                    fieldLabel: 'Ответственный менеджер',
                                    store: Ext.create('app.store.user.can_auth'),
                                    anchor: '100%',
                                    labelAlign: 'top',
                                    displayField: 'name',
                                    minChars: 2,
                                    listeners: {
                                        select: function (combo, records) {
                                            var rec = records[0];
                                            var form = this.up('form').getForm();
                                            form.setValues({
                                                manager_id: rec.get('id')
                                            });
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'manager_id'
                                },
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Блокирован',
                                    name: 'is_blocked',
                                    inputValue: '1'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            margin: '10 10 10 0',
                            anchor: '100%',
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'reclama',
                                    fieldLabel: 'Реклама',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    readOnly: true
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'reclama_id'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'create_date',
                                    fieldLabel: 'Дата создания карточки',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    readOnly: true
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});