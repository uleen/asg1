Ext.define('app.view.form.cagent.contact', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.formCagentContact',
    border: 0,
    flex: 1,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    title: 'Контактные лица',
    items: [
        {
            tbar: [
                {
                    text: 'Добавить',
                    action: 'create',
                    iconCls: 'icon_add'
                },
                {
                    text: 'Основной',
                    action: 'set_main',
                    iconCls: 'icon_cp_set_main'
                }
            ],
            xtype: 'gridContactPersonal',
            itemId: 'grid-contact-personal',
            flex: 1
        }
    ]
});