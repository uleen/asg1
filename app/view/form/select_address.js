Ext.define('app.view.form.select_address', {
    extend: 'Ext.window.Window',
    alias: 'widget.formSelectAddress',
    title: 'Выбор адреса',
    layout: 'fit',
    autoShow: true,
    modal: true,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'zip',
                    id: 'select-address-zip',
                    labelWidth: 50,
                    width: 500,
                    padding: '10 10 10 10',
                    fieldLabel: 'Индекс',
                    labelAlign: 'right',
                    readOnly: true,
                    baseCls: 'read-only'
                },
                {
                    xtype: 'combobox',
                    name: 'city_name',
                    id: 'select-address-city',
                    width: 500,
                    anchor: '100%',
                    labelWidth: 50,
                    padding: '0 10 10 10',
                    fieldLabel: 'Город',
                    labelAlign: 'right',
                    typeAhead: true,
                    store: 'address_city',
                    valueField: 'parent',
                    displayField: 'parent',
                    minChars: 3,
                    listeners: {
                        select: function (combo, value) {
                            this.up('form').getForm().setValues({
                                fias_id: '',
                                street_name: '',
                                street_id: '',
                                house_name: '',
                                house_id: '',
                                zip: value[0].data.zip,
                                city_id: value[0].data.id
                            });
                            upAddress();
                        }
                    }
                },
                {
                    xtype: 'combobox',
                    name: 'street_name',
                    id: 'select-address-street',
                    width: 500,
                    anchor: '100%',
                    labelWidth: 50,
                    padding: '0 10 10 10',
                    fieldLabel: 'Улица',
                    typeAhead: true,
                    labelAlign: 'right',
                    store: 'address_street',
                    valueField: 'parent',
                    displayField: 'parent',
                    minChars: 3,
                    listeners: {
                        select: function (combo, value) {
                            this.up('form').getForm().setValues({
                                fias_id: '',
                                house_name: '',
                                house_id: '',
                                zip: value[0].data.zip,
                                street_id: value[0].data.id
                            });
                            upAddress();
                        },
                        beforequery: function (queryPlan) {
                            var city_id = this.up('form').getForm().getValues().city_id;
                            if (city_id) {
                                queryPlan.combo.store.proxy.extraParams.city_id = city_id;
                                queryPlan.combo.store.reload();
                            } else {
                                queryPlan.cancel = true;
                            }
                        }
                    }
                },
                {
                    xtype: 'combobox',
                    name: 'house_name',
                    id: 'select-address-house',
                    width: 500,
                    anchor: '100%',
                    labelWidth: 50,
                    padding: '0 10 10 10',
                    fieldLabel: 'Дом',
                    labelAlign: 'right',
                    typeAhead: true,
                    minChars: 1,
                    store: 'address_house',
                    valueField: 'parent',
                    displayField: 'parent',
                    listeners: {
                        select: function (combo, value) {
                            this.up('form').getForm().setValues({
                                fias_id: value[0].data.fias_id,
                                zip: value[0].data.zip
                            });
                            upAddress();
                        },
                        blur: function () {
                            upAddress();
                        },
                        beforequery: function (queryPlan) {
                            var street_id = this.up('form').getForm().getValues().street_id;
                            if (street_id) {
                                queryPlan.combo.store.proxy.extraParams.street_id = street_id;
                                queryPlan.combo.store.reload();
                            } else {
                                queryPlan.cancel = true;
                            }
                        }
                    }
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    width: 500,
                    anchor: '100%',
                    padding: 10,
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'office_type',
                            id: 'select-address-office-type',
                            store: ['оф.', 'кв.'],
                            width: 50,
                            padding: '0 5 0 0',
                            hideEmptyLabel: true,
                            editable: false,
                            listeners: {
                                change: function () {
                                    upAddress();
                                },
                                render: function () {
                                    this.setValue('оф.');
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'office',
                            id: 'select-address-office',
                            flex: 1,
                            hideEmptyLabel: true,
                            listeners: {
                                change: function () {
                                    upAddress();
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'address',
                    id: 'select-address-string',
                    width: 500,
                    anchor: '100%',
                    readOnly: true,
                    labelWidth: 50,
                    padding: '0 10 10 10',
                    fieldLabel: 'Адрес',
                    labelAlign: 'right',
                    baseCls: 'read-only'
                },
                {
                    xtype: 'hidden',
                    name: 'fias_id',
                    id: 'select-address-fias-id'
                },
                {
                    xtype: 'hidden',
                    name: 'city_id',
                    id: 'select-address-city-id'
                },
                {
                    xtype: 'hidden',
                    name: 'street_id',
                    id: 'select-address-street-id'
                },
                {
                    xtype: 'hidden',
                    name: 'fias_id',
                    id: 'select-address-fias-id'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Выбрать',
            action: 'save'
        }
    ]
});

function upAddress() {
    var address = [];

    var zip = Ext.getCmp('select-address-zip').getValue();
    if (zip) address.push(zip);
    var city = Ext.getCmp('select-address-city').getValue();
    if (city) address.push(city);
    var street = Ext.getCmp('select-address-street').getValue();
    if (street) address.push(street);
    var house = Ext.getCmp('select-address-house').getValue();
    if (house) address.push(house);
    var office = Ext.getCmp('select-address-office').getValue();
    if (office) address.push(Ext.getCmp('select-address-office-type').getValue() + ' ' + office);
    Ext.getCmp('select-address-string').setValue(address.join(', '));
}