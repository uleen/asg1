Ext.define('app.view.form.nomenclature_group', {
    extend: 'Ext.window.Window',
    alias: 'widget.formNomenclatureGroup',
    title: 'Номенклатурная группа',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    width: 400,
    autoShow: true,
    initComponent: function () {
        this.store = Ext.create('app.store.characteristic');
        this.items = [
            {
                xtype: 'form',
                border: 0,
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'pid'
                    },
                    {
                        xtype: 'textfield',
                        name: 'title',
                        padding: '10',
                        fieldLabel: 'Заголовок',
                        anchor: '100%',
                        labelWidth: 65,
                        allowBlank: false
                    },
                    {
                        xtype: 'checkbox',
                        name: 'na',
                        padding: '0 10 10 10',
                        boxLabel: 'Н/А',
                        inputValue: 1
                    }
                ]
            },
            {
                xtype: 'grid',
                tbar: [
                    {
                        text: 'Добавить характеристику',
                        action: 'add',
                        iconCls: 'icon_add',
                        handler: function () {
                            var grid = this.up('grid');
                            var item = Ext.create('app.model.characteristic');
                            grid.getStore().add(item);
                        }
                    }
                ],
                border: 0,
                flex: 1,
                height: 200,
                store: this.store,
                columns: [
                    {
                        text: 'Код',
                        dataIndex: 'id',
                        width: 30
                    },
                    {
                        text: 'Наименование характеристики',
                        dataIndex: 'title',
                        flex: 3,
                        editor: {
                            xtype: 'textfield',
                            allowBlank: false

                        }
                    },
                    {
                        text: 'Сокращенное наименование',
                        dataIndex: 'title_short',
                        flex: 1,
                        editor: 'textfield'
                    },
                    {
                        text: 'Ед. изм.',
                        dataIndex: 'unit',
                        flex: 1,
                        editor: 'textfield'
                    },
                    {
                        xtype: 'actioncolumn',
                        width: 30,
                        align: 'center',
                        items: [
                            {
                                iconCls: 'icon_delete',
                                handler: function (grid, row) {
                                    grid.getStore().removeAt(row);
                                }
                            }
                        ]
                    }
                ],
                plugins: [
                    Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 1})
                ],
                viewConfig: {
                    plugins: {
                        ptype: 'gridviewdragdrop',
                        dragGroup: this,
                        dropGroup: this
                    },
                    listeners: {
                        drop: function(node, data, dropRec, dropPosition) {
                            console.log(node, data, dropRec, dropPosition);
                        }
                    }
                }
            }
        ];
        this.buttons = [
            {
                text: 'Сохранить',
                action: 'save'
            }
        ];
        this.callParent(arguments);
    }
});
