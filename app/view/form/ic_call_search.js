Ext.define('app.view.form.ic_call_search', {
    extend: 'Ext.window.Window',
    alias: 'widget.formLocalSearch',
    title: 'Поиск во входящем запросе',
    layout: 'fit',
    autoShow: true,
    store: 'search_incoming_call',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            xtype: 'datecolumn',
            text: 'Наименование',
            dataIndex: 'call_date',
            format: 'd.m.Y H:i',
            width: 120
        },
        {
            text: 'ИНН',
            dataIndex: 'is_new_title',
            width: 50
        },
        {
            text: 'Контактное лицо',
            dataIndex: 'cagent_name',
            flex: 1
        },
        {
            text: 'Контрагент',
            dataIndex: 'cagent_phone',
            width: 150,
            field: {
                xtype: 'textfield',
                readOnly: true
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'company_title',
            flex: 1
        },
        {
        	text: 'Контрагент',
        	dataIndex: 'is_cagent',
        	flex: 1,	
        },
        {
            text: 'Что интересует',
            dataIndex: 'client_interest_srv',
            flex: 1
        },
        {
            text: 'Источник',
            dataIndex: 'source_title',
            flex: 1
        },
        
        {
            text: 'Акт',
            dataIndex: 'status',
            width: 40,
            align: 'center',
            renderer: function (v) {
                return v == 1 ? 'Да' : 'Нет';
            }
        },
        {
            xtype: 'hidden',
            name: 'root_id'
        }, 
    ],
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'incoming_call',
            dock: 'bottom',
            displayInfo: true,
            beforePageText: 'Страница',
            afterPageText: 'из {0}',
            displayMsg: 'Входящие запросы {0} - {1} из {2}'
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    },

});