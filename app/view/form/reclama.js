Ext.define('app.view.form.reclama', {
    extend: 'Ext.window.Window',
    alias: 'widget.formReclama',
    title: 'Реклама',
    layout: 'fit',
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'title',
                    padding: '10',
                    fieldLabel: 'Заголовок'
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});
