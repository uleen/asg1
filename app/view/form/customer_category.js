Ext.define('app.view.form.customer_category', {
    extend: 'Ext.window.Window',
    alias: 'widget.formCustomerCategory',
    title: 'Направление продаж',
    layout: 'fit',
    width: 400,
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'title',
                    padding: '10',
                    fieldLabel: 'Заголовок',
                    anchor: '100%',
                    labelWidth: 65
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});
