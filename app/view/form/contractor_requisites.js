Ext.define('app.view.form.contractor_requisites', {
    extend: 'Ext.window.Window',
    alias: 'widget.formContractorRequisites',
    title: 'Реквизиты',
    layout: 'fit',
    width: 520,
    autoShow: true,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'contractor',
                    padding: '10',
                    fieldLabel: 'Контрагент',
                    readOnly: true,
                    width: 485,
                    labelWidth: 100
                },
                {
                    xtype: 'hidden',
                    name: 'contractor_id'
                },
                {
                    xtype: 'textfield',
                    name: 'title',
                    padding: '0 10 10 10',
                    fieldLabel: 'Наименование',
                    width: 485,
                    labelWidth: 100
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'triggerfield',
                            name: 'bank_bik',
                            padding: '0 10 10 10',
                            fieldLabel: 'БИК банка',
                            width: 215,
                            labelWidth: 100,
                            maskRe: /\d/,
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            maxLength: 9,
                            enforceMaxLength: true,
                            listeners: {
                                specialkey: function (f, e) {
                                    if (e.getKey() == e.ENTER) {
                                        this.onTriggerClick();
                                    }
                                }
                            },
                            onTriggerClick: function () {
                                var form = this.up('form').getForm();
                                var bic = this.value;
                                Ext.Ajax.request({
                                    url: '/d_contractor_requisites/bank/' + bic,
                                    method: 'GET',
                                    success: function (response) {
                                        if (response.responseText) {
                                            var bank = JSON.parse(response.responseText);
                                            form.setValues({
                                                bank_title: bank.full_name,
                                                bank_ks: bank.ks
                                            });
                                        }
                                    }
                                });
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'bank_ks',
                            readOnly: true,
                            padding: '0 10 10 10',
                            fieldLabel: 'Корр. счет',
                            width: 250,
                            labelWidth: 70
                        },
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'bank_title',
                    readOnly: true,
                    padding: '0 10 10 10',
                    fieldLabel: 'Название банк',
                    width: 485,
                    labelWidth: 100
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'number',
                            padding: '0 10 10 10',
                            fieldLabel: 'Номер счета',
                            maskRe: /\d/,
                            width: 265,
                            labelWidth: 100,
                            maxLength: 20,
                            enforceMaxLength: true
                        },
                        {
                            xtype: 'combobox',
                            name: 'type',
                            editable: false,
                            padding: '0 10 10 10',
                            fieldLabel: 'Вид счета',
                            width: 200,
                            labelWidth: 65,
                            displayField: 'title',
                            valueField: 'id',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['id', 'title'],
                                data: [
                                    {id: 1, title: 'расчетный'},
                                    {id: 2, title: 'иной'}
                                ]
                            })
                        }
                    ]
                },
                {
                    xtype: 'combobox',
                    name: 'currency_id',
                    editable: false,
                    store: 'currency',
                    padding: '0 10 10 10',
                    fieldLabel: 'Валюта',
                    width: 250,
                    labelWidth: 100,
                    displayField: 'code',
                    valueField: 'id'
                },
                {
                    xtype: 'textfield',
                    name: 'payer',
                    fieldLabel: 'Плательщик',
                    padding: '0 10 10 10',
                    width: 485,
                    labelWidth: 100
                },
                {
                    xtype: 'fieldset',
                    title: 'Для непрямых расчетов',
                    layout: 'vbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'fieldset',
                            layout: 'hbox',
                            border: 0,
                            padding: 0,
                            items: [
                                {
                                    xtype: 'triggerfield',
                                    name: 'bank2_bik',
                                    padding: '0 10 10 10',
                                    fieldLabel: 'БИК банка',
                                    width: 215,
                                    labelWidth: 100,
                                    maskRe: /\d/,
                                    triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    maxLength: 9,
                                    enforceMaxLength: true,
                                    listeners: {
                                        specialkey: function (f, e) {
                                            if (e.getKey() == e.ENTER) {
                                                this.onTriggerClick();
                                            }
                                        }
                                    },
                                    onTriggerClick: function () {
                                        var form = this.up('form').getForm();
                                        var bic = this.value;
                                        Ext.Ajax.request({
                                            url: '/d_contractor_requisites/bank/' + bic,
                                            method: 'GET',
                                            success: function (response) {
                                                if (response.responseText) {
                                                    var bank = JSON.parse(response.responseText);
                                                    form.setValues({
                                                        bank2_title: bank.full_name,
                                                        bank2_ks: bank.ks
                                                    });
                                                }
                                            }
                                        });
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'bank2_ks',
                                    readOnly: true,
                                    padding: '0 10 10 10',
                                    fieldLabel: 'Корр. счет',
                                    width: 250,
                                    labelWidth: 70
                                }
                            ]
                        },
                        {
                            xtype: 'textfield',
                            name: 'bank2_title',
                            readOnly: true,
                            padding: '0 10 10 10',
                            fieldLabel: 'Название банк',
                            width: 485,
                            labelWidth: 100
                        }
                    ]
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});

