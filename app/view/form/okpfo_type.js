Ext.define('app.view.form.okpfo_type', {
    extend: 'Ext.window.Window',
    alias: 'widget.formOkpfoType',
    title: 'Организационно-правовые формы',
    layout: 'fit',
    width: 400,
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'title',
                    padding: '10',
                    fieldLabel: 'Краткое название',
                    anchor: '100%',
                    labelWidth: 110
                },
                {
                    xtype: 'textfield',
                    name: 'full_title',
                    padding: '0 10 10 10',
                    fieldLabel: 'Полное название',
                    anchor: '100%',
                    labelWidth: 110
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});
