Ext.define('app.view.form.user_change_password', {
    extend: 'Ext.window.Window',
    alias: 'widget.userChangePassword',
    title: '',
    layout: 'fit',
    width: 415,
    height:180,
    autoShow: true,
    items: [
	        {
	            xtype: 'form',
	            border: 0,
	            layout: {
	            	type: 'vbox',
	            	align: 'strech',
	            },
	            items: [
	                    
                {
                    xtype: 'fieldset',
                    title: 'Смена пароля',
                    margin: 5,
                    items: [
                                           
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            inputType: 'password',
                            id: 'pass-field-one',
                            name: 'password',
                            fieldLabel: 'Пароль',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Пароль" обязательно для заполнения';
                                }
                                var reg = new RegExp(/^[a-z0-9_-]{1,16}$/);
                                if (!reg.test(value)) {
                                    return 'Поле "Пароль" должно содержать только латинские буквы и/или цифры';
                                }
                                return true;
                            },
                            listeners:{
                            	blur: function(field){
                            		
                            		var p1 = Ext.getCmp('pass-field-one').getValue();
                            		var p2 = Ext.getCmp('pass-field-two').getValue();
                            		if (p2 != '')
                            		{
                            			Ext.getCmp('btn-change-password-user-form').setDisabled(true);
                                		if (p1 == p2){
                                			Ext.getCmp('btn-change-password-user-form').setDisabled(false);
                                		}

                            		}
                            	}
                            }
                        },
                        {
                            labelWidth: 100,
                            xtype: 'textfield',
                            inputType: 'password',
                            id: 'pass-field-two',
                            name: 're-password',
                            fieldLabel: 'Повтор пароля',
                            width: 370,
                            validator: function (value) {
                                if (!value) {
                                    return 'Поле "Повтор пароля" обязательно для заполнения';
                                }
                                var reg = new RegExp(/^[a-z0-9_-]{1,16}$/);
                                if (!reg.test(value)) {
                                    return 'Поле "Повтор пароля" должно содержать только латинские буквы и/или цифры';
                                }
                                return true;
                            },
                            listeners:{
                            	blur: function(field){
                            		 Ext.getCmp('btn-change-password-user-form').setDisabled(true);
                            		 
                            		var p1 = Ext.getCmp('pass-field-one').getValue();
                            		var p2 = Ext.getCmp('pass-field-two').getValue();
                            		if (p1 == p2){
                            			Ext.getCmp('btn-change-password-user-form').setDisabled(false);
                            		}
                            	}
                            }
                        },
                        {
                        	xtype: 'hidden',
                        	name:'id'
                        },

                    ]
                },
	        ],
		 }
	],      
	
	buttons: [
	      
    	   {
        	text: 'Подтвердить смену пароля',
        	action: 'AcceptChangeUserPassword',
        	iconCls: 'icon_change_user_password',
            id: 'btn-change-password-user-form',
    	   }        
	       
	]
});