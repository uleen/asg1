Ext.define('app.view.form.nomenclature', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formNomenclature',
    title: 'Основные сведения',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'panel',
            itemId: 'primary',
            layout: 'hbox',
            border: 0,
            margin: '10 10 0',
            items: [
                {
                    xtype: 'textfield',
                    name: 'title',
                    fieldLabel: 'Наименование',
                    labelWidth: 150,
                    flex: 1,
                    labelAlign: 'right'
                },
                {
                    xtype: 'textfield',
                    name: 'id',
                    fieldLabel: 'Код',
                    labelWidth: 40,
                    width: 100,
                    readOnly: true,
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'triggerfield',
            triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
            labelWidth: 150,
            name: 'nomenclature_group_full_title',
            editable: false,
            fieldLabel: 'Номенклатурная группа',
            anchor: '100%',
            margin: '10 10 0',
            labelAlign: 'right',
            onTriggerClick: function () {
                var form = this.up('form').getForm();
                var win = Ext.widget('winNomenclatureGroup');
                var grid = win.getComponent('nomenclature-group');
                win.show();
                grid.on('itemdblclick', function (grid, record) {
                    var nomenclature = new app.controller.nomenclature;
                    form.setValues({
                        nomenclature_group_id: record.get('id'),
                        nomenclature_group_full_title: nomenclature.getPath(record.get('id'))
                    });
                    win.close();
                });
            }
        },
        {
            xtype: 'hidden',
            name: 'nomenclature_group_id'
        },
        {
            xtype: 'panel',
            itemId: 'secondary',
            layout: 'hbox',
            border: 0,
            margin: '10 10 0',
            items: [
                {
                    xtype: 'textfield',
                    name: 'article',
                    width: 350,
                    labelWidth: 150,
                    fieldLabel: 'Артикул',
                    labelAlign: 'right'

                },
                {
                    xtype: 'radiogroup',
                    margin: '0 0 0 30',
                    items: [
                        {name: 'type', inputValue: 'goods', width: 81, boxLabel: 'Товар'},
                        {name: 'type', inputValue: 'service', width: 85, boxLabel: 'Услуга'}
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            itemId: 'units',
            layout: 'hbox',
            border: 0,
            margin: '10 10 0',
            items: [
                {
                    xtype: 'combobox',
                    labelWidth: 150,
                    name: 'unit_id',
                    width: 250,
                    fieldLabel: 'Ед. изм',
                    labelAlign: 'right',
                    editable: false,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'unit'
                },
                {
                    xtype: 'combobox',
                    labelWidth: 70,
                    name: 'shipping_unit_id',
                    width: 170,
                    fieldLabel: 'Ед. отгр',
                    labelAlign: 'right',
                    editable: false,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'unit'
                },
                {
                    xtype: 'textfield',
                    name: 'shipping_min',
                    width: 160,
                    labelWidth: 80,
                    fieldLabel: 'Мин. отгр.',
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'combobox',
            name: 'manager_title',
            typeAhead: true,
            fieldLabel: 'Продукт-менеджер',
            width: 200,
            store: Ext.create('app.store.user.can_auth'),
            labelWidth: 150,
            margin: '10 10 0',
            anchor: '100%',
            labelAlign: 'right',
            displayField: 'name',
            minChars: 2,
            listeners: {
                select: function (combo, records) {
                    var rec = records[0];
                    var form = this.up('form').getForm();
                    form.setValues({
                        manager_id: rec.get('id')
                    });
                }
            }
        },
        {
            xtype: 'hidden',
            name: 'manager_id'
        },
        {
            xtype: 'textfield',
            labelWidth: 150,
            margin: '10 10 0',
            name: 'note',
            fieldLabel: 'Примечание',
            anchor: '100%',
            labelAlign: 'right'
        },
        {
            xtype: 'checkbox',
            name: 'na',
            padding: '10 10 0 165',
            boxLabel: 'Н/А',
            inputValue: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});