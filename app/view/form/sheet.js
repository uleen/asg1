Ext.define('app.view.form.sheet', {
    extend: 'Ext.window.Window',
    title: 'Счет',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    height: 450,
    width: 846,
    minWidth: 846,
    minHeight: 450,
    maximizable: true,
    items: [
        {
            xtype: 'form',
            padding: 0,
            border: 0,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '7 10 3',
                    flex: 2,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Контрагент',
                            border: 1,
                            padding: '2 10 0',
                            items: [
                                {
                                    xtype: 'triggerfield',
                                    fieldLabel: 'Наименование',
                                    name: 'contractor_title_inn',
                                    anchor: '100%',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    margin: '0 0 10',
                                    triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    onTriggerClick: function () {
                                        var contractor_id = this.up('form').getForm().getValues().contractor_id;
                                        var cagent = new app.controller.cagent;
                                        cagent.open(contractor_id);
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contractor_id'
                                },
                                {
                                    xtype: 'fieldset',
                                    border: 0,
                                    padding: 0,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'triggerfield',
                                            fieldLabel: 'Договор',
                                            name: 'contractor_contract_title',
                                            labelWidth: 100,
                                            labelAlign: 'right',
                                            flex: 1,
                                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                            onTriggerClick: function () {
                                                var contractor_contract_id = this.up('form').getForm().getValues().contractor_contract_id;
                                                if (contractor_contract_id) {
                                                    var cContractorContracts = new app.controller.contractor_contracts;
                                                    cContractorContracts.open(contractor_contract_id);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'hidden',
                                            name: 'contractor_contract_id'
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'currency_code',
                                            editable: false,
                                            fieldLabel: 'Валюта расчетов',
                                            width: 200,
                                            labelWidth: 130,
                                            labelAlign: 'right'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Организация',
                            border: 1,
                            padding: '2 10 5',
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Организация',
                                    name: 'company_id',
                                    labelWidth: 100,
                                    editable: false,
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    store: Ext.create('app.store.company'),
                                    displayField: 'title',
                                    valueField: 'id',
                                    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    onTrigger2Click: function () {
                                        if (this.value) {
                                            var company = new app.controller.company;
                                            company.open(this.value);
                                        }
                                    }
                                },
                                {
                                    xtype: 'fieldset',
                                    border: 0,
                                    padding: 0,
                                    margin: '10 0 5 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Банковский счет',
                                            name: 'company_requisite_id',
                                            labelWidth: 100,
                                            editable: false,
                                            labelAlign: 'right',
                                            flex: 1,
                                            store: Ext.create('app.store.company_requisites'),
                                            displayField: 'title',
                                            valueField: 'id',
                                            trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                            listeners: {
                                                beforequery: function (queryPlan) {
                                                    var company_id = this.up('form').getForm().getValues().company_id;
                                                    if (company_id) {
                                                        queryPlan.combo.store.proxy.extraParams.company_id = company_id;
                                                        queryPlan.combo.store.reload();
                                                    } else {
                                                        queryPlan.cancel = true;
                                                    }
                                                }
                                            },
                                            onTrigger2Click: function () {
                                                if (this.value) {
                                                    var company_requisite = new app.controller.company_requisite;
                                                    company_requisite.open(this.value);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Тип',
                                            name: 'sheet_type_id',
                                            labelWidth: 50,
                                            editable: false,
                                            labelAlign: 'right',
                                            width: 150,
                                            store: Ext.create('app.store.sheet_type'),
                                            displayField: 'title',
                                            valueField: 'id'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Счет',
                    width: 300,
                    border: 1,
                    margin: '7 10 3 0',
                    padding: '2 10 5',
                    items: [
                        {
                            xtype: 'fieldset',
                            border: 0,
                            padding: 0,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Номер',
                                    name: 'id',
                                    labelWidth: 50,
                                    labelAlign: 'right',
                                    flex: 1,
                                    readOnly: true
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Дата',
                                    name: 'create_date',
                                    dateFormat: 'd.m.Y H:i',
                                    labelWidth: 50,
                                    labelAlign: 'right',
                                    flex: 1,
                                    readOnly: true
                                }
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Автор',
                            name: 'create_user_name',
                            anchor: '100%',
                            labelWidth: 50,
                            labelAlign: 'right',
                            margin: '0 0 10'
                        },
                        {
                            xtype: 'triggerfield',
                            fieldLabel: 'Заказ',
                            name: 'client_order_title',
                            anchor: '100%',
                            labelWidth: 50,
                            labelAlign: 'right',
                            margin: '0 0 10',
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            onTriggerClick: function () {
                                var client_order_id = this.up('form').getForm().getValues().client_order_id;
                                if (client_order_id) {
                                    var cClientOrder = new app.controller.client_order;
                                    cClientOrder.open(client_order_id);
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'client_order_id'
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Статус',
                            name: 'sheet_status_id',
                            labelWidth: 50,
                            labelAlign: 'right',
                            anchor: '100%',
                            store: Ext.create('app.store.sheet_status'),
                            displayField: 'title',
                            valueField: 'id',
                            margin: '0 0 10'
                        },
                        {
                            xtype: 'textareafield',
                            emptyText: 'Комментарий',
                            name: 'note',
                            anchor: '100%',
                            height: 20
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'grid.client_order_nomenclature',
            border: 0,
            flex: 1,
            style: 'border-top: 1px solid #99bce8'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            itemId: 'buttons',
            items: [
                {
                    text: 'История статусов',
                    iconCls: 'icon_clock_history_frame',
                    padding: 5,
                    handler: function () {
                        var record = this.up('window').down('form').getRecord();
                        if (record.get('id')) {
                            var win = Ext.widget('win.client_order_status_history');
                            var grid = win.down('grid');
                            var store = grid.getStore();
                            store.getProxy().extraParams.client_order_id = record.get('id');
                            store.load();
                            win.show();
                        }
                    }
                },
                {
                    text: 'Печать',
                    itemId: 'print',
                    iconCls: 'icon_printer',
                    padding: 5,
                    hidden: true,
                    handler: function () {
                        var sheet_id = this.up('window').down('form').getForm().findField('id').getValue();
                        window.open('/sheet/print/' + sheet_id, '_blank');
                    }
                },
                '->',
                {
                    text: 'Отменить',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});