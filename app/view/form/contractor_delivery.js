Ext.define('app.view.form.contractor_delivery', {
    extend: 'Ext.window.Window',
    alias: 'widget.formContractorDelivery',
    title: 'Точка доставки / загрузки',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    width: 650,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'contractor',
                            padding: '10',
                            fieldLabel: 'Контрагент',
                            labelAlign: 'right',
                            readOnly: true,
                            width: 430,
                            labelWidth: 90
                        },
                        {
                            xtype: 'hidden',
                            name: 'contractor_id'
                        },
                        {
                            xtype: 'triggerfield',
                            name: 'address',
                            padding: '0 10 10 10',
                            fieldLabel: 'Адрес',
                            labelAlign: 'right',
                            width: 430,
                            labelWidth: 90,
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            id: 'contractor-delivery-address',
                            listeners: {
                                specialkey: function (f, e) {
                                    if (e.getKey() == e.ENTER) {
                                        this.onTriggerClick();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'commentary',
                            padding: '0 10 10 10',
                            fieldLabel: 'Комментарий',
                            labelAlign: 'right',
                            width: 430,
                            labelWidth: 90
                        },
                        {
                            xtype: 'fieldset',
                            layout: 'hbox',
                            border: 0,
                            padding: 0,
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'time_from',
                                    padding: '0 10 0 10',
                                    fieldLabel: 'Окно доставки с',
                                    labelAlign: 'right',
                                    width: 150,
                                    labelWidth: 100,
                                    plugins: [new Ext.ux.InputTextMask('99:99', true)]
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'time_to',
                                    padding: '0 10 0 0',
                                    fieldLabel: 'по',
                                    labelAlign: 'right',
                                    width: 70,
                                    labelWidth: 20,
                                    plugins: [new Ext.ux.InputTextMask('99:99', true)]
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'delay',
                                    maskRe: /\d/,
                                    padding: '0 10 0 0',
                                    labelAlign: 'right',
                                    fieldLabel: 'Вр. пребывания (мин)',
                                    width: 190,
                                    labelWidth: 150
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: 0,
                    width: 180,
                    padding: '10 0 0 0',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'lat',
                            padding: '0 10 10 10',
                            fieldLabel: 'Широта',
                            width: 160,
                            labelWidth: 55,
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'lng',
                            padding: '0 10 10 10',
                            fieldLabel: 'Долгота',
                            width: 160,
                            labelWidth: 55,
                            readOnly: true
                        },
                        {
                            xtype: 'checkbox',
                            name: 'na',
                            padding: '50 10 0 130',
                            boxLabel: 'Н/А',
                            inputValue: 1
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            border: 0,
            id: 'contractor-delivery-map',
            height: 300,
            style: 'border-top: 1px solid #99bce8;'
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save',
            id: 'btnSaveContractorDelivery'
        }
    ]
});