Ext.define('app.view.form.company.requisite', {
    extend: 'Ext.window.Window',
    alias: 'widget.form.company.requisite',
    title: 'Реквизиты',
    layout: 'fit',
    width: 520,
    resizable: false,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'hidden',
                    name: 'company_id'
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: '10 10 0',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'title',
                            fieldLabel: 'Наименование',
                            labelAlign: 'right',
                            labelWidth: 100,
                            flex: 3
                        },
                        {
                            xtype: 'combobox',
                            name: 'currency_id',
                            editable: false,
                            store: 'currency',
                            displayField: 'code',
                            valueField: 'id',
                            fieldLabel: 'Валюта',
                            labelAlign: 'right',
                            labelWidth: 80,
                            flex: 2
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: '0 10',
                    items: [
                        {
                            xtype: 'triggerfield',
                            name: 'bank_bik',
                            fieldLabel: 'БИК банка',
                            labelAlign: 'right',
                            flex: 3,
                            labelWidth: 100,
                            maskRe: /\d/,
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            maxLength: 9,
                            enforceMaxLength: true,
                            listeners: {
                                specialkey: function (f, e) {
                                    if (e.getKey() == e.ENTER) {
                                        this.onTriggerClick();
                                    }
                                }
                            },
                            onTriggerClick: function () {
                                var form = this.up('form').getForm();
                                var bic = this.value;
                                Ext.Ajax.request({
                                    url: '/d_contractor_requisites/bank/' + bic,
                                    method: 'GET',
                                    success: function (response) {
                                        if (response.responseText) {
                                            var bank = JSON.parse(response.responseText);
                                            form.setValues({
                                                bank_title: bank.full_name,
                                                bank_ks: bank.ks
                                            });
                                        }
                                    }
                                });
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'bank_ks',
                            readOnly: true,
                            fieldLabel: 'Корр. счет',
                            labelAlign: 'right',
                            flex: 4,
                            labelWidth: 90
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'bank_title',
                    readOnly: true,
                    fieldLabel: 'Название банка',
                    labelAlign: 'right',
                    labelWidth: 100,
                    anchor: '100%',
                    padding: '10 10 5'
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: '0 10',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'number',
                            fieldLabel: 'Номер счета',
                            labelAlign: 'right',
                            maskRe: /\d/,
                            flex: 3,
                            labelWidth: 100,
                            maxLength: 20,
                            enforceMaxLength: true
                        },
                        {
                            xtype: 'combobox',
                            name: 'type',
                            editable: false,
                            fieldLabel: 'Вид счета',
                            labelAlign: 'right',
                            flex: 2,
                            labelWidth: 80,
                            displayField: 'title',
                            valueField: 'id',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['id', 'title'],
                                data: [
                                    {id: 1, title: 'расчетный'},
                                    {id: 2, title: 'иной'}
                                ]
                            })
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'payer',
                    fieldLabel: 'Плательщик',
                    labelAlign: 'right',
                    labelWidth: 100,
                    anchor: '100%',
                    padding: '10 10 5'
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    boxLabel: 'Н/А',
                    inputValue: 1,
                    padding: '10 10 5'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        '->',
                        {
                            text: 'Сохранить',
                            action: 'company_requisite_save',
                            iconCls: 'icon_disk',
                            padding: 5
                        }
                    ]
                }
            ]
        }
    ]
});

