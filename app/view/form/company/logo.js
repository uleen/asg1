Ext.define('app.view.form.company.logo', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.form.company.logo',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'filefield',
            name: 'file',
            itemId: 'file-form',
            padding: '10',
            fieldLabel: 'Логотип',
            anchor: '100%',
            labelWidth: 70,
            labelAlign: 'right',
            listeners: {
                change: function (el) {
                    var form = this.up('form').getForm();
                    el.up('form').getForm().submit({
                        url: '/upload',
                        waitMsg: 'Загрузка файла...',
                        success: function (form, action) {
                            form.setValues({
                                logo_file_id: action.result.item.id
                            });
                            var fileLink = '<a href="/files/' + action.result.item.name + '" target="_blank">' + action.result.item.title + '</a>';
                            el.up('form').getComponent('file-upload').getComponent('file-title').update(fileLink);
                            this.value = fileLink;
                        }
                    });
                }
            }
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            itemId: 'file-upload',
            border: 0,
            padding: '10',
            hidden: true,
            items: [
                {
                    xtype: 'label',
                    text: 'Логотип:',
                    width: 70,
                    style: 'text-align: right'
                },
                {
                    xtype: 'label',
                    itemId: 'file-title',
                    padding: '0 10'
                },
                {
                    xtype: 'button',
                    iconCls: 'icon_cross',
                    handler: function () {
                        this.up('form').getForm().setValues({logo_file_id: null});
                    }
                },
                {
                    xtype: 'hidden',
                    name: 'logo_file_id',
                    listeners: {
                        change: function () {
                            if (this.value) {
                                this.up('form').getComponent('file-upload').show();
                                this.up('form').getComponent('file-form').hide();
                            } else {
                                this.up('form').getComponent('file-upload').hide();
                                this.up('form').getComponent('file-form').show();
                            }
                        }
                    }
                }
            ]
        },
        {
            xtype: 'htmleditor',
            name: 'advertising_text',
            fieldLabel: 'Рекламный текст',
            labelAlign: 'top',
            anchor: '100%',
            height: 175,
            padding: '0 10'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});
