Ext.define('app.view.form.company.main', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.form.company.main',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'textfield',
            name: 'title',
            fieldLabel: 'Полное название',
            labelWidth: 110,
            labelAlign: 'right',
            anchor: '100%',
            padding: '10 15 5 15'
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            border: 0,
            padding: '0 15',
            anchor: '100%',
            items: [
                {
                    xtype: 'textfield',
                    name: 'title_short',
                    fieldLabel: 'Краткое название',
                    labelAlign: 'right',
                    labelWidth: 110,
                    width: 250
                },
                {
                    xtype: 'textfield',
                    name: 'prefix',
                    fieldLabel: 'Префикс',
                    labelWidth: 80,
                    width: 220,
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            border: 0,
            padding: '0 15',
            items: [
                {
                    xtype: 'radiogroup',
                    width: 250,
                    items: [
                        {name: 'type', inputValue: '1', width: 81, boxLabel: 'Юр. лицо', checked: true},
                        {name: 'type', inputValue: '2', width: 85, boxLabel: 'Физ. лицо'},
                        {name: 'type', inputValue: '3', width: 43, boxLabel: 'ИП'}
                    ]
                },
                {
                    xtype: 'combobox',
                    name: 'okpfo_type_id',
                    fieldLabel: 'ОПФ',
                    store: 'okpfo_type',
                    displayField: 'title',
                    valueField: 'id',
                    labelWidth: 80,
                    width: 220,
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            border: 0,
            padding: '0 15',
            anchor: '100%',
            items: [
                {
                    xtype: 'textfield',
                    name: 'inn',
                    fieldLabel: 'ИНН',
                    maskRe: /\d/,
                    labelAlign: 'right',
                    labelWidth: 110,
                    width: 250
                },
                {
                    xtype: 'textfield',
                    name: 'kpp',
                    fieldLabel: 'КПП',
                    maskRe: /\d/,
                    labelWidth: 80,
                    width: 220,
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            border: 0,
            padding: '0 15',
            items: [
                {
                    xtype: 'textfield',
                    name: 'ogrn',
                    fieldLabel: 'ОГРН',
                    maskRe: /\d/,
                    labelAlign: 'right',
                    labelWidth: 110,
                    width: 250
                },
                {
                    xtype: 'textfield',
                    name: 'okpo',
                    fieldLabel: 'ОКПО',
                    maskRe: /\d/,
                    labelWidth: 80,
                    width: 220,
                    labelAlign: 'right'
                }
            ]
        },
        {
            xtype: 'checkbox',
            name: 'na',
            padding: '0 15 10',
            boxLabel: 'Н/А',
            inputValue: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});
