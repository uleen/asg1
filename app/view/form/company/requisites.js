Ext.define('app.view.form.company.requisites', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.company.requisites',
    initComponent: function () {
        this.store = Ext.create('app.store.company_requisites');
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 30
        },
        {
            header: 'Наименование счета',
            dataIndex: 'title',
            flex: 1
        },
        {
            header: 'Банк',
            dataIndex: 'bank_title',
            flex: 2
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    action: 'add_requisite',
                    iconCls: 'icon_add',
                    padding: 5
                }
            ]
        }
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

