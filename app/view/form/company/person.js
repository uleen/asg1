Ext.define('app.view.form.company.person', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.form.company.person',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'hidden',
            name: 'company_id'
        },
        {
            xtype: 'hidden',
            name: 'type'
        },
        {
            xtype: 'hidden',
            name: 'employee_id'
        },
        {
            xtype: 'combobox',
            fieldLabel: 'ФИО (и.п.)',
            name: 'fio_ip',
            typeAhead: true,
            minChars: 3,
            anchor: '100%',
            labelWidth: 130,
            store: Ext.create('app.store.user.main'),
            labelAlign: 'right',
            padding: '10 10 5',
            trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
            displayField: 'fio',
            listeners: {
                beforequery: function (queryPlan) {
                    var win = this.up('window');
                    queryPlan.combo.store.pageSize = 999;
                    queryPlan.combo.store.proxy.extraParams.company_id = win.company.get('id'); // только по текущей организации
                    queryPlan.combo.store.proxy.extraParams.status = 1; // только работающие
                },
                select: function (combo, records) {
                    var form = this.up('form').getForm();
                    var rec = records[0];
                    var emp = new RussianName(rec.get('fio'));
                    var pos = new RussianName(rec.get('position'));

                    form.setValues({
                        fio_ip: rec.get('fio'),
                        fio_rp: emp.fullName(emp.gcaseRod),
                        employee_id: rec.get('id'),
                        post_ip: rec.get('position'),
                        post_rp: pos.fullName(pos.gcaseRod)
                    });
                }
            },
            onTrigger2Click: function () {
                var employee_id = this.up('form').getForm().getValues().employee_id;
                if (employee_id) {
                    var cEmployee = new app.controller.employee;
                    cEmployee.open(employee_id);
                }
            }
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'fio_rp',
            fieldLabel: 'ФИО (р.п.)',
            labelAlign: 'right',
            anchor: '100%',
            padding: '0 10 5'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'post_ip',
            fieldLabel: 'Должность (и.п.)',
            labelAlign: 'right',
            anchor: '100%',
            padding: '0 10 5'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'post_rp',
            fieldLabel: 'Должность (р.п.)',
            labelAlign: 'right',
            anchor: '100%',
            padding: '0 10 5'
        },
        {
            xtype: 'combobox',
            name: 'reason',
            fieldLabel: 'На основании',
            labelAlign: 'right',
            padding: '0 10 5',
            labelWidth: 130,
            anchor: '100%',
            displayField: 'title',
            store: Ext.create('Ext.data.Store', {
                fields: ['title'],
                data: [
                    {title: 'Приказа №___ от <дд-мм-гггг>г.'},
                    {title: 'Доверенности №___ от <дд-мм-гггг>г.'},
                    {title: 'Устава от <дд-мм-гггг>г.'}
                ]
            })
        },
        {
            xtype: 'fieldset',
            border: 0,
            padding: '0 10',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'datefield',
                    name: 'date_from',
                    fieldLabel: 'Период действия с',
                    labelAlign: 'right',
                    width: 230,
                    labelWidth: 130
                },
                {
                    xtype: 'datefield',
                    name: 'date_to',
                    padding: '0 0 0 5',
                    fieldLabel: 'по',
                    labelAlign: 'right',
                    width: 120,
                    labelWidth: 20
                }
            ]
        },
        {
            xtype: 'filefield',
            name: 'file',
            itemId: 'file-form',
            padding: '0 10 5',
            fieldLabel: 'Скан подписи',
            anchor: '100%',
            labelWidth: 130,
            labelAlign: 'right',
            listeners: {
                change: function (el) {
                    var form = this.up('form').getForm();
                    el.up('form').getForm().submit({
                        url: '/upload',
                        waitMsg: 'Загрузка файла...',
                        success: function (form, action) {
                            form.setValues({
                                scan_file_id: action.result.item.id
                            });
                            var fileLink = '<a href="/files/' + action.result.item.name + '" target="_blank">' + action.result.item.title + '</a>';
                            el.up('form').getComponent('file-upload').getComponent('file-title').update(fileLink);
                            this.value = fileLink;
                        }
                    });
                }
            }
        },
        {
            xtype: 'fieldset',
            layout: 'hbox',
            itemId: 'file-upload',
            border: 0,
            padding: '0 10 5',
            hidden: true,
            items: [
                {
                    xtype: 'label',
                    text: 'Скан подписи:',
                    width: 130,
                    style: 'text-align: right'
                },
                {
                    xtype: 'label',
                    itemId: 'file-title',
                    padding: '0 10'
                },
                {
                    xtype: 'button',
                    iconCls: 'icon_cross',
                    handler: function () {
                        this.up('form').getForm().setValues({scan_file_id: null});
                    }
                },
                {
                    xtype: 'hidden',
                    name: 'scan_file_id',
                    listeners: {
                        change: function () {
                            if (this.value) {
                                this.up('form').getComponent('file-upload').show();
                                this.up('form').getComponent('file-form').hide();
                            } else {
                                this.up('form').getComponent('file-upload').hide();
                                this.up('form').getComponent('file-form').show();
                            }
                        }
                    }
                }
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save-person',
                    action: 'save_person',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});