Ext.define('app.view.form.company.contact', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.form.company.contact',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'addressfield',
            name: 'ur_address_text',
            labelWidth: 130,
            fieldLabel: 'Юридический адрес',
            valueField: 'ur_address_fias',
            jsonField: 'ur_address_json',
            anchor: '100%',
            labelAlign: 'right',
            padding: '10 10 5'
        },
        {
            xtype: 'hidden',
            name: 'ur_address_fias'
        },
        {
            xtype: 'hidden',
            name: 'ur_address_json'
        },
        {
            xtype: 'addressfield',
            name: 'post_address_text',
            labelWidth: 130,
            fieldLabel: 'Почтовый адрес',
            valueField: 'post_address_fias',
            jsonField: 'post_address_json',
            anchor: '100%',
            labelAlign: 'right',
            padding: '0 10 5'
        },
        {
            xtype: 'hidden',
            name: 'post_address_fias'
        },
        {
            xtype: 'hidden',
            name: 'post_address_json'
        },
        {
            xtype: 'addressfield',
            name: 'fact_address_text',
            labelWidth: 130,
            fieldLabel: 'Фактический адрес',
            valueField: 'fact_address_fias',
            jsonField: 'fact_address_json',
            anchor: '100%',
            labelAlign: 'right',
            padding: '0 10 5'
        },
        {
            xtype: 'hidden',
            name: 'fact_address_fias'
        },
        {
            xtype: 'hidden',
            name: 'fact_address_json'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'phone',
            fieldLabel: 'Телефон',
            labelAlign: 'right',
            anchor: '100%',
            plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)],
            padding: '0 10 5'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'fax',
            fieldLabel: 'Факс',
            labelAlign: 'right',
            anchor: '100%',
            plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)],
            padding: '0 10 5'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'email',
            fieldLabel: 'E-mail',
            labelAlign: 'right',
            anchor: '100%',
            vtype: 'email',
            padding: '0 10 5'
        },
        {
            labelWidth: 130,
            xtype: 'textfield',
            name: 'url',
            fieldLabel: 'Сайт',
            labelAlign: 'right',
            anchor: '100%',
            padding: '0 10 5'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    action: 'cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    action: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});