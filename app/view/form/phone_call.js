Ext.define('app.view.form.phone_call', {
    extend: 'Ext.window.Window',
    alias: 'widget.formPhoneCall',
    title: 'Звонок',
    layout: 'fit',
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    margin: 5,
                    labelWidth: 70,
                    name: 'cagent_phone',
                    xtype: 'textfield',
                    width: 255,
                    padding: '0 20 0 0',
                    fieldLabel: 'Телефон'
//                    plugins: [new Ext.ux.InputTextMask('(999) 999-99-99', true)]
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Открыть форму "Входящий запрос"',
            action: 'openIncomingCall'
        }
    ]
});