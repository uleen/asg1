Ext.define('app.view.form.reference_scope', {
    extend: 'Ext.window.Window',
    alias: 'widget.formReferenceScope',
    title: 'Сфера деятельности',
    autoShow: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'title',
                    fieldLabel: 'Название',
                    padding: 10
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ]
        },
        {
            xtype: 'grid',
            tbar: [
                {
                    text: 'Добавить ОКВЭД',
                    action: 'add',
                    iconCls: 'icon_add'
                }
            ],
            border: 0,
            flex: 1,
            name: 'okved',
            height: 200,
            columns: [
                {text: 'Код', dataIndex: 'code', width: 70},
                {text: 'Название', dataIndex: 'name', flex: 1},
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    align: 'center',
                    items: [
                        {
                            iconCls: 'icon_delete',
                            handler: function (grid, rowIndex, colIndex) {
                                grid.getStore().removeAt(rowIndex);
                            }
                        }
                    ]
                }
            ],
            width: 400
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});