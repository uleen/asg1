Ext.define('app.view.form.contractor_contracts', {
    extend: 'Ext.window.Window',
    alias: 'widget.formContractorContracts',
    title: 'Договоры',
    layout: 'fit',
    width: 520,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'contractor',
                    padding: '10',
                    fieldLabel: 'Контрагент',
                    readOnly: true,
                    anchor: '100%',
                    labelWidth: 110
                },
                {
                    xtype: 'hidden',
                    name: 'contractor_id'
                },
                {
                    xtype: 'combobox',
                    name: 'org_id',
                    editable: false,
                    padding: '0 10 10 10',
                    fieldLabel: 'Организация',
                    anchor: '100%',
                    labelWidth: 110,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'company'
                },
                {
                    xtype: 'combobox',
                    name: 'type',
                    editable: false,
                    padding: '0 10 10 10',
                    fieldLabel: 'Вид договора',
                    anchor: '100%',
                    labelWidth: 110,
                    displayField: 'title',
                    valueField: 'id',
                    store: Ext.create('Ext.data.Store', {
                        fields: ['id', 'title'],
                        data: [
                            {id: 1, title: 'с покупателем'},
                            {id: 2, title: 'с поставщиком'}
                        ]
                    })
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'doc_num',
                            padding: '0 10 10 10',
                            fieldLabel: 'Номер договора',
                            width: 230,
                            labelWidth: 110
                        },
                        {
                            xtype: 'datefield',
                            name: 'doc_date',
                            padding: '0 10 10 0',
                            fieldLabel: 'от',
                            labelAlign: 'right',
                            width: 120,
                            labelWidth: 20
                        },
                        {
                            xtype: 'checkbox',
                            name: 'is_frame',
                            padding: '0 10 10 10',
                            boxLabel: 'рамочный',
                            inputValue: 1
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'label',
                            text: 'Срок действия',
                            padding: '0 10 10 10',
                            width: 110
                        },
                        {
                            xtype: 'datefield',
                            name: 'action_from_date',
                            padding: '0 10 10 10',
                            fieldLabel: 'с',
                            labelAlign: 'right',
                            width: 120,
                            labelWidth: 20
                        },
                        {
                            xtype: 'datefield',
                            name: 'action_to_date',
                            padding: '0 10 10 0',
                            fieldLabel: 'по',
                            labelAlign: 'right',
                            width: 120,
                            labelWidth: 20
                        },
                        {
                            xtype: 'checkbox',
                            name: 'prolongation',
                            padding: '0 10 10 10',
                            boxLabel: 'с пролонгацией',
                            inputValue: 1
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'title',
                    padding: '0 10 10 10',
                    fieldLabel: 'Наименование',
                    anchor: '100%',
                    labelWidth: 110
                },
                {
                    xtype: 'combobox',
                    name: 'direction_sales_id',
                    editable: false,
                    padding: '0 10 10 10',
                    fieldLabel: 'Направление',
                    anchor: '100%',
                    labelWidth: 110,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'direction_sales'
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    border: 0,
                    padding: 0,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'amount',
                            padding: '0 10 10 10',
                            fieldLabel: 'Сумма договора',
                            maskRe: /\d/,
                            width: 200,
                            labelWidth: 110
                        },
                        {
                            xtype: 'combobox',
                            name: 'currency_id',
                            editable: false,
                            store: 'currency',
                            padding: '0 10 10 10',
                            fieldLabel: 'Валюта',
                            width: 110,
                            labelWidth: 50,
                            labelAlign: 'right',
                            displayField: 'code',
                            valueField: 'id'
                        },
                        {
                            xtype: 'textfield',
                            name: 'volume',
                            padding: '0 10 10 10',
                            fieldLabel: 'Объем',
                            maskRe: /\d/,
                            width: 135,
                            labelWidth: 50,
                            labelAlign: 'right'
                        }
                    ]
                },
                {
                    xtype: 'combobox',
                    name: 'type_price_id',
                    editable: false,
                    padding: '0 10 10 10',
                    fieldLabel: 'Тип цены',
                    anchor: '100%',
                    labelWidth: 110,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'type_price'
                },
                {
                    xtype: 'combobox',
                    name: 'type_payment_id',
                    editable: false,
                    padding: '0 10 10 10',
                    fieldLabel: 'Вид расчетов',
                    anchor: '100%',
                    labelWidth: 110,
                    displayField: 'title',
                    valueField: 'id',
                    store: 'type_payment'
                },
                {
                    xtype: 'filefield',
                    name: 'file',
                    itemId: 'file-form',
                    padding: '0 10 10 10',
                    fieldLabel: 'Эл. версия',
                    anchor: '100%',
                    labelWidth: 110,
                    listeners: {
                        change: function (el) {
                            var form = this.up('form').getForm();
                            el.up('form').getForm().submit({
                                url: '/upload',
                                waitMsg: 'Загрузка файла...',
                                success: function (form, action) {
                                    form.setValues({
                                        file_id: action.result.item.id
                                    });
                                    var fileLink = '<a href="/files/' + action.result.item.name + '" target="_blank">' + action.result.item.title + '</a>';
                                    el.up('form').getComponent('file-upload').getComponent('file-title').update(fileLink);
                                    el.up('form').getComponent('file-upload').show();
                                    el.up('form').getComponent('file-form').hide();
                                }
                            });
                        }
                    }
                },
                {
                    xtype: 'fieldset',
                    layout: 'hbox',
                    itemId: 'file-upload',
                    border: 0,
                    padding: 0,
                    hidden: true,
                    items: [
                        {
                            xtype: 'label',
                            text: 'Эл. версия:',
                            padding: '0 10 10 10',
                            width: 110
                        },
                        {
                            xtype: 'label',
                            itemId: 'file-title',
                            padding: '0 10 10 20'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'icon_cross',
                            handler: function () {
                                this.up('form').getForm().setValues({file_id: null});
                                this.up('form').getComponent('file-upload').hide();
                                this.up('form').getComponent('file-form').show();
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'file_id'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'commentary',
                    padding: '0 10 10 10',
                    fieldLabel: 'Комментарий',
                    anchor: '100%',
                    labelWidth: 110
                },
                {
                    xtype: 'checkbox',
                    name: 'na',
                    padding: '0 10 10 10',
                    boxLabel: 'Н/А',
                    inputValue: 1
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        '->',
                        {
                            text: 'Сохранить',
                            itemId: 'btn-save',
                            action: 'save',
                            iconCls: 'icon_disk',
                            padding: 5
                        }
                    ]
                }
            ]
        }
    ]
});


