Ext.define('app.view.form.company', {
    extend: 'Ext.window.Window',
    alias: 'widget.formCompany',
    title: 'Организация',
    layout: 'border',
    height: 310,
    width: 700,
    resizable: false,
    items: [
        {
            xtype: 'menu.company',
            itemId: 'company-menu',
            region: 'west',
            width: 170,
            padding: 1
        },
        {
            xtype: 'panel',
            itemId: 'company-form',
            region: 'center',
            padding: 1,
            border: 0,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'form.company.main',
                    itemId: 'form-company-main',
                    flex: 1,
                    hidden: false
                },
                {
                    xtype: 'form.company.contact',
                    itemId: 'form-company-contact',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'grid.company.requisites',
                    itemId: 'grid-company-requisites',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'form.company.person',
                    itemId: 'form-company-chief',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'form.company.person',
                    itemId: 'form-company-booker',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'form.company.logo',
                    itemId: 'form-company-logo',
                    flex: 1,
                    hidden: true
                }
            ]
        }
    ]
});
