Ext.define('app.view.Form', {
    extend: 'Ext.window.Window',
    alias: 'widget.formwindow',
    title: 'Форма',
    layout: 'fit',
    autoShow: true,

    initComponent: function () {



        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        name: 'name',
                        fieldLabel: 'поле 1'
                    },
                    {
                        xtype: 'textfield',
                        name: 'author',
                        fieldLabel: 'поле 2'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Сохранить',
                action: 'save'
            },
            {
                text: 'Отменить',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});

