Ext.define('app.view.Grid3', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid3',
    height: '100%',
    width: '100%',
    columns: [
        {text: 'ID', dataIndex: 'id'},
        {text: 'Name', dataIndex: 'name'},
        {text: 'Phone', dataIndex: 'phone'},
        {text: 'Age', dataIndex: 'age'},
        {
            xtype: 'actioncolumn',
            width: 40,
            items: [
                {
                    iconCls: 'icon_cross',
                    tooltip: 'Удалить',
                    handler: function (grid, i) {
                        var store = grid.getStore();
                        var record = store.getAt(i);// определяем удаляемую запись по номеру строки
                        store.remove(record); // удаление записи
                    }
                }
            ]
        }
    ]
});

