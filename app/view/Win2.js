Ext.define('app.view.Win2', {
    extend: 'Ext.window.Window',
    alias: 'widget.win2',
    title: 'Окно с деревом и виджетом',
    layout: 'fit',
//    autoShow: true,
    height: 300,
    width: 750,
    tbar: [
        {
            text: 'Добавить',
            iconCls: 'icon_note_add'
        },
        {
            text: 'Изменить',
            iconCls: 'icon_note_edit'
        },
        {
            text: 'Удалить',
            iconCls: 'icon_note_delete'
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'treepanel',
                    store: 'Store1',
                    rootVisible: false,
                    width: 200,
                    padding: '1 0 1 1'

                },
                {
                    xtype: 'editgrid',
                    store: 'RecordStore',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ]
});
