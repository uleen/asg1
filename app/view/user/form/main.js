Ext.define('app.view.user.form.main', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.user.form.main',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: 0,
            padding: 0,
            flex: 3,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Основное',
                    margin: '5 5 5 10',
                    items: [
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Организация',
                            name: 'company_title',
                            labelWidth: 100,
                            labelAlign: 'right',
                            anchor: '100%',
                            editable: false,
                            store: Ext.create('app.store.company'),
                            displayField: 'title',
                            listeners: {
                                select: function (combo, records) {
                                    var rCompany = records[0];
                                    this.up('form').getForm().setValues({
                                        company_id: rCompany.get('id')
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'company_id'
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Подразделение',
                            name: 'company_unit_title',
                            labelWidth: 100,
                            labelAlign: 'right',
                            anchor: '100%',
                            editable: false,
                            store: Ext.create('app.store.company_unit'),
                            displayField: 'title',
                            listeners: {
                                select: function (combo, records) {
                                    var rCompanyUnit = records[0];
                                    this.up('form').getForm().setValues({
                                        company_unit_id: rCompanyUnit.get('id')
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'company_unit_id'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'ФИО сотрудника',
                            name: 'name',
                            labelWidth: 100,
                            labelAlign: 'right',
                            anchor: '100%'
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Должность',
                            name: 'position_title',
                            labelWidth: 100,
                            labelAlign: 'right',
                            anchor: '100%',
                            editable: false,
                            store: Ext.create('app.store.dictionary.position'),
                            displayField: 'title',
                            listeners: {
                                select: function (combo, records) {
                                    var rPosition = records[0];
                                    this.up('form').getForm().setValues({
                                        position_id: rPosition.get('id')
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'position_id'
                        },
                        {
                            xtype: 'fieldset',
                            layout: 'hbox',
                            border: 0,
                            padding: 0,
                            margin: '5 0 0',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'datefield',
                                    format: 'd.m.Y',
                                    fieldLabel: 'Дата рождения',
                                    name: 'date_birth',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 200
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0 10',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'datefield',
                                    format: 'd.m.Y',
                                    fieldLabel: 'Дата приема',
                                    name: 'date_hire',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 200
                                },
                                {
                                    xtype: 'datefield',
                                    format: 'd.m.Y',
                                    fieldLabel: 'Дата увольнения',
                                    name: 'date_dismissal',
                                    labelWidth: 130,
                                    labelAlign: 'right',
                                    width: 230
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Контакты',
                    margin: '5 5 5 10',
                    items: [
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Телефон раб',
                                    name: 'phone_work',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230,
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'доб',
                                    name: 'phone_work_add',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Телефон моб',
                                    name: 'phone_mobile',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230,
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Телефон дом',
                                    name: 'phone_home',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230,
                                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0 10',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'textfield',
                                    vtype: 'email',
                                    fieldLabel: 'E-mail лич',
                                    name: 'email_personal',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230
                                },
                                {
                                    xtype: 'textfield',
                                    vtype: 'email',
                                    fieldLabel: 'E-mail раб',
                                    name: 'email_work',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Доступ',
                    margin: '5 5 5 10',
                    items: [
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Логин',
                                    name: 'login',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'can_auth',
                                    margin: '0 0 0 30',
                                    boxLabel: 'Доступ разрешен',
                                    inputValue: 1,
                                    flex: 1
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            layout: {type: 'hbox', align: 'stretch'},
                            border: 0,
                            padding: 0,
                            margin: '5 0 10',
                            flex: 1,
                            items: [
                                {
                                    xtype: 'textfield',
                                    inputType: 'password',
                                    fieldLabel: 'Пароль',
                                    name: 'password',
                                    labelWidth: 100,
                                    labelAlign: 'right',
                                    width: 230
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Права доступа',
            flex: 2,
            margin: '5 10 10 5',
            layout: 'fit',
            items: [
/*
                {
                    xtype: 'user.grid.rules',
                    border: 0,
                    padding: 0
                }
*/
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            itemId: 'buttons',
            items: [
                '->',
                {
                    text: 'Отменить',
                    itemId: 'btn-cancel',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'btn-save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ]
});