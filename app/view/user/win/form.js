Ext.define('app.view.user.win.form', {
    extend: 'Ext.window.Window',
    alias: 'widget.user.win.form',
    title: 'Данные сотрудника',
    width: 856,
    height: 476,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'user.form.main',
            border: 0,
            flex: 1
        }
    ]
});
