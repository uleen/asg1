Ext.define('app.view.user.win.list', {
    extend: 'Ext.window.Window',
    alias: 'widget.user.win.list',
    title: 'Справочник "Сотрудники"',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height: 400,
    width: 850,
    maximizable: true,
    minimizable: true,
    items: [
        {
            xtype: 'user.grid.companies',
            itemId: 'companies',
            width: 150,
            hideHeaders: true,
            padding: 1,
            listeners: {
                itemclick: function(view, rCompany) {
                    var gridUsers = this.up('window').getComponent('users');
                    var storeUsers = gridUsers.getStore();
                    storeUsers.proxy.extraParams.company_id = rCompany.get('id');
                    storeUsers.load();
                }
            }
        },
        {
            xtype: 'user.grid.main',
            itemId: 'users',
            flex: 1,
            padding: 1
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});