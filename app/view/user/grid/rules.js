Ext.define('app.view.user.grid.rules', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.user.grid.rules',
    columns: [
        {
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text: 'Task',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        },
        {
            xtype: 'checkcolumn',
            header: 'Done',
            dataIndex: 'leaf',
            width: 55,
            stopSelection: false,
            menuDisabled: true
        }
    ],
    initComponent: function () {
        this.store = Ext.create('Ext.data.TreeStore', {
            root: {
                text: 'Все разделы',
                expanded: true,
                children: [
                    {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-cagent-main'},
                    {text: "Контактные лица", leaf: true, iconCls: 'icon_group', formId: 'form-cagent-contact'},
                    {text: "Точки доставки", leaf: true, iconCls: 'icon_lorry', formId: 'grid-cagent-delivery'},
                    {text: "Реквизиты и Договоры", leaf: true, iconCls: 'icon_page_copy', formId: 'form-cagent-contract'},
                    {text: "Документы", children: [
                        {text: "Заказы", leaf: true, iconCls: 'icon_calculator', formId: 'grid-cagent-client-order'},
                        {text: "Счета", leaf: true, iconCls: 'icon_page_white_text', formId: 'grid-cagent-sheet'},
                        {text: "Оплаты", leaf: true, iconCls: 'icon_money'},
                        {text: "Акты (сделки)", leaf: true},
                        {text: "Рекламации", leaf: true, iconCls: 'icon_page_red'}
                    ]},
                    {text: "Маркетинг", children: [
                        {text: "Входящие запросы", leaf: true, iconCls: 'icon_bell', formId: 'form-cagent-incoming-call'},
                        {text: "Исходящие звонки", leaf: true, iconCls: 'icon_phone'},
                        {text: "Рассылки", leaf: true, iconCls: 'icon_email'},
                        {text: "Коммерческие предложения", leaf: true, iconCls: 'icon_page_white_go'}
                    ]},
                    {text: "Отчеты", leaf: true, iconCls: 'icon_report'}
                ]
            }
        });
        this.callParent(arguments);
    }
});