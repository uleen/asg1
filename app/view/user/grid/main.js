Ext.define('app.view.user.grid.main', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.user.grid.main',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('can_auth')) {
                    return 'icon_user_go';
                }
            }
        },
        {
            text: 'ФИО',
            dataIndex: 'name',
            flex: 2
        },
        {
            text: 'Должность',
            dataIndex: 'position_title',
            flex: 1
        },
        {
            xtype:'datecolumn',
            text: 'Дата приема',
            dataIndex: 'date_hire',
            format: 'd.m.Y',
            width: 100
        },
        {
            xtype:'datecolumn',
            text: 'Дата увольнения',
            dataIndex: 'date_dismissal',
            format: 'd.m.Y',
            width: 100
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        },
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.user.main');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                itemId: 'buttons',
                items: [
                    {
                        text: 'Добавить',
                        itemId: 'btn-add',
                        iconCls: 'icon_add',
                        padding: 5
                    },
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 300,
                        store: this.store,
                        padding: 5,
                        margin: '0 0 0 30'
                    },
                    '->',
                    {
                        xtype: 'combobox',
                        action: 'filter_status',
                        store: 'filter_status',
                        editable: false,
                        value: 1,
                        forceSelection: true,
                        valueField: 'id',
                        displayField: 'title',
                        padding: '3 10 3 3',
                        listeners: {
                            change: function (view, val) {
                                var store = view.up('grid').getStore();
                                store.getProxy().extraParams.status = val;
                                store.load();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ]
        this.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

