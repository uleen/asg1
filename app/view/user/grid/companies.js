Ext.define('app.view.user.grid.companies', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.user.grid.companies',
    columns: [
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.company');
        this.callParent(arguments);
    }
});