Ext.define('app.view.LeftPanel', {
        extend: 'Ext.panel.Panel',
        alias: 'widget.lp',
        title: 'Первая панель',
        region: 'west',
        collapsible: true,
        width: 300
    }
);