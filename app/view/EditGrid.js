Ext.define('app.view.EditGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.editgrid',
    height: '100%',
    width: '100%',
    initComponent: function () {
        this.columns = [
            {text: 'Company', dataIndex: 'company'},
            {text: 'Price', dataIndex: 'price'},
            {text: 'Change', dataIndex: 'change'},
            {text: '% Change', dataIndex: 'pctChange'},
            {text: 'Last Updated', dataIndex: 'lastChange'}
        ];

        this.callParent(arguments);
    }
});
