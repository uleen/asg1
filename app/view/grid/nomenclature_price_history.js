Ext.define('app.view.grid.nomenclature_price_history', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.nomenclature_price_history',
    columns: [
        {
            xtype: 'datecolumn',
            text: 'Дата и время',
            dataIndex: 'change_date',
            format: 'd.m.Y H:i',
            width: 100
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            flex: 2
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 70
        },
        {
            text: 'Пользователь',
            dataIndex: 'change_user_name',
            flex: 3
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_price_history');
        this.callParent();
    }
});


