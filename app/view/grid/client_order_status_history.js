Ext.define('app.view.grid.client_order_status_history', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.client_order_status_history',
    initComponent: function () {
        this.store = Ext.create('app.store.client_order_status_history');
        this.callParent(arguments);
    },
    columns: [
        {
            xtype: 'datecolumn',
            text: 'Дата изменения',
            dataIndex: 'create_date',
            format: 'd.m.Y H:i',
            flex: 3
        },
        {
            text: 'Статус',
            dataIndex: 'client_order_status_title',
            flex: 2
        },
        {
            text: 'Автор',
            dataIndex: 'user_name',
            flex: 3
        },
        {
            text: 'Комментарий',
            dataIndex: 'client_order_status_note',
            flex: 5
        }
    ]
});