Ext.define('app.view.grid.cagent_list', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridCagentList',
    height: '100%',
    width: '100%',
    store: 'cagent',
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Наименование',
            dataIndex: 'title',
            flex: 1
        },
        {
            text: 'ИНН',
            dataIndex: 'inn',
            width: 80
        },
        /*
        {
            text: 'Категория',
            dataIndex: 'client_category_title',
            width: 70
        },
        */
        {
            text: 'Телефон',
            dataIndex: 'phone',
            width: 80,
        },
        {
            text: 'E-mail',
            dataIndex: 'email',
            width: 80,
        },
        /*
        {
            text: 'Менеджер',
            dataIndex: 'manager_title',
            width: 70
        },
        */
        {
            text: 'Блокирован',
            dataIndex: 'is_blocked',
            width: 50,
            align: 'center',
            renderer: function (v) {
                return v == 1 ? 'Да' : 'Нет';
            }
        }
    ],
    tbar: [
        {
            labelWidth: 40,
            fieldLabel: 'Поиск',
            xtype: 'searchfield',
            width: 400,
            store: 'cagent',
            padding: 5
        },
    ],
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'cagent',
            dock: 'bottom',
            displayInfo: true,
            beforePageText: 'Страница',
            afterPageText: 'из {0}',
            displayMsg: 'Контрагенты {0} - {1} из {2}'
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('is_blocked') == 2 ? 'active-row' : 'no-active-row';
        }
    }
});

