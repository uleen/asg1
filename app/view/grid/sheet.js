Ext.define('app.view.grid.sheet', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.sheet',
    columns: [
        {
            text: 'Номер',
            dataIndex: 'id',
            width: 50
        },
        {
            text: 'Тип',
            dataIndex: 'sheet_type_title',
            width: 30,
            align: 'center'
        },
        {
            xtype: 'datecolumn',
            text: 'Дата счета',
            dataIndex: 'create_date',
            format: 'd.m.Y',
            width: 100
        },
        {
            text: 'Состояние',
            dataIndex: 'sheet_status_title',
            width: 100
        },
        {
            text: 'Контрагент',
            dataIndex: 'contractor_title',
            flex: 1
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 100
        },
        {
            xtype: 'numbercolumn',
            text: 'Сумма счета',
            dataIndex: 'sheet_sum',
            format: '0.00',
            align: 'right',
            flex: 1
        },
        {
            xtype: 'numbercolumn',
            text: 'Сумма НДС',
            dataIndex: 'sheet_sum_nds',
            format: '0.00',
            align: 'right',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.sheet');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                itemId: 'top',
                items: [
                    '->',
                    {
                        xtype: 'combobox',
                        store: Ext.create('app.store.sheet_status'),
                        editable: false,
                        valueField: 'id',
                        displayField: 'title',
                        padding: '3 10 3 3',
                        listeners: {
                            change: function (view, val) {
                                var store = view.up('grid').getStore();
                                store.getProxy().extraParams.status = val;
                                store.load();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ]
        this.callParent();
    }
});

