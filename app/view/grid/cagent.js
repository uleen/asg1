Ext.define('app.view.grid.cagent', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridCagent',
    height: '100%',
    width: '100%',
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Наименование',
            dataIndex: 'title',
            flex: 1
        },
        {
            text: 'ИНН',
            dataIndex: 'inn',
            width: 150
        },
        {
            text: 'Категория',
            dataIndex: 'client_category_title',
            width: 70
        },
        {
            text: 'Телефон',
            dataIndex: 'phone',
            width: 150,
        },
        {
            text: 'E-mail',
            dataIndex: 'email',
            width: 150,
        },
        {
            text: 'Менеджер',
            dataIndex: 'manager_title',
            width: 70
        },
        {
            text: 'Блокирован',
            dataIndex: 'is_blocked',
            width: 50,
            align: 'center',
            renderer: function (v) {
                return v == 1 ? 'Да' : 'Нет';
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.cagent');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 400,
                        store: this.store,
                        padding: 5
                    },
                    {
                        text: 'Добавить поставщика',
                        action: 'btnCagent',
                        iconCls: 'icon_group_add',
                        padding: 5,
                        margin: '0 0 0 30'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ];
        this.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('is_blocked') == 2 ? 'active-row' : 'no-active-row';
        }
    }
});

