Ext.define('app.view.grid.reference_scope', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridReferenceScope',
    height: '100%',
    width: '100%',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            action: 'edit',
            tooltip: 'Редактировать',
            iconCls: 'icon_pencil'
        }
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

