Ext.define('app.view.grid.nomenclature_price', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridNomenclaturePrice',
    title: 'Отпускные цены',
    columns: [
        {
            text: 'Код',
            dataIndex: 'type_price_id',
            width: 30
        },
        {
            text: 'Наименование',
            dataIndex: 'type_price_title',
            flex: 2
        },
        {
            xtype: 'numbercolumn',
            text: 'Значение',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            flex: 1,
            field: {
                xtype: 'pricefield'
            }
        },
        {
            text: 'Валюта',
            dataIndex: 'currency',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            tooltip: 'История цен',
            iconCls: 'icon_table_chart',
            handler: function (grid, rowIndex) {
                var rec = grid.getStore().getAt(rowIndex);
                var win = Ext.create('app.view.win.nomenclature_price_history');

                win.down('form').getForm().setValues({
                    nomenclature_title: rec.get('nomenclature_title'),
                    type_price_title: rec.get('type_price_title')
                });

                win.down('grid').getStore().load({
                    params: {
                        nomenclature_id: rec.get('nomenclature_id'),
                        type_price_id: rec.get('type_price_id')
                    },
                    callback: function (records, operation, success) {
                        if (success) {
                            win.show();
                        }
                    }
                });
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_price');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: [
                    '->',
                    {
                        text: 'Сохранить',
                        iconCls: 'icon_disk',
                        padding: 5,
                        handler: function () {
                            this.up('grid').getStore().sync();
                        }
                    }
                ]
            }
        ]
        this.callParent();
    },
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ]
});


