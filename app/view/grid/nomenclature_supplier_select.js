Ext.define('app.view.grid.nomenclature_supplier_select', {
    extend: 'Ext.grid.Panel',
    columns: [
        {
            text: 'Код',
            dataIndex: 'contractor_delivery_id',
            width: 35
        },
        {
            text: 'Поставщик',
            dataIndex: 'contractor_title',
            flex: 2
        },
        {
            text: 'Адрес',
            dataIndex: 'contractor_delivery_address',
            flex: 3
        },
        {
            text: 'Артикул поставщика',
            dataIndex: 'article',
            flex: 2
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            flex: 1
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 50
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 50
        },
        {
            header: 'Наличие',
            dataIndex: 'in_stock',
            width: 65,
            renderer: function (v) {
                return v ? 'Есть' : 'Нет';
            }
        },
        {
            text: 'Примечание',
            dataIndex: 'note',
            flex: 1
        },
        {
            text: 'Расстояние',
            dataIndex: 'distance',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_supplier');
        this.callParent();
    },
    viewConfig: {
        markDirty: false,
        getRowClass: function (record) {
            return record.get('in_stock') ? 'active-row' : 'no-active-row';
        }
    }
});


