Ext.define('app.view.grid.client_order', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.client_order',
    columns: [
        {
            text: 'Номер',
            dataIndex: 'id',
            width: 50
        },
        {
            xtype: 'datecolumn',
            text: 'Время создания',
            format: 'd.m.Y H:i',
            dataIndex: 'create_date',
            width: 120
        },
        {
            text: 'Наименование контрагента',
            dataIndex: 'contractor_title',
            flex: 2
        },
        {
            text: 'Дата доставки',
            width: 180,
            renderer: function (v, c, r) {
                return 'с ' + Ext.Date.format(r.get('date_from'), 'd.m.Y') + ' по ' + Ext.Date.format(r.get('date_to'), 'd.m.Y');
            }
        },
        {
            text: 'Адрес доставки',
            dataIndex: 'contractor_delivery_address',
            flex: 3
        },
        {
            text: 'Статус',
            dataIndex: 'client_order_status_title',
            flex: 1
        },
        {
            text: 'Автор',
            dataIndex: 'create_user_name',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.client_order');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                itemId: 'top',
                items: [
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 400,
                        store: this.store,
                        padding: 5
                    },
                    {
                        text: 'Добавить',
                        action: 'btnAddClientOrder',
                        iconCls: 'icon_add',
                        padding: 5,
                        margin: '0 0 0 30'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ]
        this.callParent();
    },
    viewConfig: {
        getRowClass: function (record) {
//            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

