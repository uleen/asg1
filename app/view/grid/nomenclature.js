Ext.define('app.view.grid.nomenclature', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridNomenclature',
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Номенкл. группа',
            dataIndex: 'nomenclature_group_title',
            flex: 1
        },
        {
            text: 'Артикул',
            dataIndex: 'article',
            flex: 1
        },
        {
            text: 'Тип',
            dataIndex: 'type',
            width: 50,
            renderer: function (value) {
                return value == 'goods' ? 'товар' : 'услуга';
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'title',
            flex: 1
        },
        {
            text: 'Характеристики',
            dataIndex: 'characteristics',
            flex: 1
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 150
        },
        {
            text: 'Ед. отгр.',
            dataIndex: 'shipping_unit_title',
            width: 150
        },
        {
            text: 'Мин. отгр.',
            dataIndex: 'shipping_min',
            width: 150
        },
        {
            text: 'Менеджер',
            dataIndex: 'manager_title',
            width: 100
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 400,
                        store: this.store,
                        padding: 5
                    },
                    {
                        text: 'Добавить',
                        action: 'btnAddNomenclature',
                        iconCls: 'icon_add',
                        padding: 5,
                        margin: '0 0 0 30'
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ]
        this.callParent();
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

