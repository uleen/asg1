Ext.define('app.view.grid.nomenclature_group', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.gridNomenclatureGroup',
    height: '100%',
    width: '100%',
    hideHeaders: true,
    useArrows: true,
    rootVisible: false,
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_group');
        this.columns = [
            {
                width: 30,
                dataIndex: 'id'
            },
            {
                xtype: 'treecolumn', //this is so we know which column will show the tree
                flex: 1,
                dataIndex: 'title'
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                align: 'center',
                getClass: function (v, meta, rec) {
                    if (rec.get('status') == 1) {
                        this.items[0].tooltip = 'Активно';
                        return 'icon_bullet_green';
                    }
                    if (rec.get('status') == 2) {
                        this.items[0].tooltip = 'Не активно';
                        return 'icon_bullet_red';
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                align: 'center',
                action: 'edit',
                tooltip: 'Редактировать',
                iconCls: 'icon_pencil'
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                align: 'center',
                action: 'add',
                tooltip: 'Добавить подкатегорию',
                iconCls: 'icon_add'
            }
        ];

        this.callParent();
    }
});

