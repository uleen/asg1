Ext.define('app.view.grid.contact_personal', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridContactPersonal',
    height: '100%',
    width: '100%',
    initComponent: function () {
        this.store = Ext.create('app.store.contact_personal');
        this.dockedItems = [
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}',
                displayMsg: 'Контактные лица {0} - {1} из {2}',
                itemid: 'paging-contact-personal'
            }
        ];
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'ФИО',
            dataIndex: 'fio',
            flex: 1
        },
        {
            text: 'Контрагент',
            dataIndex: 'cagent_title',
            flex: 1
        },
        {
            text: 'Телефон',
            dataIndex: 'cagent_phone',
            flex: 1
        },

        {
            text: 'Доп.',
            dataIndex: 'cagent_add_phone',
            width: 50,
        },
        {
            text: 'E-mail',
            dataIndex: 'cagent_email',
            flex: 1
        },
        {
            text: 'Должность',
            dataIndex: 'position',
            flex: 1
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('cp_status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('cp_status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        },
        /*
         {
         xtype: 'actioncolumn',
         width: 30,
         sortable: false,
         align: 'center',
         action: 'edit',
         tooltip: 'Редактировать',
         iconCls: 'icon_pencil'
         },
         */
        {
            text: 'org_id',
            hidden: true,
            dataIndex: 'org_id'
        },
        {
            text: 'contractor_id',
            hidden: true,
            dataIndex: 'contractor_id'
        }
    ],
    viewConfig: {
        getRowClass: function (record) {
            // return record.get('cp_status') == 1 ? 'active-row' : 'no-active-row';
            //console.log(record);
            if ((record.get('cp_status')) == 1 && (record.get('contact_personal_id') == record.get('main_personal_id'))) {
                console.log('getMP: ' + record.get('contact_personal_id'));
                return 'sel-row';

            }
            else if (record.get('cp_status') == 1 && (record.get('contact_personal_id') != record.get('main_personal_id'))) {
                console.log('active-row: ' + record.get('contact_personal_id'));
                return 'active-row';

            }
            else if (record.get('cp_status') == 2 && (record.get('contact_personal_id') != record.get('main_personal_id'))) {
                console.log('no-row: ' + record.get('contact_personal_id'));
                return 'no-active-row';

            }
            else if (record.get('cp_status') == 2 && (record.get('contact_personal_id') == record.get('main_personal_id'))) {
                console.log('no-row: ' + record.get('contact_personal_id'));
                return 'no-active-row';

            }
            else {
                return 'active-row';
            }

        }
    }
});

