Ext.define('app.view.grid.nomenclature_supplier', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridNomenclatureSupplier',
    title: 'Поставщики',
    columns: [
        {
            text: 'Код',
            dataIndex: 'contractor_delivery_id',
            width: 35
        },
        {
            text: 'Поставщик',
            dataIndex: 'contractor_title',
            flex: 2,
            field: {
                xtype: 'triggerfield',
                triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
                editable: false,
                onTriggerClick: function () {
                    var record = this.up('grid').getSelectionModel().selected.items[0];
                    var exclude = [];
                    this.up('grid').getStore().each(function (rec) {
                        exclude.push(rec.get('contractor_delivery_id'));
                    });

                    var win = Ext.widget('winContractorDelivery');
                    var grid = win.down('grid');
                    grid.getDockedComponent('for-all').show();
                    win.show();

                    var store = grid.getStore();
                    store.getProxy().extraParams.status = 1;    // только активные
                    store.getProxy().extraParams.org_type = 2;  // только поставщики
                    store.getProxy().extraParams.exclude = exclude.join(',');  // исключений точек, которые уже выбраны
                    store.load();

                    grid.on('itemdblclick', function (grid, rec) {
                        record.set('contractor_delivery_id', rec.get('id'));
                        record.set('contractor_title', rec.get('contractor'));
                        win.close();
                    });
                }
            }
        },
        {
            text: 'Артикул поставщика',
            dataIndex: 'article',
            flex: 2,
            field: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            flex: 1,
            field: {
                xtype: 'pricefield',
                allowBlank: false
            }
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 50,
            field: {
                xtype: 'combobox',
                store: Ext.create('app.store.currency'),
                displayField: 'code',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('currency_id', rec.get('id'));
                        record.set('currency_code', rec.get('code'));
                    }
                }
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            tooltip: 'История цен',
            iconCls: 'icon_table_chart',
            action: 'history'
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 50,
            field: {
                xtype: 'combobox',
                store: Ext.create('app.store.unit'),
                displayField: 'title',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('unit_id', rec.get('id'));
                        record.set('unit_title', rec.get('title'));
                    }
                }
            }
        },
        {
            xtype: 'checkcolumn',
            header: 'Наличие',
            dataIndex: 'in_stock',
            width: 65
        },
        {
            text: 'Примечание',
            dataIndex: 'note',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_supplier');
        this.callParent();
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('grid');
                        var record = Ext.create('app.model.nomenclature_supplier', {
                            nomenclature_id: grid.up('window').nomenclature.id,
                            in_stock: 1
                        });
                        grid.getStore().add(record);
                    }
                }
            ]
        },
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Сохранить',
                    iconCls: 'icon_disk',
                    padding: 5,
                    handler: function () {
                        this.up('grid').getStore().sync();
                    }
                }
            ]
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ]
})
;


