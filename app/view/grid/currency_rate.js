Ext.define('app.view.grid.currency_rate', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridCurrencyRate',
    initComponent: function () {
        this.store = Ext.create('app.store.currency_rate');
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'Дата',
            dataIndex: 'cdate',
            flex: 1
        },
        {
            text: 'Курс',
            dataIndex: 'rate',
            flex: 1
        },
        {
            text: 'Номинал',
            dataIndex: 'nom',
            flex: 1
        }
    ]
});

