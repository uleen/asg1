Ext.define('app.view.grid.incoming_call', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridIncomingCall',
    height: '100%',
    width: '100%',
    store: 'incoming_call',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
        	xtype: 'actioncolumn',
            dataIndex: 'ise',
            width: 20,
            sortable: true,
            align: 'center',
          
            getClass: function (v, meta, rec) {
            	 if (rec.get('executed') == 1) {
                     this.items[0].tooltip = 'Выполнен';
                     return 'icon_accept';
                 }
            	 /*
                 if (rec.get('is_cagent') == 0) {
                     this.items[0].tooltip = 'ЛИД';
                     return 'icon_error';
                 }
            	 */
            },
        },

        {
            xtype: 'datecolumn',
            text: 'Дата и время',
            dataIndex: 'call_date',
            format: 'd.m.Y H:i',
            width: 120
        },
        {
            text: 'Новый',
            dataIndex: 'is_new_title',
            width: 50
        },
        {
            text: 'Контактное лицо',
            dataIndex: 'cagent_name',
            flex: 1
        },
        {
            text: 'Телефон',
            dataIndex: 'cagent_phone',
            width: 150,
            field: {
                xtype: 'textfield',
                readOnly: true
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'company_title',
            flex: 1
        },
        {
            text: 'Что интересует',
            dataIndex: 'client_interest_srv',
            flex: 1
        },
        {
            text: 'Источник',
            dataIndex: 'source_title',
            flex: 1
        },
        {
            text: 'Акт',
            dataIndex: 'status',
            width: 40,
            align: 'center',
            renderer: function (v) {
                return v == 1 ? 'Да' : 'Нет';
            }
        }
    ],
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'incoming_call',
            dock: 'bottom',
            displayInfo: true,
            beforePageText: 'Страница',
            afterPageText: 'из {0}',
            displayMsg: 'Входящие запросы {0} - {1} из {2}',
            itemid: 'paging-incoming-call'
        },
        {
            xtype: 'textareafield',
            dock: 'bottom',
            readOnly: true,
            itemid: 'area-client-interest'

        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    },
    listeners: {
        select: function (grid, record) {
            this.getDockedItems('[itemid=area-client-interest]')[0].setValue(record.get('client_interest'));
        }
    }

});

