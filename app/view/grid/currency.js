Ext.define('app.view.grid.currency', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridCurrency',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Код',
            dataIndex: 'num',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            tooltip: 'История',
            handler: function (grid, row) {
                var record = grid.getStore().getAt(row);
                var win = Ext.widget('winCurrencyRate');
                var grid = win.down('grid');
                var store = grid.getStore();
                store.getProxy().extraParams.num = record.get('num');
                store.load();
                win.show();
            },
            getClass: function (v, meta, rec) {
               return rec.get('num') ? 'icon_table_chart' : '';
            }
        },
        {
            text: 'Символ',
            dataIndex: 'code',
            width: 100,
            field: {
                xtype: 'combobox',
                store: Ext.create('app.store.currency_all'),
                displayField: 'code',
                listeners: {
                    select: function(combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('num', rec.get('num'));
                        record.set('code', rec.get('num'));
                    }
                }
            }
        },
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            handler: function (grid, row) {
                var record = grid.getStore().getAt(row);
                record.set('status', record.get('status') == 1 ? 2 : 1);
            },
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    initComponent : function () {
        this.store = Ext.create('app.store.currency');
        this.store.getProxy().extraParams = {status: 1};
        this.store.load();
        this.callParent(arguments);
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('grid');
                        var record = Ext.create('app.model.currency', {
                            status: 1
                        });
                        grid.getStore().add(record);
                    }
                },
                '->',
                {
                    xtype: 'combobox',
                    action: 'filter_status',
                    store: 'filter_status',
                    editable: false,
                    value: 1,
                    forceSelection: true,
                    valueField: 'id',
                    displayField: 'title',
                    listeners: {
                        change: function (view, val) {
                            var store = view.up('window').down('grid').getStore();
                            store.getProxy().extraParams = {
                                status: val
                            };
                            store.load();
                        }
                    }
                }
            ]
        },
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Сохранить',
                    iconCls: 'icon_disk',
                    padding: 5,
                    handler: function () {
                        this.up('grid').getStore().sync();
                    }
                }
            ]
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

