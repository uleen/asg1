Ext.define('app.view.grid.table_manager', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridTableManager',
    features: [
        {
            ftype: 'grouping',
            groupHeaderTpl: '{name}'
        }
    ],
    columns: [
        {
            text: 'Номенкл. группа',
            dataIndex: 'nomenclature_group_title',
            flex: 2
        },
        {
            text: 'Артикул',
            dataIndex: 'nomenclature_article',
            flex: 2
        },
        {
            text: 'Тип',
            dataIndex: 'nomenclature_type',
            width: 50,
            renderer: function (v) {
                return v == 'goods' ? 'товар' : 'услуга';
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'nomenclature_title',
            flex: 2
        },
        {
            text: 'Характеристики',
            dataIndex: 'nomenclature_characteristics',
            flex: 2
        },
        {
            text: 'Мин. отгр.',
            width: 70,
            renderer: function (v, c, r) {
                return r.get('nomenclature_shipping_min') ? (r.get('nomenclature_shipping_min') + (r.get('nomenclature_shipping_unit_title') ? ' ' + r.get('nomenclature_shipping_unit_title') : '')) : '';
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Отпускная цена',
            dataIndex: 'our_price',
            format: '0.00',
            align: 'right',
            flex: 1
        },
        {
            text: 'Валюта',
            dataIndex: 'our_currency_code',
            width: 50,
            renderer: function (v, c, r) {
                return r.get('our_price') ? v : '';
            }
        },
        {
            text: 'Точка отгрузки',
            dataIndex: 'contractor_delivery_address',
            flex: 3
        },
        {
            text: 'Артикул поставщика',
            dataIndex: 'article',
            flex: 2
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена поставщика',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            flex: 1
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 50
        },
        {
            text: 'Наличие',
            dataIndex: 'in_stock',
            width: 50,
            renderer: function (v, c, r) {
                return r.get('nomenclature_type') == 'goods' ? (v ? 'Есть' : 'Нет') : '';
            }
        },
        {
            text: 'Примечание',
            dataIndex: 'note',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_supplier');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 400,
                        store: this.store,
                        padding: 5
                    },
                    {
                        text: 'По поставщикам',
                        iconCls: 'icon_group',
                        padding: 5,
                        margin: '0 0 0 30',
                        enableToggle: true,
                        store: this.store,
                        handler: function () {
                            if (this.pressed) {
                                this.store.group('contractor_title');
                            } else {
                                this.store.clearGrouping();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ]
        this.callParent();
    }
});