Ext.define('app.view.grid.type_payment', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridTypePayment',
    height: '100%',
    width: '100%',
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    action: 'add',
                    iconCls: 'icon_add'
                },
                '->',
                {
                    xtype: 'combobox',
                    store: 'filter_status',
                    editable: false,
                    value: 1,
                    forceSelection: true,
                    valueField: 'id',
                    displayField: 'title',
                    padding: '3 10 3 3',
                    listeners: {
                        change: function (view, val) {
                            var store = view.up('grid').getStore();
                            store.getProxy().extraParams.status = val;
                            store.load();
                        }
                    }
                }
            ]
        }
    ],
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Title',
            dataIndex: 'title',
            flex: 1
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            action: 'edit',
            tooltip: 'Редактировать',
            iconCls: 'icon_pencil'
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.type_payment');
        this.store.getProxy().extraParams.status = 1;
        this.store.load();
        this.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

