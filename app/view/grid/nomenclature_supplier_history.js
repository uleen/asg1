Ext.define('app.view.grid.nomenclature_supplier_history', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridNomenclatureSupplierHistory',
    columns: [
        {
            xtype: 'datecolumn',
            text: 'Дата и время',
            dataIndex: 'create_date',
            format: 'd.m.Y H:i',
            width: 100
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            width: 70
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 50
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 50
        },
        {
            text: 'Наличие',
            dataIndex: 'in_stock',
            width: 50,
            renderer: function (v) {
                return v ? 'Есть' : 'Нет';
            }
        },
        {
            text: 'Пользователь',
            dataIndex: 'user_title',
            width: 70
        },
        {
            text: 'Примечание',
            dataIndex: 'note',
            flex: 1
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_supplier_history');
        this.callParent();
    }
});


