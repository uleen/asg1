Ext.define('app.view.grid.client_order_nomenclature', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.client_order_nomenclature',
    columns: [
        {
            text: 'Код',
            dataIndex: 'nomenclature_id',
            width: 30
        },
        {
            text: 'Артикул',
            dataIndex: 'nomenclature_article',
            width: 70,
            field: {
                xtype: 'combobox',
                store: 'nomenclature',
                typeAhead: true,
                minChars: 1,
                displayField: 'article',
                listeners: {
                    beforequery: function (queryPlan) {
                        queryPlan.combo.store.proxy.extraParams = {
                            article: queryPlan.query,
                            status: 1
                        };
                    },
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('nomenclature_id', rec.get('id'));
                        record.set('nomenclature_title', rec.get('title'));
                        record.set('nomenclature_characteristics', rec.get('characteristics'));
                        record.set('unit_id', rec.get('unit_id'));
                        record.set('unit_title', rec.get('unit_title'));
                        record.set('price', rec.get('price'));
                    }
                }
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'nomenclature_title',
            flex: 1,
            field: {
                xtype: 'combobox',
                store: 'nomenclature',
                typeAhead: true,
                minChars: 1,
                displayField: 'title',
                listeners: {
                    beforequery: function (queryPlan) {
                        queryPlan.combo.store.proxy.extraParams = {
                            title: queryPlan.query,
                            status: 1
                        };
                    },
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('nomenclature_id', rec.get('id'));
                        record.set('nomenclature_article', rec.get('article'));
                        record.set('nomenclature_characteristics', rec.get('characteristics'));
                        record.set('unit_id', rec.get('unit_id'));
                        record.set('unit_title', rec.get('unit_title'));
                        record.set('price', rec.get('price'));
                    }
                }
            }
        },
        {
            text: 'Характеристики',
            dataIndex: 'nomenclature_characteristics',
            width: 120
        },
        {
            text: 'Кол-во',
            dataIndex: 'volume',
            width: 50,
            field: {
                xtype: 'textfield',
                allowBlank: false,
                maskRe: /\d/
            }
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 50
        },
        {
            text: 'Поставщик',
            dataIndex: 'contractor_title',
            flex: 1,
            field: {
                xtype: 'triggerfield',
                triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
                onTriggerClick: function () {
                    var record = this.up('grid').getSelectionModel().selected.items[0];
                    if (record.get('nomenclature_id')) {
                        var form = this.up('window').down('form').getForm();
                        var orderData = form.getValues();

                        var win = Ext.create('app.view.win.nomenclature_supplier_select');
                        win.show();

                        var grid = win.down('grid');
                        grid.on('itemclick', function (view, rec) {
                            map.geoObjects.each(function (geoObject) {
                                if (!geoObject.getOverlay()) {
                                    map.geoObjects.remove(geoObject);
                                }
                            });
                            if (rec.route) {
                                var rp = rec.route.getPaths();
                                rp.options.set({ strokeWidth: 5, strokeColor: '0000ffff', opacity: 0.5 });
                                map.geoObjects.add(rp);
                            }
                        });
                        grid.on('itemdblclick', function (view, rec) {
                            if (record.get('nomenclature_id') == rec.get('nomenclature_id')) {
                                record.set('contractor_delivery_id', rec.get('contractor_delivery_id'));
                                record.set('contractor_title', rec.get('contractor_title'));
                            }
                            win.close();
                        });

                        var panel = win.getComponent('map');
                        var map = new ymaps.Map(panel.id + '-innerCt', {
                            center: [55.76, 37.64],
                            zoom: 10,
                            behaviors: ['default', 'scrollZoom']
                        });
                        win.on('resize', function () {
                            map.container.fitToViewport();
                        });

                        var pointDelivery = null;
                        if (orderData.contractor_delivery_lat && orderData.contractor_delivery_lng) {
                            pointDelivery = new ymaps.Placemark([orderData.contractor_delivery_lat, orderData.contractor_delivery_lng]);
                            map.geoObjects.add(pointDelivery);
                        }

                        var store = grid.getStore();
                        store.load({
                            params: {nomenclature_id: record.get('nomenclature_id')},
                            callback: function (records, operation, success) {
                                if (success) {
                                    store.each(function (rec) {
                                        var pm = new ymaps.Placemark([rec.get('contractor_delivery_lat'), rec.get('contractor_delivery_lng')], {
                                            balloonContentHeader: rec.get('contractor_title'),
                                            balloonContentBody: rec.get('contractor_delivery_address'),
                                            balloonContentFooter: rec.get('note'),
                                            iconContent: rec.get('price') + ' ' + rec.get('currency_code') + '/' + rec.get('unit_title')
                                        }, {preset: rec.get('in_stock') ? 'twirl#darkgreenStretchyIcon' : 'twirl#greyStretchyIcon'});
                                        map.geoObjects.add(pm);
                                        // построение маршрута
                                        if (pointDelivery) {
                                            ymaps.route([pm.geometry.getCoordinates(), pointDelivery.geometry.getCoordinates()]).then(function (route) {
                                                rec.route = route;
                                                rec.set('distance', route.getHumanLength());
                                            }, function (error) {
                                                alert('Возникла ошибка: ' + error.message);
                                            });
                                        }
                                    });
                                } else {
                                    alert('error');
                                }
                            }
                        });
                    } else {
                        Ext.MessageBox.alert('Ошибка!', 'Сначала выберите номенклатуру.');
                    }
                }
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Цена',
            dataIndex: 'price',
            format: '0.00',
            align: 'right',
            width: 70,
            field: {
                xtype: 'pricefield',
                allowBlank: false
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Стоимость',
            dataIndex: 'cost',
            format: '0.00',
            align: 'right',
            width: 70
        },
        {
            xtype: 'numbercolumn',
            text: 'в т.ч. НДС',
            dataIndex: 'cost_nds',
            format: '0.00',
            align: 'right',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            itemId: 'delete',
            hidden: true,
            width: 30,
            align: 'center',
            items: [
                {
                    iconCls: 'icon_delete',
                    handler: function (grid, row) {
                        grid.getStore().removeAt(row);
                    }
                }
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('grid');
                        var record = Ext.create(grid.getStore().model.modelName, {
                            status: 1
                        });
                        grid.getStore().add(record);
                        grid.getSelectionModel().select(grid.getStore().getCount() - 1);
                    }
                }
            ]
        },
        {
            xtype: 'form',
            dock: 'bottom',
            itemid: 'total',
            border: 0,
            layout: {
                type: 'vbox',
                align: 'right'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'itogo',
                    margin: '0 0 0 20',
                    fieldStyle: 'border: none; background: none',
                    readOnly: true,
                    labelAlign: 'right',
                    fieldLabel: 'Итого',
                    width: 200,
                    labelWidth: 100
                },
                {
                    xtype: 'textfield',
                    name: 'nds',
                    margin: '0 0 0 20',
                    fieldStyle: 'border: none; background: none',
                    readOnly: true,
                    labelAlign: 'right',
                    fieldLabel: 'в т.ч. НДС',
                    width: 200,
                    labelWidth: 100
                }
            ]
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.client_order_nomenclature');
        this.callParent();
    },
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners: {
                edit: function (editor, e) {
                    e.record.set('cost', e.record.get('price') * e.record.get('volume'));
                    e.record.set('cost_nds', e.record.get('cost') * 18 / 118);
                }
            }
        })
    ]
});

