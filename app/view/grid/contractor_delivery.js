Ext.define('app.view.grid.contractor_delivery', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridContractorDelivery',
    flex: 1,
    initComponent: function () {
        this.store = Ext.create('app.store.contractor_delivery');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                itemId: 'for-one',
                dock: 'top',
                hidden: true,
                items: [
                    {
                        text: 'Добавить',
                        action: 'add',
                        iconCls: 'icon_add'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                itemId: 'for-all',
                dock: 'top',
                hidden: true,
                items: [
                    {
                        labelWidth: 40,
                        fieldLabel: 'Поиск',
                        xtype: 'searchfield',
                        width: 300,
                        store: this.store,
                        padding: '3 3 3 10'
                    },
                    '->',
                    {
                        xtype: 'combobox',
                        action: 'filter_status',
                        store: 'filter_status',
                        editable: false,
                        value: 1,
                        forceSelection: true,
                        valueField: 'id',
                        displayField: 'title',
                        padding: '3 10 3 3',
                        listeners: {
                            change: function (view, val) {
                                var store = view.up('grid').getStore();
                                store.getProxy().extraParams.status = val;
                                store.load();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                beforePageText: 'Страница',
                afterPageText: 'из {0}'
            }
        ];
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Контрагент',
            dataIndex: 'contractor',
            flex: 1
        },
        {
            text: 'Адрес',
            dataIndex: 'address',
            flex: 2
        },
        {
            text: 'Широта',
            dataIndex: 'lat',
            width: 80
        },
        {
            text: 'Долгота',
            dataIndex: 'lng',
            width: 80
        },
        {
            text: 'Время пребывания',
            dataIndex: 'delay',
            width: 110,
            align: 'right',
            renderer: function (v) {
                return v ? (v + ' мин') : '';
            }
        },
        {
            text: 'Окно доставки',
            dataIndex: 'delay',
            width: 110,
            renderer: function (v, c, r) {
                return (r.get('time_from') ? 'с ' + r.get('time_from') + ' ' : '') + (r.get('time_to') ? ' по ' + r.get('time_to') : '');
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.items[0].tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.items[0].tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});

