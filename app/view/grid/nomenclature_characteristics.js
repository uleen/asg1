Ext.define('app.view.grid.nomenclature_characteristics', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridNomenclatureCharacteristics',
    title: 'Характеристики',
    columns: [
        {
            text: 'Код',
            dataIndex: 'characteristic_id',
            width: 30
        },
        {
            text: 'Характеристика',
            dataIndex: 'title',
            flex: 2
        },
        {
            text: 'Значение',
            dataIndex: 'value',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit',
            width: 50
        },
    ],
    initComponent : function () {
        this.store = Ext.create('app.store.nomenclature_characteristic');
        this.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: [
                    '->',
                    {
                        text: 'Сохранить',
                        action: 'btnSaveNomenclatureCharacteristics',
                        iconCls: 'icon_disk',
                        padding: 5,
                        handler: function () {
                            this.up('grid').getStore().sync();
                        }
                    }
                ]
            }
        ]
        this.callParent();
    },
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ]
});


