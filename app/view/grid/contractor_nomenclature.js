Ext.define('app.view.grid.contractor_nomenclature', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridContractorNomenclature',
    title: 'Номенклатуры и цены',
    features: [
        {
            ftype: 'grouping'
        }
    ],
    columns: [
        {
            text: 'Номенкл. группа',
            dataIndex: 'nomenclature_group_title',
            flex: 2
        },
        {
            text: 'Артикул',
            dataIndex: 'nomenclature_article',
            flex: 2,
            field: {
                xtype: 'combobox',
                store: 'nomenclature',
                typeAhead: true,
                minChars: 1,
                displayField: 'article',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('nomenclature_id', rec.get('id'));
                        record.set('nomenclature_article', rec.get('article'));
                        record.set('nomenclature_title', rec.get('title'));
                        record.set('nomenclature_group_title', rec.get('nomenclature_group_title'));
                    }
                }
            }
        },
        {
            text: 'Наименование',
            dataIndex: 'nomenclature_title',
            flex: 2,
            field: {
                xtype: 'combobox',
                store: 'nomenclature',
                typeAhead: true,
                minChars: 1,
                displayField: 'title',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('nomenclature_id', rec.get('id'));
                        record.set('nomenclature_article', rec.get('article'));
                        record.set('nomenclature_title', rec.get('title'));
                        record.set('nomenclature_group_title', rec.get('nomenclature_group_title'));
                    }
                }
            }
        },
        {
            text: 'Точка отгрузки',
            dataIndex: 'contractor_delivery_address',
            flex: 2,
            field: {
                xtype: 'triggerfield',
                triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
                editable: false,
                onTriggerClick: function () {
                    var record = this.up('grid').getSelectionModel().selected.items[0];
                    var win = Ext.widget('winContractorDelivery');
                    var grid = win.down('grid');
                    grid.getDockedComponent('for-all').show();
                    var store = grid.getStore();
                    store.getProxy().extraParams.contractor_id = this.up('window').cagent.id;
                    store.load();
                    win.show();
                    grid.on('itemdblclick', function (grid, rec) {
                        record.set('contractor_delivery_id', rec.get('id'));
                        record.set('contractor_delivery_address', rec.get('address'));
                        win.close();
                    });
                }
            }
        },
        {
            text: 'Артикул поставщика',
            dataIndex: 'article',
            flex: 2,
            field: {
                xtype: 'textfield'
            }
        },
        {
            text: 'Цена',
            dataIndex: 'price',
            flex: 1,
            field: {
                xtype: 'pricefield',
                allowBlank: false
            }
        },
        {
            text: 'Валюта',
            dataIndex: 'currency_code',
            width: 50,
            field: {
                xtype: 'combobox',
                store: Ext.create('app.store.currency'),
                displayField: 'code',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('currency_id', rec.get('id'));
                        record.set('currency_code', rec.get('code'));
                    }
                }
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            tooltip: 'История цен',
            iconCls: 'icon_table_chart',
            action: 'history'
        },
        {
            text: 'Ед. изм.',
            dataIndex: 'unit_title',
            width: 50,
            field: {
                xtype: 'combobox',
                store: Ext.create('app.store.unit'),
                displayField: 'title',
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var record = this.up('grid').getSelectionModel().selected.items[0];
                        record.set('unit_id', rec.get('id'));
                        record.set('unit_title', rec.get('title'));
                    }
                }
            }
        },
        {
            xtype: 'checkcolumn',
            header: 'Наличие',
            dataIndex: 'in_stock',
            width: 65
        },
        {
            text: 'Примечание',
            dataIndex: 'note',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.nomenclature_supplier');
        this.store.group('contractor_delivery_address');
        this.callParent();
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('grid');
                        var record = Ext.create('app.model.nomenclature_supplier', {
                            in_stock: 1
                        });
                        grid.getStore().add(record);
                    }
                }
            ]
        },
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Сохранить',
                    iconCls: 'icon_disk',
                    padding: 5,
                    handler: function () {
                        this.up('grid').getStore().sync();
                    }
                }
            ]
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ]
});


