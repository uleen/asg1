Ext.define('app.view.win.nomenclature_group', {
    extend: 'Ext.window.Window',
    alias: 'widget.winNomenclatureGroup',
    title: 'Справочник "Номенклатурные группы"',
    layout: 'fit',
    height: 300,
    width: 400,
    minimizable: true,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title',
            listeners: {
                change: function (view, val) {
                    var store = view.up('window').getComponent('nomenclature-group').getStore();
                    store.getProxy().extraParams.status = val;
                    store.load();
                }
            }
        }
    ],
    items: [
        {
            xtype: 'gridNomenclatureGroup',
            itemId: 'nomenclature-group',
            flex: 1,
            padding: 1,
            border: 1
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});

