Ext.define('app.view.win.type_price', {
    extend: 'Ext.window.Window',
    alias: 'widget.winTypePrice',
    title: 'Справочник "Виды цен"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 400,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title',
            listeners: {
                change: function (view, val) {
                    var store = view.up('window').down('grid').getStore();
                    store.getProxy().extraParams = {
                        status: val
                    };
                    store.load();
                }
            }
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridTypePrice',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ]
});

