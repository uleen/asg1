Ext.define('app.view.win.contractor_requisites', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContractorRequisites',
    title: 'Реквизиты контрагентов',
    layout: 'fit',
    height: 500,
    width: 1000,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridContractorRequisites',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ]
});