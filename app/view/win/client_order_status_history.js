Ext.define('app.view.win.client_order_status_history', {
    extend: 'Ext.window.Window',
    alias: 'widget.win.client_order_status_history',
    title: 'История статусов',
    layout: 'fit',
    height: 300,
    width: 500,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'grid.client_order_status_history',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ]
});