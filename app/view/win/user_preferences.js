Ext.define('app.view.win.user_preferences', {
    extend: 'Ext.window.Window',
    alias: 'widget.winUserPreferences',
    title: 'Настройки пользователя',
    height: 400,
    width: 850,
    layout: 'border',
   
    maximizable: true,
    items: [
        {
            xtype: 'menuUserPreferences',
            itemId: 'menu-user-preferences',
            region: 'west',
            width: 170,
            padding: 1,
        },
        
        
        {
            xtype: 'panel',
            itemId: 'user-preferences-form',
            region: 'center',
            padding: 1,
            border: 0,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            
            
            items: [
                   
                {
                    xtype: 'formUserPreferencesMain',
                    itemId: 'form-user-preferences-main',
                    hidden: false
                },
                
                
                {
                    xtype: 'formUserPreferences',
                    itemId: 'form-user-preferences',
                    hidden: true
                },
                
                {
                	
                    xtype: 'formUAIc',
                    itemId: 'form-user-access-ic',
                    autoScroll: true,
                    hidden: true
                },
                /*
                {
                	xtype: 'formUserAccess',
                	itemId: 'form-user-access',
                	hidden: true
                },
                */
            
            ],
            dockedItems: [
                          {
                              xtype: 'toolbar',
                              dock: 'bottom',
                              items: [
                                  '->',
                                  {
                                      text: 'Отменить',
                                      itemId: 'btn-cancel',
                                      action: 'cancel',
                                      iconCls: 'icon_cancel',
                                      padding: 5,
                                      handler: function () {
                                          this.up('window').close();
                                      }
                                  },
                                  {
                                      text: 'Сохранить',
                                      itemId: 'btn-save',
                                      action: 'save',
                                      iconCls: 'icon_disk',
                                      padding: 5
                                  }
                              ]
                          }
                   ],
            
        }
        
    ],

    user_id: null
});
