Ext.define('app.view.win.direction_sales', {
    extend: 'Ext.window.Window',
    alias: 'widget.winDirectionSales',
    title: 'Справочник "Направление продаж"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 400,
    minimizable: true,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title'
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridDirectionSales',
                    store: 'direction_sales',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});

