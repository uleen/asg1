Ext.define('app.view.win.nomenclature', {
    extend: 'Ext.window.Window',
    alias: 'widget.winNomenclature',
    title: 'Карточка номенклатуры',
    layout: 'border',
    height: 330,
    width: 800,
    maximizable: true,
    minimizable: true,
    items: [
        {
            xtype: 'menuNomenclature',
            region: 'west',
            width: 170,
            padding: 1
        },
        {
            xtype: 'panel',
            itemId: 'main',
            region: 'center',
            padding: 1,
            border: 0,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'formNomenclature',
                    itemId: 'form-nomenclature',
                    flex: 1,
                    hidden: false
                },
                {
                    xtype: 'gridNomenclatureCharacteristics',
                    itemId: 'grid-nomenclature-characteristics',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'gridNomenclaturePrice',
                    itemId: 'grid-nomenclature-price',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'gridNomenclatureSupplier',
                    itemId: 'grid-nomenclature-supplier',
                    flex: 1,
                    hidden: true
                }
            ]
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});


