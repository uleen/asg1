Ext.define('app.view.win.sheet', {
    extend: 'Ext.window.Window',
    alias: 'widget.win.sheet',
    title: 'Выставленные счета',
    layout: 'fit',
    closeAction: 'remove',
    height: 350,
    width: 700,
    items: [
        {
            xtype: 'grid.sheet',
            flex: 1,
            border: 0
        }
    ]
});