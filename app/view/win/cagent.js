Ext.define('app.view.win.cagent', {
    extend: 'Ext.window.Window',
    alias: 'widget.winCagent',
    title: 'Карточка контрагента',
    layout: 'border',
    height: 550,
    width: 1070,
    maximizable: true,
    minimizable: true,
    items: [
        {
            xtype: 'menuCagentClient',
            itemId: 'menu-for-client',
            region: 'west',
            width: 170,
            padding: 1,
            hidden: true
        },
        {
            xtype: 'menuCagentSupplier',
            itemId: 'menu-for-supplier',
            region: 'west',
            width: 170,
            padding: 1
        },
        {
            xtype: 'panel',
            itemId: 'cagent-form',
            region: 'center',
            padding: 1,
            border: 0,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'formCagentMain',
                    itemId: 'form-cagent-main',
                    flex: 1,
                    hidden: false
                },
                {
                    xtype: 'formCagentContact',
                    itemId: 'form-cagent-contact',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'formCagentContract',
                    itemId: 'form-cagent-contract',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'gridContractorDelivery',
                    itemId: 'grid-cagent-delivery',
                    title: 'Точки доставки / загрузки',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'gridContractorNomenclature',
                    itemId: 'grid-contractor-nomenclature',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'formCagentIncomingCall',
                    itemId: 'form-cagent-incoming-call',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'grid.client_order',
                    itemId: 'grid-cagent-client-order',
                    flex: 1,
                    hidden: true
                },
                {
                    xtype: 'grid.sheet',
                    itemId: 'grid-cagent-sheet',
                    flex: 1,
                    hidden: true
                },
            ]
        }
    ],
    cagent_id: null,
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});


