Ext.define('app.view.win.okpfo_type', {
    extend: 'Ext.window.Window',
    alias: 'widget.winOkpfoType',
    title: 'Справочник "Организационно-правовые формы"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 500,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title'
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridOkpfoType',
                    store: 'okpfo_type',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ]
});

