Ext.define('app.view.win.ic_search', {
    extend: 'Ext.window.Window',
    alias: 'widget.winICSearch',
    title: 'Поиск контрагентов и Лидов',
    layout: 'fit',
    autoShow: true,
    strore: 'ic_search',
    height: 350,
    width: 700,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [

                {
                    xtype: 'grid',
                    store: 'ic_search',
                    flex: 1,
                    padding: 1,
                    height: '100%',
                    width: '100%',
                  
                    columns: [
                        {
                            text: 'ID',
                            dataIndex: 'cagent_id',
                            width: 30
                        },
                        {
                        	xtype: 'actioncolumn',
                            dataIndex: 'isa',
                           
                            width: 20,
                            sortable: true,
                            align: 'center',
                          
                            getClass: function (v, meta, rec) {
                            	 if (rec.get('is_cagent') > 0) {
                                     this.items[0].tooltip = 'Контрагент';
                                     return 'icon_accept_cagent';
                                 }
                                 if (rec.get('is_cagent') == 0) {
                                     this.items[0].tooltip = 'ЛИД';
                                     return 'icon_error';
                                 }

                            },
                        },
                        {
                            text: 'Контрагент',
                            dataIndex: 'is_cagent',
                            flex: 1,
                            renderer: function (v) {
                                return v > 0  ? 'Да' : 'Нет';
                            }
                            
                        },
                        {
                            text: 'ИНН',
                            dataIndex: 'cagent_inn',
                            width: 100,
                            renderer: function (v) {
                                return v  ? v : 'Не определен';
                            }

                        },
                        {
                            text: 'Название',
                            dataIndex: 'cagent_title',
                            width: 100
                        },
   
                        {
                            text: 'Контактное лицо',
                            dataIndex: 'cagent_personal',
                            flex: 1
                        },

                        {
                            text: 'Телефон',
                            dataIndex: 'cagent_phone',
                            flex: 1
                        },
                        {
                            text: 'E-mail',
                            dataIndex: 'cagent_email',
                            flex: 1
                        },
                    ]
                }
            ],

            dockedItems: [
                          {
                              xtype: 'pagingtoolbar',
                              store: 'ic_search',
                              dock: 'bottom',
                              displayInfo: true,
                              beforePageText: 'Страница',
                              afterPageText: 'из {0}',
                              displayMsg: 'Контрагенты и Лиды {0} - {1} из {2}',

                          }
                      ],

        },

    ],

    
});

