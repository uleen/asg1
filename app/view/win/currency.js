Ext.define('app.view.win.currency', {
    extend: 'Ext.window.Window',
    alias: 'widget.winCurrency',
    title: 'Валюты',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridCurrency',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});