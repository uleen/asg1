Ext.define('app.view.win.contact_personal_phone', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContactPhoneAdd',
    title: 'Контактные лица добавление телефона',
    layout: 'fit',
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'number',
                    padding: '10',
                    fieldLabel: 'Телефон',
                    plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)],
                    
                },
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save_phone'
        }
    ]
});
