Ext.define('app.view.win.nomenclature_supplier_select', {
    extend: 'Ext.window.Window',
    title: 'Выбор поставщика',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    closeAction: 'remove',
    height: 500,
    width: 800,
    items: [
        Ext.create('app.view.grid.nomenclature_supplier_select', {
            border: 0,
            flex: 1
        }),
        {
            xtype: 'panel',
            itemId: 'map',
            flex: 2,
            border: 0,
            style: 'border-top: 1px solid #99bce8'
        }
    ]
});

