Ext.define('app.view.win.type_payment', {
    extend: 'Ext.window.Window',
    alias: 'widget.winTypePayment',
    title: 'Справочник "Виды расчетов"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 400,
    minimizable: true,
    items: [
        {
            xtype: 'gridTypePayment',
            flex: 1,
            padding: 1,
            borderLeft: 0
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});

