Ext.define('app.view.win.customer_category', {
    extend: 'Ext.window.Window',
    alias: 'widget.winCustomerCategory',
    title: 'Справочник "Направление продаж"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 400,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title'
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridCustomerCategory',
                    store: 'customer_category',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ]
});

