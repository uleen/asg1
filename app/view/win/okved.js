Ext.define('app.view.win.okved', {
    extend: 'Ext.window.Window',
    alias: 'widget.winOKVED',
    title: 'Справочник "ОКВЭД"',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    autoShow: true,
    height: 500,
    width: 800,
    items: [
        {
            xtype: 'treepanel',
            id: 'okved-tree',
            flex: 1,
            displayField: 'title',
            useArrows: true,
            rootVisible: false,
            store: 'okved'
        },
        {
            xtype: 'panel',
            id: 'okved-info',
            height: 100,
            overflowY: 'scroll'
        }

    ]
});

