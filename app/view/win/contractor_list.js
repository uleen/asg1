Ext.define('app.view.win.contractor_list', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContactractorList',
    title: 'Справочник "Контрагенты"',
    layout: 'fit',
    height: 300,
    width: 600,
    maximizable: true,
    items: [
            {
                xtype: 'gridCagentList',
                padding: 1,
                region: 'center'
            },
    ]
});