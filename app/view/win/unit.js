Ext.define('app.view.win.unit', {
    extend: 'Ext.window.Window',
    alias: 'widget.winUnit',
    title: 'Справочник "Единицы измерения"',
    layout: 'fit',
    autoShow: true,
    height: 300,
    width: 400,
    items: [
        {
            xtype: 'gridUnit',
            flex: 1,
            padding: 0,
            border: 0
        }
    ]
});

