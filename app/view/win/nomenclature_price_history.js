Ext.define('app.view.win.nomenclature_price_history', {
    extend: 'Ext.window.Window',
    title: 'История изменения отпускных цен',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    height: 300,
    width: 400,
    closeAction: 'remove',
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'nomenclature_title',
                    padding: '7',
                    fieldLabel: 'Номенклатура',
                    readOnly: true,
                    anchor: '100%',
                    labelAlign: 'right',
                    labelWidth: 100
                },
                {
                    xtype: 'textfield',
                    name: 'type_price_title',
                    padding: '0 7 7',
                    fieldLabel: 'Тип цены',
                    readOnly: true,
                    anchor: '100%',
                    labelAlign: 'right',
                    labelWidth: 100
                }
            ]
        },
        {
            xtype: 'grid.nomenclature_price_history',
            flex: 1,
            border: 0,
            style: 'border-top: 1px solid #99bce8;'
        }
    ]
});

