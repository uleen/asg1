Ext.define('app.view.win.contact_personal_email', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContactEmailAdd',
    title: 'Контактные лица добавление email',
    layout: 'fit',
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'number',
                    padding: '10',
                    fieldLabel: 'Email',
                    plugins: [new Ext.ux.InputTextMask('', true)],
                    
                },
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save_email'
        }
    ]
});