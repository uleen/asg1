Ext.define('app.view.win.contact_personal', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContactPersonal',
    title: 'Справочник "Контактные лица"',
    layout: 'fit',
    height: 350,
    width: 650,
    maximizable: true,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title',
            listeners: {
                change: function (view, val) {
                    var store = view.up('window').down('grid').getStore();
                    store.getProxy().extraParams = {
                        status: val
                    };
                    store.load();
                }
            }
        }
    ],
    items: [
        {
            xtype: 'gridContactPersonal',
            itemId: 'grid-contact-personal',
            flex: 1,
            padding: 1,
            border: 0
        }
    ]
});