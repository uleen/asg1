Ext.define('app.view.win.contractor', {
    extend: 'Ext.window.Window',
    title: 'Контрагенты',
    layout: 'fit',
    height: 500,
    width: 1000,
    closeAction: 'remove',
    items: [
        {
            xtype: 'gridCagent',
            border: 0
        }
    ]
});