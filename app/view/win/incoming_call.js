Ext.define('app.view.win.incoming_call', {
    extend: 'Ext.window.Window',
    alias: 'widget.winIncomingCall',
    title: 'Входящие запросы',
    height: 500,
    width: 850,
    layout: 'border',
    items: [
        {
            xtype: 'grid',
            padding: 1,
            region: 'center',
            height: '100%',
            width: '100%',
            store: 'incoming_call',
            columns: [
                {
                    text: 'ID',
                    dataIndex: 'id',
                    width: 30
                },
                {
                    xtype: 'datecolumn',
                    text: 'Дата и время',
                    dataIndex: 'call_date',
                    format: 'd.m.Y H:i',
                    width: 120
                },
                {
                    text: 'Новый',
                    dataIndex: 'is_new_title',
                    width: 50
                },
                {
                    text: 'Контактное лицо',
                    dataIndex: 'cagent_name',
                    flex: 1
                },
                {
                    text: 'Телефон',
                    dataIndex: 'cagent_phone',
                    width: 150,
                    field: {
                        xtype: 'textfield',
                        readOnly: true
                    }
                },
                {
                    text: 'Наименование',
                    dataIndex: 'company_title',
                    flex: 1
                },
                {
                    text: 'Что интересует',
                    dataIndex: 'client_interest_srv',
                    flex: 1
                },
                {
                    text: 'Источник',
                    dataIndex: 'source_title',
                    flex: 1
                },
                {
                    text: 'Акт',
                    dataIndex: 'status',
                    width: 40,
                    align: 'center',
                    renderer: function (v) {
                        return v == 1 ? 'Да' : 'Нет';
                    }
                },
                {
                    text: 'Сохранить',
                    xtype:'actioncolumn',
                    align: 'center',
                    width: 70,
                    items:[
                        {
                            getClass: function () {
                                return 'icon_application_double';
                            },
                            handler:function (grid, rowIndex, colIndex,record) {
                                var win = this.up('window');
                                var form = Ext.getCmp('formIncomingCalls').down('form').getForm();
                                var selectionModel = grid.getSelectionModel();
                                selectionModel.select(rowIndex);
                                record = selectionModel.getSelection()[0];
                                Ext.ModelManager.getModel('app.model.incoming_call').load(record.get('id'), {
                                    success: function (rec) {
                                        form.setValues({
                                            client_interest: rec.get('client_interest'),
                                            client_interest_srv: rec.get('client_interest_srv')
                                        });
                                        Ext.getCmp('formIncomingCalls').down('form').getComponent('client_interest_panel').getComponent('client_interest').getComponent('comment_title').update('Скопирована с #'+rec.get('id'));
                                        win.close();

                                    }
                                });
                        }
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    beforePageText: 'Страница',
                    afterPageText: 'из {0}',
                    displayMsg: 'Пользователи {0} - {1} из {2}'
                },
                {
                    xtype: 'textareafield',
                    dock: 'bottom',
                    readOnly: true,
                    itemid: 'area-client-interest'

                }
            ],
            viewConfig: {
                getRowClass: function (record) {
                    return record.get('status') == 1 ? 'active-row' : 'no-active-row';
                }
            },
            listeners: {
                select: function (grid, record) {
                    this.getDockedItems('[itemid=area-client-interest]')[0].setValue(record.get('client_interest'));
                }
            }
        }
    ]
});
