Ext.define('app.view.win.currency_rate', {
    extend: 'Ext.window.Window',
    alias: 'widget.winCurrencyRate',
    title: 'История курса',
    layout: 'fit',
    height: 500,
    width: 300,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridCurrencyRate',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ]
});