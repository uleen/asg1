Ext.define('app.view.win.okei', {
    extend: 'Ext.window.Window',
    alias: 'widget.winOKEI',
    title: 'Справочник "ОКЕИ"',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    height: 500,
    width: 800,
    items: [
        {
            xtype: 'grid',
            store: 'okei',
            flex: 1,
            border: 0,
            columns: [
                {
                    text: 'Код',
                    dataIndex: 'code',
                    width: 30
                },
                {
                    text: 'Название',
                    dataIndex: 'title',
                    flex: 3
                },
                {
                    text: 'Сокр. RU',
                    dataIndex: 'title_short_ru',
                    flex: 1
                },
                {
                    text: 'Сокр. EN',
                    dataIndex: 'title_short_en',
                    flex: 1
                },
                {
                    text: 'Символ RU',
                    dataIndex: 'symbol_ru',
                    flex: 1
                },
                {
                    text: 'Символ EN',
                    dataIndex: 'symbol_en',
                    flex: 1
                }
            ]
        }
    ]
});

