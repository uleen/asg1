Ext.define('app.view.win.fias', {
    extend: 'Ext.window.Window',
    alias: 'widget.winFias',
    title: 'Справочник "ФИАС"',
    autoShow: true,
    height: 500,
    width: 400,
    layout: 'fit',
    items: [
        {
            xtype: 'treepanel',
            overflowY: 'auto',
            border: 0,
            displayField: 'title',
            useArrows: true,
            rootVisible: false,
            store: 'fias'
        }
    ]
});

