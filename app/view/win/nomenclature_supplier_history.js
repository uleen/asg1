Ext.define('app.view.win.nomenclature_supplier_history', {
    extend: 'Ext.window.Window',
    title: 'История изменения цен поставщика',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    height: 300,
    width: 400,
    items: [
        {
            xtype: 'form',
            border: 0,
            items: [
                {
                    xtype: 'textfield',
                    name: 'nomenclature_title',
                    padding: '7',
                    fieldLabel: 'Номенклатура',
                    readOnly: true,
                    anchor: '100%',
                    labelAlign: 'right',
                    labelWidth: 100
                },
                {
                    xtype: 'textfield',
                    name: 'contractor_title',
                    padding: '0 7 7',
                    fieldLabel: 'Контрагент',
                    readOnly: true,
                    anchor: '100%',
                    labelAlign: 'right',
                    labelWidth: 100
                },
                {
                    xtype: 'textfield',
                    name: 'contractor_delivery_address',
                    padding: '0 7 7',
                    fieldLabel: 'Точка поставки',
                    readOnly: true,
                    anchor: '100%',
                    labelAlign: 'right',
                    labelWidth: 100
                }
            ]
        },
        {
            xtype: 'gridNomenclatureSupplierHistory',
            flex: 1,
            border: 0,
            style: 'border-top: 1px solid #99bce8;'
        }
    ]
});

