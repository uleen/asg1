Ext.define('app.view.win.reference_scope', {
    extend: 'Ext.window.Window',
    alias: 'widget.winReferenceScope',
    title: 'Справочник "Сфера деятельности"',
    layout: 'fit',
    height: 300,
    width: 300,
    tbar: [
        {
            text: 'Добавить',
            action: 'create',
            iconCls: 'icon_add'
        },
        '->',
        {
            xtype: 'combobox',
            action: 'filter_status',
            store: 'filter_status',
            editable: false,
            value: 1,
            forceSelection: true,
            valueField: 'id',
            displayField: 'title'
        }
    ],
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridReferenceScope',
                    store: 'reference_scope',
                    flex: 1,
                    padding: 1,
                    borderLeft: 0
                }
            ]
        }
    ]
});