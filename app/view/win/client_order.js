Ext.define('app.view.win.client_order', {
    extend: 'Ext.window.Window',
    title: 'Заказ клиента',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    closeAction: 'remove',
    height: 500,
    width: 846,
    minWidth: 846,
    minHeight: 500,
    maximizable: true,
    minimizable: true,
    items: [
        {
            xtype: 'form',
            padding: 0,
            border: 0,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '7 10 3',
                    flex: 2,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Контрагент',
                            border: 1,
                            padding: '2 10 0',
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Наименование',
                                    name: 'contractor_title_inn',
                                    typeAhead: true,
                                    minChars: 3,
                                    anchor: '100%',
                                    labelWidth: 110,
                                    store: Ext.create('app.store.cagent'),
                                    labelAlign: 'right',
                                    margin: '0 0 10',
                                    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    displayField: 'title_inn',
                                    listeners: {
                                        beforequery: function (queryPlan) {
                                            queryPlan.combo.store.pageSize = 999;
                                            queryPlan.combo.store.proxy.extraParams.status = 1;    // только активные
                                            queryPlan.combo.store.proxy.extraParams.org_type = 1;  // только клиенты
                                        },
                                        select: function (combo, records) {
                                            var form = this.up('form').getForm();
                                            var rec = records[0];
                                            form.setValues({
                                                contractor_id: rec.get('id'),
                                                contact_personal_id: null,
                                                contractor_contract_id: null,
                                                contractor_contract_title: null
                                            });
                                        }
                                    },
                                    onTrigger2Click: function () {
                                        var contractor_id = this.up('form').getForm().getValues().contractor_id;
                                        if (contractor_id) {
                                            var cagent = new app.controller.cagent;
                                            cagent.open(contractor_id);
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contractor_id'
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Контактное лицо',
                                    name: 'contact_personal_id',
                                    editable: false,
                                    labelWidth: 110,
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    margin: '0 0 10',
                                    store: Ext.create('app.store.contact_personal'),
                                    displayField: 'fio',
                                    valueField: 'id',
                                    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    listeners: {
                                        beforequery: function (queryPlan) {
                                            var contractor_id = this.up('form').getForm().getValues().contractor_id;
                                            if (contractor_id) {
                                                queryPlan.combo.store.proxy.extraParams.contractor_id = contractor_id;
                                                queryPlan.combo.store.reload();
                                            } else {
                                                queryPlan.cancel = true;
                                            }
                                        }
                                    },
                                    onTrigger2Click: function () {
                                        if (this.value) {
                                            var contact_personal = new app.controller.contact_personal;
                                            contact_personal.open(this.value);
                                        }
                                    }
                                },
                                {
                                    xtype: 'fieldset',
                                    border: 0,
                                    padding: 0,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Договор',
                                            name: 'contractor_contract_title',
                                            editable: false,
                                            labelWidth: 110,
                                            labelAlign: 'right',
                                            flex: 1,
                                            store: Ext.create('app.store.contractor_contracts'),
                                            displayField: 'title',
                                            valueField: 'title',
                                            trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                            listeners: {
                                                beforequery: function (queryPlan) {
                                                    var contractor_id = this.up('form').getForm().getValues().contractor_id;
                                                    if (contractor_id) {
                                                        queryPlan.combo.store.proxy.extraParams.contractor_id = contractor_id;
                                                        queryPlan.combo.store.proxy.extraParams.is_actual = 1; // только актуальные
                                                        queryPlan.combo.store.reload();
                                                    } else {
                                                        queryPlan.cancel = true;
                                                    }
                                                },
                                                select: function (combo, records) {
                                                    var rec = records[0];
                                                    var form = this.up('form').getForm();
                                                    form.setValues({
                                                        contractor_contract_id: rec.get('id'),
                                                        type_price_id: rec.get('type_price_id'),
                                                        currency_id: rec.get('currency_id')
                                                    });
                                                    form.findField('currency_id').setReadOnly(true);
                                                }
                                            },
                                            onTrigger2Click: function () {
                                                var contractor_contract_id = this.up('form').getForm().getValues().contractor_contract_id;
                                                if (contractor_contract_id) {
                                                    var contractor_contracts = new app.controller.contractor_contracts;
                                                    contractor_contracts.open(contractor_contract_id);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'hidden',
                                            name: 'contractor_contract_id'
                                        },
                                        {
                                            xtype: 'combobox',
                                            name: 'currency_id',
                                            editable: false,
                                            store: 'currency',
                                            fieldLabel: 'Валюта расчетов',
                                            width: 200,
                                            labelWidth: 130,
                                            labelAlign: 'right',
                                            displayField: 'code',
                                            valueField: 'id'
                                        },
                                        {
                                            xtype: 'hidden',
                                            name: 'type_price_id'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Доставка',
                            border: 1,
                            padding: '2 10 5',
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Адрес',
                                    name: 'contractor_delivery_id',
                                    labelWidth: 50,
                                    editable: false,
                                    labelAlign: 'right',
                                    anchor: '100%',
                                    store: Ext.create('app.store.contractor_delivery'),
                                    displayField: 'address',
                                    valueField: 'id',
                                    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                    listeners: {
                                        beforequery: function (queryPlan) {
                                            var contractor_id = this.up('form').getForm().getValues().contractor_id;
                                            if (contractor_id) {
                                                queryPlan.combo.store.proxy.extraParams.contractor_id = contractor_id;
                                                queryPlan.combo.store.reload();
                                            } else {
                                                queryPlan.cancel = true;
                                            }
                                        },
                                        select: function (combo, records) {
                                            var rec = records[0];
                                            this.up('form').getForm().setValues({
                                                time_from: rec.get('time_from'),
                                                time_to: rec.get('time_to'),
                                                contractor_delivery_lat: rec.get('lat'),
                                                contractor_delivery_lng: rec.get('lng')
                                            });
                                        }
                                    },
                                    onTrigger2Click: function () {
                                        if (this.value) {
                                            var contractor_delivery = new app.controller.contractor_delivery;
                                            contractor_delivery.open(this.value);
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contractor_delivery_lat'
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'contractor_delivery_lng'
                                },
                                {
                                    xtype: 'fieldset',
                                    border: 0,
                                    padding: 0,
                                    margin: '10 0 5 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            name: 'date_from',
                                            fieldLabel: 'Дата с',
                                            labelAlign: 'right',
                                            width: 150,
                                            labelWidth: 50
                                        },
                                        {
                                            xtype: 'datefield',
                                            name: 'date_to',
                                            padding: '0 0 0 5',
                                            fieldLabel: 'по',
                                            labelAlign: 'right',
                                            width: 120,
                                            labelWidth: 20
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'time_from',
                                            padding: '0 0 0 30',
                                            fieldLabel: 'Время с',
                                            labelAlign: 'right',
                                            width: 100,
                                            labelWidth: 50,
                                            plugins: [new Ext.ux.InputTextMask('99:99', true)]
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'time_to',
                                            padding: '0 0 0 5',
                                            fieldLabel: 'по',
                                            labelAlign: 'right',
                                            width: 70,
                                            labelWidth: 20,
                                            plugins: [new Ext.ux.InputTextMask('99:99', true)]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Заказ',
                    width: 300,
                    border: 1,
                    margin: '7 10 3 0',
                    padding: '2 10 5',
                    items: [
                        {
                            xtype: 'fieldset',
                            border: 0,
                            padding: 0,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Номер',
                                    name: 'id',
                                    labelWidth: 70,
                                    labelAlign: 'right',
                                    flex: 1,
                                    readOnly: true
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Дата',
                                    name: 'create_date',
                                    dateFormat: 'd.m.Y H:i',
                                    labelWidth: 50,
                                    labelAlign: 'right',
                                    flex: 1,
                                    readOnly: true
                                }
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Автор',
                            name: 'create_user_name',
                            anchor: '100%',
                            labelWidth: 70,
                            labelAlign: 'right',
                            margin: '0 0 10'
                        },
                        {
                            xtype: 'fieldset',
                            border: 0,
                            padding: 0,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Статус',
                                    name: 'client_order_status_id',
                                    forceSelection: true,
                                    labelWidth: 70,
                                    labelAlign: 'right',
                                    flex: 1,
                                    store: Ext.create('app.store.client_order_status'),
                                    displayField: 'title',
                                    valueField: 'id',
                                    listeners: {
                                        select: function (combo, records) {
                                            var form = this.up('form').getForm();
                                            var rec = records[0];

                                            // отказ
                                            if (rec.get('id') == 3) {
                                                Ext.Msg.prompt('Отказ', 'Укажите причину отказа:', function (btn, text) {
                                                    if (btn == 'ok' && text != '') {
                                                        form.setValues({
                                                            client_order_status_note: text
                                                        });
                                                    } else {
                                                        form.setValues({
                                                            client_order_status_id: form.getRecord().get('client_order_status_id')
                                                        });
                                                    }
                                                });
                                            } else {
                                                form.setValues({
                                                    client_order_status_note: ''
                                                });
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'hidden',
                                    name: 'client_order_status_note'
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'prepayment',
                                    width: 90,
                                    margin: '0 0 0 10',
                                    boxLabel: 'Предоплата',
                                    inputValue: 1
                                }
                            ]
                        },
                        {
                            xtype: 'triggerfield',
                            name: 'incoming_call',
                            fieldLabel: 'Вх. запрос',
                            anchor: '100%',
                            editable: false,
                            labelWidth: 70,
                            labelAlign: 'right',
                            margin: '0 0 10',
                            triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
                            trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                            onTriggerClick: function () {
                                var form = this.up('form').getForm();
                                var contractor_id = form.getValues().contractor_id;
                                if (contractor_id) {
                                    var win = Ext.widget('winIncomingCall');
                                    var grid = win.down('grid');
                                    var store = grid.getStore();
                                    store.getProxy().extraParams.contractor_id = contractor_id;
                                    store.load();
                                    win.show();
                                    grid.on('itemdblclick', function (view, record) {
                                        form.setValues({
                                            incoming_call_id: record.get('id'),
                                            incoming_call: '№ ' + record.get('id') + ' от ' + Ext.Date.format(record.get('call_date'), 'd.m.Y H:i')
                                        });
                                        win.close();
                                    });
                                }
                            },
                            onTrigger2Click: function () {
                                var incoming_call_id = this.up('form').getForm().getValues().incoming_call_id;
                                if (incoming_call_id) {
                                    var incoming_call = new app.controller.incoming_call;
                                    incoming_call.open(incoming_call_id);
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'incoming_call_id'
                        },
                        {
                            xtype: 'textareafield',
                            emptyText: 'Комментарий',
                            name: 'note',
                            anchor: '100%',
                            height: 52
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'grid.client_order_nomenclature',
            border: 0,
            flex: 1,
            style: 'border-top: 1px solid #99bce8'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            itemId: 'buttons',
            items: [
                {
                    text: 'История статусов',
                    iconCls: 'icon_clock_history_frame',
                    padding: 5,
                    handler: function () {
                        var record = this.up('window').down('form').getRecord();
                        if (record.get('id')) {
                            var win = Ext.widget('win.client_order_status_history');
                            var grid = win.down('grid');
                            var store = grid.getStore();
                            store.getProxy().extraParams.client_order_id = record.get('id');
                            store.load();
                            win.show();
                        }
                    }
                },
                {
                    text: 'Выставить счет',
                    itemId: 'sheet',
                    iconCls: 'icon_sallary_deferrais',
                    padding: 5
                },
                {
                    xtype: 'triggerfield',
                    itemId: 'sheet_title',
                    fieldLabel: 'Выставлен счет',
                    editable: false,
                    hidden: true,
                    width: 300,
                    labelWidth: 100,
                    labelAlign: 'right',
                    trigger1Cls: Ext.baseCSSPrefix + 'form-point-trigger',
                    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    onTrigger1Click: function () {
                        var sheet_id = this.up('toolbar').getComponent('sheet_id').getValue();
                        window.open('/sheet/print/' + sheet_id, '_blank');
                    },
                    onTrigger2Click: function () {
                        var sheet_id = this.up('toolbar').getComponent('sheet_id').getValue();
                        var cSheet = new app.controller.sheet;
                        cSheet.open(sheet_id);
                    }
                },
                {
                    xtype: 'hidden',
                    itemId: 'sheet_id'
                },
                '->',
                {
                    text: 'Отменить',
                    iconCls: 'icon_cancel',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                },
                {
                    text: 'Сохранить',
                    itemId: 'save',
                    iconCls: 'icon_disk',
                    padding: 5
                }
            ]
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});