Ext.define('app.view.win.contractor_delivery', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContractorDelivery',
    title: 'Точки доставки / загрузки',
    layout: 'fit',
    height: 500,
    width: 1000,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridContractorDelivery',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ]
});