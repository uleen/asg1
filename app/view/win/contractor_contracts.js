Ext.define('app.view.win.contractor_contracts', {
    extend: 'Ext.window.Window',
    alias: 'widget.winContractorContracts',
    id: 'winContractorContracts',
    title: 'Договоры',
    layout: 'fit',
    height: 500,
    width: 1000,
    minimizable: true,
    items: [
        {
            xtype: 'panel',
            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'gridContractorContracts',
                    flex: 1,
                    border: 0
                }
            ]
        }
    ],
    listeners: {
        minimize: function() {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function() {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});