Ext.define('app.view.Form3', {
    extend: 'Ext.window.Window',
    alias: 'widget.form3',
    requires: ['Ext.ux.TreeCombo', 'Ext.ux.TreePicker'],
    title: 'Книга',
    layout: 'fit',
    autoShow: true,
    items: [
        {
            xtype: 'form',
            border: 0,
            padding: 5,
            items: [
                {
                    xtype: 'treecombo',
                    name: 'group_id',
                    fieldLabel: 'Раздел',
                    valueField: 'id',
                    displayField: 'text',
                    store: Ext.create('app.store.Store1'),
                    rootVisible: false,
                    selectChildren: false,
                    canSelectFolders: false
                },
                {
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Имя'
                },
                {
                    xtype: 'textfield',
                    name: 'phone',
                    fieldLabel: 'Телефон'
                },
                {
                    xtype: 'numberfield',
                    name: 'age',
                    fieldLabel: 'Возраст'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Сохранить',
            action: 'save'
        }
    ]
});