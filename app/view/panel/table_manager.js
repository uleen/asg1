Ext.define('app.view.panel.table_manager', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelTableManager',
    title: 'Номенклатура',
    flex: 1,
    region: 'center',
    layout: 'border',
    items: [
        {
            xtype: 'treepanel',
            itemId: 'tree',
            title: 'Группы',
            region: 'west',
            collapsible: true,
            split: true,
            width: 250,
            padding: '1 0 1 1',
            store: 'nomenclature_group',
            rootVisible: false,
            hideHeaders: true,
            useArrows: true,
            columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: 'title',
                    flex: 1
                }
            ]
        },
        {
            xtype: 'gridTableManager',
            itemId: 'grid',
            padding: 1,
            region: 'center'
        }
    ]
})
;
