Ext.define('app.view.panel.cagent', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelCagent',
    title: 'Контрагенты',
    flex: 1,
    region: 'center',
    layout: 'border',
    items: [
        {
            xtype: 'treepanel',
            title: 'Разделы',
            region: 'west',
            collapsible: true,
            split: true,
            width: 200,
            padding: '1 0 1 1',
            store: 'TreeMenuStore',
            rootVisible: false,
        },
        {
            xtype: 'gridCagent',
            itemId: 'grid',
            padding: 1,
            region: 'center'
        },
        {
            xtype: 'form',
            width: 180,
            id: 'cagent-search-in-panel',
            region: 'east',
            padding: 1,
            collapsible: true,
            collapsed: true,
            title: 'Расширенный поиск',
            items: [
                {
                    xtype: 'panel',
                    margin: 10,
                    border: 0,
                    items: [
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Тип',
                            name: 'org_type',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'name'],
                                data: [
                                    {value: 1, name: 'Клиент'},
                                    {value: 2, name: 'Поставщик'}
                                ]
                            }),
                            displayField: 'name',
                            valueField: 'value'
                        },
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Вид',
                            name: 'type',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'name'],
                                data: [
                                    {value: 1, name: 'Юр. лицо'},
                                    {value: 2, name: 'Физ. лицо'},
                                    {value: 3, name: 'ИП'}
                                ]
                            }),
                            displayField: 'name',
                            valueField: 'value'
                        },
                        {
                            xtype: 'textfield',
                            name: 'title',
                            labelAlign: 'top',
                            fieldLabel: 'Наименование'
                        },
                        {
                            xtype: 'combobox',
                            name: 'client_category',
                            store: 'customer_category',
                            labelAlign: 'top',
                            fieldLabel: 'Категория',
                            displayField: 'title',
                            valueField: 'id'
                        },
                        {
                            xtype: 'combobox',
                            name: 'manager_id',
                            store: Ext.create('app.store.user.can_auth'),
                            labelAlign: 'top',
                            fieldLabel: 'Менеджер',
                            displayField: 'name',
                            valueField: 'id'
                        },
                        {
                            xtype: 'textfield',
                            name: 'inn',
                            labelAlign: 'top',
                            fieldLabel: 'ИНН',
                            maskRe: /\d/
                        },
                        {
                            xtype: 'textfield',
                            name: 'phone',
                            labelAlign: 'top',
                            fieldLabel: 'Телефон',
                            plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)]
                        },
                        {
                            xtype: 'textfield',
                            name: 'email',
                            labelAlign: 'top',
                            fieldLabel: 'E-mail',
                            vtype: 'email'
                        },
                        {
                            labelWidth: 110,
                            xtype: 'combobox',
                            fieldLabel: 'Блокирован',
                            name: 'is_blocked',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'name'],
                                data: [
                                    {value: 1, name: 'Да'},
                                    {value: 2, name: 'Нет'}
                                ]
                            }),
                            displayField: 'name',
                            valueField: 'value',
                            padding: '0 0 5 0'
                        },
                        {
                            xtype: 'button',
                            text: 'Найти',
                            margin: '10 0 0 10',
                            action: 'search'
                        },
                        {
                            xtype: 'button',
                            text: 'Очистить',
                            margin: '10 0 0 10',
                            action: 'clear'
                        }
                    ]
                }
            ]
        }
    ]
});
