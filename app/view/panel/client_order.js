Ext.define('app.view.panel.client_order', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panel.client_order',
    title: 'Заказы клиентов',
    flex: 1,
    region: 'center',
    layout: 'border',
    items: [
        {
            xtype: 'panel',
            itemId: 'left',
            title: 'Группы',
            region: 'west',
            collapsible: true,
            split: true,
            width: 250,
            padding: '1 0 1 1',
            html: '...'
        },
        {
            xtype: 'grid.client_order',
            itemId: 'grid',
            padding: 1,
            region: 'center'
        }
    ]
});
