Ext.define('app.view.panel.incoming_call', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelIncomingCall',
    title: 'Входящие запросы',
    flex: 1,
    region: 'center',
    layout: 'border',
    items: [
        {
            xtype: 'gridIncomingCall',
            store: 'incoming_call',
            padding: 1,
            region: 'center',
            tbar: [
                {
                    labelWidth: 40,
                    fieldLabel: 'Поиск',
                    xtype: 'searchfield',
                    width: 400,
                    store: 'incoming_call',
                    padding: 5
                }
            ]
        },
        {
            xtype: 'form',
            width: 180,
            region: 'west',
            padding: 1,
            collapsible: true,
            title: 'Разделы',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'grid',
                    store: Ext.create('Ext.data.ArrayStore', {
                        fields: [
                            {name: 'id', type: 'int'},
                            {name: 'title', type: 'string'}
                        ],
                        data: [
                            [1, 'Сегодня'],
                            [2, 'Вчера'],
                            [0, 'Все']
                        ]
                    }),
                    width: 150,
                    hideHeaders: true,
                    columns: [
                        {dataIndex: 'title', flex: 1}
                    ],
                    border: 0
                },
                {
                    xtype: 'panel',
                    split: true,
                    border: 0,
                    title: 'Фильтры',
                    html: 'здесь будут фильтры',
                    flex: 1
                }
            ]
        },
        {
            xtype: 'form',
            width: 180,
            region: 'east',
            padding: 1,
            collapsible: true,
            collapsed: true,
            title: 'Расширенный поиск',
            items: [
                {
                    xtype: 'panel',
                    margin: 10,
                    border: 0,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'company_title',
                            labelAlign: 'top',
                            fieldLabel: 'Наименование',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'textfield',
                            name: 'cagent_name',
                            labelAlign: 'top',
                            fieldLabel: 'Контактное лицо',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'textfield',
                            name: 'cagent_phone',
                            labelAlign: 'top',
                            fieldLabel: 'Телефон',
                            plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)],
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'textfield',
                            name: 'cagent_email',
                            labelAlign: 'top',
                            fieldLabel: 'E-mail',
                            vtype: 'email',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'combobox',
                            store: 'reference_scope',
                            name: 'reference_scope',
                            labelAlign: 'top',
                            fieldLabel: 'Сфера деят.',
                            displayField: 'title',
                            valueField: 'id',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'textfield',
                            name: 'company_fias_title',
                            labelAlign: 'top',
                            fieldLabel: 'Факт. адрес',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            labelWidth: 110,
                            xtype: 'combobox',
                            fieldLabel: 'Новый запрос',
                            name: 'is_new_title',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'name'],
                                data: [
                                    {value: 1, name: 'Да'},
                                    {value: 2, name: 'Нет'}
                                ]
                            }),
                            displayField: 'name',
                            valueField: 'name',
                            padding: '0 183 5 0',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            labelWidth: 110,
                            xtype: 'combobox',
                            fieldLabel: 'Активные',
                            name: 'status',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'name'],
                                data: [
                                    {value: 1, name: 'Да'},
                                    {value: 2, name: 'Нет'}
                                ]
                            }),
                            displayField: 'name',
                            valueField: 'value',
                            padding: '0 183 5 0',
                            listeners: {
                                specialkey: submitOnEnter
                            },
                        },
                        {
                            xtype: 'button',
                            text: 'Найти',
                            margin: '10 0 0 10',
                            action: 'searchIncomingCall'
                        },
                        {
                            xtype: 'button',
                            text: 'Очистить',
                            margin: '10 0 0 10',
                            action: 'clearIncomingCallSearchForm'
                        }
                    ]
                }
            ]
        }
    ]
});

function submitOnEnter(field, event) {
    if (event.getKey() == event.ENTER) {
        var form = field.up('form').getForm();
        form.submit();
    }
}