Ext.define('app.view.panel.nomenclature', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelNomenclature',
    title: 'Номенклатура',
    flex: 1,
    region: 'center',
    layout: 'border',
    items: [
        {
            xtype: 'treepanel',
            itemId: 'tree',
            title: 'Группы',
            region: 'west',
            collapsible: true,
            split: true,
            width: 250,
            padding: '1 0 1 1',
            store: 'nomenclature_group',
            rootVisible: false,
            hideHeaders: true,
            useArrows: true,
            columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: 'title',
                    flex: 1
                }
            ]
        },
        {
            xtype: 'gridNomenclature',
            itemId: 'grid',
            padding: 1,
            region: 'center'
        },
        {
            xtype: 'form',
            width: 180,
            itemId: 'search',
            region: 'east',
            padding: 1,
            collapsible: true,
            collapsed: true,
            title: 'Расширенный поиск',
            items: [
                {
                    xtype: 'panel',
                    margin: 10,
                    border: 0,
                    items: [
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Вид',
                            name: 'type',
                            labelAlign: 'top',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['value', 'title'],
                                data: [
                                    {value: 'goods', title: 'Товар'},
                                    {value: 'service', title: 'Услуга'}
                                ]
                            }),
                            displayField: 'title',
                            valueField: 'value'
                        },
                        {
                            xtype: 'textfield',
                            name: 'title',
                            labelAlign: 'top',
                            fieldLabel: 'Наименование'
                        },
                        {
                            xtype: 'textfield',
                            name: 'article',
                            labelAlign: 'top',
                            fieldLabel: 'Артикул'
                        },
                        {
                            xtype: 'combobox',
                            name: 'manager_id',
                            store: Ext.create('app.store.user.can_auth'),
                            labelAlign: 'top',
                            fieldLabel: 'Продукт-менеджер',
                            displayField: 'name',
                            valueField: 'id'
                        },
                        {
                            xtype: 'combobox',
                            name: 'supplier_id',
                            store: 'cagent_supplier',
                            labelAlign: 'top',
                            fieldLabel: 'Поставщик',
                            displayField: 'title',
                            valueField: 'id'
                        },
                        {
                            xtype: 'combobox',
                            name: 'status',
                            store: 'filter_status',
                            labelAlign: 'top',
                            fieldLabel: 'Статус',
                            editable: false,
                            value: 1,
                            forceSelection: true,
                            valueField: 'id',
                            displayField: 'title'
                        },
                        {
                            xtype: 'button',
                            text: 'Найти',
                            margin: '10 0 0 10',
                            action: 'search'
                        },
                        {
                            xtype: 'button',
                            text: 'Очистить',
                            margin: '10 0 0 10',
                            action: 'clear'
                        }
                    ]
                }
            ]
        }
    ]
})
;
