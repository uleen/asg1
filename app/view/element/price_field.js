Ext.define('app.view.element.price_field', {
    extend: 'Ext.form.field.Number',
    alias: 'widget.pricefield',
    minValue: 0,
    forcePrecision: true,
    decimalPrecision: 2,
    hideTrigger: true,
    keyNavEnabled: false,
    mouseWheelEnabled: false
});