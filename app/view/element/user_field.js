Ext.define('app.view.element.user_field', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.userfield',
    editable: false,
    triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
    initComponent: function () {
        this.callParent();
    },
    onTriggerClick: function () {
        var self = this;
        var form = this.up('form').getForm();
        var win = Ext.widget('winUser');
        win.show();
        // обработка двойного клика в гриде
        win.down('grid').on(
            'itemdblclick', function (grid, record) {
                form.setValues([
                    {id: self.name, value: record.get('title')},
                    {id: self.valueField, value: record.get('id')}
                ]);
                win.close();
            }
        );
    }
});