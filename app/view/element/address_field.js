Ext.define('app.view.element.address_field', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.addressfield',
    editable: false,
    triggerCls: Ext.baseCSSPrefix + 'form-point-trigger',
    initComponent: function () {
        this.callParent();
    },
    onTriggerClick: function () {
        var self = this;
        var form = this.up('form').getForm();
        var winAddress = Ext.widget('formSelectAddress');
        var formAddress = winAddress.down('form').getForm();

        var json = form.getValues()[self.jsonField];
        if (json) {
            formAddress.setValues(JSON.parse(json));
        }
        winAddress.show();

        winAddress.down('button').on({click: function () {
            form.setValues([
                {id: self.name, value: formAddress.getValues().address},
                {id: self.valueField, value: formAddress.getValues().fias_id},
                {id: self.jsonField, value: JSON.stringify(formAddress.getValues())}
            ]);
            winAddress.close();
        }});
    }
});