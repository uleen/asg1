Ext.define('app.view.menu.user_preferences', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.menuUserPreferences',
    title: 'Разделы',
    rootVisible: false,
    initComponent: function () {
    	
    	var user_manage = App.Global.checkRights(1);
    	
    	if (user_manage == true)
    	{
            this.store = Ext.create('Ext.data.TreeStore', {
                root: {
                    text: 'Все разделы',
                    expanded: true,
                    children: [
                       {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-user-preferences-main'},
                       {text: "Права доступа", iconCls: 'icon_user_preferences_accept', formId: 'form-user-access-ic', children:
                    	   [   
                    	       {text: "Шаблоны", leaf: true, iconCls: 'icon_calculator'},
                    	    ]},
                    ]
                }
            });
    	}
    	else
    	{
            this.store = Ext.create('Ext.data.TreeStore', {
                root: {
                    text: 'Все разделы',
                    expanded: true,
                    children: [
                       {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-user-preferences-main'},
                       {text: "Права доступа", leaf: true, iconCls: 'icon_user_preferences_accept', formId: 'form-user-access-ic'},
                    ]
                }
            });	
    	}

        this.callParent(arguments);
    }
});
