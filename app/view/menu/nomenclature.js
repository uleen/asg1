Ext.define('app.view.menu.nomenclature', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.menuNomenclature',
    title: 'Разделы',
    rootVisible: false,
    initComponent: function () {
        this.store = Ext.create('Ext.data.TreeStore', {
            root: {
                text: 'Все разделы',
                expanded: true,
                children: [
                    {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-nomenclature'},
                    {text: "Характеристики", leaf: true, iconCls: 'icon_page_white_text', formId: 'grid-nomenclature-characteristics'},
                    {text: "Отпускные цены", leaf: true, iconCls: 'icon_money', formId: 'grid-nomenclature-price'},
                    {text: "Поставщики", leaf: true, iconCls: 'icon_lorry', formId: 'grid-nomenclature-supplier'},
                ]
            }
        });
        this.callParent(arguments);
    }
});