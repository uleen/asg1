Ext.define('app.view.menu.cagent_supplier', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.menuCagentSupplier',
    title: 'Разделы',
    rootVisible: false,
    initComponent: function () {
        this.store = Ext.create('Ext.data.TreeStore', {
            root: {
                text: 'Все разделы',
                expanded: true,
                children: [
                    {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-cagent-main'},
                    {text: "Контактные лица", leaf: true, iconCls: 'icon_group', formId: 'form-cagent-contact'},
                    {text: "Точки загрузки", leaf: true, iconCls: 'icon_lorry', formId: 'grid-cagent-delivery'},
                    {text: "Реквизиты и Договоры", leaf: true, iconCls: 'icon_page_copy', formId: 'form-cagent-contract'},
                    {text: "Номенклатуры и цены", leaf: true, iconCls: 'icon_inbox_images', formId: 'grid-contractor-nomenclature'},
                    {text: "Документы", children: [
                        {text: "Заказы", leaf: true, iconCls: 'icon_calculator'},
                        {text: "Оплаты", leaf: true, iconCls: 'icon_money'},
                        {text: "Поступления", leaf: true}
                    ]},
                    {text: "Отчеты", leaf: true, iconCls: 'icon_report'}
                ]
            }
        });
        this.callParent(arguments);
    }
});
