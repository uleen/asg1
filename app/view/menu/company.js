Ext.define('app.view.menu.company', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.menu.company',
    rootVisible: false,
    initComponent: function () {
        this.store = Ext.create('Ext.data.TreeStore', {
            root: {
                text: 'Все разделы',
                expanded: true,
                children: [
                    {text: "Основное", leaf: true, iconCls: 'building', formId: 'form-company-main'},
                    {text: "Реквизиты", leaf: true, iconCls: 'icon_page_copy', formId: 'form-company-contact'},
                    {text: "Банковские счета", leaf: true, iconCls: 'icon_page_copy', formId: 'grid-company-requisites'},
                    {text: "Ответственные лица", expanded: true, children: [
                        {text: "Руководитель", leaf: true, iconCls: 'form-company-chief', formId: 'form-company-chief'},
                        {text: "Бухгалтер", leaf: true, iconCls: 'form-company-booker', formId: 'form-company-booker'}
                    ]},
                    {text: "Логотип и текст", leaf: true, iconCls: 'icon_document_image', formId: 'form-company-logo'}
                ]
            }
        });
        this.callParent(arguments);
    }
});
