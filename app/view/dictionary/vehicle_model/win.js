Ext.define('app.view.dictionary.vehicle_model.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.vehicle_model.win',
    title: 'Модели автомобилей',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.vehicle_model.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});