Ext.define('app.view.dictionary.vehicle_model.form', {
	extend: 'Ext.window.Window',
	alias: 'widget.dictionary.vehicle_model.form',
	title: 'Модель автомобиля',
	layout: 'fit',
	resizable: false,
	items: [
		{
			xtype: 'form',
			border: 0,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [
				{
					xtype: 'panel',
					border: 0,
					flex: 1,
					items: [
						{
							xtype: 'fieldset',
							title: 'Технические характеристики',
							margin: 10,
							padding: '5 10',
							items: [
								{
									xtype: 'fieldcontainer',
									layout: 'vbox',
									items: [
										{
											xtype: 'hiddenfield',
											name: 'id',
											readOnly: true
										},
										{
											xtype: 'textfield',
											name: 'title',
											fieldLabel: 'Наименование',
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'width',
											fieldLabel: 'Ширина(м)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'height',
											fieldLabel: 'Высота(м)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'depth',
											fieldLabel: 'Глубина(м)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'net_volume',
											fieldLabel: 'Полезный объем (куб.м.)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'carrying_capacity',
											fieldLabel: 'Грузоподъемность (т)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130

										}
									]
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					border: 0,
					flex: 1,
					items: [
						{
							xtype: 'fieldset',
							title: 'Топливо',
							margin: 10,
							padding: '5 10',
							items: [
								{
									xtype: 'panel',
									border: 0,
									margin: '0 0 10 0',
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											xtype: 'numberfield',
											name: 'fuel_consumption_empty',
											fieldLabel: 'Расход топлива (пустой)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'numberfield',
											name: 'fuel_consumption_laden',
											fieldLabel: 'Расход топлива (груженый)',
											allowDecimals: true,
											decimalPrecision: 2,
											step: 0.1,
											labelWidth: 130
										},
										{
											xtype: 'combobox',
											fieldLabel: 'Вид топлива',
											name: 'fuel_title',
											labelWidth: 130,
											editable: false,
											store: Ext.create('app.store.dictionary.fuel'),
											displayField: 'title',
											listeners: {
												select: function (combo, records) {
													var rVehicleModel = records[0];
													this.up('form').getForm().setValues({
														fuel_id: rVehicleModel.get('id')
													});
												}
											}
										},
										{
											xtype: 'hidden',
											name: 'fuel_id'
										}
									]
								}
							]
						}
					]
				}
			]
		}
	],
	buttons: [
		{
			text: 'Сохранить',
			action: 'save'
		}
	]
});
