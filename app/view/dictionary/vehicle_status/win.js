Ext.define('app.view.dictionary.vehicle_status.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.vehicle_status.win',
    title: 'Статусы транспортного средства',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.vehicle_status.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});