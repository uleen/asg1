Ext.define('app.view.dictionary.fuel.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.fuel.win',
    title: 'Виды ГСМ',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.fuel.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});