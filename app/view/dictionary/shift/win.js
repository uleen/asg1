Ext.define('app.view.dictionary.shift.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.shift.win',
    title: 'Вахты (смены)',
    layout: 'fit',
    height: 300,
    width: 600,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.shift.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});