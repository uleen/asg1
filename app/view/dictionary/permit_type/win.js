Ext.define('app.view.dictionary.permit_type.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.permit_type.win',
    title: 'Виды пропусков',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.permit_type.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});