Ext.define('app.view.dictionary.type_document.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.type_document.win',
    title: 'Виды документов',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.type_document.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});