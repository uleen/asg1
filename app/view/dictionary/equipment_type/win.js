Ext.define('app.view.dictionary.equipment_type.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.dictionary.equipment_type.win',
    title: 'Виды оборудования',
    layout: 'fit',
    height: 300,
    width: 500,
    minimizable: true,
    items: [
        {
            xtype: 'dictionary.equipment_type.grid',
            flex: 1,
            border: 0
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});