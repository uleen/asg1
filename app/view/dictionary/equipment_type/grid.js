Ext.define('app.view.dictionary.equipment_type.grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dictionary.equipment_type.grid',
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 30
        },
        {
            text: 'Название',
            dataIndex: 'title',
            flex: 1,
            field: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            sortable: false,
            align: 'center',
            handler: function (grid, row) {
                var record = grid.getStore().getAt(row);
                record.set('status', record.get('status') == 1 ? 2 : 1);
            },
            getClass: function (v, meta, rec) {
                if (rec.get('status') == 1) {
                    this.tooltip = 'Активно';
                    return 'icon_bullet_green';
                }
                if (rec.get('status') == 2) {
                    this.tooltip = 'Не активно';
                    return 'icon_bullet_red';
                }
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.dictionary.equipment_type');
        this.callParent(arguments);
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('grid');
                        var record = Ext.create('app.model.dictionary.equipment_type', {
                            status: 1
                        });
                        grid.getStore().add(record);
                    }
                },
                '->',
                {
                    xtype: 'combobox',
                    action: 'filter_status',
                    store: 'filter_status',
                    editable: false,
                    value: 1,
                    padding: '0 5',
                    forceSelection: true,
                    valueField: 'id',
                    displayField: 'title',
                    listeners: {
                        change: function (view, val) {
                            var store = view.up('window').down('grid').getStore();
                            store.getProxy().extraParams.status = val;
                            store.load();
                        }
                    }
                }
            ]
        },
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                '->',
                {
                    text: 'Сохранить',
                    iconCls: 'icon_disk',
                    padding: 5,
                    handler: function () {
                        this.up('grid').getStore().sync();
                    }
                }
            ]
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    viewConfig: {
        getRowClass: function (record) {
            return record.get('status') == 1 ? 'active-row' : 'no-active-row';
        }
    }
});