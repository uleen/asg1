Ext.define('app.view.WinGrid', {
    extend: 'Ext.window.Window',
    alias: 'widget.wingrid',
    title: 'Форма GRID',
    layout: 'fit',
    autoShow: true,
    height: 500,
    width: 400,
    tbar: [
        {
            text: 'Добавить',
            iconCls: 'icon_note_add'
        },
        {
            text: 'Изменить',
            iconCls: 'icon_note_edit'
        },
        {
            text: 'Удалить',
            iconCls: 'icon_note_delete'
        }
    ],
    items: [
        {
            xtype: 'editgrid',
            store: 'StoreReclama',
            border: 0,
            width: '100%',
            height: '100%'
        }
    ]
});
