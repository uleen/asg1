Ext.define('app.view.MainView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.main',
    width: '100%',
    height: '100%',
    border: 0,
    tbar: [
        {
            text: 'Справочники',
            menu: [
                {
                    action: 'menuCurrency',
                    text: 'Валюты',
                    iconCls: 'icon_money_dollar'
                },
                {
                    action: 'menuDictionaryTypeDocument',
                    text: 'Виды документов'
                },
                {
                    action: 'menuTypePayment',
                    text: 'Виды расчетов'
                },
                {
                    action: 'menuContractorContracts',
                    text: 'Договоры',
                    iconCls: 'icon_page_copy'
                },
                {
                    action: 'menuDictionaryPosition',
                    text: 'Должности',
                    iconCls: 'icon_group'
                },
                {
                    action: 'menuUnit',
                    text: 'Единицы измерения',
                    iconCls: 'icon_balance'
                },
                {
                    action: 'menuCustomerCategory',
                    text: 'Категория клиента'
                },
                {
                    action: 'menuContactPersonal',
                    text: 'Контактные лица',
                    iconCls: 'icon_contact_personal'
                },
                {
                    action: 'menuDirectionSales',
                    text: 'Направление продаж'
                },
                {
                    action: 'menuNomenclatureGroup',
                    text: 'Номенклатурные группы'
                },
                {
                    action: 'menuOKPFO',
                    text: 'ОПФ'
                },
                {
                    action: 'menuCompany',
                    text: 'Организации',
                    iconCls: 'icon_building'
                },
                {
                    action: 'menuContractorRequisites',
                    text: 'Реквизиты',
                    iconCls: 'icon_page_copy'
                },
                {
                    action: 'menuReclama',
                    text: 'Реклама'
                },
                {
                    action: 'menuUser',
                    text: 'Сотрудники',
                    iconCls: 'icon_user'
                },
                {
                    action: 'menuReferenceScope',
                    text: 'Сфера деятельности'
                },
                {
                    action: 'menuContractorDelivery',
                    text: 'Точки доставки / загрузки',
                    iconCls: 'icon_lorry'
                },
                {
                    text: 'Транспорт',
                    iconCls: 'icon_folder',
                    menu: [
                        {
                            action: 'menuDictionaryVehicleStatus',
                            text: 'Статусы транспортного средства'
                        },
                        {
                            action: 'menuDictionaryVehicleType',
                            text: 'Виды транспортных средств'
                        },
                        {
                            action: 'menuDictionaryEquipmentType',
                            text: 'Виды оборудования'
                        },
						{
							action: 'menuDictionaryPermitType',
							text: 'Виды пропусков'
						},
                        {
                            action: 'menuDictionaryFuel',
                            text: 'Виды ГСМ'
                        },
                        {
                            action: 'menuDictionaryVehicleModel',
                            text: 'Модели автомобилей'
                        },
                        {
                            action: 'menuVehicle',
                            text: 'Транспортные средства',
                            iconCls: 'icon_tipper'
                        },
                        {
                            action: 'menuDictionaryShift',
                            text: 'Вахты (смены)'
                        },
                        {
                            action: 'menuDriver',
                            text: 'Водители',
                            iconCls: 'icon_user_pilot'
                        }
                    ]
                },
                {
                    action: 'menuTypePrice',
                    text: 'Цены',
                    iconCls: 'icon_money'
                },
                {
                    text: 'Классификаторы',
                    iconCls: 'icon_folder',
                    menu: [
                        {
                            action: 'menuOKVED',
                            text: 'ОКВЭД'
                        },
                        {
                            action: 'menuOKEI',
                            text: 'ОКЕИ'
                        },
                        {
                            action: 'menuFias',
                            text: 'ФИАС'
                        }
                    ]
                }
            ]
        },
        {
            text: 'Документы',
            menu: [
                {
                    action: 'menuListIncomingCall',
                    text: 'Входящие запросы',
                    iconCls: 'icon_bell'
                },
                {
                    action: 'menuListSheet',
                    text: 'Выставленные счета',
                    iconCls: 'icon_invoice'
                }
            ]
        },
        '->',
        {
            xtype: 'tbtext',
            text: '',
            id: 'user-name-toolbar'
        },
        {
            text: 'настройки',
            iconCls: 'icon_cog',
            action: 'menuUserPreferences'
        },
        {
            text: 'выход',
            iconCls: 'icon_door_in',
            handler: function () {
                Ext.Msg.show({
                    msg: 'Вы действительно хотите выйти?',
                    buttons: Ext.Msg.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            window.location = '/logout?_dc=' + Math.random();
                        }
                    }
                });
            }
        }
    ],
    layout: 'border',
    items: [
        {
            xtype: 'panel',
            region: 'north',
            bodyCls: 'bg-panel',
            border: 0,
            padding: '2 0',
            items: [
                {
                    xtype: 'button',
                    action: 'menuIncomingCall',
                    text: 'Входящий запрос',
                    padding: 10,
                    margin: '0 0 0 2',
                    iconCls: 'icon_bell_add'
                },
                {
                    xtype: 'button',
                    action: 'menuListCagent',
                    text: 'Контрагенты',
                    padding: 10,
                    margin: '0 0 0 2',
                    iconCls: 'icon_group'
                },
                {
                    xtype: 'button',
                    action: 'menuListNomenclature',
                    text: 'Номенклатура',
                    padding: 10,
                    margin: '0 0 0 2',
                    iconCls: 'icon_inbox_images'
                },
                {
                    xtype: 'button',
                    action: 'menuTableManager',
                    text: 'Каталог менеджера',
                    padding: 10,
                    margin: '0 0 0 2',
                    iconCls: 'icon_report'
                },
                {
                    xtype: 'button',
                    action: 'menuClientOrder',
                    text: 'Заказы клиентов',
                    padding: 10,
                    margin: '0 0 0 2',
                    iconCls: 'icon_document_prepare'
                }

            ]
        },
        {
            xtype: 'panel',
            id: 'main-panel',
            region: 'center',
            border: 0,
            layout: 'border',
            items: [
                {
                    xtype: 'panel',
                    region: 'center',
                    bodyStyle: "background: #eee url('/images/bg_main.png') center center no-repeat"
                }
            ]
        }
    ]
});

