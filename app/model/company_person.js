Ext.define('app.model.company_person', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'company_id', type: 'int', useNull: true},
        {name: 'type', type: 'string', useNull: true}, //
        {name: 'fio_ip', type: 'string', useNull: true}, // ФИО (и.п.)',
        {name: 'fio_rp', type: 'string', useNull: true}, // ФИО (р.п.)',
        {name: 'post_ip', type: 'string', useNull: true}, // Должность (и.п.)',
        {name: 'post_rp', type: 'string', useNull: true}, // Должность (р.п.)',
        {name: 'reason', type: 'string', useNull: true}, // На основании',
        {name: 'date_from', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Период действия с',
        {name: 'date_to', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Период действия по',
        {name: 'title', type: 'string', useNull: true}, // Рабочее наименование',
        {name: 'scan_file_id', type: 'int', useNull: true}, // ID файла скана подписи'
        {name: 'file_title', type: 'string', useNull: true}, // пользовательское название файла
        {name: 'file_name', type: 'string', useNull: true}, // имя файла
        {name: 'file_link', convert: function (val, rec) { // ссылка на загрузку файла
            return rec.get('scan_file_id') ? ('<a href="/files/' + rec.get('file_name') + '" target="_blank">' + rec.get('file_title') + '</a>') : null;
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_company/person',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});
