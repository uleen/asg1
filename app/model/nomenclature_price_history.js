Ext.define('app.model.nomenclature_price_history', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true}, //
        {name: 'nomenclature_id', type: 'int', useNull: true}, // ID номенклатуры
        {name: 'type_price_id', type: 'int', useNull: true}, // ID типа цены
        {name: 'price', type: 'double', useNull: true}, // Цена
        {name: 'currency_id', type: 'int', useNull: true}, // ID валюты цены
        {name: 'currency_code', type: 'string', useNull: true}, // Код валюты цены
        {name: 'change_date', type: 'date', dateFormat: 'd.m.Y H:i', useNull: true}, // Дата и время изменения записи
        {name: 'change_user_id', type: 'int', useNull: true}, // ID пользователя, изменившего запись
        {name: 'change_user_name', type: 'string', useNull: true} // Имя пользователя, изменившего запись
    ]
});