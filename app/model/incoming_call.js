Ext.define('app.model.incoming_call', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'root_id', type: 'int'},
        {name: 'org_id', type: 'int'},
        {name: 'contractor_id', type: 'int'},
        {name: 'contact_personal_id', type:'int'},
        {name: 'icr_id', type: 'int'},
        {name: 'from_search', type: 'int'},
        {name: 'call_date', type: 'date', dateFormat: 'd.m.Y H:i'},
        {name: 'call_number', type: 'string'},
        {name: 'incoming_source_id', type: 'int'},
        {name: 'save_date', type: 'date', dateFormat: 'd.m.Y H:i'},
        {name: 'author_name', type: 'string'},
        {name: 'cagent_id', type: 'int'},
        {name: 'company_title', type: 'string'},
        {name: 'company_inn', type: 'string'},
        {name: 'reference_scope_id', type: 'string'},
        {name: 'reference_scope', type: 'string'},
        {name: 'reclama_id', type: 'int'},
        {name: 'client_interest', type: 'string'},
        {name: 'reclama_interest', type: 'string'},
        {name: 'cagent_name', type: 'string'},
        {name: 'cagent_old_name', type: 'string'},
        {name: 'cagent_phone', type: 'string'},
        {name: 'cagent_email', type: 'string'},
        {name: 'company_type', type: 'string'},
        {name: 'company_fias_id', type: 'string'},
        {name: 'company_fias_title', type: 'string'},
        {name: 'client_interest_srv', type: 'string'},
        {name: 'is_new', type: 'int'},
        {name: 'status', type: 'int'},
        {name: 'is_new_title', type: 'string'},
        {name: 'reclama_title', type: 'string'},
        {name: 'source_title', type: 'string'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }},
        {name: 'ref_cagent_id', type: 'int'},
        {name: 'ref_user_id', type: 'int'},
        {name: 'ref_user', type: 'string'},
        {name: 'responsible_user', type: 'int'}, // ответственный сотрудник
        {name: 'executed_by_author', type: 'int'},
        {name: 'executed', type: 'int'},
        {name: 'author_id', type: 'int'},
        {name: 'manager_id', type: 'int'},
        {name: 'manager_name', type: 'string'},
        {name: 'responsible_user_id', type: 'int'},
        {name: 'responsible_user', type:'string'},
        {name: 'cagent_add_phone', type:'string'},
        {name: 'uniq_form_id', type:'int'},
        {name: 'title', type:'string'}
    ],

    validations: [
        {field: 'company_title', type: 'format', matcher: /^[A-Za-zА-Яа-я0-9]/, message: 'Поле "Наименование" должно начинаться с буквы или цифры'},
        {field: 'company_title', type: 'presence', message: 'Поле "Наименование" обязательно для заполнения'},
        //{field: 'client_interest_srv', type: 'presence', message: 'Поле "Услуга" обязательно для заполнения'},
        {field: 'cagent_phone', type: 'format', matcher: /^\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}$/, message: 'Поле "Телефон" указано не верно'},
        {field: 'client_interest', type: 'presence', message: 'Поле "Комментарий" обязательно для заполнения'},
    ],
    proxy: {
        type: 'rest',
        url: '/incoming_call_register',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});