Ext.define('app.model.user', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'login', type: 'string'},
        {name: 'work_email', type: 'string'},
        {name: 'post', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'empl_id', type: 'int'},
        {name: 'password', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_user',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
        },
        /*
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
        }
        */
    }
});
