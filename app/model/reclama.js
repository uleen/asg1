Ext.define('app.model.reclama', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'type', type: 'int', useNull: true},
        {name: 'action', type: 'int', useNull: true},
        {name: 'title', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
        {type: 'presence', field: 'title', message: 'Вы не заполнили поле "Заголовок"'}
    ],
    proxy: {
        type: 'rest',
        url : '/spec/d_reclama'
    }
});

