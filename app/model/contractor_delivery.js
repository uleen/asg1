Ext.define('app.model.contractor_delivery', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'contractor', type: 'string'}, // контрагент
        {name: 'contractor_id', type: 'int'}, // ID контрагента
        {name: 'address', type: 'string', useNull: true}, // адрес
        {name: 'commentary', type: 'string', useNull: true}, // комментарий
        {name: 'lat', type: 'float', useNull: true}, // широта (по яндексу)
        {name: 'lng', type: 'float', useNull: true}, // долгота (по яндексу)
        {name: 'delay', type: 'int', useNull: true}, // задержка на точке (мин)
        {name: 'time_from', type: 'string', useNull: true}, // окно доставки - от
        {name: 'time_to', type: 'string', useNull: true}, // окно доставки - до
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
    ],
    proxy: {
        type: 'rest',
        url: '/d_contractor_delivery',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});