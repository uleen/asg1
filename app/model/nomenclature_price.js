Ext.define('app.model.nomenclature_price', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'nomenclature_id', type: 'int'}, // ID номенклатуры
        {name: 'nomenclature_title', type: 'string', useNull: true}, // Название номенклатуры
        {name: 'type_price_id', type: 'int'}, // ID типа цены
        {name: 'type_price_title', type: 'string', useNull: true}, // Название типа цены
        {name: 'price', type: 'double', useNull: true}, // Цена
        {name: 'currency', type: 'string', useNull: true} // Валюта
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/price',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});