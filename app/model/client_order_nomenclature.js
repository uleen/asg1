Ext.define('app.model.client_order_nomenclature', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'client_order_id', type: 'int', useNull: true},
        {name: 'nomenclature_id', type: 'int', useNull: true},
        {name: 'nomenclature_title', type: 'string', useNull: true},
        {name: 'nomenclature_article', type: 'string', useNull: true},
        {name: 'nomenclature_characteristics', type: 'string', useNull: true},
        {name: 'contractor_delivery_id', type: 'int', useNull: true},
        {name: 'contractor_delivery_address', type: 'string', useNull: true},
        {name: 'contractor_title', type: 'string', useNull: true},
        {name: 'unit_id', type: 'int', useNull: true},
        {name: 'unit_title', type: 'string', useNull: true},
        {name: 'volume', type: 'int', useNull: true},
        {name: 'price', type: 'double', useNull: true},
        {name: 'cost', type: 'double', useNull: true},
        {name: 'cost_nds', type: 'double', useNull: true}
    ],
    validations: [
    ],
    proxy: {
        type: 'rest',
        url: '/client_order/nomenclature',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});