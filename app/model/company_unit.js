Ext.define('app.model.company_unit', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'company_id', type:'int'},
        {name: 'title', type: 'string', useNull: true}, // название организации
    ],

    proxy: {
        type: 'rest',
        url: '/spec/d_company_unit'
    }
});
