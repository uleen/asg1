Ext.define('app.model.company_requisite', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'company_id', type: 'int', useNull: true}, // ID организации
        {name: 'title', type: 'string', useNull: true}, // название счета
        {name: 'number', type: 'string', useNull: true}, // номер счета
        {name: 'bank_title', type: 'string', useNull: true}, // название банка
        {name: 'bank_bik', type: 'string', useNull: true}, // бик банка
        {name: 'bank_ks', type: 'string', useNull: true}, // корр. счет банка
        {name: 'currency_id', type: 'int', useNull: true}, // ID валюты
        {name: 'payer', type: 'string', useNull: true}, // Плательщик
        {name: 'type', type: 'int', useNull: true}, // Вид счета (1 - р/с, 2 - иной)
        {name: 'status', type: 'int', useNull: true}, // статус
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_company/requisite',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});
