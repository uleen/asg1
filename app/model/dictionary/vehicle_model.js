Ext.define('app.model.dictionary.vehicle_model', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true},
        {name: 'status', type: 'int', useNull: true},
		{name: 'vehicle_type_id', type: 'int', useNull: true},
		{name: 'width', type: 'float', useNull: true},
		{name: 'height', type: 'float', useNull: true},
		{name: 'depth', type: 'float', useNull: true},
		{name: 'net_volume', type: 'float', useNull: true},
		{name: 'carrying_capacity', type: 'float', useNull: true},
		{name: 'fuel_id', type: 'int', useNull: true},
		{name: 'fuel_title', type: 'string', useNull: true},
		{name: 'fuel_consumption_empty', type: 'float', useNull: true},
		{name: 'fuel_consumption_laden', type: 'float', useNull: true},

    ],
	proxy: {
		type: 'rest',
		url: '/dictionary/vehicle_model',
		successProperty: 'success',
		reader: {
			type: 'json',
			root: 'item'
		}
	}
});