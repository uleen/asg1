Ext.define('app.model.dictionary.shift', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true}, // Наименование
        {name: 'title_short', type: 'string', useNull: true}, // Наименование краткое
        {name: 'time_start', type: 'string', useNull: true}, // Время начала (hh:mm)
        {name: 'time_end', type: 'string', useNull: true}, // Время окончания (hh:mm)
        {name: 'color', type: 'string', useNull: true}, // Цвет в графике (#rgb)
        {name: 'status', type: 'int', useNull: true}
    ]
});