Ext.define('app.model.dictionary.fuel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'nomenclature_id', type: 'int', useNull: true}, // ID номенклатуры
        {name: 'nomenclature_title', type: 'string', useNull: true}, // Название номенклатуры
        {name: 'title', type: 'string', useNull: true}, // Наименование
        {name: 'status', type: 'int', useNull: true}
    ]
});