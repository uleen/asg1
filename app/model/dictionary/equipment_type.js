Ext.define('app.model.dictionary.equipment_type', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true},
        {name: 'status', type: 'int', useNull: true}
    ]
});