Ext.define('app.model.okei', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'code', type: 'string'},
        {name: 'title', type: 'string'},
        {name: 'title_short_ru', type: 'string'},
        {name: 'title_short_en', type: 'string'},
        {name: 'symbol_ru', type: 'string'},
        {name: 'symbol_en', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/dictionary/okei'
    }
});

