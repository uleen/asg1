Ext.define('app.model.address_house', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'fias_id', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'fullname', type: 'string'},
        {name: 'parent', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'typeShort', type: 'string'},
        {name: 'zip', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_address/house'
    }
});
