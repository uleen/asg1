Ext.define('app.model.sheet_status_history', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'sheet_id', type: 'int'}, // ID счета
        {name: 'sheet_status_id', type: 'int'}, // ID статуса
        {name: 'sheet_status_title', type: 'string'}, // Название статуса
        {name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s'}, // Дата изменения
        {name: 'user_id', type: 'int'}, // ID пользователя
        {name: 'user_name', type: 'string'}, // Имя пользователя
        {name: 'note', type: 'string', useNull: true} // Комментарий к изменению статуса
    ],
    proxy: {
        type: 'rest',
        url: '/sheet/status_history',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});