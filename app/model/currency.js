Ext.define('app.model.currency', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'num', type: 'int', useNull: true},
        {name: 'code', type: 'string', useNull: true},
        {name: 'title', type: 'string', useNull: true},
        {name: 'status', type: 'int', useNull: true}
    ]
});