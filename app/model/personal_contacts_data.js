Ext.define('app.model.personal_contacts_data', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
       // {name: 'phone_code', type: 'int', useNull: true},
       // {name: 'phone_prefix', type: 'int', useNull: true},
       // {name: 'phone_number', type: 'int', useNull: true},
        {name: 'is_active', type: 'int'},
        {name: 'contact_personal_id', type: 'int'},
        {name: 'number', type: 'string'},
        {name: 'is_main', type: 'int'},
        {name: 'contact_type', type: 'string'},
        {name: 'add_contact', type: 'string'},
        {name: 'na', type: 'int'}
    ],
    validations: [

    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_personal_contacs_data'
    }
});
