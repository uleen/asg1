Ext.define('app.model.client_order_status_history', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'client_order_id', type: 'int'}, // ID клиентского заказа
        {name: 'client_order_status_id', type: 'int'}, // ID статуса
        {name: 'client_order_status_title', type: 'string'}, // Название статуса
        {name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s'}, // Дата изменения
        {name: 'user_id', type: 'int'}, // ID пользователя
        {name: 'user_name', type: 'string'}, // Имя пользователя
        {name: 'client_order_status_note', type: 'string', useNull: true} // Комментарий к изменению статуса
    ],
    proxy: {
        type: 'rest',
        url: '/client_order/status_history',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});