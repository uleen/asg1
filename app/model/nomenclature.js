Ext.define('app.model.nomenclature', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'nomenclature_group_id', type: 'int', useNull: true},    // ID номенклатурной группы
        {name: 'nomenclature_group_title', type: 'string', useNull: true},    // название номенклатурной группы
        {name: 'nomenclature_group_full_title', type: 'string', useNull: true},    // полное название номенклатурной группы
        {name: 'article', type: 'string', useNull: true}, // Артикул
        {name: 'title', type: 'string', useNull: true}, // Наименование
        {name: 'unit_id', type: 'int', useNull: true}, // Ед. изм.
        {name: 'unit_title', type: 'string', useNull: true}, // Ед. изм.
        {name: 'shipping_unit_id', type: 'int', useNull: true}, // Ед. отгр.
        {name: 'shipping_unit_title', type: 'string', useNull: true}, // Ед. отгр.
        {name: 'shipping_min', type: 'string', useNull: true}, // Мин. отгр.
        {name: 'manager_id', type: 'int', useNull: true}, // ID менеджера
        {name: 'manager_title', type: 'string', useNull: true}, // Имя менеджер
        {name: 'create_date', type: 'string', useNull: true},
        {name: 'characteristics', type: 'string', useNull: true}, // конкатенированные характеристики
        {name: 'price', type: 'double', useNull: true}, // отпускная цена
        {name: 'type', type: 'string', useNull: true},
        {name: 'note', type: 'string', useNull: true},
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});