Ext.define('app.model.okved', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'code', type: 'string'},
        {name: 'reference_scope_id', type: 'int'},
        {name: 'title', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'additional_info', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/dictionary/okved'
    }
});

