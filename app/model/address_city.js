Ext.define('app.model.address_city', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'fullname', type: 'string'},
        {name: 'parent', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'typeShort', type: 'string'},
        {name: 'zip', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_address/city'
    }
});
