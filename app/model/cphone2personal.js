Ext.define('app.model.cphone2personal', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'phone_id', type: 'int'},
        {name: 'personal_id', type: 'int'},
        {name: 'is_main', type: 'int'}
    ],
    validations: [

    ],
    proxy: {
        type: 'rest',
        url : '/spec/d_cphone2personal'
    }
});
