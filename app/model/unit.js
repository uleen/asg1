Ext.define('app.model.unit', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'code', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true},
        {name: 'title_full', type: 'string', useNull: true},
        {name: 'status', type: 'int'}
    ],
    validations: [
        {type: 'presence', field: 'title', message: 'Вы не заполнили поле "Заголовок"'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_unit'
    }
});