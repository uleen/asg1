Ext.define('app.model.cagent', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'type', type: 'int'},
        {name: 'type_title', type: 'string'},
        {name: 'okpfo_type', type: 'int', useNull: true},
        {name: 'okpfo_type_title', type: 'string', useNull: true},
        {name: 'org_type', type: 'int'},
        {name: 'ref_scope_id', type: 'string'},
        {name: 'ref_scope', type: 'string', useNull: true},
        {name: 'client_category', type: 'int', useNull: true},
        {name: 'client_category_title', type: 'string', useNull: true},
        {name: 'manager_id', type: 'int', useNull: true},
        {name: 'manager_title', type: 'string', useNull: true},
        {name: 'reclama_id', type: 'int'},
        {name: 'reclama', type: 'string', useNull: true},
        {name: 'create_date', type: 'date', dateFormat: 'd.m.Y H:i'},
        {name: 'ur_adress_fias', type: 'string'},
        {name: 'post_adress_fias', type: 'string'},
        {name: 'fact_address_fias', type: 'string'},
        {name: 'ur_address_text', type: 'string'},
        {name: 'post_address_text', type: 'string'},
        {name: 'fact_address_text', type: 'string'},
        {name: 'ur_address_json', type: 'string'},
        {name: 'post_address_json', type: 'string'},
        {name: 'fact_address_json', type: 'string'},
        {name: 'phone', type: 'string'},
        {name: 'fax', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'url', type: 'string'},
        {name: 'title', type: 'string'},
        {name: 'title_full', type: 'string'},
        {name: 'comment', type: 'string'},
        {name: 'is_blocked', type: 'int'},
        {name: 'inn', type: 'string'},
        {name: 'kpp', type: 'string'},

        {name: 'opf_title_full', type: 'string', useNull: true},
        {name: 'opf_title', type: 'string', useNull: true},
        {name: 'org_id', type: 'int'},
        {name: 'contractor_id', type: 'int'},
        {name: 'main_personal_id', type: 'int'},

        {name: 'title_inn', convert: function (value, record) {
            return record.get('title') + (record.get('inn') ? ', ' + record.get('inn') : '');
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/d_contractor',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});
