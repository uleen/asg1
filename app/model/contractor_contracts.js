Ext.define('app.model.contractor_contracts', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'}, //
        {name: 'contractor_id', type: 'int'}, // ID контрагента
        {name: 'contractor', type: 'string'}, // контрагент
        {name: 'org_id', type: 'int', useNull: true}, // ID организации
        {name: 'type', type: 'int'}, // Вид договора (1 - с покупателем, 2 - с поставщиком)
        {name: 'doc_num', type: 'string'}, // Номер договора
        {name: 'doc_date', type: 'date', dateFormat: 'd.m.Y'}, // Дата договора
        {name: 'action_from_date', type: 'date', dateFormat: 'd.m.Y'}, // Период действия - с
        {name: 'action_to_date', type: 'date', dateFormat: 'd.m.Y'}, // Период действия - по
        {name: 'title', type: 'string', useNull: true}, // Наименование
        {name: 'direction_sales_id', type: 'int', useNull: true}, // ID направления продаж
        {name: 'type_payment_id', type: 'int', useNull: true}, // ID вида расчетов
        {name: 'type_price_id', type: 'int', useNull: true}, // ID типа цены
        {name: 'currency_id', type: 'int', useNull: true}, // ID валюты
        {name: 'amount', type: 'int', useNull: true}, // Сумма договора
        {name: 'volume', type: 'int', useNull: true}, // Объем договора
        {name: 'file_id', type: 'int', useNull: true}, // Ссылка на файл
        {name: 'commentary', type: 'string', useNull: true}, // Комментарий
        {name: 'is_frame', type: 'bool', useNull: true}, // рамочный
        {name: 'prolongation', type: 'bool', useNull: true}, // с пролонгацией
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }},
        {name: 'contractor', type: 'string', useNull: true},
        {name: 'currency', type: 'string', useNull: true},
        {name: 'direction_sales', type: 'string', useNull: true},
        {name: 'company', type: 'string', useNull: true},
        {name: 'type_title', type: 'string', useNull: true},
        {name: 'file_title', type: 'string', useNull: true},
        {name: 'file_name', type: 'string', useNull: true},
        {name: 'file_link', convert: function (val, rec) {
            return rec.get('file_id') ? ('<a href="/files/' + rec.get('file_name') + '" target="_blank">' + rec.get('file_title') + '</a>') : null;
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/d_contractor_contracts',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});


