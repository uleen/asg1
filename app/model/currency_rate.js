Ext.define('app.model.currency_rate', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'cdate', type: 'string'},
        {name: 'rate', type: 'string'},
        {name: 'nom', type: 'string'}
    ]
});