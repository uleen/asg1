Ext.define('app.model.ic_search', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'cagent_id', type: 'int'},
        {name: 'root_id', type: 'int'},
        {name: 'contact_personal_id', type: 'int'},
        {name: 'cagent_title', type: 'string'},
        {name: 'cagent_inn', type: 'int'},
        {name: 'cagent_personal', type: 'string'},
        {name: 'is_cagent', type: 'int'},
        {name: 'cagent_phone', type: 'string'},
        {name: 'cagent_add_phone', type: 'string'},
        {name: 'cagent_email', type: 'string'},
        {name: 'contractor_id', type:'int'},
        {name: 'org_id', type: 'int'},
        {name: 'manager_id', type: 'int'},
        {name: 'manager_name', type: 'string'},
        {name: 'is_executed', type: 'int'}
    ],
});