Ext.define('app.model.nomenclature_supplier', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'create_date', type: 'date', dateFormat: 'd.m.Y H:i', useNull: true},

        {name: 'nomenclature_id', type: 'int', useNull: true},
        {name: 'nomenclature_article', type: 'string', useNull: true},
        {name: 'nomenclature_title', type: 'string', useNull: true},
        {name: 'nomenclature_type', type: 'string', useNull: true},
        {name: 'nomenclature_group_title', type: 'string', useNull: true},
        {name: 'nomenclature_characteristics', type: 'string', useNull: true}, // конкатенированные характеристики
        {name: 'nomenclature_unit_id', type: 'int', useNull: true}, // ID ед. изм.
        {name: 'nomenclature_unit_title', type: 'string', useNull: true}, // Название ед. изм.
        {name: 'nomenclature_shipping_unit_id', type: 'int', useNull: true}, // ID ед. отгр.
        {name: 'nomenclature_shipping_unit_title', type: 'string', useNull: true}, // Название ед. отгр.
        {name: 'nomenclature_shipping_min', type: 'int', useNull: true}, // Мин. отгрузка

        {name: 'contractor_delivery_id', type: 'int', useNull: true},
        {name: 'contractor_delivery_title', type: 'string', useNull: true},
        {name: 'contractor_delivery_address', type: 'string', useNull: true},
        {name: 'contractor_title', type: 'string', useNull: true},
        {name: 'contractor_delivery_lat', type: 'float', useNull: true},
        {name: 'contractor_delivery_lng', type: 'float', useNull: true},

        {name: 'price', type: 'double', useNull: true},
        {name: 'currency_id', type: 'int', useNull: true},
        {name: 'currency_code', type: 'string', useNull: true},
        {name: 'currency_rate', type: 'float', useNull: true},

        {name: 'our_price', type: 'double', useNull: true}, // наша цена
        {name: 'our_currency_id', type: 'int', useNull: true}, // наша валюта
        {name: 'our_currency_code', type: 'string', useNull: true},
        {name: 'our_currency_rate', type: 'float', useNull: true},

        {name: 'unit_id', type: 'int', useNull: true},
        {name: 'unit_title', type: 'string', useNull: true},
        {name: 'user_id', type: 'int', useNull: true},
        {name: 'user_title', type: 'string', useNull: true},
        {name: 'article', type: 'string', useNull: true},
        {name: 'in_stock', type: 'bool'},
        {name: 'note', type: 'string', useNull: true},

        {name: 'distance', type: 'string', useNull: true}
    ]
});