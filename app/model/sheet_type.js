Ext.define('app.model.sheet_type', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true}, // Наименование
        {name: 'assignment', type: 'string', useNull: true},  // Назначение
        {name: 'export_1c', type: 'bool'} // Выгружать в 1С
    ]
});