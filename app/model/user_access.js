Ext.define('app.model.user_access', {
    extend: 'Ext.data.Model',
    alias: 'UserAccess',
    fields: [
             {name: 'obj_text',  type: 'string'},
             {name: 'access_id', type: 'int'},
         ],
    /*
    proxy: {
        type: 'rest',
        url: '/spec/d_user',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
        },
        
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
        }
        
    }
    */
});