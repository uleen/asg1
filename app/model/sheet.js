Ext.define('app.model.sheet', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true}, // дата создания
        {name: 'create_user_id', type: 'int', useNull: true},  // ID пользователя, который создал заказ
        {name: 'create_user_name', type: 'string', useNull: true},  // имя пользователя, который создал заказ
        {name: 'contractor_id', type: 'int', useNull: true},  // ID контрагента
        {name: 'contractor_title', type: 'string', useNull: true}, // Название контрагента
        {name: 'contractor_inn', type: 'string', useNull: true}, // ИНН контрагента
        {name: 'contractor_contract_id', type: 'int', useNull: true}, // ID договора
        {name: 'contractor_contract_title', type: 'string', useNull: true}, // Название договора
        {name: 'client_order_id', type: 'int', useNull: true}, // ID клиентского заказа
        {name: 'client_order_date', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true}, // Дата клиентского заказа
        {name: 'sheet_type_id', type: 'int', useNull: true}, // ID типа счета
        {name: 'sheet_type_title', type: 'string', useNull: true}, // Название типа счета
        {name: 'sheet_status_id', type: 'int', useNull: true}, // ID статуса счета
        {name: 'sheet_status_title', type: 'string', useNull: true}, // Название статуса счета
        {name: 'note', type: 'string', useNull: true}, // Комментарий к счету
        {name: 'company_id', type: 'int', useNull: true}, // ID контрагента
        {name: 'company_title', type: 'string', useNull: true}, // Название контрагента
        {name: 'company_requisite_id', type: 'int', useNull: true}, // ID договора контрагента
        {name: 'company_requisite_title', type: 'string', useNull: true}, // Название договора контрагента
        {name: 'currency_id', type: 'int', useNull: true}, // ID валюты счета
        {name: 'currency_code', type: 'string', useNull: true}, // Код валюты счета
        {name: 'sheet_sum', type: 'double', useNull: true}, // Общая сумма по счету
        {name: 'sheet_sum_nds', type: 'double', useNull: true}, // Общая сумма НДС по счету
        {name: 'contractor_title_inn', convert: function (value, record) {
            return record.get('contractor_title') + (record.get('contractor_inn') ? ', ' + record.get('contractor_inn') : '');
        }},
        {name: 'client_order_title', convert: function (value, record) {
            return '№' + record.get('client_order_id') + ' от ' + Ext.Date.format(record.get('client_order_date'), 'd.m.Y');
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/sheet',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});