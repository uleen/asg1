Ext.define('app.model.nomenclature_characteristic', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'nomenclature_id', type: 'int'},
        {name: 'characteristic_id', type: 'int'},
        {name: 'title', type: 'string', useNull: true},
        {name: 'title_short', type: 'string', useNull: true},
        {name: 'unit', type: 'string', useNull: true},
        {name: 'value', type: 'string', useNull: true},
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/characteristic',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});


