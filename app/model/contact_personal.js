Ext.define('app.model.contact_personal', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'contact_personal_id', type:'int'},
        {name: 'main_personal_id', type: 'int'},
        {name: 'fias_id', type: 'int', useNull: true},
        {name: 'lastname', type: 'string', useNull: true},
        {name: 'firstname', type: 'string', useNull: true},
        {name: 'middlename', type: 'string', useNull: true},
        {name: 'fio', type: 'string', useNull: true},
        {name: 'org_id', type: 'int'},
        {name: 'contractor_id', type: 'int'},
        {name: 'phone', type: 'string', useNull: true},
        {name: 'email', type: 'string', useNull: true},
        {name: 'status', type: 'int'},
        {name: 'cp_status', type: 'int'},
        {name: 'dateofbirth', type: 'date',  dateFormat: 'd.m.Y', useNull: true}, // дата рождения
        {name: 'is_send', type: 'int'}, // признак  "включать в рассылку или нет"
        {name: 'is_fired', type: 'int'}, // признак "уволен" - не используется
        {name: 'address', type:'string'}, // адрес
        {name: 'cagent_title', type: 'string'}, // название контрагента
        {name: 'cagent_id', type:'int'}, // ID контаргента
        {name: 'cagent_phone', type:'string',useNull: true}, // ID контаргента
        {name: 'cagent_add_phone', type:'string',useNull: true}, // ID контаргента
        {name: 'cagent_email', type:'string',useNull: true}, // ID контаргента 
        {name: 'position', type: 'string'}, // должность
        {name: 'na', type: 'int'},
        
        {name: 'status', type: 'int', defaultValue:1}, // статус конкретного контакта
        {
        	name: 'name',
            convert: function (value, record) {
                return record.get('fio');
            }
        },
        {
            name: 'na',
            convert: function (value, record) {
                return record.get('cp_status') == 1 ? 0 : 1;
            } 
        }
        
    ],
    validations: [

    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_contact_personal',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    },
    //hasMany: {model: 'app.model.contact_phone', name: 'contact_phone', foreignKey: 'contact_personal_id'}
    hasMany: {model: 'app.model.personal_contacts_data', name: 'personal_contacts_data', foreignKey: 'contact_personal_id'}
});
