Ext.define('app.model.fias', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'title', type: 'string'},

    ],
    proxy: {
        type: 'rest',
        url: '/dictionary/fias'
    }
});

