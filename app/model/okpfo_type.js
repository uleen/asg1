Ext.define('app.model.okpfo_type', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'title', type: 'string'},
        {name: 'full_title', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
        {type: 'presence', field: 'title', message: 'Вы не заполнили поле "Заголовок"'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_okpfo_type'
    }
});

