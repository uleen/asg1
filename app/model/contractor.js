Ext.define('app.model.contractor', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'company_title', type: 'string', useNull: true},
        {name: 'company_fias_title', type: 'string', useNull: true},
        {name: 'company_fias_id', type: 'int', useNull: true},
        {name: 'reference_scope', type: 'string', useNull: true},
        {name: 'reference_scope_id', type: 'int', useNull: true},
        {name: 'company_type', type: 'int', useNull: true},
        {name: 'org_id', type: 'int'},
        {name: 'main_personal_id', type: 'int'}
    ],
    validations: [
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_contractor'
    }
});
