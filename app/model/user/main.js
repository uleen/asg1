Ext.define('app.model.user.main', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true}, // ID
        {name: 'name', type: 'string', useNull: true}, // ФИО сотрудника',
        {name: 'login', type: 'string', useNull: true}, // Логин',
        {name: 'password', type: 'string', useNull: true}, // Пароль',
        {name: 'status', type: 'int', useNull: true}, // 1-активен, 2-неактиване',
        {name: 'company_id', type: 'int', useNull: true}, // ID компании
        {name: 'company_title', type: 'string', useNull: true}, // Название компании
        {name: 'company_unit_id', type: 'int', useNull: true}, // ID подразделения компании
        {name: 'company_unit_title', type: 'string', useNull: true}, // Название подразделения компании
        {name: 'position_id', type: 'int', useNull: true}, // ID должности
        {name: 'position_title', type: 'string', useNull: true}, // Название должности
        {name: 'date_birth', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата рождения
        {name: 'date_hire', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата приема на работу
        {name: 'date_dismissal', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата увольнения
        {name: 'phone_work', type: 'string', useNull: true}, // Телефон рабочий
        {name: 'phone_work_add', type: 'string', useNull: true}, // Телефон рабочий доб.
        {name: 'phone_mobile', type: 'string', useNull: true}, // Телефон мобильный
        {name: 'phone_home', type: 'string', useNull: true}, // Телефон домашний
        {name: 'email_personal', type: 'string', useNull: true}, // Email личный
        {name: 'email_work', type: 'string', useNull: true}, // Email рабочий
        {name: 'import_1c', type: 'bool'}, // Импортирован из 1С
        {name: 'can_auth', type: 'bool'}, // Может авторизоваться
    ],
    proxy: {
        type: 'rest',
        url: '/user',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});