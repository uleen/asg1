Ext.define('app.model.position', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'name',  type: 'string' },
        { name: 'position', type: 'int'},
        { name: 'winWidth',  type: 'int'}
    ]
});
