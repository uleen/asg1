Ext.define('app.model.client_order', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true}, // дата создания
        {name: 'create_user_id', type: 'int', useNull: true},  // ID пользователя, который создал заказ
        {name: 'create_user_name', type: 'string', useNull: true},  // имя пользователя, который создал заказ
        {name: 'contractor_id', type: 'int', useNull: true},  // ID контрагента
        {name: 'contractor_title', type: 'string', useNull: true}, // Название контрагента
        {name: 'contractor_inn', type: 'string', useNull: true}, // ИНН контрагента
        {name: 'contractor_contract_id', type: 'int', useNull: true}, // ID договора
        {name: 'contractor_contract_title', type: 'string', useNull: true}, // Название договора
        {name: 'contact_personal_id', type: 'int', useNull: true}, // ID контактного лица
        {name: 'contact_personal_fio', type: 'string', useNull: true}, // ФИО контактного лица
        {name: 'incoming_call_id', type: 'int', useNull: true}, // ID вх. запроса
        {name: 'incoming_call', type: 'string', useNull: true}, // Название вх. запроса
        {name: 'contractor_delivery_id', type: 'int', useNull: true}, // ID точки доставки
        {name: 'contractor_delivery_address', type: 'string', useNull: true}, // Адрес точки доставки
        {name: 'contractor_delivery_lat', type: 'float', useNull: true}, // Lat точки доставки
        {name: 'contractor_delivery_lng', type: 'float', useNull: true}, // Lng точки доставки
        {name: 'client_order_status_id', type: 'int', useNull: true}, // ID статуса заказа
        {name: 'client_order_status_title', type: 'string', useNull: true}, // Название статуса заказа
        {name: 'client_order_status_note', type: 'string', useNull: true}, // Комментарий к изменению статуса
        {name: 'date_from', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата доставки с
        {name: 'date_to', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата доставки по
        {name: 'time_from', type: 'string', useNull: true}, // Время доставки с
        {name: 'time_to', type: 'string', useNull: true}, // Время доставки по
        {name: 'note', type: 'string', useNull: true}, // Комментарий к заказу
        {name: 'type_price_id', type: 'int', useNull: true}, // ID типа цены
        {name: 'prepayment', type: 'bool'}, // Предоплата
        {name: 'currency_id', type: 'int', useNull: true}, // ID валюты расчетов
        {name: 'currency_code', type: 'string', useNull: true}, // Код валюты расчетов
        {name: 'sheet_id', type: 'int', useNull: true}, // ID выставленного счета
        {name: 'sheet_date', type: 'date', dateFormat: 'd.m.Y', useNull: true}, // Дата выставленного счета
        {name: 'sheet_title', convert: function (value, record) {
            return '№ ' + record.get('sheet_id') + ' от ' + Ext.Date.format(record.get('sheet_date'), 'd.m.Y');
        }},
        {name: 'contractor_title_inn', convert: function (value, record) {
            return record.get('contractor_title') + (record.get('contractor_inn') ? ', ' + record.get('contractor_inn') : '');
        }}
    ],
    validations: [
    ],
    proxy: {
        type: 'rest',
        url: '/client_order',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});