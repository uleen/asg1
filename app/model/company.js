Ext.define('app.model.company', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'title', type: 'string', useNull: true}, // название организации
        {name: 'title_short', type: 'string', useNull: true}, // краткое название организации
        {name: 'prefix', type: 'string', useNull: true}, // префикс
        {name: 'type', type: 'int', useNull: true}, // тип (1 - юр.лицо, 2 - физ.лицо, 3 - ИП)
        {name: 'okpfo_type_id', type: 'int', useNull: true}, // ID ОПФ
        {name: 'okpfo_type', type: 'string', useNull: true}, // ОПФ
        {name: 'inn', type: 'int', useNull: true}, // ИНН
        {name: 'kpp', type: 'int', useNull: true}, // КПП
        {name: 'ogrn', type: 'int', useNull: true}, // ОГРН
        {name: 'okpo', type: 'int', useNull: true}, // ОКПО
        {name: 'status', type: 'int'},
        {name: 'org_id', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }},
        {name: 'ur_address_text', type: 'string', useNull: true}, // Юридический адрес
        {name: 'ur_address_fias', type: 'string', useNull: true}, // Юридический адрес
        {name: 'ur_address_json', type: 'string', useNull: true}, // Юридический адрес
        {name: 'post_address_text', type: 'string', useNull: true}, // Почтовый адрес
        {name: 'post_address_fias', type: 'string', useNull: true}, // Почтовый адрес
        {name: 'post_address_json', type: 'string', useNull: true}, // Почтовый адрес
        {name: 'fact_address_text', type: 'string', useNull: true}, // Фактический адрес
        {name: 'fact_address_fias', type: 'string', useNull: true}, // Фактический адрес
        {name: 'fact_address_json', type: 'string', useNull: true}, // Фактический адрес
        {name: 'phone', type: 'string', useNull: true}, // Телефон
        {name: 'fax', type: 'string', useNull: true}, // Факс
        {name: 'email', type: 'string', useNull: true}, // E-mail
        {name: 'url', type: 'string', useNull: true}, // Сайт
        {name: 'advertising_text', type: 'string', useNull: true}, // Рекламный текст
        {name: 'logo_file_id', type: 'int', useNull: true}, // ID файла с логотипом компании
        {name: 'file_title', type: 'string', useNull: true}, // пользовательское название файла
        {name: 'file_name', type: 'string', useNull: true}, // имя файла
        {name: 'file_link', convert: function (val, rec) { // ссылка на загрузку файла
            return rec.get('logo_file_id') ? ('<a href="/files/' + rec.get('file_name') + '" target="_blank">' + rec.get('file_title') + '</a>') : null;
        }}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_company',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});
