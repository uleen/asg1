Ext.define('app.model.reference_scope', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'status', type: 'int', useNull: true},
        {name: 'title', type: 'string', useNull: true},
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
        {type: 'presence', field: 'title', message: 'Вы не заполнили поле "Заголовок"'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_reference_scope',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    },
    hasMany: {
        model: 'app.model.okved',
        name: 'okved',
        foreignKey: 'reference_scope_id'
    }
});
