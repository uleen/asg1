Ext.define('app.model.vehicle.equipment', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/vehicle/equipment'
    }
});