Ext.define('app.model.vehicle.driver', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'},
    ],
    proxy: {
        type: 'rest',
        url: '/vehicle/driver'
    }
});