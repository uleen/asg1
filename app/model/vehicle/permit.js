Ext.define('app.model.vehicle.permit', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/vehicle/permit'
    }
});