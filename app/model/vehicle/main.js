Ext.define('app.model.vehicle.main', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
		{name: 'reg_number', type: 'string'},
		{name: 'vehicle_model_id', type: 'int'},
		{name: 'vehicle_model_title', type: 'string'},
		{name: 'vehicle_status_id', type: 'int'},
		{name: 'vin', type: 'string'},
		{name: 'year', type: 'int'},
		{name: 'buy_date', type: 'date'},
		{name: 'buy_km', type: 'string'},
		{name: 'buy_price', type: 'float'},
		{name: 'sell_date', type: 'date'},
		{name: 'sell_km', type: 'string'},
		{name: 'sell_price', type: 'float'},
		{name: 'pts_series', type: 'string'},
		{name: 'pts_number', type: 'string'},
		{name: 'pts_date', type: 'date'},
		{name: 'note', type: 'string'}
    ],
    proxy: {
        type: 'rest',
        url: '/vehicle',
		successProperty: 'success',
		reader: {
			type: 'json',
			root: 'item'
		}
    }
});