Ext.define('app.model.characteristic', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'nomenclature_group_id', type: 'int'},
        {name: 'title', type: 'string'},
        {name: 'title_short', type: 'string'},
        {name: 'unit', type: 'string'},
        {name: 'sort', type: 'int'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_characteristic'
    }
});
