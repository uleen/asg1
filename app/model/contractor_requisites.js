Ext.define('app.model.contractor_requisites', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'contractor_id', type: 'int'}, // ID контрагента
        {name: 'contractor', type: 'string'}, // контрагент
        {name: 'title', type: 'string', useNull: true}, // название счета
        {name: 'number', type: 'string', useNull: true}, // номер счета
        {name: 'bank_title', type: 'string', useNull: true}, // название банка
        {name: 'bank_bik', type: 'string', useNull: true}, // бик банка
        {name: 'bank_ks', type: 'string', useNull: true}, // кс банка
        {name: 'currency_id', type: 'int', useNull: true}, // id валюты
        {name: 'currency', type: 'string', useNull: true}, // название валюты
        {name: 'bank2_title', type: 'string', useNull: true}, // название банка (для непрямых расчетов)
        {name: 'bank2_bik', type: 'string', useNull: true}, // бик банка (для непрямых расчетов)
        {name: 'bank2_ks', type: 'string', useNull: true}, // кс банка (для непрямых расчетов)
        {name: 'payer', type: 'string', useNull: true}, // плательщик
        {name: 'type', type: 'int', useNull: true}, // тип
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    validations: [
        {field: 'title', type: 'presence', message: 'Поле "Наименование" обязательно для заполнения'},
        {field: 'number', type: 'presence', message: 'Поле "Номер счета" обязательно для заполнения'},
        {field: 'bank_title', type: 'presence', message: 'Поле "Банк" обязательно для заполнения'},
        {field: 'bank_bik', type: 'presence', message: 'Поле "БИК" обязательно для заполнения'},
        {field: 'currency_id', type: 'presence', message: 'Поле "Валюта" обязательно для заполнения'},
        {field: 'payer', type: 'presence', message: 'Поле "Плательщик" обязательно для заполнения'},
        {field: 'type', type: 'presence', message: 'Поле "Вид счета" обязательно для заполнения'},
    ],
    proxy: {
        type: 'rest',
        url: '/d_contractor_requisites',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});