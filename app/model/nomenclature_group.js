Ext.define('app.model.nomenclature_group', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'pid', type: 'int', useNull: true},
        {name: 'title', type: 'string'},
        {name: 'status', type: 'int'},
        {name: 'na', convert: function (value, record) {
            return record.get('status') == 1 ? 0 : 1;
        }}
    ],
    associations: [
        {
            type: 'hasMany',
            model: 'app.model.characteristic',
            name: 'characteristics',
            foreignKey: 'nomenclature_group_id'
        }
    ],
    validations: [
        {type: 'presence', field: 'title', message: 'Вы не заполнили поле "Заголовок"'}
    ],
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature_group',
        successProperty: 'success',
        reader: {
            type: 'json',
            root: 'item'
        }
    }
});

