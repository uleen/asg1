Ext.define('app.controller.contractor_requisites', {
    extend: 'Ext.app.Controller',
    models: ['contractor_requisites', 'currency'],
    stores: ['contractor_requisites', 'currency'],
    views: ['form.contractor_requisites', 'win.contractor_requisites', 'grid.contractor_requisites'],

    init: function () {
        this.control({
            '[action=menuContractorRequisites]': {
                click: this.openList
            },
            'gridContractorRequisites': {
                itemdblclick: function (grid, record) {
                    this.grid = grid;
                    this.open(record.get('id'));
                }
            },
            'gridContractorRequisites [action=add]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.cagent = btn.up('window').cagent;
                    this.open(0);
                }
            },
            'formContractorRequisites [action=save]': {
                click: this.saveRecord
            }
        });
    },

    // форма
    open: function (id) {
        var win = Ext.widget('formContractorRequisites');
        var cagent = this.cagent;
        if (id) {
            Ext.ModelManager.getModel('app.model.contractor_requisites').load(id, {
                success: function (rec) {
                    win.down('form').loadRecord(rec);
                }
            });
        } else {
            win.down('form').getForm().setValues({
                contractor_id: cagent.id,
                contractor: cagent.title,
                payer: cagent.opf_title_full
            });
        }
        win.show();
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.contractor_requisites', valRecord);
        } else {
            record.set(valRecord);
        }
        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record) {
                    win.close();
                    grid.getStore().reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // общий список
    openList: function () {
        var win = Ext.widget('winContractorRequisites');
        var grid = win.down('grid');
        grid.getDockedComponent('for-all').show();
        var store = grid.getStore();
        store.getProxy().extraParams.status = 1;
        store.load();
        win.show();
    }

});


