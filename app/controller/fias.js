Ext.define('app.controller.fias', {
    extend: 'Ext.app.Controller',
    models: ['fias'],
    stores: ['fias'],
    views: ['win.fias'],

    init: function () {
        this.control({
            '[action=menuFias]': {click: this.clickMenuFias}
        });
    },

    // меню - справочник ОКВЭД
    clickMenuFias: function () {
        Ext.create('widget.winFias');
    }

});


