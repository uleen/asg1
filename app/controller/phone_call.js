Ext.define('app.controller.phone_call', {
    extend: 'Ext.app.Controller',
    views: ['form.phone_call'],

    init: function () {
        this.control({
            '[action=menuPhoneCall]': {click: this.clickMenuPhoneCall},
            '[action=openIncomingCall]': {click: this.openIncomingCall},
            'formPhoneCall textfield': {
                specialkey: function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        var button = field.up('window').down('button');
                        this.openIncomingCall(button);
                    }
                }
            }
        });
    },

    // имитация вх звонка
    clickMenuPhoneCall: function () {
        Ext.create('widget.formPhoneCall');
    },

    // открытие формы вх запроса
    openIncomingCall: function (button) {
        var win = button.up('window');
        var cagent_phone = win.down('form').getValues().cagent_phone;
        win.close();

        // открытие окна
        var win = Ext.create('widget.formIncomingCall', {closable: false});
        win.down('form').getForm().setValues({
            call_date: Ext.Date.format(new Date(), 'd.m.Y H:i'),
            cagent_phone: cagent_phone,
            incoming_source_id: 'Телефон'
        });
        win.show();

        // поиск по контактным лицам
        Ext.Ajax.request({
            url: '/spec/d_contact_personal/search',
            method: 'GET',
            params: {
                phone: cagent_phone
            },
            success: function (response) {
                if (response.responseText) {
                    var cp = Ext.create('app.model.contact_personal', JSON.parse(response.responseText));
                    win.down('form').getForm().cagent = cp;
                    win.down('form').getForm().setValues({
                        cagent_id: cp.get('id'),
                        cagent_name: cp.get('name'),
                        cagent_email: cp.get('email')
                    });
                }
            }
        });
    }
});
