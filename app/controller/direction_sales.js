Ext.define('app.controller.direction_sales', {
    extend: 'Ext.app.Controller',
    models: ['direction_sales'],
    stores: ['direction_sales'],
    views: ['win.direction_sales', 'grid.direction_sales', 'form.direction_sales'],

    init: function () {
        this.control({
            'gridDirectionSales actioncolumn[action=edit]': {
                click: this.openFormEdit
            },
            'winDirectionSales [action=create]': {
                click: this.openFormAdd
            },
            'formDirectionSales [action=save]': {
                click: this.saveRecord
            },
            'winDirectionSales [action=filter_status]':{
                change: this.filterStatus
            }
        });
    },

    // открытие формы дял добавления
    openFormAdd: function (grid, record) {
        var view = Ext.widget('formDirectionSales');
    },

    // открытие формы для редактирования
    openFormEdit: function (view, cell, row) {
        var record = view.getStore().getAt(row);
        var view = Ext.widget('formDirectionSales');
        view.down('form').loadRecord(record);
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.direction_sales', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record, operation) {
                    win.close();
                    var store = Ext.StoreManager.get('direction_sales');
                    store.reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
//            store.add(record);
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // фильтр по статусу
    filterStatus: function (view, val) {
        var store = Ext.StoreManager.get('direction_sales');
        store.getProxy().extraParams = {
            status: val
        };
        store.load();
    }

});


