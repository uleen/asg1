Ext.define('app.controller.cagent', {
    extend: 'Ext.app.Controller',
    models: ['cagent'],
    stores: ['cagent', 'cagent_client', 'cagent_supplier'],
    views: [
        'panel.cagent',
        'win.cagent',
        'form.cagent.main',
        'form.cagent.contact',
        'form.cagent.contract',
        'form.cagent.incoming_call',
        'grid.cagent',
        'grid.contractor_nomenclature',
        'menu.cagent_client',
        'menu.cagent_supplier'
    ],

    init: function () {
        this.control({
            '[action=btnCagent]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.open();
                }
            },
            'winCagent treepanel': {
                itemclick: this.changeForm
            },
            'winCagent [action=save]': {
                click: this.saveRecord
            },
            '[action=menuListCagent]': {
                click: this.menuListCagent
            },
            'panelCagent [action=search]': {
                click: this.searchInPanel
            },
            'panelCagent [action=clear]': {
                click: this.clearSearchInPanel
            }
        });
    },


    // изменение формы
    changeForm: function (view, item) {

        if (!view.up('window').cagent) {
            alert('Необходимо сохранить контрагента!');
            return;
        }

        if (item.raw.formId) {
            var cagentForm = view.up('window').getComponent('cagent-form');
            Ext.each(cagentForm.items.items, function (el) {
                if (el.itemId == item.raw.formId) {
                    el.show();
                } else {
                    el.hide();
                }
            });
        }

        // реквизиты и договоры
        if (item.raw.formId == 'form-cagent-contract') {
            var grid = cagentForm.getComponent(item.raw.formId).getComponent('grid-contractor-requisites');
            grid.getDockedComponent('for-one').show();
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = view.up('window').cagent.id;
            store.load();

            var grid = cagentForm.getComponent(item.raw.formId).getComponent('grid-contractor-contracts');
            grid.getDockedComponent('for-one').show();
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = view.up('window').cagent.id;
            store.load();
        }

        // список входящих запросов
        if (item.raw.formId == 'form-cagent-incoming-call') {
            var grid = cagentForm.getComponent(item.raw.formId).getComponent('grid-cagent-incomming-call');
            var store = Ext.create('app.store.incoming_call');
            store.clearFilter();
            store.getProxy().extraParams = {contractor_id: view.up('window').cagent.id};
            store.load({
                callback: function () {
                    grid.columns[5].setVisible(false);
                }
            });
            grid.reconfigure(store);
            grid.getDockedItems('[itemid=paging-incoming-call]')[0].bindStore(store);
        }

        // Контактные лица
        if (item.raw.formId == 'form-cagent-contact') {
            var grid = cagentForm.getComponent(item.raw.formId).getComponent('grid-contact-personal');
            var store = grid.getStore();

            store.clearFilter();
            store.getProxy().extraParams = {
                contractor_id: view.up('window').cagent.id
            };
            grid.reconfigure(store);
            store.load({
                callback: function () {
                    grid.down('[dataIndex=cagent_title]').setVisible(false);
                    grid.columns[2].setVisible(false);
                }
            });
        }

        // точки доставки / загрузки
        if (item.raw.formId == 'grid-cagent-delivery') {
            var cagent = view.up('window').cagent;
            var grid = cagentForm.getComponent(item.raw.formId);
            grid.getHeader().setTitle(cagent.org_type == 1 ? 'Точки доставки' : 'Точки загрузки');
            grid.getDockedComponent('for-one').show();
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = cagent.id;
            store.load();
            grid.on('itemdblclick', function (view, record) {
                var cd = new app.controller.contractor_delivery;
                cd.grid = grid;
                cd.cagent = cagent;
                cd.open(record.get('id'));
            });
        }

        // номенклатура
        if (item.raw.formId == 'grid-contractor-nomenclature') {
            var grid = cagentForm.getComponent(item.raw.formId);
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = view.up('window').cagent.id;
            store.load();
        }

        // клиентские заказы
        if (item.raw.formId == 'grid-cagent-client-order') {
            var grid = cagentForm.getComponent(item.raw.formId);
            grid.getDockedComponent('top').hide();
            grid.down('[dataIndex=contractor_title]').setVisible(false);
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = view.up('window').cagent.id;
            store.load();

            grid.on('itemdblclick', function (view, rClientOrder) {
                var cClientOrder = new app.controller.client_order;
                cClientOrder.open(rClientOrder.get('id'));
            });
        }

        // выставленные счета
        if (item.raw.formId == 'grid-cagent-sheet') {
            var grid = cagentForm.getComponent(item.raw.formId);
            grid.getDockedComponent('top').hide();
            grid.down('[dataIndex=contractor_title]').setVisible(false);
            var store = grid.getStore();
            store.getProxy().extraParams.contractor_id = view.up('window').cagent.id;
            store.load();

            grid.on('itemdblclick', function (view, rSheet) {
                var cSheet = new app.controller.sheet;
                cSheet.open(rSheet.get('id'));
            });
        }

    },


    // открытие формы для редактирования
    open: function (id) {
        var win = Ext.widget('winCagent');
        var form = win.down('form').getForm();

        Ext.StoreManager.get('okpfo_type').load();
        Ext.StoreManager.get('customer_category').load();

        if (id) {
            Ext.ModelManager.getModel('app.model.cagent').load(id, {
                success: function (rec) {
                    form.loadRecord(rec);
                    form.setValues({
                        create_date: Ext.Date.format(rec.get('create_date'), 'd.m.Y H:i')
                    });
                    win.cagent = rec.data;
                    win.defaultData = win.down('form').getForm().getValues();
                    win.setTitle(win.title + ' "' + win.cagent.title + '"');

                    if (rec.get('org_type') == 1) {
                        win.getComponent('menu-for-client').show();
                        win.getComponent('menu-for-supplier').hide();
                    } else {
                        form.findField('reclama').hide();
                    }
                    win.show();
                }
            });
        } else {
            var record = Ext.create('app.model.cagent');
            form.loadRecord(record);
            form.defaultData = form.getValues();
            form.findField('reclama').hide();
            win.show();
        }

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            var data = this.down('form').getForm().getValues();
            if (JSON.stringify(data) == JSON.stringify(this.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var form = win.getComponent('cagent-form').getComponent('form-cagent-main');
                            var toolbar = form.getDockedItems('toolbar[dock=bottom]')[0];
                            var button = toolbar.getComponent('btn-save');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },


    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form').getForm();
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.is_blocked = valRecord.is_blocked == 1 ? 1 : 2;
        record.set(valRecord);

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    win.confirmClosed = false;
                    grid.getStore().reload();
                    win.cagent = record.data;
                    form.loadRecord(record);
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.markInvalid(errors);
                    if ('inn_alert' in errors) {
                        Ext.Msg.alert('Внимание!', errors.inn_alert);
                    }
                }
            });
        } else {
            var errors = record.validate();
            form.markInvalid(errors);
        }
    },

    // загрузка панели контрагентов
    menuListCagent: function () {
        var self = this;
        var mainPanel = Ext.getCmp('main-panel');
        mainPanel.removeAll();

        var panelCagent = Ext.create('widget.panelCagent');
        var grid = panelCagent.down('grid');
        var store = grid.getStore();
        store.clearFilter();
        store.getProxy().extraParams = {};
        store.load();

        grid.on('itemdblclick', function (grid, record) {
            self.grid = grid;
            self.open(record.get('id'));
        });

        mainPanel.add(panelCagent);
        mainPanel.doLayout();
    },


    // расширеный поиск в панели
    searchInPanel: function () {
        var form = Ext.getCmp('cagent-search-in-panel').getForm();
        var grid = Ext.getCmp('cagent-search-in-panel').up('panel').getComponent('grid');
        var store = grid.getStore();
        store.getProxy().extraParams = form.getValues();
        store.load();
    },

    // очистка формы поиска в панели
    clearSearchInPanel: function () {
        Ext.getCmp('cagent-search-in-panel').getForm().reset();
        this.searchInPanel();
    }

});

// функция проверки ИНН
function checkINN(inputNumber) {
    inputNumber = "" + inputNumber;
    inputNumber = inputNumber.split('');
    if ((inputNumber.length == 10) && (inputNumber[9] == ((2 * inputNumber[  0] + 4 * inputNumber[1] + 10 * inputNumber[2] + 3 * inputNumber[3] + 5 * inputNumber[4] + 9 * inputNumber[5] + 4 * inputNumber[6] + 6 * inputNumber[7] + 8 * inputNumber[8]) % 11) % 10)) {
        return true;
    } else if ((inputNumber.length == 12) && ((inputNumber[10] == ((7 * inputNumber[ 0] + 2 * inputNumber[1] + 4 * inputNumber[2] + 10 * inputNumber[3] + 3 * inputNumber[4] + 5 * inputNumber[5] + 9 * inputNumber[6] + 4 * inputNumber[7] + 6 * inputNumber[8] + 8 * inputNumber[9]) % 11) % 10) && (inputNumber[11] == ((3 * inputNumber[ 0] + 7 * inputNumber[1] + 2 * inputNumber[2] + 4 * inputNumber[3] + 10 * inputNumber[4] + 3 * inputNumber[5] + 5 * inputNumber[6] + 9 * inputNumber[7] + 4 * inputNumber[8] + 6 * inputNumber[9] + 8 * inputNumber[10]) % 11) % 10))) {
        return true;
    } else {
        return false;
    }
}

