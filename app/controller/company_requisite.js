Ext.define('app.controller.company_requisite', {
    extend: 'Ext.app.Controller',
    models: ['company_requisite'],
    views: [
        'form.company.requisites',
        'form.company.requisite'
    ],

    init: function () {
        this.control({
            'formCompany [action=add_requisite]': {
                click: function (button) {
                    var win = button.up('window');
                    this.grid = button.up('grid');
                    this.open(null, {
                        company_id: win.company.get('id')
                    });
                }
            },
            '[action=company_requisite_save]': {
                click: function (button) {
                    var win = button.up('window');
                    this.saveRecord(win);
                }
            }
        });
    },


    // открытие формы для редактирования
    open: function (id, data) {
        var win = Ext.widget('form.company.requisite');
        var form = win.down('form').getForm();
        win.grid = this.grid;

        form.findField('currency_id').store.load();

        if (id) {
            Ext.ModelManager.getModel('app.model.company_requisite').load(id, {
                success: function (record) {
                    form.loadRecord(record);
                    win.requisite = record;

                    form.defaultData = form.getValues();
                    win.show();
                }
            });
        } else {
            data = typeof data !== 'undefined' ? data : {};
            data.status = 1;
            var record = Ext.create('app.model.company_requisite', data);
            form.loadRecord(record);
            win.requisite = record;
            form.defaultData = form.getValues();
            win.show();
        }

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            if (JSON.stringify(form.getValues()) == JSON.stringify(form.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = win.down('form').getDockedItems('[dock=bottom]')[0].getComponent('btn-save');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },


    // сохранение измененных данных
    saveRecord: function (win) {
        var form = win.down('form');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        record.set(valRecord);

        // сохранение объекта
        record.save({
            // объект сохранен на сервере
            success: function () {
                win.grid.getStore().reload();
                win.confirmClosed = false;
                win.close();
            },
            // объект не прошел валидацию на сервере
            failure: function (record, operation) {
                var errors = operation.request.scope.reader.jsonData["errors"];
                form.getForm().markInvalid(errors);
            }
        });
    }

});


