Ext.define('app.controller.nomenclature', {
    extend: 'Ext.app.Controller',
    models: ['nomenclature', 'nomenclature_characteristic'],
    stores: ['nomenclature', 'nomenclature_characteristic'],
    views: [
        'panel.nomenclature',
        'panel.table_manager',
        'grid.nomenclature',
        'grid.table_manager',
        'win.nomenclature',
        'menu.nomenclature',
        'form.nomenclature',
        'grid.nomenclature_characteristics',
        'grid.nomenclature_price',
        'grid.nomenclature_price_history',
        'grid.nomenclature_supplier',
        'grid.nomenclature_supplier_history'
    ],

    init: function () {
        this.control({
            '[action=menuListNomenclature]': {
                click: this.menuListNomenclature
            },
            '[action=menuTableManager]': {
                click: this.menuTableManager
            },
            'panelNomenclature [itemId=tree], panelTableManager [itemId=tree]': {
                itemclick: function (view, record) {
                    this.selectedGroup = record.get('id');
                    var grid = view.up('panel').up('panel').getComponent('grid');
                    var store = grid.getStore();
                    store.getProxy().extraParams.nomenclature_group_id = this.selectedGroup;
                    store.load();
                }
            },
            '[action=btnAddNomenclature]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.open();
                }
            },
            'winNomenclature [action=save]': {
                click: this.saveRecord
            },
            'gridNomenclature': {
                itemdblclick: function (grid, record) {
                    this.grid = grid;
                    this.open(record.get('id'));
                }
            },
            'winNomenclature treepanel': {
                itemclick: function (view, item) {
                    var self = this;
                    var dirty = false;
                    var store = null;

                    Ext.each(view.up('window').getComponent('main').items.items, function (e) {
                        if (e.store && !e.hidden) {
                            e.store.each(function (r) {
                                if (r.dirty) {
                                    dirty = true;
                                    store = e.store;
                                }
                            });
                        }
                    });
                    // проверка изменений
                    if (dirty) {
                        Ext.Msg.show({
                            msg: 'Данные были изменены. Сохранить изменения?',
                            buttons: Ext.Msg.YESNOCANCEL,
                            fn: function (btn) {
                                if (btn == 'no') {
                                    self.changeForm(view, item);
                                }
                                if (btn == 'yes') {
                                    store.sync();
                                    self.changeForm(view, item);
                                }
                            }
                        });
                    } else {
                        self.changeForm(view, item);
                    }
                }
            },
            'panelNomenclature [action=search]': {
                click: this.searchInPanel
            },
            'panelNomenclature [action=clear]': {
                click: this.clearSearchInPanel
            },
            'gridContractorNomenclature actioncolumn[action=history], gridNomenclatureSupplier actioncolumn[action=history]': {
                click: function (grid, cell, row) {
                    var record = grid.getStore().getAt(row);
                    this.openHistory(record);
                }
            }
        });
    },


    // изменение формы (переключение вкладок)
    changeForm: function (view, item) {

        if (!view.up('window').nomenclature) {
            alert('Необходимо сохранить номенклатуру!');
            return;
        }

        
        if (item.raw.formId) {
            var mainForm = view.up('window').getComponent('main');
            Ext.each(mainForm.items.items, function (el) {
                if (el.itemId == item.raw.formId) {
                    el.show();
                } else {
                    el.hide();
                }
            });
        }

        // характеристики
        if (item.raw.formId == 'grid-nomenclature-characteristics') {
            var grid = mainForm.getComponent(item.raw.formId);
            var store = grid.getStore();
            store.getProxy().extraParams.nomenclature_id = view.up('window').nomenclature.id;
            store.load();
        }

        // цены
        if (item.raw.formId == 'grid-nomenclature-price') {
            var grid = mainForm.getComponent(item.raw.formId);
            var store = grid.getStore();
            store.getProxy().extraParams.nomenclature_id = view.up('window').nomenclature.id;
            store.load();
        }

        // поставщики
        if (item.raw.formId == 'grid-nomenclature-supplier') {
            var grid = mainForm.getComponent(item.raw.formId);
            var store = grid.getStore();
            store.getProxy().extraParams.nomenclature_id = view.up('window').nomenclature.id;
            store.load();
        }
    },


    // открытие формы для редактирования
    open: function (id) {
        var self = this;
        var win = Ext.widget('winNomenclature');
        var form = win.down('form').getForm();
        Ext.StoreManager.get('unit').load();

        if (id) {
            Ext.ModelManager.getModel('app.model.nomenclature').load(id, {
                success: function (record) {
                    record.set('nomenclature_group_full_title', self.getPath(record.get('nomenclature_group_id')));
                    form.loadRecord(record);
                    win.nomenclature = record.data;
                    win.setTitle(win.title + ' "' + win.nomenclature.title + '"');
                    form.defaultData = form.getValues();
                    win.show();
                }
            });
        } else {
            var record = Ext.create('app.model.nomenclature', {
                nomenclature_group_id: self.selectedGroup,
                nomenclature_group_full_title: self.getPath(self.selectedGroup),
                type: 'goods',
                status: 1
            });           
            form.loadRecord(record);
            form.defaultData = form.getValues();
            win.show();
        }

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            if (JSON.stringify(form.getValues()) == JSON.stringify(form.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = win.down('form').getDockedItems('[dock=bottom]')[0].getComponent('btn-save');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },


    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form').getForm();
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        record.set(valRecord);

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    win.confirmClosed = false;
                    grid.getStore().reload();
                    win.nomenclature = record.data;
                    form.loadRecord(record);
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.markInvalid(errors);
        }
    },


    // загрузка панели
    menuListNomenclature: function () {
        var mainPanel = Ext.getCmp('main-panel');
        var panelCagent = Ext.create('widget.panelNomenclature');
        this.selectedGroup = null;

        var store = panelCagent.getComponent('grid').getStore();
        store.getProxy().extraParams.nomenclature_group_id = 0;
        store.load();

        mainPanel.removeAll();
        mainPanel.add(panelCagent);
        mainPanel.doLayout();
    },


    // загрузка панели "каталог менеджера"
    menuTableManager: function () {
        var mainPanel = Ext.getCmp('main-panel');
        var panelCagent = Ext.create('widget.panelTableManager');
        var grid = panelCagent.getComponent('grid');

        grid.on('itemdblclick', function (grid, record) {
            var win = Ext.create('app.view.win.nomenclature_supplier_on_map');
            win.show();

            var map = new ymaps.Map(win.id + '-innerCt', {
                center: [55.76, 37.64],
                zoom: 10,
                behaviors: ['default', 'scrollZoom']
            });

            // загрузка точек
            var store = Ext.create('app.store.nomenclature_supplier');
            store.load({
                params: {nomenclature_id: record.get('nomenclature_id')},
                callback: function (records, operation, success) {
                    if (success) {
                        store.each(function (rec) {
                            var pm = new ymaps.Placemark([rec.get('contractor_delivery_lat'), rec.get('contractor_delivery_lng')], {
                                balloonContentHeader: rec.get('contractor_title'),
                                balloonContentBody: rec.get('contractor_delivery_address'),
                                balloonContentFooter: rec.get('note'),
                                iconContent: rec.get('price') + ' ' + rec.get('currency_code') + '/' + rec.get('unit_title')
                            }, {preset: rec.get('in_stock') ? 'twirl#darkgreenStretchyIcon' : 'twirl#greyStretchyIcon'});
                            map.geoObjects.add(pm);
                        });
                    } else {
                        alert('error');
                    }
                }
            });

            win.on('resize', function () {
                map.container.fitToViewport();
            });
        });

        var store = grid.getStore();
        store.getProxy().extraParams.nomenclature_group_id = 0;
        store.load();

        mainPanel.removeAll();
        mainPanel.add(panelCagent);
        mainPanel.doLayout();
    },


    // определение пути
    getPath: function (group_id) {
        if (group_id) {
            var path = {};
            Ext.Ajax.request({
                url: '/spec/d_nomenclature_group/path/' + group_id,
                async: false,
                success: function (result) {
                    path = JSON.parse(result.responseText);
                }
            });
            return path.title;
        } else {
            return null;
        }
    },


    // расширеный поиск в панели
    searchInPanel: function (btn) {
        var panel = btn.up('form').up('panel');
        var grid = panel.getComponent('grid');
        var tree = panel.getComponent('tree');
        var form = btn.up('form').getForm();
        var store = grid.getStore();
        var ng = tree.getSelectionModel().getSelection()[0];
        store.getProxy().extraParams = form.getValues();
        store.getProxy().extraParams.nomenclature_group_id = ng ? ng.get('id') : 0;
        store.reload();
    },

    // очистка формы поиска в панели
    clearSearchInPanel: function (btn) {
        btn.up('form').getForm().reset();
        this.searchInPanel(btn);
    },

    // окно истории изменения цены поставщика
    openHistory: function (record) {
        var win = Ext.create('app.view.win.nomenclature_supplier_history');
        var form = win.down('form').getForm();
        form.setValues({
            nomenclature_title: record.get('nomenclature_title'),
            contractor_title: record.get('contractor_title'),
            contractor_delivery_address: record.get('contractor_delivery_address')
        });
        var grid = win.down('grid');
        var store = grid.getStore();
        store.getProxy().extraParams = {
            nomenclature_id: record.get('nomenclature_id'),
            contractor_delivery_id: record.get('contractor_delivery_id')
        }
        store.load();
        win.show();
    }

});
