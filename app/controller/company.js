Ext.define('app.controller.company', {
    extend: 'Ext.app.Controller',
    models: ['company', 'company_person'],
    stores: ['company'],
    views: [
        'win.company',
        'grid.company',
        'form.company',
        'form.company.main',
        'form.company.contact',
        'form.company.requisites',
        'form.company.person',
        'form.company.logo',
        'menu.company'
    ],

    init: function () {
        this.control({
            'gridCompany actioncolumn[action=edit]': {
                click: function (view, cell, row) {
                    var record = view.getStore().getAt(row);
                    this.open(record.get('id'));
                }
            },
            'formCompany treepanel': {
                itemclick: this.changeForm
            },
            'winCompany [action=create]': {
                click: function () {
                    this.open();
                }
            },
            'formCompany [action=save]': {
                click: this.saveRecord
            },
            'formCompany [action=save_person]': {
                click: this.savePerson
            }
        });
    },


    // изменение формы
    changeForm: function (view, item) {
        var win = view.up('window');

        if (!win.company && false) {
            alert('Необходимо сохранить организацию!');
            return;
        }

        if (item.raw.formId) {
            var companyForm = view.up('window').getComponent('company-form');
            Ext.each(companyForm.items.items, function (el) {
                if (el.itemId == item.raw.formId) {
                    el.show();
                } else {
                    el.hide();
                }
            });
        }

        // контакты
        if (item.raw.formId == 'form-company-contact') {
            var form = companyForm.getComponent(item.raw.formId).getForm();
            form.loadRecord(win.company);
            form.defaultData = form.getValues();
        }

        // Банковские счета
        if (item.raw.formId == 'grid-company-requisites') {
            var grid = companyForm.getComponent(item.raw.formId);
            var store = grid.getStore();
            store.getProxy().extraParams.company_id = win.company.get('id');
            store.load();
            // редактирование счета
            grid.on('itemdblclick', function (view, record) {
                var cr = new app.controller.company_requisite;
                cr.grid = grid;
                cr.open(record.get('id'));
            });
        }

        // Руководитель
        if (item.raw.formId == 'form-company-chief') {
            var form = companyForm.getComponent(item.raw.formId).getForm();
            Ext.ModelManager.getModel('app.model.company_person').load(null, {
                params: {
                    company_id: win.company.get('id'),
                    type: 'chief'
                },
                success: function (record) {
                    form.loadRecord(record);
                    form.defaultData = form.getValues();
                    companyForm.getComponent(item.raw.formId).getComponent('file-upload').getComponent('file-title').update(record.get('file_link'));
                }
            });
        }

        // Бухгалтер
        if (item.raw.formId == 'form-company-booker') {
            var form = companyForm.getComponent(item.raw.formId).getForm();
            Ext.ModelManager.getModel('app.model.company_person').load(null, {
                params: {
                    company_id: win.company.get('id'),
                    type: 'booker'
                },
                success: function (record) {
                    form.loadRecord(record);
                    form.defaultData = form.getValues();
                    companyForm.getComponent(item.raw.formId).getComponent('file-upload').getComponent('file-title').update(record.get('file_link'));
                }
            });
        }

        // Логотип
        if (item.raw.formId == 'form-company-logo') {
            var form = companyForm.getComponent(item.raw.formId).getForm();
            form.loadRecord(win.company);
            form.defaultData = form.getValues();
            companyForm.getComponent(item.raw.formId).getComponent('file-upload').getComponent('file-title').update(win.company.get('file_link'));
        }

    },


    // открытие формы для редактирования
    open: function (id) {
        var win = Ext.widget('formCompany');
        var form = win.down('form').getForm();

        if (id) {
            Ext.ModelManager.getModel('app.model.company').load(id, {
                success: function (record) {
                    form.loadRecord(record);
                    win.company = record;
                    form.defaultData = form.getValues();
                    win.show();
                }
            });
        } else {
            var record = Ext.create('app.model.company', {
                status: 1
            });
            form.loadRecord(record);
            win.company = record;
            form.defaultData = form.getValues();
            win.show();
        }

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            if (JSON.stringify(form.getValues()) == JSON.stringify(form.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = win.down('form').getDockedItems('[dock=bottom]')[0].getComponent('btn-save');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },


    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = button.up('form');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        record.set(valRecord);

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record, operation) {
                    win.close();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },


    // сохранение данных ответственного лица
    savePerson: function (button) {
        var win = button.up('window');
        var form = button.up('form');
        var record = form.getRecord();
        record.set(form.getValues());
        // сохранение модели
        record.save({
            // объект сохранен на сервере
            success: function (record, operation) {
                win.close();
            },
            // объект не прошел валидацию на сервере
            failure: function (record, operation) {
                var errors = operation.request.scope.reader.jsonData["errors"];
                form.getForm().markInvalid(errors);
            }
        });
    },


    // фильтр по статусу
    filterStatus: function (view, val) {
        var store = Ext.StoreManager.get('company');
        store.getProxy().extraParams = {
            status: val
        };
        store.load();
    }

});


