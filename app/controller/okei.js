Ext.define('app.controller.okei', {
    extend: 'Ext.app.Controller',
    models: ['okei'],
    stores: ['okei'],
    views: ['win.okei'],

    init: function () {
        this.control({
            '[action=menuOKEI]': {
                click: function() {
                    var win = Ext.create('widget.winOKEI');
                    var store = Ext.StoreManager.get('okei');
                    store.load();
                    win.show();
                }
            }
        });
    }

});