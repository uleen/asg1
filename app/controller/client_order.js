Ext.define('app.controller.client_order', {
    extend: 'Ext.app.Controller',
    views: [
        'panel.client_order', 'grid.client_order', 'grid.client_order_nomenclature',
        'win.client_order_status_history', 'grid.client_order_status_history'
    ],

    init: function () {
        this.control({
            '[action=menuClientOrder]': {
                click: function () {
                    var self = this;
                    var mainPanel = Ext.getCmp('main-panel');
                    var panelClientOrder = Ext.create('widget.panel.client_order');

                    var grid = panelClientOrder.getComponent('grid');
                    grid.getStore().load();

                    grid.on('itemdblclick', function (view, record) {
                        self.grid = grid;
                        self.open(record.get('id'));
                    });

                    mainPanel.removeAll();
                    mainPanel.add(panelClientOrder);
                    mainPanel.doLayout();
                }
            },
            '[action=btnAddClientOrder]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.open();
                }
            }
        });
    },


    // открытие формы для редактирования
    open: function (id, data) {
        var self = this;
        var win = Ext.create('app.view.win.client_order');
        var form = win.down('form').getForm();
        var grid = win.down('grid');
        var buttons = win.getDockedComponent('buttons');

        form.findField('client_order_status_id').store.load();
        form.findField('currency_id').store.load();

        grid.down('[itemId=delete]').setVisible(true);

        if (id) {
            Ext.ModelManager.getModel('app.model.client_order').load(id, {
                success: function (record) {
                    form.loadRecord(record);

                    form.findField('contractor_title_inn').setValue(record.get('contractor_title') + ', ' + record.get('contractor_inn'));

                    form.findField('contact_personal_id').store.load({
                        params: {contractor_id: record.get('contractor_id')}
                    });
                    form.findField('contractor_delivery_id').store.load({
                        params: {contractor_id: record.get('contractor_id')}
                    });

                    // если по заказу уже выставлен счет
                    if (record.get('sheet_id')) {
                        buttons.getComponent('sheet_title').setValue(record.get('sheet_title'));
                        buttons.getComponent('sheet_id').setValue(record.get('sheet_id'));
                        buttons.getComponent('sheet_title').show();
                    }

                    form.defaultData = form.getValues();
                    win.show();

                    grid.getStore().load({
                        params: {client_order_id: record.get('id')},
                        callback: function (records, operation, success) {
                            if (success) {
                                self.upTotal(win.down('grid'));
                            } else {
                                console.log('error');
                            }
                        }
                    });
                }
            });
        } else {
            data = typeof data !== 'undefined' ? data : {};
            data.client_order_status_id = 1;
            var record = Ext.create('app.model.client_order', data);
            form.loadRecord(record);
            if (record.get('contractor_id')) {
                form.findField('contact_personal_id').store.load({
                    params: {contractor_id: record.get('contractor_id')}
                });
            }
            form.defaultData = form.getValues();
            win.show();
        }

        // сохранение заказа
        var buttonSave = buttons.getComponent('save');
        buttonSave.on('click', function () {
            self.saveRecord(win);
        });

        // выставление счета
        var buttonSheet = buttons.getComponent('sheet');
        buttonSheet.on('click', function () {
            self.createSheet(win);
        });

        grid.on('edit', function () {
            self.upTotal(grid);
        });

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            var gridDirty = false;
            if (grid.getStore().removed.length > 0) {
                gridDirty = true;
            } else {
                grid.getStore().each(function (rec) {
                    if (rec.dirty) {
                        gridDirty = true;
                    }
                });
            }

            // если объект не изменился, то проверка не нужна
            if (JSON.stringify(form.getValues()) == JSON.stringify(form.defaultData) && !gridDirty) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            self.saveRecord(win);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },


    // сохранение измененных данных
    saveRecord: function (win) {
        var self = this;

        // запись, полученная из формы
        var form = win.down('form').getForm();
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.prepayment = valRecord.prepayment == 1 ? true : false;
        record.set(valRecord);

        // позиции заказа
        var grid = win.down('grid');
        var store_client_order_nom = grid.getStore();

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    form.loadRecord(record);
                    form.defaultData = form.getValues();
                    store_client_order_nom.each(function (r) {
                        r.set('client_order_id', record.get('id'));
                    });
                    store_client_order_nom.sync();
                    self.grid.getStore().load();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.markInvalid(errors);
        }
    },


    // выставление счета
    createSheet: function (win) {
        var rClientOrder = win.down('form').getForm().getRecord();
console.log(rClientOrder);
        // открываем форму счета
        var cSheet = new app.controller.sheet;
        cSheet.create(rClientOrder);
    },


    // обновление итоговой суммы
    upTotal: function (grid) {
        var itogo = 0;

        grid.getStore().each(function (rec) {
            if (rec.get('cost')) {
                itogo += parseFloat(rec.get('cost'));
            }
        });

        var formTotal = grid.getDockedItems('[itemid=total]')[0].getForm();
        if (itogo) {
            formTotal.setValues({
                itogo: itogo + ' руб.',
                nds: (Math.round(itogo * 18 * 100 / 118) / 100) + ' руб.'
            });
        } else {
            formTotal.setValues({
                itogo: null,
                nds: null
            });
        }
    }

});
