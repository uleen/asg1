Ext.define('app.controller.nomenclature_group', {
    extend: 'Ext.app.Controller',
    models: ['nomenclature_group'],
    stores: ['nomenclature_group'],
    views: ['win.nomenclature_group', 'grid.nomenclature_group', 'form.nomenclature_group'],

    init: function () {
        this.control({
            '[action=menuNomenclatureGroup]': {
                click: this.openList
            },
            'gridNomenclatureGroup actioncolumn[action=add]': {
                click: function (view, cell, row) {
                    this.grid = view.up('window').getComponent('nomenclature-group');
                    var record = view.getStore().getAt(row);
                    this.open(null, record.get('id'));
                }
            },
            'gridNomenclatureGroup actioncolumn[action=edit]': {
                click: function (view, cell, row) {
                    this.grid = view.up('window').getComponent('nomenclature-group');
                    var record = view.getStore().getAt(row);
                    this.open(record.get('id'), null);
                }
            },
            'winNomenclatureGroup [action=create]': {
                click: function (btn) {
                    this.grid = btn.up('window').getComponent('nomenclature-group');
                    this.open(null, null);
                }
            },
            'formNomenclatureGroup [action=save]': {
                click: this.saveRecord
            }
        });
    },


    // форма
    open: function (id, pid) {
        var win = Ext.widget('formNomenclatureGroup');

        if (id) {
            Ext.ModelManager.getModel('app.model.nomenclature_group').load(id, {
                success: function (rec) {
                    win.down('form').loadRecord(rec);
                    win.down('grid').reconfigure(rec.characteristics());
                }
            });
        } else {
            var rec = Ext.create('app.model.nomenclature_group', {
                pid: pid,
                status: 1
            });
            win.down('form').loadRecord(rec);
            win.down('grid').reconfigure(rec.characteristics());
        }

        win.show();
    },


    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var characteristics = record.characteristics();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        record.set(valRecord);

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    var n = 0;
                    characteristics.each(function(r){
                        r.set('nomenclature_group_id', record.get('id'));
                        r.set('sort', ++n);
                    });
                    characteristics.sync();
                    grid.getStore().load();
                    win.close();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },


    // общий список
    openList: function () {
        var win = Ext.widget('winNomenclatureGroup');
        win.show();
    }

});


