Ext.define('app.controller.type_price', {
    extend: 'Ext.app.Controller',
    models: ['type_price'],
    stores: ['type_price'],
    views: ['win.type_price', 'grid.type_price', 'form.type_price'],

    init: function () {
        this.control({
            '[action=menuTypePrice]': {
                click: this.clickMenuTypePrice
            },
            'gridTypePrice actioncolumn[action=edit]': {
                click: this.openFormEdit
            },
            'winTypePrice [action=create]': {
                click: this.openFormAdd
            },
            'formTypePrice [action=save]': {
                click: this.saveRecord
            }
        });
    },

    // меню Виды расчетов
    clickMenuTypePrice: function () {
        var win = Ext.create('widget.winTypePrice');
        win.show();
    },

    // открытие формы дял добавления
    openFormAdd: function (btn) {
        this.grid = btn.up('window').down('grid');
        var view = Ext.widget('formTypePrice');
    },

    // открытие формы для редактирования
    openFormEdit: function (view, cell, row) {
        this.grid = view.up('grid');
        var record = view.getStore().getAt(row);
        var view = Ext.widget('formTypePrice');
        view.down('form').loadRecord(record);
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var self = this;
        var win = button.up('window');
        var form = win.down('form');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.type_price', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    self.grid.getStore().reload();
                    win.close();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
//            store.add(record);
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    }

});