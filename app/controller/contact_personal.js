Ext.define('app.controller.contact_personal', {
    extend: 'Ext.app.Controller',
   // models: ['contact_personal', 'contact_phone'],
    models: ['contact_personal','personal_contacts_data'],
    stores: ['contact_personal', 'cagent'],
    views: ['win.contact_personal', 'grid.contact_personal', 'form.contact_personal', 'form.contact_personal_new',
            'form.cagent.contact', 'win.contractor_list', 'grid.cagent_list',
            'win.contact_personal_phone','win.contact_personal_email'],

    init: function () {
        this.control({
            'gridContactPersonal': {
               // itemdblclick: this.openFormEdit 
                itemdblclick: function (grid, record) {
                    this.grid = grid;
                    this.record = record;
                    //this.openFormEdit;
                    this.open(record.get('id'));
                }
            },

            'winContactPersonal [action=create]': {
                click: this.openFormAdd
            },
            'formCagentContact [action=create]': {
            	 click: function (btn) {
                     this.cagent = btn.up('window').cagent;
                     this.grid = btn.up('grid');
                     this.openFormAddCagent(0);
                 }
            },
            'formContactPersonal [action=save]': {
                click: this.saveRecord
            },
            'formContactPersonalNew [action=save]': {
                click: this.saveRecord
            },
            'formContactPersonal [action=add_phone]': {
                click: this.addPhone
            },
            'formContactPersonalNew [action=add_phone]': {
                click: this.addPhone
            }, 
            'formCagentContact [action=set_main]': {
                click: this.setMain
            },
            'winContactPersonal [action=filter_status]': {
                change: this.filterStatus
            },
            'formIncomingCall [action=filter_status]': {
                change: this.filterStatus
            },
            'formContactPersonalNew [action=add_pho]':{
           	 click: this.addPhoneWindow
           },
           'formContactPersonalNew [action=add_email]':{
           	 click: this.addEmailWindow
           },
           'formContactPersonal [action=add_pho]':{
          	 click: this.addPhoneWindow
           },
          'formContactPersonal [action=add_email]':{
          	 click: this.addEmailWindow
          },
        });
    },

    addPhoneWindow: function(button)
    {
    	 var contactStore = button.up('window').down('grid').getStore();
         var row_count = contactStore.getCount(); 
         contactStore.add({})[0];
         
         button.up('window').down('grid').getSelectionModel().select(row_count);
         rec = button.up('window').down('grid').getSelectionModel().getSelection()[0];
         
         var gridview = Ext.getCmp('cp-grid').getView();
         var nameCell = gridview.getHeaderCt().gridDataColumns[1];
         
         nameCell.setEditor(
           {
                xtype: 'textfield',
                plugins: [new Ext.ux.InputTextMask('+7 (999) 999-99-99', true)], 
                enableKeyEvents: true
  
           }
         );
         if (row_count == 0){
          	rec.set('is_main',1);
          }else{
          	rec.set('is_main',0);
          }
          
         rec.set("contact_type","Телефон");
         rec.set("add_cintact",'');
          rec.set("is_active",1);
          rec.set("cp_status",1);

    },
    
    addEmailWindow: function(button)
    {
    	 var contactStore = button.up('window').down('grid').getStore();
         var row_count = contactStore.getCount(); 
         contactStore.add({})[0];

         button.up('window').down('grid').getSelectionModel().select(row_count);
         rec = button.up('window').down('grid').getSelectionModel().getSelection()[0];
         

         var gridview = Ext.getCmp('cp-grid').getView();
         var nameCell = gridview.getHeaderCt().gridDataColumns[1];

         nameCell.setEditor({
             xtype: 'textfield',
             allowBlank: true,
             enableKeyEvents: true
             
         });
         
         rec.set("contact_type","E-mail");
         rec.set("is_active",1);
         rec.set("cp_status",1);
         if (row_count == 0){
          	rec.set('is_main',1);
          }else{
          	rec.set('is_main',0);
         }
         
    },
   
    // открытие формы для добавления из справочника
    // Новые данные
    openFormAdd: function (grid, record) {
        var win = Ext.widget('formContactPersonalNew');
        var rec = Ext.create('app.model.contact_personal', {cp_status: 1});

       // var storePhone = rec.contact_phone();
        var storeContact = rec.personal_contacts_data();
        win.down('form').loadRecord(rec);
        //win.down('grid').reconfigure(storePhone);
        win.down('grid').reconfigure(storeContact);
        win.show();
        
        

    },
    
    // открытие формы дял добавления из контрагента
    openFormAddCagent: function (id) {
        var win = Ext.widget('formContactPersonal');

        var rec = Ext.create('app.model.contact_personal', {cp_status: 1});
        var cagent = this.cagent;
              
        var storeContact = rec.personal_contacts_data();
        win.down('form').loadRecord(rec);
        win.down('form').getForm().setValues({
        	cagent_title: cagent.title,
        	contractor_id: cagent.contractor_id,
        	org_id: cagent.org_id,
        });
        
        win.down('grid').reconfigure(storeContact);
        win.show();
        
    }, 
    
    open: function(id)
    {
    	var record = this.record;
        var win = Ext.widget('formContactPersonal');
        Ext.ModelManager.getModel('app.model.contact_personal').load(id, {
            success: function (rec) {
                win.down('form').loadRecord(rec);
                var storeContact = rec.personal_contacts_data();
                
                win.down('grid').reconfigure(storeContact);
                win.down('form').getForm().setValues({
                	contractor_id: record.get('contractor_id'),
                });
                win.defaultData = win.down('form').getForm().getValues();
                win.storeDefault = storeContact;
                win.show();
            }
        });
        
        if (record.get('contact_personal_id') == record.get('main_personal_id')){
        	Ext.getCmp('is-active-user').setDisabled(true);
        	Ext.getCmp('is-active-user').setBoxLabel('Это основное контактное лицо');
        }
        win.confirmClosed = true;
        win.on('beforeclose', function ()
        {
        	var record = this.down('form').getForm().getValues();
        	//var record = this.down('form').down('grid').getValues();

            if (JSON.stringify(record) == JSON.stringify(win.defaultData)) {
                win.confirmClosed = false;
            }

            
            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = Ext.getCmp('btn-save-contact-personal');
                            button.fireEvent('click', button);
                            win.confirmClosed = false;
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },

    // открытие формы для редактирования
   // openFormEdit: function (view, cell, row) {
    openFormEdit: function (grid, record)
    { 
    	
        var win = Ext.widget('formContactPersonal');
        Ext.ModelManager.getModel('app.model.contact_personal').load(record.get('id'), {
            success: function (rec) {
                win.down('form').loadRecord(rec);
                var storeContact = rec.personal_contacts_data();
                
                win.down('grid').reconfigure(storeContact);
                win.down('form').getForm().setValues({
                	contractor_id: record.get('contractor_id'),
                });
                win.defaultData = win.down('form').getForm().getValues();
                win.storeDefault = storeContact;
                win.show();
            }
        });
        
        if (record.get('contact_personal_id') == record.get('main_personal_id')){
        	Ext.getCmp('is-active-user').setDisabled(true);
        	Ext.getCmp('is-active-user').setBoxLabel('Это основное контактное лицо');
        }
        win.confirmClosed = true;
        win.on('beforeclose', function ()
        {
        	var record = this.down('form').getForm().getValues();
        	//var record = this.down('form').down('grid').getValues();

            if (JSON.stringify(record) == JSON.stringify(win.defaultData)) {
                win.confirmClosed = false;
            }

            
            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = Ext.getCmp('btn-save-contact-personal');
                            button.fireEvent('click', button);
                            win.confirmClosed = false;
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
        
     
        
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        
        this.vals = valRecord;
        
        valRecord.cp_status = valRecord.na == 1 ? 2 : 1;

        if (!record) {
            record = Ext.create('app.model.contact_personal', valRecord);
        } else {
            record.set(valRecord);
        }
        
        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record) {
                	
                    record.personal_contacts_data().each(function (r) {
                    	
                        r.set('contact_personal_id', record.get('id'));
                    
                        if (r.get('contact_type') == 'Телефон'){
                        	r.set('contact_type', 1);
                        }
                        if (r.get('contact_type') == 'E-mail'){
                        	r.set('contact_type', 2);
                        }
                        
                    });
                    win.confirmClosed = false;
                    win.close();
                    
                    
                    record.personal_contacts_data().sync({
                        success: function () {
                            Ext.StoreManager.get('contact_personal').reload();
                        }
                    });
                    
                    
                   
                    // перегрузка гридла
                    if (grid){
                    	var store = Ext.create('app.store.contact_personal');
                    	store.clearFilter();
                    	store.getProxy().extraParams = {contractor_id: record.get('contractor_id')};
                        grid.getStore().reload();
                    }
                    
                    
                    
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
        

    },

    // фильтр по статусу
    filterStatus: function (view, val) {
    	
    	 

         var win = Ext.widget('winContactPersonal');

         /*
         if(valRecord.org_id!=0)
         {
 	        storeCP.getProxy().extraParams = {
 	            org_id: valRecord.org_id,
 	            status: 1
 	        }
         }
    	*/
        var store = Ext.StoreManager.get('contact_personal');
        
        store.getProxy().extraParams = {
            status: val,
        };
        store.load();
    },

    // добавление телефона
    // проверку, что строа заполнена и не позволять добвылять еще, пока строка не добавлена
    addPhone: function (button)
    {
        var storePhone = button.up('window').down('grid').getStore();
        var row_count = storePhone.getCount(); 
        storePhone.add({})[0];
        
        button.up('window').down('grid').getSelectionModel().select(row_count);
        rec = button.up('window').down('grid').getSelectionModel().getSelection()[0];
        
        console.log('количество контактов: '+row_count);
        
        if (row_count == 0){
        	rec.set('is_main',1);
        }else{
        	rec.set('is_main',0);
        }
        
        rec.set("contact_type","Телефон");
        rec.set("is_active",1);
        rec.set("cp_status",1);
      
       
    },
    
    setMain: function(button)
    {
    	 var store = button.up('window').down('grid');
    	 var row = store.getSelectionModel().getSelection()[0];
    	 
    	 // проерка на статус пользователя!
    	 var cp_status = row.get('cp_status');
    	 if (cp_status != 1)
    	 {
    			Ext.Msg.alert('Недопустимая операция','Контактное лицо неактивно!\nНазначение невозможно!');  
    	 }
    	 else
    	 {
             Ext.Ajax.request({
                 url: '/spec/d_contact_personal/setmain',
                 method: 'GET',
                 params: {
                     contact_personal_id:  row.get('contact_personal_id'),
                     contractor_id: row.get('contractor_id')
                 },
                 success: function (response) {
                     if (response.responseText) {
                         var ic = JSON.parse(response.responseText);
                         //form.setValues(ic);
                         
                         // перерисовка грида
                         var store = Ext.StoreManager.get('contact_personal');
                         var grid = Ext.getCmp('form-cagent-contact').down('grid');
                         store.clearFilter();
                         store.getProxy().extraParams = {contractor_id: row.get('contractor_id')};
                         store.load({
                             callback: function(){
                                     grid.columns[2].setVisible(false);                   
                             }
                         });
                         
                         grid.reconfigure(store);
                         
                     
                     }
                     else {

                     }
                 }
             });
    	 }    	 
    },
    
    // изменение статуса записи
    changeStatusRecord: function (view, cell, row) {
        var record = view.getStore().getAt(row);
        record.set('is_active', record.get('is_active') == 1 ? 1 : 0);
        console.log('change cp contact active');
        record.save();
    },
    
    changeMain: function (view, cell, row) {
        var record = view.getStore().getAt(row);
        record.set('is_main', record.get('is_main') == 1 ? 1 : 0);
        console.log('change cp contact is main');
        record.save();
    },
});


