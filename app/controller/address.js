Ext.define('app.controller.address', {
    extend: 'Ext.app.Controller',
    models: ['address_city', 'address_street', 'address_house'],
    stores: ['address_city', 'address_street', 'address_house'],
    views: ['form.select_address'],
    init: function () {
    }
});


