Ext.define('app.controller.dictionary.position', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.position.win',
        'dictionary.position.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.position.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});