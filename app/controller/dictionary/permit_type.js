Ext.define('app.controller.dictionary.permit_type', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.permit_type.win',
        'dictionary.permit_type.grid'
    ],

    // общий список
	openList: function () {
		var win = Ext.widget('dictionary.permit_type.win');
		var grid = win.down('grid');
		var store = grid.getStore();
		store.load();
		win.show();
	}

});