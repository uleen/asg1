Ext.define('app.controller.dictionary.equipment_type', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.equipment_type.win',
        'dictionary.equipment_type.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.equipment_type.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});