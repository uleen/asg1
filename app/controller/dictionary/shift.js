Ext.define('app.controller.dictionary.shift', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.shift.win',
        'dictionary.shift.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.shift.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});