Ext.define('app.controller.dictionary.vehicle_model', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.vehicle_model.win',
        'dictionary.vehicle_model.grid',
		'dictionary.vehicle_model.form'
    ],
	models: ['app.model.dictionary.vehicle_model'],
	stores: ['app.store.dictionary.vehicle_model'],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.vehicle_model.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    },
	open: function (id) {
		var self = this;
		var win = Ext.widget('dictionary.vehicle_model.form');
		var form = win.down('form').getForm();
		// открытие по id
		if (id) {
			Ext.ModelManager.getModel('app.model.dictionary.vehicle_model').load(id, {
				success: function (rec) {
					form.loadRecord(rec);
					win.show();
				}
			});
		} else {
			win.show();
		}
		var buttonSave = win.down('button');
		buttonSave.on('click', function () {
			self.saveRecord(win);
		});
	},
	// сохранение формы
	saveRecord: function (win) {
		var self = this;
		var form = win.down('form').getForm();
		var record = form.getRecord();
		var values = form.getValues();
		record.set(values);

		// сохранение объекта
		record.save({
			// объект сохранен на сервере
			success: function () {
				if (self.gridReload) {
					self.gridReload.getStore().reload();
				}
				win.close();
			},
			// объект не прошел валидацию на сервере
			failure: function (record, operation) {
				var errors = operation.request.scope.reader.jsonData["errors"];
				form.markInvalid(errors);
			}
		});
	}

});