Ext.define('app.controller.dictionary.fuel', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.fuel.win',
        'dictionary.fuel.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.fuel.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});