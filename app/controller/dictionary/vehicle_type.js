Ext.define('app.controller.dictionary.vehicle_type', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.vehicle_type.win',
        'dictionary.vehicle_type.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.vehicle_type.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});