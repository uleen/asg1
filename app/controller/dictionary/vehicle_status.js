Ext.define('app.controller.dictionary.vehicle_status', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.vehicle_status.win',
        'dictionary.vehicle_status.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.vehicle_status.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});