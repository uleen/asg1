Ext.define('app.controller.dictionary.type_document', {
    extend: 'Ext.app.Controller',
    views: [
        'dictionary.type_document.win',
        'dictionary.type_document.grid'
    ],

    // общий список
    openList: function () {
        var win = Ext.widget('dictionary.type_document.win');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();
    }

});