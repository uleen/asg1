Ext.define('app.controller.user', {
    extend: 'Ext.app.Controller',
    models: ['user.main'],
    views: [
        'user.win.list', 'user.win.form',
        'user.grid.main', 'user.grid.rules', 'user.grid.companies',
        'user.form.main'
    ],

    // открытие формы для редактирования
    open: function (id) {
        var self = this;
        var win = Ext.widget('user.win.form');
        var form = win.down('form').getForm();
        var grid = win.down('grid');
        var buttons = win.down('form').getDockedComponent('buttons');

        // загрузка данных форму
        if (id) {
            Ext.ModelManager.getModel('app.model.user.main').load(id, {
                success: function (rUser) {
                    form.loadRecord(rUser);
                    win.show();
                }
            });
        }
        else {
            var rUser = Ext.create('app.model.user.main', {
                status: 1
            });
            form.loadRecord(rUser);
            win.show();
        }

        // сохранение формы
        var btnSave = buttons.getComponent('btn-save');
        btnSave.on('click', function () {
            self.save(win);
        });
    },


    // открытие списка пользователей
    openList: function () {
        var self = this;
        var win = Ext.widget('user.win.list');

        var gridCompanies = win.getComponent('companies');
        gridCompanies.getStore().load();

        var gridUsers = win.getComponent('users');
        gridUsers.getStore().load();

        // редактирование существующего
        gridUsers.on('itemdblclick', function (view, rCompany) {
            self.open(rCompany.get('id'));
        });

        // добавление нового
        var btnAdd = gridUsers.getDockedComponent('buttons').getComponent('btn-add');
        btnAdd.on('click', function () {
            self.open();
        });

        win.show();
    },


    // сохранение формы пользователя
    save: function (win) {
        var form = win.down('form');

        var rUser = form.getRecord();
        var dUser = form.getValues();

        dUser.can_auth = dUser.can_auth ? 1 : 0;
        rUser.set(dUser);

        // сохранение объекта
        rUser.save({
            // объект сохранен на сервере
            success: function () {
                win.close();
            },
            // объект не прошел валидацию на сервере
            failure: function (record, operation) {
                var errors = operation.request.scope.reader.jsonData["errors"];
                form.getForm().markInvalid(errors);
            }
        });
    }

});