Ext.define('app.controller.sheet', {
    extend: 'Ext.app.Controller',
    models: ['sheet'],
    views: ['grid.sheet'],

    init: function () {
        this.control({
            '[action=menuListSheet]': {
                click: this.openList
            }
        });
    },

    // общий список
    openList: function () {
        var self = this;
        var win = Ext.create('app.view.win.sheet');
        var grid = win.down('grid');

        var store = grid.getStore();
        store.getProxy().extraParams.status = 1;
        store.load();

        // открытие формы счета по двойному клику
        grid.on('itemdblclick', function (store, rSheet) {
            self.open(rSheet.get('id'));
        });

        win.show();
    },


    // открытие формы для редактирования
    open: function (id, rSheet) {
        var self = this;
        var win = Ext.create('app.view.form.sheet');
        var form = win.down('form').getForm();
        var grid = win.down('grid');
        var buttons = win.getDockedComponent('buttons');

        // загрузка справочников
        form.findField('sheet_status_id').store.load();
        form.findField('sheet_type_id').store.load();

        // открытие по id
        if (id) {
            Ext.ModelManager.getModel('app.model.sheet').load(id, {
                success: function (rSheet) {
                    form.findField('company_id').store.load();
                    form.findField('company_requisite_id').store.load({
                        params: {company_id: rSheet.get('company_id')}
                    });
                    form.loadRecord(rSheet);
                    buttons.getComponent('print').show();
                    self.loadNomenclature(win);
                    win.show();
                }
            });
        } else {
            form.loadRecord(rSheet);
            self.loadNomenclature(win);
            win.show();
        }

        // сохранение счета
        var buttonSave = buttons.getComponent('save');
        buttonSave.on('click', function () {
            self.save(win);
        });
    },


    // создание счета из формы заказа клиента
    create: function (rClientOrder) {
        // преобразование клиентского заказа в счет
        var rSheet = Ext.create('app.model.sheet', {
            contractor_id: rClientOrder.get('contractor_id'),
            contractor_title: rClientOrder.get('contractor_title'),
            contractor_inn: rClientOrder.get('contractor_inn'),
            contractor_contract_id: rClientOrder.get('contractor_contract_id'),
            contractor_contract_title: rClientOrder.get('contractor_contract_title'),
            currency_id: rClientOrder.get('currency_id'),
            currency_code: rClientOrder.get('currency_code'),
            client_order_id: rClientOrder.get('id'),
            client_order_date: rClientOrder.get('create_date'),
            sheet_type_id: 1,
            sheet_status_id: 1
        });
        this.open(null, rSheet);
    },


    // загрузка списка номенклатуры
    loadNomenclature: function (win) {
        var form = win.down('form').getForm();
        var grid = win.down('grid');

        // номенклатура тянется из заказа
        var client_order_id = form.getValues().client_order_id;
        grid.getStore().load({
            params: {
                client_order_id: client_order_id
            },
            callback: function (records, operation, success) {
                if (success) {
                    // расчет ИТОГО
                    var itogo = 0;
                    grid.getStore().each(function (rec) {
                        if (rec.get('cost')) {
                            itogo += parseFloat(rec.get('cost'));
                        }
                    });
                    var formTotal = grid.getDockedItems('[itemid=total]')[0].getForm();
                    if (itogo) {
                        formTotal.setValues({
                            itogo: itogo + ' руб.',
                            nds: (Math.round(itogo * 18 * 100 / 118) / 100) + ' руб.'
                        });
                    }
                } else {
                    console.log('error');
                }
            }
        });

        // отключаем редактирование грида
        grid.on('beforeedit', function () {
            return false;
        });
        grid.getDockedItems('[dock=top]')[0].hide();
    },


    // печать счета
    print: function (win) {

    },


    // сохранение формы
    save: function (win) {
        var form = win.down('form').getForm();

        var rSheet = form.getRecord();
        var dSheet = form.getValues();
        rSheet.set(dSheet);

        // сохранение объекта
        rSheet.save({
            // объект сохранен на сервере
            success: function () {
                win.close();
            },
            // объект не прошел валидацию на сервере
            failure: function (record, operation) {
                var errors = operation.request.scope.reader.jsonData["errors"];
                form.markInvalid(errors);
            }
        });
    }

});
