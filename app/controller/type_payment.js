Ext.define('app.controller.type_payment', {
    extend: 'Ext.app.Controller',
    models: ['type_payment'],
    stores: ['type_payment'],
    views: ['win.type_payment', 'grid.type_payment', 'form.type_payment'],

    init: function () {
        this.control({
            'gridTypePayment actioncolumn[action=edit]': {
                click: this.openFormEdit
            },
            'winTypePayment [action=add]': {
                click: this.openFormAdd
            },
            'formTypePayment [action=save]': {
                click: this.saveRecord
            }
        });
    },


    // открытие формы дял добавления
    openFormAdd: function (btn) {
        this.grid = btn.up('grid');
        var view = Ext.widget('formTypePayment');
    },


    // открытие формы для редактирования
    openFormEdit: function (view, cell, row) {
        this.grid = view.up('grid');
        var record = view.getStore().getAt(row);
        var view = Ext.widget('formTypePayment');
        view.down('form').loadRecord(record);
    },


    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.type_payment', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    grid.getStore().reload();
                    win.close();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
//            store.add(record);
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    }


});


