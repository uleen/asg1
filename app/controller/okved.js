Ext.define('app.controller.okved', {
    extend: 'Ext.app.Controller',
    models: ['okved'],
    stores: ['okved'],
    views: ['win.okved'],

    init: function () {
        this.control({
            '#okved-tree': {
                itemclick: this.showAdditionalInfo
            }
        });
    },

    // отображение информации по элементу справочника
    showAdditionalInfo: function (grid, record) {
        Ext.getCmp('okved-info').update(record.get('additional_info'));
    }
});


