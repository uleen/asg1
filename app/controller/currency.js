Ext.define('app.controller.currency', {
    extend: 'Ext.app.Controller',
    models: ['currency', 'currency_rate'],
    stores: ['currency', 'currency_rate'],
    views: [
        'win.currency',
        'win.currency_rate',
        'grid.currency',
        'grid.currency_rate'
    ],

    init: function () {
        this.control({
            '[action=menuCurrency]': {
                click: this.openList
            }
        });
    },

    // общий список
    openList: function () {
        var win = Ext.widget('winCurrency');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.getProxy().extraParams.status = 1;
        store.load();
        win.show();
    }

});
