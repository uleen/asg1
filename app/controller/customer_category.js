Ext.define('app.controller.customer_category', {
    extend: 'Ext.app.Controller',
    models: ['customer_category'],
    stores: ['customer_category'],
    views: ['win.customer_category', 'grid.customer_category', 'form.customer_category'],

    init: function () {
        this.control({
            'gridCustomerCategory actioncolumn[action=edit]': {
                click: this.openFormEdit
            },
            'winCustomerCategory [action=create]': {
                click: this.openFormAdd
            },
            'formCustomerCategory [action=save]': {
                click: this.saveRecord
            },
            'winCustomerCategory [action=filter_status]':{
                change: this.filterStatus
            }
        });
    },

    // открытие формы дял добавления
    openFormAdd: function (grid, record) {
        var view = Ext.widget('formCustomerCategory');
    },

    // открытие формы для редактирования
    openFormEdit: function (view, cell, row) {
        var record = view.getStore().getAt(row);
        var view = Ext.widget('formCustomerCategory');
        view.down('form').loadRecord(record);
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.customer_category', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record, operation) {
                    win.close();
                    var store = Ext.StoreManager.get('customer_category');
                    store.reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
//            store.add(record);
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // фильтр по статусу
    filterStatus: function (view, val) {
        var store = Ext.StoreManager.get('customer_category');
        store.getProxy().extraParams = {
            status: val
        };
        store.load();
    }

});


