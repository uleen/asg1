Ext.define('app.controller.vehicle', {
    extend: 'Ext.app.Controller',
    views: [
        'vehicle.win.list',
        'vehicle.win.form',

        'vehicle.form.menu',
        'vehicle.form.main',

        'vehicle.grid.main',
        'vehicle.grid.equipment',
        'vehicle.grid.permit',
        'vehicle.grid.driver'
    ],

	models: ['app.model.vehicle.main'],
	stores: ['app.store.vehicle.main'],


    // общий список
    openList: function () {
        var self = this;
        var win = Ext.widget('vehicle.win.list');
        var grid = win.down('grid');
        var store = grid.getStore();
        store.load();
        win.show();

        // кнопка добавления
        var btnAdd = grid.getDockedComponent('top').getComponent('add');
        btnAdd.on('click', function () {
            self.open();
        });
    },


    // открытие формы для редактирования
    open: function (id, record) {
        var self = this;
		var win = Ext.widget('vehicle.win.form');
		var buttons = win.down('form').getDockedComponent('buttons');
        win.vehicle = Ext.create('app.model.vehicle.main');
		var form = win.down('form').getForm();
		if (id) {
			Ext.ModelManager.getModel('app.model.vehicle.main').load(id, {
				success: function (rec) {
					form.loadRecord(rec);
					win.show();
					console.log(rec);
				}
			});
		} else {
			var data = typeof data !== 'undefined' ? data : {};
			data.client_order_status_id = 1;
			record = Ext.create('app.model.vehicle.main', data);
			form.loadRecord(record);
			form.defaultData = form.getValues();
			win.show();
		}
		var btnSave = buttons.getComponent('btn-save');
		btnSave.on('click', function () {
			self.saveRecord(win);
		});
        // левое меню
        var menu = win.getComponent('menu');
        menu.on('itemclick', this.changeForm);
    },
	// сохранение формы
	saveRecord: function (win) {
		var self = this;
		var form = win.down('form').getForm();
		var record = form.getRecord();
		var values = form.getValues();
		console.log(values);
		record.set(values);

		// сохранение объекта
		record.save({
			// объект сохранен на сервере
			success: function () {
				alert('OK');
				win.close();
			},
			// объект не прошел валидацию на сервере
			failure: function (record, operation) {
				var errors = operation.request.scope.reader.jsonData["errors"];
				form.markInvalid(errors);
			}
		});
	},

    // переключение формы
    changeForm: function (view, item) {

        if (!view.up('window').vehicle) {
            alert('Необходимо сохранить транспортное средство!');
            return;
        }

        if (item.raw.formId) {
            var form = view.up('window').getComponent('form');
            Ext.each(form.items.items, function (el) {
                if (el.itemId == item.raw.formId) {
                    el.show();
                } else {
                    el.hide();
                }
            });
        }
        // Оборудование
        if (item.raw.formId == 'vehicle-grid-equipment') {
            console.log('Оборудование');
        }

        // Пропуска
        if (item.raw.formId == 'vehicle-grid-permit') {
            console.log('Пропуска');
        }

        // Водители
        if (item.raw.formId == 'vehicle-grid-driver') {
            console.log('Водители');
        }


    }


});