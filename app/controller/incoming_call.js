Ext.define('app.controller.incoming_call', {
    extend: 'Ext.app.Controller',
    models: ['incoming_call', 'cagent', 'ic_search'],
    stores: ['incoming_call', 'cagent', 'ic_search', 'nom', 'search_incoming_call'],
    views: ['form.incoming_call', 'win.incoming_call', 'panel.incoming_call', 'grid.incoming_call',
            'form.ic_call_search', 'win.ic_search', 'form.incoming_call_edit', 'form.incoming_call_view'],
    requires: ['Ext.ux.form.SearchField'],

    init: function () {
    	
        this.control({
            'formIncomingCall [action=save]': {
                click: this.saveRecord
            },
            'formIncomingCallEdit [action=save]': {
                click: this.saveRecord
            },
            'formIncomingCallViewOnly [action=save]':{
            	 click: this.saveRecord
            },
            'gridIncomingCall': {
                itemdblclick: function (grid, record) {
                    this.open(record.get('id'));
                }
            },
            'formIncomingCall [name=cagent_name]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {
                        // открытие формы по двойному клику
                        var win = Ext.widget('formContactPersonal');
                          
                        var id = e.up('form').getForm().getValues().contact_personal_id;
                        var icr_id = e.up('form').getForm().getValues().icr_id;
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        
                        
                       
                        
                        Ext.ModelManager.getModel('app.model.contact_personal').load(id, {
                            success: function (rec) {
                            	
                                win.down('form').loadRecord(rec);
                                var storeContact = rec.personal_contacts_data();
                                win.down('grid').reconfigure(storeContact);
                            }
                        });
                    });
                }
            },
            'formIncomingCallEdit [name=cagent_name]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {
                        // открытие формы по двойному клику
                        var win = Ext.widget('formContactPersonal');
                       
                        
                        var id = e.up('form').getForm().getValues().contact_personal_id;
                        var icr_id = e.up('form').getForm().getValues().icr_id;
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        
                        
                       
                        
                        Ext.ModelManager.getModel('app.model.contact_personal').load(id, {
                            success: function (rec) {
                            	
                                win.down('form').loadRecord(rec);
                                var storeContact = rec.personal_contacts_data();
                                win.down('grid').reconfigure(storeContact);
                            }
                        });
                    });
                }
            },
            'formIncomingCallViewOnly [name=cagent_name]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {
                        // открытие формы по двойному клику
                        var win = Ext.widget('formContactPersonal');
  
                        
                        var id = e.up('form').getForm().getValues().contact_personal_id;
                        var icr_id = e.up('form').getForm().getValues().icr_id;
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        
                        
                       
                        
                        Ext.ModelManager.getModel('app.model.contact_personal').load(id, {
                            success: function (rec) {
                            	
                                win.down('form').loadRecord(rec);
                                var storeContact = rec.personal_contacts_data();
                                win.down('grid').reconfigure(storeContact);
                            }
                        });
                    });
                }
            },
            
            
            
            'formIncomingCall [name=company_title]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {              	
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        if (contractor_id > 0) {
                            var cagent = new app.controller.cagent;
                            cagent.open(contractor_id);
                        }
                    });
                }
            },
            
            'formIncomingCallEdit [name=company_title]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {              	
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        if (contractor_id > 0) {
                            var cagent = new app.controller.cagent;
                            cagent.open(contractor_id);
                        }
                    });
                }
            },
            
            'formIncomingCallViewOnly [name=company_title]': {
                render: function (e) {
                    e.getEl().on('dblclick', function () {              	
                        var contractor_id = e.up('form').getForm().getValues().contractor_id;
                        if (contractor_id > 0) {
                            var cagent = new app.controller.cagent;
                            cagent.open(contractor_id);
                        }
                    });
                }
            },
            'formIncomingCall [action=search]': {
                click: this.searchRecord
            },
            'formIncomingCall [name=cagent_phone]': {
                blur: this.searchRecord
            },
            'panelIncomingCall [action=searchIncomingCall]': {
                click: this.searchIncomingCall
            },
            'panelIncomingCall [action=clearIncomingCallSearchForm]': {
                click: this.clearIncomingCallSearchForm
            },
            '[action=menuIncomingCall]': {
                click: this.clickMenuIncomingCall
            },
            '[action=menuListIncomingCall]': {
                click: this.menuListIncomingCall
            },
            '[action=showHistoryIncomingCall]': {
                click: this.showHistoryIncomingCall
            },
            '[action=executedByAuthor]': {
            	click: this.executedByAuthor
            },
            'formIncomingCall [action=execute]':{
            	click: this.ExecuteIncommingCall
            },
            'formIncomingCallEdit [action=execute]':{
            	click: this.ExecuteIncommingCall
            },
            '[action=create-client-order]': {
                click: function(btn) {
                    var win = btn.up('window');
                    var form = win.down('form');
                    var ic_id = form.getRecord().get('id');

                    // перезагрузка модели, что бы получить все данные
                    Ext.ModelManager.getModel('app.model.incoming_call').load(ic_id, {
                        success: function (record)
                        {
                            win.close();
                            // открываем окно заказа клиента
                            var client_order = new app.controller.client_order;
                            client_order.open(null, {
                                incoming_call_id: record.get('id'),
                                incoming_call: '№ ' + record.get('id') + ' от ' + Ext.Date.format(record.get('call_date'), 'd.m.Y H:i'),
                                contractor_id: record.get('contractor_id'),
                                contractor_title: record.get('company_title'),
                                contractor_inn: record.get('company_inn'),
                                contact_personal_id: record.get('contact_personal_id')
                            });
                        }
                    });
                }
            }
        });
       
    },
    
    // окно "Входящий запрос"
    clickMenuIncomingCall: function () {
        var win = Ext.create('widget.formIncomingCall');

        // данные авторизованного пользователя
        var user;
        Ext.Ajax.request({
            url: '/get_user_data',
            async: false,
            success: function (result, request) {
                user = JSON.parse(result.responseText);
            }
        });

        // дефолтная модель
        win.down('form').getForm().setValues({
            call_date: Ext.Date.format(new Date(), 'd.m.Y H:i'),
            incoming_source_id: 1,
            author_name: user.name,
            author_id: user.id,
            is_new: 1
        });
        
        var defRecord = win.down('form').getForm().getValues();
        //added
        Ext.getCmp('btn-execute-incomming-call').setDisabled(true);

        win.show();

        // подтверждение закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            var record = this.down('form').getForm().getValues();
            console.log(record);
            if (JSON.stringify(record) == JSON.stringify(defRecord)) {
                win.confirmClosed = false;
            }
            else if ( record.executed == 1){
            	 win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = Ext.getCmp('btn-save-incoming-call');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
               
                return false;
            } else {
                return true;
            }
        }
        );
       
    },

    // открытие формы для редактирования
    // Используем 2 формы по правам!
    open: function (id)
    {
        Ext.ModelManager.getModel('app.model.incoming_call').load(id,
        {
            success: function (rec)
            {
            	// получаем необходимые  данные с сервера
            	var  reclama_id =  rec.get('reclama_id');
            	var responsible_user_id = rec.get('manager_id');
            	var responsible_user = rec.get('manager_name');
            	var ic_author_id = rec.get('author_id');
            	var logged_user_id = App.Global.user_id;
            	var call_date = Ext.Date.format(rec.get('call_date'), 'd.m.Y H:i');
            	var save_date = Ext.Date.format(rec.get('save_date'), 'd.m.Y H:i');
            	var is_active = rec.get('status');
            	
                if (rec.get('executed') == 1 )
                {
                	var win = Ext.widget('formIncomingCallViewOnly');
                	win.down('form').loadRecord(rec);
                    
                    win.down('form').getForm().setValues({
    	                    call_date: call_date,
    	                    save_date: save_date,
    	                    reclama_id: reclama_id,
    	                    responsible_user_id: responsible_user_id,
    	                    responsible_user: responsible_user,
    	                });
                    win.show();
                    
                    Ext.getCmp('field-responsible-user').setValue(responsible_user);
                    
                	Ext.getCmp('label-executed-incomming-call').show();
                	Ext.getCmp('btn-execute-incomming-call').hide();
                }
                else 
                {
                	// @todo: права доступа
                	var executed_btn = 0;
                	// отвественный != залогиненному и залогиненный != автору
                	if ( responsible_user_id != logged_user_id  && logged_user_id != ic_author_id)
                	{
                		var win = Ext.widget('formIncomingCallViewOnly');
                		Ext.getCmp('field-responsible-user').setValue(responsible_user);
                		var executed_btn = 0;
                	} 

                	// отвественный = залогиненному, ответсвенный != автору
                	if ( responsible_user_id == logged_user_id && logged_user_id != ic_author_id )
                	{
                		var win = Ext.widget('formIncomingCallEdit');
                		var executed_btn = 1;
                	}
                	
                	if ( responsible_user_id == logged_user_id && logged_user_id == ic_author_id )
                	{             		
                		var win = Ext.widget('formIncomingCallEdit');
                		var executed_btn = 1;
                	}
                	
                	if ( responsible_user_id != logged_user_id && logged_user_id == ic_author_id )
                	{             		
                		var win = Ext.widget('formIncomingCallEdit');
                		var executed_btn = 0;
                	}
                	
                	win.down('form').loadRecord(rec);

                    win.down('form').getForm().setValues({
    	                    call_date: call_date,
    	                    save_date: save_date,
    	                    reclama_id: reclama_id,
    	                    responsible_user_id: responsible_user_id,
    	                    responsible_user: responsible_user,
    	                });
                    win.show();
                		
                    if (Ext.getCmp('checkbox-executed_by_author')){
                    	Ext.getCmp('checkbox-executed_by_author').show();
                    }

                	if (responsible_user_id == ic_author_id){
                		if (Ext.getCmp('checkbox-executed_by_author')) {
                			 Ext.getCmp('checkbox-executed_by_author').setValue(true);
                		}
                	}
                	 
                	if (Ext.getCmp('btn-execute-incomming-call')){
                		if (executed_btn == 1){
                    		Ext.getCmp('btn-execute-incomming-call').setDisabled(false);
                    	}
                    	else {
                    		Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
                    	}
                	}
                	
                	if (Ext.getCmp('btn-save-incoming-call')){
                		Ext.getCmp('btn-save-incoming-call').setDisabled(false);
                	}  	
                }
    
                if (Ext.getCmp('company-type-radios')){
                	 Ext.getCmp('company-type-radios').setValue({company_type:rec.get('contact_type')});
                }
                
                // источник обращения
                if (rec.get('ref_cagent_id')) {
                    Ext.StoreManager.get('cagent').load();
                    Ext.getCmp('field-ref-cagent').show();
                }
                
                if (rec.get('ref_user_id')) {
                	Ext.getCmp('field-ref-user').show();
                }               
               
                if (rec.get('contractor_id')!=0){
                    Ext.getCmp('label-company-title-is-find').show();
                    Ext.getCmp('label-company-title-is-not-find').hide();
                    //Ext.getCmp('btn-create-client-incoming-call').hide();   
                    Ext.getCmp('btn-create-client-incoming-call').setDisabled(true);
                    Ext.getCmp('btn-create-client-order').setDisabled(false);
                }
                else{
                    Ext.getCmp('label-company-title-is-find').hide();
                    Ext.getCmp('label-company-title-is-not-find').show();                   
                    Ext.getCmp('btn-create-client-incoming-call').setDisabled(false);
                }
                
                if (rec.get('is_new') != 1) {
                    Ext.getCmp('incoming-call-is-new').hide();
                    Ext.getCmp('incoming-call-source').hide();
                    Ext.getCmp('incoming-call-cagent-info').hide();
                    Ext.getCmp('incoming-call-client-interest-srv').inputEl.removeCls('field-required');
                } else {
                    Ext.StoreManager.get('reclama').load();
                }
            }
        });
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var ic = this;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;        
        
        
        if (valRecord.x_responsible_user_id!=0){
        	valRecord.responsible_user_id = valRecord.x_responsible_user_id;
        }
        
        if (valRecord.responsible_user_id instanceof Array){
        	valRecord.responsible_user_id = valRecord.responsible_user_id[0];
        }
        

        if (!record) {
            record = Ext.create('app.model.incoming_call', valRecord);
        } else {
            record.set(valRecord);
        }
        // сохранение только контрагента
        
        if (button.id == 'btn-create-client-incoming-call' && record.get('id')) {
            ic.saveCagent(record);
        } 

        else if (record.isValid()) {
            // сохранение вх. запроса и контрагента
            button.disable();
            record.save({
                // объект сохранен на сервере
                success: function (record, op) {
                    win.confirmClosed = false;
                    button.enable();
                    Ext.StoreManager.get('incoming_call').reload();
                    form.loadRecord(record);

                    var ix = JSON.parse(op.response.responseText);

                    form.getForm().setValues({
                    	id: ix.items.uniq_form_id,
                    	org_id: ix.items.org_id,
                    });                    
                    record.set(form.getValues());

                    // сохранение контрагента
                    if (button.id == 'btn-create-client-incoming-call') {                        
                    	ic.saveCagentJson(ix.items);
                    }
                    
                    if (record.get('contractor_id')) {
                        Ext.getCmp('btn-create-client-order').setDisabled(false);
                    }
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                    button.enable();
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // поиск записи по истории
    searchRecord: function (button) {
        var form = button.up('window').down('form').getForm();
        var data = form.getValues();

        // поиск по контактным лицам
        if (data.contractor_id ==0 )
        {
        	console.log('enter ajax request');
	        Ext.Ajax.request({
	            url: '/incoming_call_register/search',
	            method: 'GET',
	            params: {
	                cagent_phone: data.cagent_phone
	            },
	            success: function (response) {
	                if (response.responseText) {
	                    var ic = JSON.parse(response.responseText);
	                    form.setValues(ic);
	                    Ext.getCmp('incoming-call-is-new').hide();
	                    Ext.getCmp('incoming-call-source').hide();
	                    Ext.getCmp('incoming-call-cagent-info').hide();
                        Ext.getCmp('incoming-call-client-interest-srv').inputEl.removeCls('field-required');

                        if (ic.cagent_id != 0) {
	                    	console.log('org is contragent');
	                        Ext.getCmp('label-company-title-is-find').show();
	                        Ext.getCmp('label-company-title-is-not-find').hide();
	                        //Ext.getCmp('btn-create-client-incoming-call').hide();
	                        Ext.getCmp('btn-create-client-incoming-call').setDisabled(true);
	                        
	                        var pp = Ext.getCmp('conctact-personal-id');
	                        pp.hideTrigger = false;
	                        
	                        
	                        var author_id = form.getValues().author_id;
                        	 
                      	    console.log('authot_id: '+author_id);
                      	    Ext.getCmp('field-responsible-user').setValue('');
                      	    form.setValues({
                          	   responsible_user: ic.manager_name,
                          	   responsible_user_id: ic.manager_id,
                             });

                            Ext.getCmp('checkbox-executed_by_author').setValue(false);
                            Ext.getCmp('field-responsible-user').setValue(ic.manager_name);
                            
                             
                             // Показ кнопки "Выполнить"
                            // Ext.getCmp('btn-execute-incomming-call').hide();
                             Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
                             if (ic.manager_id == author_id) {
                          	   //Ext.getCmp('btn-execute-incomming-call').show();
                               Ext.getCmp('btn-execute-incomming-call').setDisabled(false);
                          	   Ext.getCmp('checkbox-executed_by_author').setValue(true);
                             }
	                        
	                    } else {
	                        Ext.getCmp('label-company-title-is-find').hide();
	                        Ext.getCmp('label-company-title-is-not-find').show();
	                        //Ext.getCmp('btn-create-client-incoming-call').show();
	                        Ext.getCmp('btn-create-client-incoming-call').setDisabled(false);
	                        
	                        var pp = Ext.getCmp('conctact-personal-id');
	                        
	                        pp.hideTrigger = true;
	                        console.log('search from phone is not contragent');
	                        
	                      //Ext.getCmp('btn-execute-incomming-call').hide();
	                        Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
                       	    Ext.getCmp('checkbox-executed_by_author').setValue(false);
                       	    Ext.getCmp('field-responsible-user').setValue('');
                       	    form.setValues({
                        	   responsible_user: '',
                        	   //root_id:0,
                       	    });
	                        console.log('new!');
	                    }
	                }
	                else {
	                	console.log('search from phone not found');
	                    Ext.getCmp('incoming-call-is-new').show();
	                    Ext.getCmp('incoming-call-source').show();
	                    
	                    Ext.getCmp('incoming-call-cagent-info').show();
                        Ext.getCmp('incoming-call-client-interest-srv').inputEl.addCls('field-required');
                        Ext.getCmp('label-company-title-is-find').hide();
                        Ext.getCmp('label-company-title-is-not-find').hide();
                        
	                    form.setValues({
	                    	//cagent_phone: '',
	                    	//cagent_email: '',
	                    	//cagent_name: '',
	                    	//company_title: '',
	                    	org_id:0,
	                    	contractor_id:0,
	                    	contact_personal_id:0,
	                    	cagent_id:0,
	                    	responsible_user_id: '',
	                    	responsible_user: '',
	                    	root_id:0,
	                    });
                        var pp = Ext.getCmp('conctact-personal-id');
                        pp.hideTrigger = true;
                        
                       
	                }
	            }
	            
	        });
        }
    },

    // поиск в гриде
    searchIncomingCall: function (button) {
        var form = button.up('form').getForm();
        var store = Ext.StoreManager.get('incoming_call');
        store.getProxy().extraParams = form.getValues();
        store.reload();
    },

    // список входящих запросов
    menuListIncomingCall: function () {
        var mainPanel = Ext.getCmp('main-panel');
        mainPanel.removeAll();

        var panelIncomingCall = Ext.create('widget.panelIncomingCall');
        var store = Ext.StoreManager.get('incoming_call');
        store.clearFilter();
        store.getProxy().extraParams = {};
        store.load();

        mainPanel.add(panelIncomingCall);
        mainPanel.doLayout();
    },

    clearIncomingCallSearchForm: function (button) {
        button.up('form').getForm().reset();
        var store = Ext.StoreManager.get('incoming_call');
        store.getProxy().extraParams = {};
        store.reload();
    },

    // отображение истории запросов
    showHistoryIncomingCall: function (button) {
        var form = button.up('form').getForm();
        var phone = form.getValues().cagent_phone;
        if (phone) {
            var win = Ext.create('widget.winIncomingCall');
            var store = Ext.create('app.store.incoming_call');
            var grid = win.down('grid');

            store.clearFilter();
            store.getProxy().extraParams = {cagent_phone: phone};
            store.load();
            grid.reconfigure(store);
            grid.getDockedItems()[1].store = store;
            win.show();

            grid.on('itemdblclick', function (grid, record) {
                Ext.ModelManager.getModel('app.model.incoming_call').load(record.get('id'), {
                    success: function (rec) {
                        form.setValues({
                            client_interest: rec.get('client_interest'),
                            client_interest_srv: rec.get('client_interest_srv')
                        });
                        win.close();
                    }
                });
            });
        }
    },

    // сохранение контрагента из вх. запроса
    saveCagent: function (record) {
        Ext.Ajax.request({
            url: '/d_contractor/firstsave',
            method: 'POST',
            params: {
                cagent_name: record.get('cagent_name'),
                cagent_email: record.get('cagent_email'),
                cagent_phone: record.get('cagent_phone'),
                cagent_title: record.get('company_title'),
                cagent_type: record.get('company_type'),
                ref_scope: record.get('reference_scope'),
                ref_scope_id: record.get('reference_scope_id'),
                fact_address_text: record.get('company_fias_title'),
                fact_address_fias: record.get('company_fias_id'),
                reclama_id: record.get('reclama_id'),
                root_id: record.get('root_id'),
                org_id: record.get('org_id'),
                org_type: 1 // Тип контрагента: "клиент"
            },
            success: function (response) {
                if (response.responseText) {
                    var ic = JSON.parse(response.responseText);

                    // блокировка кнопок
                    Ext.getCmp('btn-create-client-incoming-call').setDisabled(true);
                    Ext.getCmp('btn-create-client-order').setDisabled(false);
                    if (Ext.getCmp('btn-save-incoming-call')){
                    	 Ext.getCmp('btn-save-incoming-call').setDisabled(true);
                    }

                    var cagent_id = ic.cagent_id;
                    // открываем форму Контрагента
                    var cagent = new app.controller.cagent;
                    cagent.open(cagent_id);
                }
            }
        });
    },
    
    // сохранение контрагента из вх. запроса
    saveCagentJson: function (json) {
        Ext.Ajax.request({
            url: '/d_contractor/firstsave',
            method: 'POST',
            params: {
                cagent_name: json.cagent_name,
                cagent_email: json.cagent_email,
                cagent_phone: json.cagent_phone,
                cagent_title: json.company_title,
                cagent_type: json.company_type,
                ref_scope: json.reference_scope,
                ref_scope_id: json.reference_scope_id,
                fact_address_text: json.company_fias_title,
                fact_address_fias: json.company_fias_id,
                reclama_id: json.reclama_id,
                root_id: json.root_id,
                org_id: json.org_id,
                org_type: 1 // Тип контрагента: "клиент"
            },
            success: function (response) {
                if (response.responseText) {
                    var ic = JSON.parse(response.responseText);

                    // блокировка кнопок
                    Ext.getCmp('btn-create-client-incoming-call').setDisabled(true);
                    Ext.getCmp('btn-create-client-order').setDisabled(false);
                    Ext.getCmp('btn-save-incoming-call').setDisabled(true);
                    
                    var cagent_id = ic.cagent_id;
                    // открываем форму Контрагента
                    var cagent = new app.controller.cagent;
                    cagent.open(cagent_id);
                }
            }
        });
    },
    
    ExecuteIncommingCall: function(button)
    {
    	
        Ext.Msg.show({
            msg: 'Выполненный запрос будет недоступен для редактирования.\nПродолжить?',
            buttons: Ext.Msg.YESNOCANCEL,
            fn: function (btn) {
                if (btn == 'no') {
                }
                if (btn == 'yes')
                {
                    //
             	   var win = button.up('window');
                   var form = win.down('form');
                   var ic = this;

                   // запись, полученная из формы
                   var record = form.getRecord();
                   var valRecord = form.getValues();
                   valRecord.status = valRecord.na == 1 ? 2 : 1;
                   
                   if (valRecord.x_responsible_user_id!=0){
                   	valRecord.responsible_user_id = valRecord.x_responsible_user_id;
                   }
                   
                   if (valRecord.responsible_user_id instanceof Array){
                   	valRecord.responsible_user_id = valRecord.responsible_user_id[0];
                   }
                   
                   valRecord.executed = 1;
                   if (!record) {
                       record = Ext.create('app.model.incoming_call', valRecord);
                   } else {
                       record.set(valRecord);
                   }
                                     
                   console.log(record);
                   if (record.isValid()) {
                       button.disable();
                       record.save({
                           // объект сохранен на сервере
                           success: function (record, op) {

                               var ix = JSON.parse(op.response.responseText);
                        	   
                               Ext.StoreManager.get('incoming_call').reload();   
                               //button.enable();
                               
                               Ext.getCmp('label-executed-incomming-call').show();
                               Ext.getCmp('checkbox-executed_by_author').hide();
                                                             
                               form.getForm().setValues({
                                  	id: ix.items.uniq_form_id,
                                  	org_id: ix.items.org_id,
                                  	executed: 1
                                  });                    
                               record.set(form.getValues());
                               
                            
                               Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
                               
                             //  Ext.getCmp('btn-save-incoming-call').hide();
                               Ext.getCmp('btn-save-incoming-call').setDisabled(true);
                               console.log('succ!!!');
                               
                           },
                           // объект не прошел валидацию на сервере
                           failure: function (record, operation) {
                               var errors = operation.request.scope.reader.jsonData["errors"];
                               form.getForm().markInvalid(errors);
                               console.log('fail!!!!!');
                               button.enable();
                           }
                       });
                      // button.enable();
                       
                      
                      
                       
                   } else {
                       var errors = record.validate();
                       form.getForm().markInvalid(errors);
                   }
                    
                }
                //Ext.getCmp('btn-execute-incomming-call').setDisabled(true);
                return true;
            }
        });
        
       
    }

});
