Ext.define('app.controller.Main', {
    extend: 'Ext.app.Controller',
    models: [],
    stores: ['LineMenuStore', 'TreeMenuStore', 'Store1', 'filter_status'],
    views: ['MainView', 'Form', 'WinGrid', 'EditGrid', 'Win2'],
    requires: ['Ext.ux.InputTextMask'],

    init: function () {
        this.control({

            '[action=menuRefGrid]': {click: this.menuClickGrid},
            '[action=menuRefTree]': {click: this.menuClickTree},
            '[action=menuRefTreeForm]': {click: this.menuClickTreeForm},

            '[action=menuReclama]': {click: this.clickMenuReclama},
            '[action=menuDirectionSales]': {click: this.clickMenuDirectionSales},
            '[action=menuCustomerCategory]': {click: this.clickMenuCustomerCategory},
            '[action=menuTypePayment]': {click: this.clickMenuTypePayment},

            '[action=menuCompany]': {click: this.clickMenuCompany},
            '[action=menuEmployee]': {click: this.clickMenuEmployee},
            '[action=menuOKVED]': {click: this.clickMenuOKVED},
            '[action=menuOKPFO]': {click: this.clickMenuOKPFO},
            '[action=menuReferenceScope]': {click: this.clickMenuReferenceScope},
            '[action=menuContactPersonal]': {click: this.clickMenuContactPersonal},
            '[action=menuUserPreferences]': {click: this.clickMenuUserPreferences},

            // Транспортные средства
            '[action=menuVehicle]': {click: this.menuVehicle},
            // Сотрудники / Пользователи
            '[action=menuUser]': {click: this.menuUser},

            // Справочники
            '[action=menuDictionaryTypeDocument]': {click: this.menuDictionaryTypeDocument},
            '[action=menuDictionaryVehicleStatus]': {click: this.menuDictionaryVehicleStatus},
            '[action=menuDictionaryVehicleType]': {click: this.menuDictionaryVehicleType},
            '[action=menuDictionaryEquipmentType]': {click: this.menuDictionaryEquipmentType},
			'[action=menuDictionaryPermitType]': {click: this.menuDictionaryPermitType},
            '[action=menuDictionaryFuel]': {click: this.menuDictionaryFuel},
            '[action=menuDictionaryVehicleModel]': {click: this.menuDictionaryVehicleModel},
            '[action=menuDictionaryPosition]': {click: this.menuDictionaryPosition},
            '[action=menuDictionaryShift]': {click: this.menuDictionaryShift},
        });
    },

    // транспортные средства
    menuVehicle: function () {
        var cVehicle = Ext.create('app.controller.vehicle');
        cVehicle.openList();
    },

    // Сотрудники / Пользователи
    menuUser: function () {
        var cUser = Ext.create('app.controller.user');
        cUser.openList();
    },

    // окно - грид
    menuClickGrid: function (button) {
        var viewGrid = Ext.create('widget.wingrid');
    },

    // окно - дерево
    menuClickTree: function (button) {
        var win = Ext.create('widget.win2');
        win.show();
    },

    // окно - дерево - редактирование
    menuClickTreeForm: function (button) {
        var win = Ext.create('widget.win3');
        win.show();
    },

    // меню реклама
    clickMenuReclama: function () {
        var win = Ext.create('widget.winReclama');
        var store = Ext.StoreManager.get('reclama');
        store.getProxy().extraParams = {status: 1};
        store.load();
        win.show();
    },

    // меню Направление продаж
    clickMenuDirectionSales: function () {
        var win = Ext.create('widget.winDirectionSales');
        var store = Ext.StoreManager.get('direction_sales');
        store.getProxy().extraParams = {status: 1};
        store.load();
        win.show();
    },

    // меню Виды расчетов
    clickMenuTypePayment: function () {
        var win = Ext.create('widget.winTypePayment');
        win.show();
    },

    // меню категории клиентов
    clickMenuCustomerCategory: function () {
        var win = Ext.create('widget.winCustomerCategory');
        var store = Ext.StoreManager.get('customer_category');
        store.getProxy().extraParams = {status: 1};
        store.load();
        win.show();
    },

    // меню организации
    clickMenuCompany: function () {
        var win = Ext.create('widget.winCompany');
        var store = Ext.StoreManager.get('company');
        store.getProxy().extraParams = {status: 1};
        store.load();
        win.show();
    },

    // меню - справочник "сотрудники"
    clickMenuEmployee: function () {
        var win = Ext.create('widget.winEmployee');
        var store = Ext.StoreManager.get('company');
        store.load();
        win.show();
    },

    // меню - справочник ОКВЭД
    clickMenuOKVED: function () {
        Ext.create('widget.winOKVED');
    },

    // меню - справочник ОКПФО
    clickMenuOKPFO: function () {
        var win = Ext.create('widget.winOkpfoType');
        var store = Ext.StoreManager.get('okpfo_type');
        store.load();
        win.show();
    },

    // меню - справочник "Сфера деятельности"
    clickMenuReferenceScope: function () {
        var win = Ext.create('widget.winReferenceScope');
        var store = Ext.StoreManager.get('reference_scope');
        store.getProxy().extraParams = {status: 1};
        store.clearFilter();
        store.load();
        win.show();
    },

    // меню - справочник "Контактные лица"
    clickMenuContactPersonal: function () {
        var win = Ext.create('widget.winContactPersonal');
        var grid = win.getComponent('grid-contact-personal');
        var store = grid.getStore();
        store.getProxy().extraParams = {status: 1};
        store.clearFilter();
        store.load();
        win.show();
    },

    // меню - настройки пользователя
    clickMenuUserPreferences: function () {
        //var win = Ext.create('widget.winUserPreferences');
        // uid;
        //win.show();

        var user_pref = new app.controller.user_preferences;
        user_pref.open(App.Global.user_id, '');
    },




    // справочник "Виды документов"
    menuDictionaryTypeDocument: function () {
        var controller = Ext.create('app.controller.dictionary.type_document');
        controller.openList();
    },

    // справочник "Статусы транспортного средства"
    menuDictionaryVehicleStatus: function () {
        var controller = Ext.create('app.controller.dictionary.vehicle_status');
        controller.openList();
    },

    // справочник "Виды транспортных средств"
    menuDictionaryVehicleType: function () {
        var controller = Ext.create('app.controller.dictionary.vehicle_type');
        controller.openList();
    },

    // справочник "Виды оборудования"
    menuDictionaryEquipmentType: function () {
        var controller = Ext.create('app.controller.dictionary.equipment_type');
        controller.openList();
    },

	// справочник "Виды пропусков"
	menuDictionaryPermitType: function () {
		var controller = Ext.create('app.controller.dictionary.permit_type');
		controller.openList();
	},

    // справочник "Виды ГСМ"
    menuDictionaryFuel: function () {
        var controller = Ext.create('app.controller.dictionary.fuel');
        controller.openList();
    },

    // справочник "Модели автомобилей"
    menuDictionaryVehicleModel: function () {
        var controller = Ext.create('app.controller.dictionary.vehicle_model');
        controller.openList();
    },

    // справочник "Должности"
    menuDictionaryPosition: function () {
        var controller = Ext.create('app.controller.dictionary.position');
        controller.openList();
    },

    // справочник "Вахты (смены)"
    menuDictionaryShift: function () {
        var controller = Ext.create('app.controller.dictionary.shift');
        controller.openList();
    }

});

