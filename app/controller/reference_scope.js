Ext.define('app.controller.reference_scope', {
    extend: 'Ext.app.Controller',
    models: ['reference_scope'],
    stores: ['reference_scope'],
    views: ['win.reference_scope', 'grid.reference_scope', 'form.reference_scope'],

    init: function () {
        this.control({
            'gridReferenceScope actioncolumn[action=edit]': {
                click: this.openFormEdit
            },
            'winReferenceScope [action=create]': {
                click: this.openFormAdd
            },
            'formReferenceScope [action=save]': {
                click: this.saveRecord
            },
            'formReferenceScope [action=add]': {
                click: this.showWinOKVED
            },
            'winReferenceScope [action=filter_status]':{
                change: this.filterStatus
            }
        });
    },

    // открытие формы дял добавления
    openFormAdd: function (grid) {
        var win = Ext.widget('formReferenceScope');

        var rec = Ext.create('app.model.reference_scope', {
            status: 1
        });
        win.down('form').loadRecord(rec);
        win.down('grid').reconfigure(rec.okved());
    },

    // открытие формы для редактирования
    openFormEdit: function (view, cell, row) {
        var win = Ext.widget('formReferenceScope');
        var record = view.getStore().getAt(row);

        Ext.ModelManager.getModel('app.model.reference_scope').load(record.get('id'), {
            success: function(rec) {
                console.log(rec.okved());
                win.down('form').loadRecord(rec);
                win.down('grid').reconfigure(rec.okved());
            }
        });
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        var form = win.down('form');
        var grid = win.down('grid');

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        record.set(valRecord);

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function (record) {
                    record.okved().each(function(r){
                        r.set('reference_scope_id', record.get('id'));
                    });
                    record.okved().sync();
                    win.close();
                    var store = Ext.StoreManager.get('reference_scope');
                    store.reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // открытие окна для выбора ОКВЭД
    showWinOKVED: function (view) {

        var win = view.up('window');
        var form = win.down('form');
        var record = form.getRecord();

        var winOKVED = Ext.create('widget.winOKVED');
        winOKVED.down('treepanel').on('itemdblclick', function (store, item) {
            if (item.get('leaf')) {
                record.okved().add(item);
                winOKVED.close();
            }
        });
    },

    // фильтр по статусу
    filterStatus: function (view, val) {
        var store = Ext.StoreManager.get('reference_scope');
        store.getProxy().extraParams = {
            status: val
        };
        store.load();
    }

});


