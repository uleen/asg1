Ext.define('app.controller.contractor_delivery', {
    extend: 'Ext.app.Controller',
    models: ['contractor_delivery'],
    stores: ['contractor_delivery'],
    views: ['form.contractor_delivery', 'win.contractor_delivery', 'grid.contractor_delivery'],

    init: function () {
        this.control({
            '[action=menuContractorDelivery]': {
                click: this.openList
            },
            'gridContractorDelivery [action=add]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.cagent = btn.up('window').cagent;
                    this.open(0);
                }
            },
            'formContractorDelivery [action=save]': {
                click: this.saveRecord
            }
        });
    },

    // форма
    open: function (id) {
        var win = Ext.widget('formContractorDelivery');
        var cagent = this.cagent;
        var self = this;

        if (cagent) {
            win.setTitle(cagent.org_type == 1 ? 'Точка доставки' : 'Точка загрузки');
        }

        if (id) {
            Ext.ModelManager.getModel('app.model.contractor_delivery').load(id, {
                success: function (rec) {
                    win.down('form').loadRecord(rec);
                    win.defaultData = win.down('form').getForm().getValues();
                    win.show();
                    self.createMap(win, [rec.get('lat'), rec.get('lng')]);
                }
            });
        } else {
            win.down('form').getForm().setValues({
                contractor_id: cagent.id,
                contractor: cagent.title
            });
            win.defaultData = win.down('form').getForm().getValues();
            win.show();
            self.createMap(win, []);
        }


        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            var data = this.down('form').getForm().getValues();
            if (JSON.stringify(data) == JSON.stringify(this.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = Ext.getCmp('btnSaveContractorDelivery');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        win.confirmClosed = false;
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        if (!record) {
            record = Ext.create('app.model.contractor_delivery', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    win.close();
                    grid.getStore().reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // отрисовка карты
    createMap: function (win, coords) {

        var map = new ymaps.Map('contractor-delivery-map-innerCt', {
            center: coords.length ? coords : [55.76, 37.64],
            zoom: 10,
            behaviors: ['default', 'scrollZoom']
        });

        var pm = null;
        if (coords) {
            createPlacemark(coords);
        }

        // Слушаем клик по триггеру
        Ext.getCmp('contractor-delivery-address').onTriggerClick = function () {
            ymaps.geocode(this.value, {
                results: 1
            }).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);
                    var coords = firstGeoObject.geometry.getCoordinates();
                    pm.geometry.setCoordinates(coords);
                    getAddress(coords);
                    map.setCenter(coords, 10)
                });
        };

        // Слушаем клик на карте
        map.events.add('click', function (e) {
            var coords = e.get('coords');

            if (pm) {
                pm.geometry.setCoordinates(coords);
                getAddress(coords);
            } else {
                createPlacemark(coords);
            }
        });

        function createPlacemark(coords) {
            pm = new ymaps.Placemark(coords, {iconContent: 'поиск...'}, {preset: 'twirl#violetStretchyIcon', draggable: true});
            getAddress(coords);
            map.geoObjects.add(pm);
            pm.events.add('dragend', function () {
                getAddress(pm.geometry.getCoordinates());
            });
        }

        function getAddress(coords) {
            pm.properties.set('iconContent', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                pm.properties.set({
                    iconContent: firstGeoObject.properties.get('name'),
                    balloonContent: firstGeoObject.properties.get('text')
                });
                win.down('form').getForm().setValues({
                    lat: coords[0],
                    lng: coords[1],
                    address: firstGeoObject.properties.get('text')
                });
            });

        }
    },

    // общий список
    openList: function () {
        var self = this;
        var win = Ext.widget('winContractorDelivery');
        var grid = win.down('grid');
        grid.getDockedComponent('for-all').show();
        var store = grid.getStore();
        store.getProxy().extraParams.status = 1;
        store.load();
        win.show();

        // обработка двойного клика в гриде
        grid.on('itemdblclick', function (view, record) {
            self.grid = grid;
            self.open(record.get('id'));
        });
    }

});

