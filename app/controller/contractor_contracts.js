Ext.define('app.controller.contractor_contracts', {
    extend: 'Ext.app.Controller',
    models: ['contractor_contracts', 'currency'],
    stores: ['contractor_contracts', 'currency'],
    views: ['form.contractor_contracts', 'win.contractor_contracts', 'grid.contractor_contracts'],

    init: function () {
        this.control({
            '[action=menuContractorContracts]': {
                click: this.openList
            },
            'gridContractorContracts': {
                itemdblclick: function (grid, record) {
                    this.grid = grid;
                    this.open(record.get('id'));
                }
            },
            'gridContractorContracts [action=add]': {
                click: function (btn) {
                    this.grid = btn.up('grid');
                    this.cagent = btn.up('window').cagent;
                    this.open(0);
                }
            },
            'formContractorContracts [action=save]': {
                click: this.saveRecord
            }
        });
    },

    // форма
    open: function (id) {
        var win = Ext.widget('formContractorContracts');
        var form = win.down('form').getForm();
        var cagent = this.cagent;

        var store = Ext.StoreManager.get('company');
        store.getProxy().extraParams = {status: 1};
        store.load();

        var store = Ext.StoreManager.get('currency');
        store.getProxy().extraParams = {status: 1};
        store.load();

        var store = Ext.StoreManager.get('direction_sales');
        store.getProxy().extraParams = {status: 1};
        store.load();

        var store = Ext.StoreManager.get('type_payment');
        store.getProxy().extraParams = {status: 1};
        store.load();

        var store = Ext.StoreManager.get('type_price');
        store.getProxy().extraParams = {status: 1};
        store.load();

        if (id) {
            Ext.ModelManager.getModel('app.model.contractor_contracts').load(id, {
                success: function (rec) {
                    win.down('form').loadRecord(rec);
                    win.defaultData = win.down('form').getForm().getValues();
                    // отображение загруженного файла
                    if (rec.get('file_id')) {
                        win.down('form').getComponent('file-upload').getComponent('file-title').update(rec.get('file_link'));
                        win.down('form').getComponent('file-upload').show();
                        win.down('form').getComponent('file-form').hide();
                    }
                    // ограничения для рамочного договора
                    if (rec.get('is_frame')) {
                        form.findField('amount').disable();
                        form.findField('currency_id').disable();
                        form.findField('volume').disable();
                    }
                }
            });
        } else {
            win.down('form').getForm().setValues({
                contractor_id: cagent.id,
                contractor: cagent.title,
                type: this.cagent.org_type == 2 ? 2 : 1
            });
            win.defaultData = win.down('form').getForm().getValues();
        }

        win.show();


        // ограничения для рамочного договора
        var fieldFrame = form.findField('is_frame');
        fieldFrame.on('change', function () {
            if (this.checked) {
                form.findField('amount').disable();
                form.findField('currency_id').disable();
                form.findField('volume').disable();
            } else {
                form.findField('amount').enable();
                form.findField('currency_id').enable();
                form.findField('volume').enable();
            }
        });


        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            var data = this.down('form').getForm().getValues();
            if (JSON.stringify(data) == JSON.stringify(this.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            var button = win.down('form').getDockedItems('[dock=bottom]')[0].getComponent('btn-save');
                            button.fireEvent('click', button);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },

    // сохранение измененных данных
    saveRecord: function (button) {
        var win = button.up('window');
        win.confirmClosed = false;
        var form = win.down('form');
        var grid = this.grid;

        // запись, полученная из формы
        var record = form.getRecord();
        var valRecord = form.getValues();
        valRecord.status = valRecord.na == 1 ? 2 : 1;
        valRecord.is_frame = valRecord.is_frame == 1 ? true : false;
        valRecord.prolongation = valRecord.prolongation == 1 ? true : false;
        if (!record) {
            record = Ext.create('app.model.contractor_contracts', valRecord);
        } else {
            record.set(valRecord);
        }

        // валидация объекта
        if (record.isValid()) {
            // сохранение объекта
            record.save({
                // объект сохранен на сервере
                success: function () {
                    win.close();
                    grid.getStore().reload();
                },
                // объект не прошел валидацию на сервере
                failure: function (record, operation) {
                    var errors = operation.request.scope.reader.jsonData["errors"];
                    form.getForm().markInvalid(errors);
                }
            });
        } else {
            var errors = record.validate();
            form.getForm().markInvalid(errors);
        }
    },

    // общий список
    openList: function () {
        var win = Ext.getCmp('winContractorContracts');
        if (!win) {
            var win = Ext.widget('winContractorContracts');
            var grid = win.down('grid');
            grid.getDockedComponent('for-all').show();
            var store = grid.getStore();
            store.getProxy().extraParams.status = 1;
            store.load();
        }
        win.show();
    }

});
