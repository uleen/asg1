Ext.define('app.store.search_incoming_call', {
    extend: 'Ext.data.Store',
    model: 'app.model.incoming_call',
    proxy: {
        type: 'rest',
        url: '/incoming_call_register/localsearch',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});