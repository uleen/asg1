Ext.define('app.store.company', {
    extend: 'Ext.data.Store',
    model: 'app.model.company',
    proxy: {
        type: 'rest',
        url: '/spec/d_company',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});