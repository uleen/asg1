Ext.define('app.store.sheet', {
    extend: 'Ext.data.Store',
    model: 'app.model.sheet',
    proxy: {
        type: 'rest',
        url: '/sheet',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
