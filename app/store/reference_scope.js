Ext.define('app.store.reference_scope', {
    extend: 'Ext.data.Store',
    model: 'app.model.reference_scope',
    proxy: {
        type: 'rest',
        url: '/spec/d_reference_scope',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});