Ext.define('app.store.contact_personal', {
    extend: 'Ext.data.Store',
    model: 'app.model.contact_personal',
    autoSync: false,
    proxy: {
        type: 'memory',
       // url: '/spec/d_personal_contacs_data',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});