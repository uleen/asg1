Ext.define('app.store.ic_search', {
    extend: 'Ext.data.Store',
    model: 'app.model.ic_search',
    proxy: {
        type: 'rest',
        url: '/ic_search',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    },
    
});