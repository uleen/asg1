Ext.define('app.store.type_price', {
    extend: 'Ext.data.Store',
    model: 'app.model.type_price',
    proxy: {
        type: 'rest',
        url: '/spec/d_type_price',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});