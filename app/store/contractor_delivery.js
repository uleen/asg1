Ext.define('app.store.contractor_delivery', {
    extend: 'Ext.data.Store',
    model: 'app.model.contractor_delivery',
    proxy: {
        type: 'rest',
        url: '/d_contractor_delivery',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});