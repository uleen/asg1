Ext.define('app.store.nomenclature_characteristic', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature_characteristic',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/characteristic',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
