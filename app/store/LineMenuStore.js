Ext.define('app.store.LineMenuStore', {
    extend: 'Ext.data.TreeStore',
//    autoLoad: true,
    storeId: 'LineMenuStore',
    root: {
        children: [
            {
                text: "элемент 1",
                leaf: true
            },
            {
                text: "элемент 2",
                leaf: true
            },
            {
                text: "элемент 3",
                leaf: true
            }
        ]
    }
});