Ext.define('app.store.client_order_status_history', {
    extend: 'Ext.data.Store',
    model: 'app.model.client_order_status_history',
    proxy: {
        type: 'rest',
        url: '/client_order/status_history',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});