Ext.define('app.store.cagent_client', {
    extend: 'app.store.cagent',
    proxy: {
        type: 'rest',
        url: '/d_contractor',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1,
            org_type: 1
        }
    }
});