Ext.define('app.store.fias', {
    extend: 'Ext.data.TreeStore',
    model: 'app.model.fias',
    proxy: {
        type: 'ajax',
        url: '/dictionary/fias'
    },
    autoLoad: false,
    autoSync: false,
    root: {
        id: 0,
        expanded: true
    }
});