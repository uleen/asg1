Ext.define('app.store.client_order_nomenclature', {
    extend: 'Ext.data.Store',
    model: 'app.model.client_order_nomenclature',
    proxy: {
        type: 'rest',
        url: '/client_order/nomenclature',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});