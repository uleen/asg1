Ext.define('app.store.okei', {
    extend: 'Ext.data.Store',
    model: 'app.model.okei',
    proxy: {
        type: 'rest',
        url: '/dictionary/okei',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
