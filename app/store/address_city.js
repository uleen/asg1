Ext.define('app.store.address_city', {
    extend: 'Ext.data.Store',
    model: 'app.model.address_city',
    proxy: {
        type: 'rest',
        url: '/spec/d_address/city',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});