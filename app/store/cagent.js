Ext.define('app.store.cagent', {
    extend: 'Ext.data.Store',
    model: 'app.model.cagent',
    proxy: {
        type: 'rest',
        url: '/d_contractor',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});