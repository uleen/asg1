Ext.define('app.store.nomenclature_price', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature_price',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/price',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
