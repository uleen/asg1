Ext.define('app.store.okpfo_type', {
    extend: 'Ext.data.Store',
    model: 'app.model.okpfo_type',
    proxy: {
        type: 'rest',
        url: '/spec/d_okpfo_type',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});