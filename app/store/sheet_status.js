Ext.define('app.store.sheet_status', {
    extend: 'Ext.data.Store',
    model: 'app.model.sheet_status',
    proxy: {
        type: 'rest',
        url: '/spec/d_sheet_status',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});