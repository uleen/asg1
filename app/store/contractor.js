Ext.define('app.store.contractor', {
    extend: 'Ext.data.Store',
    model: 'app.model.contractor',
    proxy: {
        type: 'rest',
        url: '/spec/d_contractor',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});