Ext.define('app.store.position', {
    extend: 'Ext.data.Store',
    model: 'app.model.position',
    proxy: {
        type: 'memory'
    }
});