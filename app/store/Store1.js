Ext.define('app.store.Store1', {
    extend: 'Ext.data.TreeStore',
    storeId: 'Store1',
    root: {
        text: 'Страны СНГ',
        expanded: true,
        children: [
            {
                text: "Россия",
                children: [
                    {
                        id: 1,
                        text: "Москва",
                        leaf: true
                    },
                    {
                        id: 2,
                        text: "Санкт-Петербург",
                        leaf: true
                    },
                    {
                        id: 3,
                        text: "Волгоград",
                        leaf: true
                    }
                ],
                leaf: false
            },
            {
                text: "Украина",
                children: [
                    {
                        id: 4,
                        text: "Киев",
                        leaf: true
                    },
                    {
                        id: 5,
                        text: "Львов",
                        leaf: true
                    },
                    {
                        id: 6,
                        text: "Одесса",
                        leaf: true
                    }
                ],
                leaf: false
            },
            {
                text: "Белоруссия",
                children: [
                    {
                        id: 7,
                        text: "Минск",
                        leaf: true
                    },
                    {
                        id: 8,
                        text: "Витебск",
                        leaf: true
                    },
                    {
                        id: 9,
                        text: "Гомель",
                        leaf: true
                    }
                ],
                leaf: false
            }
        ]
    }
});