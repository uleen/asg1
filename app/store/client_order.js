Ext.define('app.store.client_order', {
    extend: 'Ext.data.Store',
    model: 'app.model.client_order',
    proxy: {
        type: 'rest',
        url: '/client_order',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});
