Ext.define('app.store.direction_sales', {
    extend: 'Ext.data.Store',
    model: 'app.model.direction_sales',
    proxy: {
        type: 'rest',
        url: '/spec/d_direction_sales',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});