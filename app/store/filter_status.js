Ext.define('app.store.filter_status', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'title', type: 'string'}
    ],
    data: [
        [1, 'Активные'],
        [2, 'Неактивные'],
        [0, 'Все']
    ]
});