Ext.define('app.store.user', {
    extend: 'Ext.data.Store',
    model: 'app.model.user',
    pageSize: 10,
    proxy: {
        type: 'rest',
//        url: '/spec/d_user',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});