Ext.define('app.store.characteristic', {
    extend: 'Ext.data.Store',
    model: 'app.model.characteristic',
    proxy: {
        type: 'rest',
        url: '/spec/d_characteristic',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});