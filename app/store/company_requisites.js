Ext.define('app.store.company_requisites', {
    extend: 'Ext.data.Store',
    model: 'app.model.company_requisite',
    proxy: {
        type: 'rest',
        url: '/spec/d_company/requisites',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
