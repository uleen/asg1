Ext.define('app.store.contractor_contracts', {
    extend: 'Ext.data.Store',
    model: 'app.model.contractor_contracts',
    proxy: {
        type: 'rest',
        url: '/d_contractor_contracts',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
