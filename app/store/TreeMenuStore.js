Ext.define('app.store.TreeMenuStore', {
    extend: 'Ext.data.TreeStore',
    autoLoad: true,
    storeId: 'TreeMenuStore',
    root: {
        text: 'Все Контрагенты',
        expanded: true,
        children: [
            {
                text: "Обратить внимание",
                leaf: true
            },
            {
                text: "По обороту",
                children: [
                    {
                        text: "Крупные",
                        leaf: true
                    },
                    {
                        text: "Средние",
                        leaf: true
                    },
                    {
                        text: "Мелкие",
                        leaf: true
                    }
                ]
            },
            {
                text: "По охвату",
                leaf: true
            },
            {
                text: "По типам",
                leaf: true
            }
        ]
    }
});