Ext.define('app.store.contractor_requisites', {
    extend: 'Ext.data.Store',
    model: 'app.model.contractor_requisites',
    proxy: {
        type: 'rest',
        url: '/d_contractor_requisites',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});