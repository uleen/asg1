Ext.define('app.store.address_street', {
    extend: 'Ext.data.Store',
    model: 'app.model.address_street',
    proxy: {
        type: 'rest',
        url: '/spec/d_address/street',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});