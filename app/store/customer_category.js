Ext.define('app.store.customer_category', {
    extend: 'Ext.data.Store',
    model: 'app.model.customer_category',
    proxy: {
        type: 'rest',
        url: '/spec/d_customer_category',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});