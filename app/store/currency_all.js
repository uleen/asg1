Ext.define('app.store.currency_all', {
    extend: 'Ext.data.Store',
    model: 'app.model.currency',
    proxy: {
        type: 'rest',
        url: '/dictionary/currency/all',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});