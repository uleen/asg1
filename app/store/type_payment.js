Ext.define('app.store.type_payment', {
    extend: 'Ext.data.Store',
    model: 'app.model.type_payment',
    proxy: {
        type: 'rest',
        url: '/spec/d_type_payment',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});