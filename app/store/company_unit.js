Ext.define('app.store.company_unit', {
    extend: 'Ext.data.Store',
    model: 'app.model.company_unit',
    proxy: {
        type: 'rest',
        url: '/spec/d_company_unit',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});