Ext.define('app.store.nomenclature_group', {
    extend: 'Ext.data.TreeStore',
    model: 'app.model.nomenclature_group',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature_group',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    },
    root: {
        id: 0,
        title: 'Все группы',
        expanded: true
    }
});