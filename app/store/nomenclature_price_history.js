Ext.define('app.store.nomenclature_price_history', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature_price_history',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/price/history',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
