Ext.define('app.store.contact_personal', {
    extend: 'Ext.data.Store',
    model: 'app.model.contact_personal',
    proxy: {
        type: 'rest',
        url: '/spec/d_contact_personal',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});