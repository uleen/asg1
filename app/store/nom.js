Ext.define('app.store.nom', {
    extend: 'Ext.data.ArrayStore',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'title', type: 'string'},
    ],
    data: [
        [1, 'Песок карьерный'],
        [2, 'Песок сеяный'],
        [3, 'Песок мытый'],
        [4, 'Щебень гравийный'],
        [5, 'Щебень известковый'],
        [6, 'Щебень гранитный']
    ]
});
