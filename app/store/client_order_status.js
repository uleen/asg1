Ext.define('app.store.client_order_status', {
    extend: 'Ext.data.Store',
    model: 'app.model.client_order_status',
    proxy: {
        type: 'rest',
        url: '/spec/d_client_order_status',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});