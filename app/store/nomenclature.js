Ext.define('app.store.nomenclature', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});
