Ext.define('app.store.user.can_auth', {
    extend: 'Ext.data.Store',
    model: 'app.model.user.main',
    proxy: {
        type: 'rest',
        url: '/user',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1,
            can_auth: 1 // только сотрудники, которые имеют доступ в систему
        }
    }
});