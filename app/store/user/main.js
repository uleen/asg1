Ext.define('app.store.user.main', {
    extend: 'Ext.data.Store',
    model: 'app.model.user.main',
    proxy: {
        type: 'rest',
        url: '/user',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});