Ext.define('app.store.address_house', {
    extend: 'Ext.data.Store',
    model: 'app.model.address_house',
    proxy: {
        type: 'rest',
        url: '/spec/d_address/house',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});