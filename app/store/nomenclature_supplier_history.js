Ext.define('app.store.nomenclature_supplier_history', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature_supplier',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/supplier/history',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
