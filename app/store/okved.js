Ext.define('app.store.okved', {
    extend: 'Ext.data.TreeStore',
    model: 'app.model.okved',
    proxy: {
        type: 'ajax',
        url: '/dictionary/okved'
    },
    autoLoad: false,
    autoSync: false,
    root: {
        id: 0,
        expanded: true
    }
});