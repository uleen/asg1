Ext.define('app.store.sheet_type', {
    extend: 'Ext.data.Store',
    model: 'app.model.sheet_type',
    proxy: {
        type: 'rest',
        url: '/spec/d_sheet_type',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
