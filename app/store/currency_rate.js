Ext.define('app.store.currency_rate', {
    extend: 'Ext.data.Store',
    model: 'app.model.currency_rate',
    proxy: {
        type: 'rest',
        url: '/dictionary/currency/rate',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});