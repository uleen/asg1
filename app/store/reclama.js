Ext.define('app.store.reclama', {
    extend: 'Ext.data.Store',
    model: 'app.model.reclama',
    proxy: {
        type: 'rest',
        url: '/spec/d_reclama',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});