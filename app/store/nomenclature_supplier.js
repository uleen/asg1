Ext.define('app.store.nomenclature_supplier', {
    extend: 'Ext.data.Store',
    model: 'app.model.nomenclature_supplier',
    proxy: {
        type: 'rest',
        url: '/spec/d_nomenclature/supplier',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});
