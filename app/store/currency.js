Ext.define('app.store.currency', {
    extend: 'Ext.data.Store',
    model: 'app.model.currency',
    proxy: {
        type: 'rest',
        url: '/dictionary/currency',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});