Ext.define('app.store.unit', {
    extend: 'Ext.data.Store',
    model: 'app.model.unit',
    proxy: {
        type: 'rest',
        url: '/spec/d_unit',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});