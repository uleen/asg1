Ext.define('app.store.vehicle.permit', {
    extend: 'Ext.data.Store',
    model: 'app.model.vehicle.permit',
    proxy: {
        type: 'rest',
        url: '/vehicle/permit',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});