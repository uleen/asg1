Ext.define('app.store.vehicle.main', {
    extend: 'Ext.data.Store',
    model: 'app.model.vehicle.main',
    proxy: {
        type: 'rest',
        url: '/vehicle',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});