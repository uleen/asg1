Ext.define('app.store.vehicle.equipment', {
    extend: 'Ext.data.Store',
    model: 'app.model.vehicle.equipment',
    proxy: {
        type: 'rest',
        url: '/vehicle/equipment',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});