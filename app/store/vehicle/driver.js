Ext.define('app.store.vehicle.driver', {
    extend: 'Ext.data.Store',
    model: 'app.model.vehicle.driver',
    proxy: {
        type: 'rest',
        url: '/vehicle/driver',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});