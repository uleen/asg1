Ext.define('app.store.sheet_status_history', {
    extend: 'Ext.data.Store',
    model: 'app.model.sheet_status_history',
    proxy: {
        type: 'rest',
        url: '/sheet/status_history',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        }
    }
});