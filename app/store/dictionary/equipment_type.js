Ext.define('app.store.dictionary.equipment_type', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.equipment_type',
    proxy: {
        type: 'rest',
        url: '/dictionary/equipment_type',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});