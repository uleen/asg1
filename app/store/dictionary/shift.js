Ext.define('app.store.dictionary.shift', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.shift',
    proxy: {
        type: 'rest',
        url: '/dictionary/shift',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});