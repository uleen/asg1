Ext.define('app.store.dictionary.vehicle_type', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.vehicle_type',
    proxy: {
        type: 'rest',
        url: '/dictionary/vehicle_type',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});