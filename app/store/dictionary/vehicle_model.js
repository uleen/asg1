Ext.define('app.store.dictionary.vehicle_model', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.vehicle_model',
    proxy: {
        type: 'rest',
        url: '/dictionary/vehicle_model',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});