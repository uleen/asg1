Ext.define('app.store.dictionary.permit_type', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.permit_type',
    proxy: {
        type: 'rest',
        url: '/dictionary/permit_type',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});