Ext.define('app.store.dictionary.vehicle_status', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.vehicle_status',
    proxy: {
        type: 'rest',
        url: '/dictionary/vehicle_status',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});