Ext.define('app.store.dictionary.position', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.position',
    proxy: {
        type: 'rest',
        url: '/dictionary/position',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});