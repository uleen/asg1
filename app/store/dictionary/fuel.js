Ext.define('app.store.dictionary.fuel', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.fuel',
    proxy: {
        type: 'rest',
        url: '/dictionary/fuel',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});