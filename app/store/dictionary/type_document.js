Ext.define('app.store.dictionary.type_document', {
    extend: 'Ext.data.Store',
    model: 'app.model.dictionary.type_document',
    proxy: {
        type: 'rest',
        url: '/dictionary/type_document',
        filterParam: 'query',
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success'
        },
        extraParams: {
            status: 1
        }
    }
});